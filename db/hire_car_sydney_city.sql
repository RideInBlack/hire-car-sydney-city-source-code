-- phpMyAdmin SQL Dump
-- version 3.5.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 27, 2015 at 11:24 AM
-- Server version: 5.5.40
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hire_car_sydney_city`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_details`
--

CREATE TABLE IF NOT EXISTS `admin_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `password_hash` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_details`
--

INSERT INTO `admin_details` (`id`, `name`, `email_id`, `username`, `password`, `password_hash`) VALUES
(1, 'Administrator', 'virag.shah@ifuturz.com', 'admin', 'admin123', '0192023A7BBD73250516F069DF18B500');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE IF NOT EXISTS `admin_user` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_type` enum('0','1') NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `nick_name` varchar(20) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `password` varchar(30) NOT NULL,
  `password_hash` varchar(32) NOT NULL,
  `status` enum('1','0') NOT NULL COMMENT '1=Active, 0=Inactive',
  `add_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`admin_id`, `admin_type`, `first_name`, `last_name`, `nick_name`, `email_id`, `password`, `password_hash`, `status`, `add_datetime`, `update_datetime`) VALUES
(1, '1', 'Virag', 'Shah', 'admin', 'virag.shah@ifuturz.com', 'admin123', '0192023a7bbd73250516f069df18b500', '1', '2015-08-13 04:40:26', '2015-09-16 04:46:02'),
(5, '0', 'test', 'testing', 'testing', 'test@gmail.com', 'test@123', 'ceb6c970658f31504a901b89dcd3e461', '1', '2015-08-23 08:20:55', '2015-08-23 08:20:55'),
(6, '0', 'Rajesh', 'Solanki', 'rajesh', 'rajesh@ifuturz.com', 'Test@123', 'f925916e2754e5e03f75dd58a5733251', '1', '2015-08-31 11:23:38', '2015-08-31 11:23:38'),
(7, '0', 'Json', 'Staton', 'json', 'json@email.com', '1213456', 'aadc03fecca9b5cc2fd64b333cb0875e', '1', '2015-09-28 05:35:11', '2015-09-28 05:35:11');

-- --------------------------------------------------------

--
-- Table structure for table `assign_ride`
--

CREATE TABLE IF NOT EXISTS `assign_ride` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ride_id` int(11) NOT NULL,
  `assign_driver_id` int(11) NOT NULL,
  `assign_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Details for Assigned Driver Details for ride.' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `assign_ride`
--

INSERT INTO `assign_ride` (`id`, `ride_id`, `assign_driver_id`, `assign_datetime`) VALUES
(1, 1, 22, '2015-08-24 06:12:07'),
(2, 26, 23, '2015-09-04 10:49:33');

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE IF NOT EXISTS `car` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `number` varchar(50) NOT NULL,
  `base_fare` float NOT NULL,
  `min_fare` float NOT NULL,
  `per_minute_rate` float NOT NULL,
  `per_km_rate` float NOT NULL,
  `driver_rent` float NOT NULL,
  `registration_filename` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `is_allocated` enum('0','1') NOT NULL,
  `is_available` enum('0','1') NOT NULL,
  `add_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`car_id`, `category_id`, `city_id`, `event_id`, `model_name`, `number`, `base_fare`, `min_fare`, `per_minute_rate`, `per_km_rate`, `driver_rent`, `registration_filename`, `status`, `is_allocated`, `is_available`, `add_datetime`, `update_datetime`) VALUES
(1, 1, 1, 0, 'A8 White', 'AU SYD 236', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-09-28 06:49:49', '2015-10-13 12:02:46'),
(3, 1, 1, 0, 'Q7 White', 'AU SYD 236', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-09-28 06:52:03', '2015-10-14 06:22:35'),
(4, 1, 1, 0, 'Q7 New', 'AU SYD 236', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-09-28 06:52:43', '2015-10-14 06:23:42'),
(5, 1, 1, 0, 'A8 New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-01 05:46:25', '2015-10-14 06:24:50'),
(7, 7, 1, 0, '300 White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-01 05:48:30', '2015-10-14 06:26:12'),
(8, 7, 1, 0, '300 New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-01 05:49:09', '2015-10-14 06:27:18'),
(9, 8, 1, 0, 'Caprice White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-01 05:50:14', '2015-10-14 06:29:09'),
(10, 8, 1, 0, 'Caprice New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-01 05:50:46', '2015-10-14 06:49:42'),
(11, 4, 1, 0, '5 Series White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-13 12:07:43', '2015-10-14 06:38:38'),
(12, 1, 1, 0, 'A6 New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:19:51', '2015-10-14 06:19:51'),
(13, 1, 1, 0, 'A6 White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:34:17', '2015-10-14 06:34:17'),
(14, 4, 1, 0, '5 Series New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:37:26', '2015-10-14 06:39:26'),
(16, 4, 1, 0, '7 Series New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:42:15', '2015-10-14 06:42:15'),
(17, 4, 1, 0, '7 Series White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:43:09', '2015-10-14 06:43:09'),
(18, 4, 1, 0, 'X5 Series New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:46:23', '2015-10-14 06:48:03'),
(19, 4, 1, 0, 'X5 Series White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:47:30', '2015-10-14 06:47:30'),
(20, 12, 1, 0, 'Cayenne White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:56:48', '2015-10-14 06:56:48'),
(21, 12, 1, 0, 'Cayenne New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:57:29', '2015-10-14 06:57:29'),
(22, 12, 1, 0, 'Panamera White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:58:08', '2015-10-14 06:58:08'),
(23, 12, 1, 0, 'Panamera New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 06:58:41', '2015-10-14 06:58:41'),
(24, 14, 1, 0, 'Sports White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:03:26', '2015-10-14 07:04:51'),
(25, 3, 1, 0, 'XF New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:03:38', '2015-10-14 07:03:38'),
(26, 14, 1, 0, 'Sports New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:04:09', '2015-10-14 07:04:09'),
(27, 3, 1, 0, 'XF White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:04:14', '2015-10-14 07:04:14'),
(28, 3, 1, 0, 'XJ New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:05:35', '2015-10-14 07:05:35'),
(29, 3, 1, 0, 'XJ White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:06:32', '2015-10-14 07:06:32'),
(31, 6, 1, 0, 'S White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:07:52', '2015-10-14 07:07:52'),
(32, 6, 1, 0, 'S New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:08:28', '2015-10-14 07:08:28'),
(33, 11, 1, 0, 'Ghibli White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:09:38', '2015-10-14 07:09:38'),
(34, 11, 1, 0, 'Ghibli New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:10:07', '2015-10-14 07:10:20'),
(35, 11, 1, 0, 'Quattroporte White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:10:57', '2015-10-14 07:10:57'),
(36, 11, 1, 0, 'Quattroporte New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:11:27', '2015-10-14 07:11:27'),
(37, 10, 1, 0, 'ES New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:15:48', '2015-10-14 07:15:48'),
(38, 10, 1, 0, 'ES White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:16:31', '2015-10-14 07:16:31'),
(39, 10, 1, 0, 'GS New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:22:19', '2015-10-14 07:22:19'),
(40, 10, 1, 0, 'GS White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:22:54', '2015-10-14 07:22:54'),
(41, 10, 1, 0, 'LS New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:34:26', '2015-10-14 07:34:26'),
(42, 10, 1, 0, 'LS White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:35:23', '2015-10-14 07:35:23'),
(43, 10, 1, 0, 'RX New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:36:11', '2015-10-14 07:36:11'),
(44, 10, 1, 0, 'RX White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 07:36:55', '2015-10-14 07:36:55'),
(45, 2, 1, 0, 'E Class New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 08:09:48', '2015-10-14 08:09:48'),
(46, 2, 1, 0, 'E Class White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 08:10:35', '2015-10-14 08:10:35'),
(47, 2, 1, 0, 'GL Class New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 08:11:33', '2015-10-14 08:11:33'),
(48, 2, 1, 0, 'GL Class White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 08:12:21', '2015-10-14 08:12:21'),
(49, 2, 1, 0, 'M Class New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 08:14:39', '2015-10-14 08:15:07'),
(50, 2, 1, 0, 'M Class White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 08:15:49', '2015-10-14 08:15:49'),
(51, 2, 1, 0, 'S Class New', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 08:16:45', '2015-10-14 08:16:45'),
(52, 2, 1, 0, 'S Class White', '', 20, 20, 1, 2.5, 50, '', '1', '0', '1', '2015-10-14 08:17:20', '2015-10-14 08:17:20');

-- --------------------------------------------------------

--
-- Table structure for table `car_category`
--

CREATE TABLE IF NOT EXISTS `car_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `car_image` varchar(500) NOT NULL,
  `car_logo` varchar(500) NOT NULL,
  `add_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `car_category`
--

INSERT INTO `car_category` (`category_id`, `name`, `car_image`, `car_logo`, `add_datetime`, `update_datetime`) VALUES
(1, 'Audi', '1441284456_image.png', '1441342516_image.png', '2015-09-03 12:47:54', '2015-09-04 04:55:16'),
(2, 'Mercedes', '1441283422_image.png', '1441342525_image.png', '2015-09-03 12:47:54', '2015-09-04 04:55:25'),
(3, 'Jaguar', '1441283468_image.png', '1441342533_image.png', '2015-09-03 12:47:54', '2015-09-04 04:55:33'),
(4, 'BMW', '1441283487_image.png', '1441342541_image.png', '2015-09-03 12:47:54', '2015-09-04 04:55:41'),
(5, 'Bentle', '1441342795_image.png', '1441342795_image1.png', '2015-09-04 04:59:55', '2015-09-04 04:59:55'),
(6, 'Tesla', '1441342856_image.png', '1441342856_image1.png', '2015-09-04 05:00:56', '2015-09-04 05:00:56'),
(7, 'Chrysler', '1441342890_image.png', '1441342890_image1.png', '2015-09-04 05:01:30', '2015-09-04 05:01:30'),
(8, 'Holden', '1441342928_image.png', '1441342928_image1.png', '2015-09-04 05:02:08', '2015-09-04 05:02:08'),
(9, 'Land Rover', '1441342993_image.png', '1441342993_image1.png', '2015-09-04 05:03:13', '2015-09-04 05:03:13'),
(10, 'Lexus', '1441343031_image.png', '1441343031_image1.png', '2015-09-04 05:03:51', '2015-09-04 05:03:51'),
(11, 'Maserati', '1441343052_image.png', '1441343052_image1.png', '2015-09-04 05:04:12', '2015-09-04 05:04:12'),
(12, 'Porsche', '1441343084_image.png', '1441343084_image1.png', '2015-09-04 05:04:44', '2015-09-04 05:04:44'),
(13, 'Rolls Royce', '1441343131_image.png', '1441343131_image1.png', '2015-09-04 05:05:30', '2015-09-04 05:05:30'),
(14, 'Range Rover', '1441343166_image.png', '1441343166_image1.png', '2015-09-04 05:06:06', '2015-10-14 07:02:37');

-- --------------------------------------------------------

--
-- Table structure for table `car_images`
--

CREATE TABLE IF NOT EXISTS `car_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `image_name` varchar(50) NOT NULL,
  `add_datetime` datetime NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `car_images`
--

INSERT INTO `car_images` (`image_id`, `car_id`, `image_name`, `add_datetime`) VALUES
(20, 1, '1444737766.png', '2015-10-13 12:02:46'),
(21, 1, '14447377661.png', '2015-10-13 12:02:46'),
(22, 11, '1444738063.png', '2015-10-13 12:07:43'),
(23, 12, '1444803591.png', '2015-10-14 06:19:51'),
(24, 12, '14448035911.png', '2015-10-14 06:19:51'),
(25, 3, '1444803755.png', '2015-10-14 06:22:35'),
(26, 3, '14448037551.png', '2015-10-14 06:22:35'),
(27, 4, '1444803822.png', '2015-10-14 06:23:42'),
(28, 4, '14448038221.png', '2015-10-14 06:23:42'),
(29, 5, '1444803890.png', '2015-10-14 06:24:50'),
(30, 7, '1444803972.png', '2015-10-14 06:26:12'),
(31, 8, '1444804038.png', '2015-10-14 06:27:18'),
(32, 8, '14448040381.png', '2015-10-14 06:27:18'),
(33, 9, '1444804149.png', '2015-10-14 06:29:09'),
(34, 9, '14448041491.png', '2015-10-14 06:29:09'),
(35, 10, '1444804199.png', '2015-10-14 06:29:59'),
(36, 13, '1444804457.png', '2015-10-14 06:34:17'),
(37, 14, '1444804679.png', '2015-10-14 06:37:59'),
(38, 16, '1444804935.png', '2015-10-14 06:42:15'),
(39, 16, '14448049351.png', '2015-10-14 06:42:15'),
(40, 17, '1444804989.png', '2015-10-14 06:43:09'),
(41, 18, '1444805183.png', '2015-10-14 06:46:23'),
(42, 18, '14448051831.png', '2015-10-14 06:46:23'),
(43, 19, '1444805250.png', '2015-10-14 06:47:30'),
(44, 19, '14448052501.png', '2015-10-14 06:47:30'),
(45, 20, '1444805808.png', '2015-10-14 06:56:48'),
(46, 21, '1444805849.png', '2015-10-14 06:57:29'),
(47, 22, '1444805888.png', '2015-10-14 06:58:08'),
(48, 23, '1444805921.png', '2015-10-14 06:58:41'),
(49, 24, '1444806207.png', '2015-10-14 07:03:26'),
(50, 25, '1444806218.png', '2015-10-14 07:03:38'),
(51, 26, '1444806250.png', '2015-10-14 07:04:09'),
(52, 27, '1444806254.png', '2015-10-14 07:04:14'),
(53, 28, '1444806335.png', '2015-10-14 07:05:35'),
(54, 29, '1444806392.png', '2015-10-14 07:06:32'),
(55, 30, '1444806403.png', '2015-10-14 07:06:43'),
(56, 31, '1444806472.png', '2015-10-14 07:07:52'),
(57, 32, '1444806508.png', '2015-10-14 07:08:28'),
(58, 33, '1444806578.png', '2015-10-14 07:09:38'),
(59, 34, '1444806620.png', '2015-10-14 07:10:20'),
(60, 35, '1444806657.png', '2015-10-14 07:10:57'),
(61, 36, '1444806687.png', '2015-10-14 07:11:27'),
(62, 37, '1444806948.png', '2015-10-14 07:15:48'),
(63, 38, '1444806991.png', '2015-10-14 07:16:31'),
(64, 39, '1444807339.png', '2015-10-14 07:22:19'),
(65, 40, '1444807374.png', '2015-10-14 07:22:54'),
(66, 41, '1444808066.png', '2015-10-14 07:34:26'),
(67, 42, '1444808123.png', '2015-10-14 07:35:23'),
(68, 43, '1444808171.png', '2015-10-14 07:36:11'),
(69, 44, '1444808215.png', '2015-10-14 07:36:55'),
(70, 45, '1444810188.png', '2015-10-14 08:09:48'),
(71, 46, '1444810235.png', '2015-10-14 08:10:35'),
(72, 47, '1444810293.png', '2015-10-14 08:11:33'),
(73, 48, '1444810341.png', '2015-10-14 08:12:21'),
(75, 49, '1444810507.png', '2015-10-14 08:15:07'),
(78, 50, '14448105492.png', '2015-10-14 08:15:49'),
(79, 50, '14448105493.png', '2015-10-14 08:15:49'),
(80, 51, '1444810605.png', '2015-10-14 08:16:45'),
(81, 52, '1444810640.png', '2015-10-14 08:17:20');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `country_id`, `name`) VALUES
(1, 1, 'Sydney'),
(5, 1, 'Bathurst'),
(6, 1, 'Broken Hill'),
(7, 1, 'Cessnock'),
(8, 1, 'Coffs Harbour'),
(9, 1, 'Dubbo'),
(10, 1, 'Gosford'),
(11, 1, 'Goulburn'),
(12, 1, 'Grafton'),
(13, 1, 'Griffith'),
(14, 1, 'Lake Macquarie'),
(15, 1, 'Lismore'),
(16, 1, 'Maitland'),
(17, 1, 'Newcastle'),
(18, 1, 'Nowra'),
(19, 1, 'Orange'),
(20, 1, 'Port Macquarie'),
(21, 1, 'Queanbeyan'),
(22, 1, 'Tamworth, New South Wales'),
(23, 1, 'Tweed Heads'),
(24, 1, 'Wagga Wagga'),
(25, 1, 'Wollongong'),
(26, 1, 'Wyong');

-- --------------------------------------------------------

--
-- Table structure for table `cms`
--

CREATE TABLE IF NOT EXISTS `cms` (
  `cms_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `content` longtext NOT NULL,
  `url` varchar(500) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`cms_id`, `title`, `content`, `url`, `status`) VALUES
(1, 'What We Are About', '<p>Hire Car Sydney City is a private label Luxury Transport Service.&nbsp;<br />\r\n<br />\r\nWe offer you the car of your dreams for absolutely any occasion. We ensure that our range of exclusive options are convenient, cost-effective, timely and most importantly, always stylish.&nbsp;<br />\r\n<br />\r\n&quot;Whether its a business appointment, special event or meeting a friend, do so in style and comfort. Hire Car Sydney City will ensure that you do, by providing a first class chauffeur service and Luxury Car to suit every occasion&quot;.<br />\r\n<br />\r\nHire Car Sydney City Service specialises in chauffeured car services. Our chauffeurs are all licensed to drive hire cars.&nbsp;<br />\r\n<br />\r\nWe pride our self on our ability to provide a professional, reliable, discreet and friendly service and will cater to all your requirements. Our highly trained chauffeurs are dedicated to delivering you safely to and from your desired destination.&nbsp;<br />\r\n<br />\r\nOur Services:&nbsp;<br />\r\n<br />\r\nSydney Airport Chauffeur&nbsp;<br />\r\nDoor to Door pick up and drop off&nbsp;<br />\r\nCorporate Travels&nbsp;<br />\r\nSpecial Occasions&nbsp;<br />\r\nPersonalised Tours&nbsp;<br />\r\nSpecial Occasions&nbsp;<br />\r\nFormal Car Hire&nbsp;<br />\r\nWedding Car Hire</p>', 'WhatWeAreAbout', 1),
(2, 'Our History', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 'OurHistory', 1),
(3, 'Support', '<p><strong>ACCOUNT:</strong></p>\r\n\r\n<p><a href="http://development.ifuturz.com/core/HireCarSydneyCity/riderprofile">Update my profile information</a></p>\r\n\r\n<p><a href="http://development.ifuturz.com/core/HireCarSydneyCity/riderprofile">Reset my password</a></p>\r\n\r\n<p><a href="http://development.ifuturz.com/core/HireCarSydneyCity/riderprofile">Confirm my mobile number</a></p>\r\n\r\n<p>Email us at support@hirecarsydneycity.com.au</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>PAYMENT</p>\r\n\r\n<p>Promotions</p>\r\n\r\n<p>Payment options</p>\r\n\r\n<p>Update payment information</p>\r\n\r\n<p>For Refund email us at support@hirecarsydneycity.com.au</p>', 'Support', 1),
(4, 'Payment', '<p><strong>Payment</strong></p>\r\n\r\n<p>All payments must be made in full prior to Pick up.</p>\r\n\r\n<p>Payments must be made via Credit Cards, Coupons or PayPal secure payment gateway facilities accessible via the website and will be subject to any terms and conditions of these providers.</p>\r\n\r\n<p><strong>Refund Policy And Transport Fares:</strong></p>\r\n\r\n<p>We require a minimum of 24 hours notice to scheduled pickup time in order to avoid 25% cancellation charge.</p>\r\n\r\n<p>If Client is unable to locate our driver for any reason, full refund will be issued. If Our Driver is at allocated pick up location and client don&rsquo;t show up for 15min and don&rsquo;t even respond to drivers Phone call full charge will be taken.</p>\r\n\r\n<p><strong>Extra Charge: </strong></p>\r\n\r\n<p>If Client damage or make a mess (Dirty, Vomit or other big messes) on vehicle during the ride, Client will be charge for the expenses occur to fix or clean.</p>\r\n\r\n<p>The basic transfer rate is based upon a pickup and drop-off without any associated delay or waiting time exceeding 30 minutes for ground transfers, 45 minutes for domestic airport &amp;international airport arrival.In the event the Client is delayed, or the waiting time is exceeded, a service fee of $20 per 10 minutes will be applied. <strong>This does not apply for hourly bookings</strong>.</p>', 'Payment', 1),
(5, 'Settings', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 'Settings', 1),
(6, 'Privacy Policy', '<p><strong>Privacy and Personal Information</strong></p>\r\n\r\n<p>Our Privacy Policy available on this website explains how your personal information is collected and managed in accordance with the National Privacy Principles in the Privacy Act 1988 (Cth).</p>\r\n\r\n<p>The privacy of your personal information is important to <strong>A&amp;K&nbsp;Hire Car Sydney City</strong></p>\r\n\r\n<p><strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>and its third parties may collect personal information directly from you when you register as a member of the website, when you place an Order, or when you contact&nbsp;<a href="mailto:info@hirecarsydneycity.com.au"><strong>info@hirecarsydneycity.</strong>com.au</a>&nbsp;.Personal information may include your name, residential and/or postal address, telephone number and email address.</p>\r\n\r\n<p>Your personal information is not collected if you only browse this website.</p>\r\n\r\n<p><strong>A&amp;K Hire Car Sydney City&nbsp;</strong>and their authorised third parties, may use your personal information for the purposes for which you give it to any of them and for their own internal purposes. You agree that&nbsp;<strong>A&amp;K Hire Car Sydney City&nbsp;</strong>and/or its third party may use your email address to send you messages concerning your membership account, any service you use and information about the serviceprovided&nbsp;via the website that&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>thinks may be of interest to you. If you would prefer not to receive promotional or other material from&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>or its third parties, please let us know and&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>will respect your request. You also agree&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>may contact you by telephone to arrange delivery or collection of your Order.</p>\r\n\r\n<p><strong>Waiver</strong></p>\r\n\r\n<p>The failure by&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>to exercise or enforce any right or provision under these terms will not constitute a waiver of such right or provision. Any waiver of any provision under these terms will only be effective if it is in writing and signed by&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City</strong></p>', 'PrivacyPolicy', 1),
(7, 'Terms & Condition', '<p><strong>A&amp;K Hire Car Sydney City</strong>will endeavour to get the customer to the destination safely, comfortably and on time, however mishaps/accidents/traffic delays etc. do happen and allowances must be made for traffic hold ups, weather, illness or vehicle breakdowns, etc. We cannot be held liable for those circumstances that are beyond our control. Should there be a mishap or vehicle breakdown every effort will be made to dispatch another vehicle to complete your function. Note that our terms and conditions also cover subcontract cars and drivers brought in, in an emergency. No refund will be forthcoming unless <strong>A&amp;K</strong>&nbsp;<strong>Hire Car Sydney City</strong>is unable to solve the situation or problem at hand.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Agreed Price</strong>&nbsp;&ndash; The Agreed Price is the price to be paid for the journey as agreed between <strong>A&amp;K</strong>&nbsp;<strong>Hire Car Sydney City</strong>and the customer. The times, route and other details of the journey are stipulated on the Booking Confirmation. Any requests to modify any of these details should be made to us at least a week in advance of the date of travel and we will endeavour to accommodate minor changes to the customer&rsquo;s requirement, subject to our ability to do so.</p>\r\n\r\n<p><strong>Payment</strong></p>\r\n\r\n<p>All payments must be made in full prior to Pick up.</p>\r\n\r\n<p>Payments must be made via Credit Cards, Coupons or PayPal secure payment gateway facilities accessible via the website and will be subject to any terms and conditions of these providers.</p>\r\n\r\n<p><strong>Refund Policy And Transport Fares:</strong></p>\r\n\r\n<p>We require a minimum of 24 hours notice to scheduled pickup time in order to avoid 25% cancellation charge.</p>\r\n\r\n<p>If Client is unable to locate our driver for any reason, full refund will be issued. If Our Driver is at allocated pick up location and client don&rsquo;t show up for 15min and don&rsquo;t even respond to drivers Phone call full charge will be taken.</p>\r\n\r\n<p><strong>Extra Charge:</strong></p>\r\n\r\n<p>If Client damage or make a mess (Dirty, Vomit or other big messes) on vehicle during the ride, Client will be charge for the expenses occur to fix or clean.</p>\r\n\r\n<p>The basic transfer rate is based upon a pickup and drop-off without any associated delay or waiting time exceeding 30 minutes for ground transfers, 45 minutes for domestic airport &amp;international airport arrival.In the event the Client is delayed, or the waiting time is exceeded, a service fee of $20 per 10 minutes will be applied.&nbsp;<strong>This does not apply for hourly bookings</strong>.</p>\r\n\r\n<p><strong>Website Terms And Conditions</strong></p>\r\n\r\n<p>These terms and conditions govern your use of this website; by using this website, you accept these terms and conditions in full. If you disagree with these terms and conditions or any part of these terms and conditions, you must not use this website and our Service.</p>\r\n\r\n<p>You must be at least [18] years of age to use this website. By using this website [and by agreeing to these terms and conditions] you warrant and represent that you are at least [18] years of age.]</p>\r\n\r\n<p>This website uses cookies. By using this website and agreeing to these terms and conditions, you consent to our&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&rsquo;s</strong>&nbsp;use of cookies in accordance with the terms of <strong>A&amp;K&nbsp;Hire Car Sydney City&rsquo;s</strong>&nbsp;privacy policy / cookies policy</p>\r\n\r\n<p><strong>License to use website</strong></p>\r\n\r\n<p><strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>own the intellectual property rights in the website and material on the website. Subject to the license below, all these intellectual property rights are reserved.</p>\r\n\r\n<p>You may view, download for caching purposes only, and print pages from the website for your own personal use, subject to the restrictions set out below and elsewhere in these terms and conditions.</p>\r\n\r\n<p><strong>You must not:</strong></p>\r\n\r\n<ul>\r\n	<li>Republish material from this website (including republication on another website);</li>\r\n	<li>Sell, rent or sub-license material from the website;</li>\r\n	<li>Show any material from the website in public;</li>\r\n	<li>Reproduce, duplicate, copy or otherwise exploit material on this website for a commercial purpose</li>\r\n	<li>Edit or otherwise modify any material on the website</li>\r\n	<li>redistribute material from this website [except for content specifically and expressly made available for redistribution].]</li>\r\n</ul>\r\n\r\n<p><strong>Acceptable use</strong></p>\r\n\r\n<p>You must not use this website in any way that causes, or may cause, damage to the website or impairment of the availability or accessibility of the website; or in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity.</p>\r\n\r\n<p>You must not use this website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software.</p>\r\n\r\n<p>You must not conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to this website without&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&rsquo;s</strong>&nbsp;express written consent.</p>\r\n\r\n<p>You must not use this website to transmit or send unsolicited commercial communications.</p>\r\n\r\n<p>You must not use this website for any purposes related to marketing without&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&rsquo;s</strong>&nbsp;express written consent.</p>\r\n\r\n<p><strong>Restricted access</strong></p>\r\n\r\n<p>Access to certain areas of this website is restricted.&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>reserves the right to restrict access to certain areas of this website, or indeed this entire website, at&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&rsquo;s</strong>&nbsp;discretion.</p>\r\n\r\n<p>If&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>provides you with a user ID and password to enable you to access restricted areas of this website or other content or services, you must ensure that the user ID and password are kept confidential.</p>\r\n\r\n<p><strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>may disable your user ID and password in&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>sole discretion without notice or explanation.</p>\r\n\r\n<p><strong>User content</strong></p>\r\n\r\n<p><strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>may, at any time, add or remove content from this website without notice</p>\r\n\r\n<p>Any information or content published on this website must be read subject to these Terms of Use.</p>\r\n\r\n<p>Although&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>uses its best endeavours to confirm the accuracy of any information published on this website, you agree that <strong>A&amp;K</strong>&nbsp;<strong>Hire Car Sydney City&nbsp;</strong>cannot be held responsible for inaccuracies or errors caused by incorrect information supplied to&nbsp;<strong>A&amp;K</strong>&nbsp;<strong>Hire Car Sydney City&nbsp;</strong>or by manufacturers or suppliers changing product specifications without notice to&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City</strong>. You agree to make your own enquiries to verify information provided and to assess the suitability of products before you purchase.</p>\r\n\r\n<p>The information, materials and services in this website is provided for general information purposes only. It is current at the time of first publication. It is not legal or other professional advice or intended to be comprehensive. You are responsible for determining the validity, quality and relevance of any information, material or service assessed and to take appropriate independent advice before acting or relying on any of it to ensure that it meets your particular requirements. You should report any error or omission in any information, material or service, via contact us.</p>\r\n\r\n<p>This website may feature or display third party advertising or content. By featuring or displaying such advertising or content,&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>does not in any way represent that&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>recommends or endorses the relevant advertiser, its products or services.</p>\r\n\r\n<p><strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>nor any third party will be liable for any errors in content, or for any actions you take in reliance on them. Neither you nor any other person may hold<strong>&nbsp;A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>liable for any delays, inaccuracies, errors or omissions in respect of such content, the transmission or delivery of such content or any loss or damage arising from any of them.</p>\r\n\r\n<p><strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>may promote, advertise or sponsor functions, events, offers, competitions or other activities, which may be conducted online, or third parties may conduct offline and which. These activities may be subject to separate terms and conditions. You participate in any such activities entirely at your own risk.&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>does not accept any responsibility in connection with your participation in activities conducted by any other party.</p>\r\n\r\n<p><strong>No warranties</strong></p>\r\n\r\n<p>This website is provided &ldquo;as is&rdquo; without any representations or warranties, express or implied.&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City</strong>&nbsp;makes no representations or warranties in relation to this website or the information and materials provided on this website.</p>\r\n\r\n<p>Without prejudice to the generality of the foregoing paragraph,&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>does not warrant that:</p>\r\n\r\n<ul>\r\n	<li>This website will be constantly available, or available at all; or</li>\r\n	<li>The information on this website is complete, true, accurate or non-misleading.</li>\r\n</ul>\r\n\r\n<p>Nothing on this website constitutes, or is meant to constitute, advice of any kind. If you require advice in relation to any legal, financial or medical] matter you should consult an appropriate professional.</p>\r\n\r\n<p><strong>Limitations of liability</strong></p>\r\n\r\n<p><strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>will not be liable to you (whether under the law of contact, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:</p>\r\n\r\n<ul>\r\n	<li>to the extent that the website is provided free-of-charge, for any direct loss;</li>\r\n	<li>for any indirect, special or consequential loss; or</li>\r\n	<li>for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.</li>\r\n</ul>\r\n\r\n<p>These limitations of liability apply even if&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>has been expressly advised of the potential loss.</p>\r\n\r\n<p><strong>Exceptions</strong></p>\r\n\r\n<p>Nothing in this website disclaimer will exclude or limit any warranty implied by law that it would be unlawful to exclude or limit; and nothing in this website disclaimer will exclude or limit<strong>&nbsp;A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>liability in respect of any:</p>\r\n\r\n<ul>\r\n	<li>death or personal injury caused by&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>negligence;</li>\r\n	<li>fraud or fraudulent misrepresentation on the part of&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;or</strong></li>\r\n	<li>matter which it would be illegal or unlawful for&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>to exclude or limit, or to attempt or purport to exclude or limit, its liability.</li>\r\n</ul>\r\n\r\n<p><strong>Reasonableness</strong></p>\r\n\r\n<p>By using this website, you agree that the exclusions and limitations of liability set out in this website disclaimer are reasonable. If you do not think they are reasonable, you must not use this website.</p>\r\n\r\n<p><strong>Other parties</strong></p>\r\n\r\n<p>You accept that, as a limited liability entity,&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>has an interest in limiting the personal liability of its officers and employees. You agree that you will not bring any claim personally against&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>officers or employees in respect of any losses you suffer in connection with the website.</p>\r\n\r\n<p>Without prejudice to the foregoing paragraph, you agree that the limitations of warranties and liability set out in this website</p>\r\n\r\n<p>disclaimer will protect&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City &lsquo;s&nbsp;</strong>officers, employees, agents, subsidiaries, successors, assigns and sub-contractors as well as&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City.</strong></p>\r\n\r\n<p><strong>Unenforceable provisions</strong></p>\r\n\r\n<p>If any provision of this website disclaimer is, or is found to be, unenforceable under applicable law, that will not affect the enforceability of the other provisions of this website disclaimer.</p>\r\n\r\n<p><strong>Indemnity</strong></p>\r\n\r\n<p>You hereby indemnify&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>and undertake to keep [NAME] indemnified against any losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>to a third party in settlement of a claim or dispute on the advice of&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>legal advisers) incurred or suffered by&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>arising out of any breach by you of any provision of these terms and conditions, or arising out of any claim that you have breached any provision of these terms and conditions.</p>\r\n\r\n<p><strong>Breaches of these terms and conditions</strong></p>\r\n\r\n<p>Without prejudice to&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>other rights under these terms and conditions, if you breach these terms and conditions in any way,&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>may take such action as&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>deems appropriate to deal with the breach, including suspending your access to the website, prohibiting you from accessing the website, blocking computers using your IP address from accessing the website, contacting your internet service provider to request that they block your access to the website and/or bringing court proceedings against you.</p>\r\n\r\n<p><strong>Variation</strong></p>\r\n\r\n<p><strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>may revise these terms and conditions from time-to-time. Revised terms and conditions will apply to the use of this website from the date of the publication of the revised terms and conditions on this website. Please check this page regularly to ensure you are familiar with the current version.</p>\r\n\r\n<p><strong>Assignment</strong></p>\r\n\r\n<p><strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>may transfer, sub-contract or otherwise deal with&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&rsquo;s</strong>&nbsp;rights and/or obligations under these terms and conditions without notifying you or obtaining your consent.</p>\r\n\r\n<p>You may not transfer, sub-contract or otherwise deal with your rights and/or obligations under these terms and conditions.&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>reserves the right, without giving notice or seeking consent, to transfer or assign the personal information, content and rights that&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>has collected from you and any agreements it has made with you.</p>\r\n\r\n<p><strong>Severability</strong></p>\r\n\r\n<p>If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect. If any unlawful and/or unenforceable provision would be lawful or enforceable if part of it were deleted, that part will be deemed to be delete, and the rest of the provision will continue in effect.</p>\r\n\r\n<p><strong>Entire agreement</strong></p>\r\n\r\n<p>By accessing and/or using the website, you accept these terms and agree to be bound by them, and an agreement is formed between you and us.</p>\r\n\r\n<p>These Terms may be amended at any time without notice and your access to this website may be terminated at any time without notice. Your continued use of the website following such amendment of these Terms will represent an agreement by you to be bound by these Terms as amended. We recommend you review the terms for amendments each time you use the website and before placing any Order. Should we choose to provide you with notice of amended terms, you agree to receive email notification of the amendments from us or our third party.</p>\r\n\r\n<p><strong>Governing Law and jurisdiction</strong></p>\r\n\r\n<p>These Terms of Use are governed by and construed in accordance with the laws of New South Wales, Australia and you irrevocably and unconditionally submit to the non-exclusive jurisdiction of the courts of New South Wales, Australia. If any provision of these Terms of Use is found to be invalid or unenforceable by a court of law, such invalidity or unenforceability will not affect the remainder of these Terms of Use, which will continue in full force and effect. If you access this website in a jurisdiction other than New South Wales, Australia, you are responsible for compliance with the laws of that jurisdiction, to the extent that they apply.&nbsp;<strong>A&amp;K&nbsp;Hire Car Sydney City&nbsp;</strong>makes no representations that the content of this website complies with the laws of any country outside Australia.</p>', 'TermsAndCondition', 1),
(8, 'welcome to Hire car sydney city', '<p><strong>We are ready to Pick you up !</strong></p>\n\n<p>Hire Car Sydney City offers a quality chauffeured car transport service for corporate and private hire clients in Sydney Area. Our chauffeur driven hire cars will pick you up from any location &ndash; office, home, restaurant or airport.</p>\n\n<p>We are very flexible as per your needs. Whether you want early morning pickups or late night pickups our chauffeurs will be they&rsquo;re waiting for you.Our chauffeurs are professionally trained and accredited by state authorities. We are in business to provide you safest and most comfortable journey.</p>\n\n<p><strong>Chauffeur Service</strong></p>\n\n<p>Hire Car Sydney Cityis Sydney&#39;s most trusted and professional chauffeur service offering chauffeured luxury cars for all occasions and airport transfer services.</p>\n\n<p>Remember us for Professional Chauffeur service, whether it&#39;s Chauffeur Hire for your next corporate function, Chauffeur Driven Airport Transfer,or VIP Chauffeur Transport for an event.</p>', 'welcometoHirecarsydneycity', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zipcode` varchar(8) NOT NULL,
  `status` enum('1','0') NOT NULL COMMENT '1=Active, 0=Inactive',
  `add_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `name`, `address`, `city_id`, `zipcode`, `status`, `add_datetime`, `update_datetime`) VALUES
(1, 'Test Company', 'Test Company', 1, '654821', '1', '2015-07-30 08:25:30', '2015-08-05 04:35:00'),
(14, 'Test Company2', 'TEST COMPANY 2', 2, '458954', '1', '2015-08-03 07:14:49', '2015-08-04 12:22:15'),
(15, 'Test Company3', 'TEst address 2', 2, '564582', '1', '2015-08-03 10:54:26', '2015-08-04 10:07:39');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_details`
--

CREATE TABLE IF NOT EXISTS `contact_us_details` (
  `contact_us_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_email_id` varchar(255) NOT NULL,
  `contact_datetime` datetime NOT NULL,
  `trip_details` varchar(1000) NOT NULL,
  PRIMARY KEY (`contact_us_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `contact_us_details`
--

INSERT INTO `contact_us_details` (`contact_us_id`, `user_name`, `user_phone`, `user_email_id`, `contact_datetime`, `trip_details`) VALUES
(1, 'hello', '1234567890', 'test@email.com', '2015-08-20 20:06:49', 'hello'),
(5, 'test', '9876543210', 'test@email.com', '2015-08-20 20:13:41', 'test'),
(6, 'Test Test', '2422132', 'Test@test.sdsd', '2015-08-24 11:47:39', 'sdsdfsfsd'),
(7, 'Rajesh SOlanki', '23492834', 'rajesh.solanki@ifuturz.com', '2015-08-27 10:20:57', 'LOrem Ipsum LOrem Ipsum'),
(8, 'Rajesh SOlanki', '23492834', 'rajesh.solanki@ifuturz.com', '2015-08-27 10:24:31', 'dkjsdkfkksdlfajl'),
(9, 'Rajesh SOlanki', '23492834', 'rajesh.solanki@ifuturz.comz', '2015-08-27 10:29:45', 'xczczxczxc'),
(10, 'test', '00', 'anjou143@gmail.com', '2015-10-26 02:09:59', 'test test'),
(11, 'test', '00', 'anjou143@gmail.com', '2015-10-26 02:10:08', 'test test'),
(12, 'test', '00', 'anjou143@gmail.com', '2015-10-26 02:10:17', 'test test'),
(13, 'Virag Shah', '5689458962', 'virag.shah@ifuturz.com', '2015-10-26 04:31:30', 'Hi, I wann a drive for a trip.'),
(14, 'Virag Shah', '5689458962', 'virag.shah@ifuturz.com', '2015-10-26 04:32:45', 'Hi'),
(15, 'Virag Shah', '5689458962', 'virag.shah@ifuturz.com', '2015-10-26 04:36:42', 'Hi.'),
(16, 'Virag Shah', '5689458962', 'virag.shah@ifuturz.com', '2015-10-26 04:40:27', 'hi'),
(17, 'Virag Shah', '12356897845', 'virag.shah@ifuturz.com', '2015-10-26 04:52:59', 'Hi'),
(18, 'Virag Shah', '5689458962', 'virag.shah@ifuturz.com', '2015-10-26 04:54:35', 'Hello.');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `creditcard_detail`
--

CREATE TABLE IF NOT EXISTS `creditcard_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `card_no` varchar(255) NOT NULL,
  `exp_month` varchar(2) NOT NULL,
  `exp_year` varchar(2) NOT NULL,
  `ccv_no` int(11) NOT NULL,
  `name_of_card` varchar(255) NOT NULL,
  `card_type` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1 => Driver and 2=> Rider',
  `add_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `creditcard_detail`
--

INSERT INTO `creditcard_detail` (`id`, `user_id`, `card_no`, `exp_month`, `exp_year`, `ccv_no`, `name_of_card`, `card_type`, `user_type`, `add_datetime`, `update_datetime`) VALUES
(1, 27, '4444333322221111', '9', '15', 123, 'TestUser', 'American Express', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 5, '6sCLxzwaRIl84p5HzKGDttqar6/J1+8OTQmiv4eGp2Tx4775Rx3qQN5KYnq9X71HWSsZ3dfsmA1GKNVGtR/HfQ==', '1', '20', 222, 'testset test', 'American Express', 1, '2015-08-23 05:22:40', '2015-08-23 05:22:40'),
(3, 18, '65Z/5Ls00Ylrr1ow+7tLNXAkLQ9SyNTzSaPaeBB5bXGXM3h6BNGbaNmmVVOeZE9JbltquFLPuh/h6hAlhvzJrw==', '09', '15', 123, 'Virag', 'Visa', 2, '2015-09-23 16:19:55', '2015-09-23 16:19:55'),
(9, 17, 'JAmXCRBg8/4rz2l2Gro2HzXdAYXnM3TPEb14G/q+G4XB+lKonglIlYlQHtaMtmsYfFs6DldL1T9ZpyZVG1mZJw==', '9', '16', 123, 'TestUser', 'American Express', 2, '2015-09-29 06:36:29', '2015-09-29 06:36:29'),
(10, 19, 'Wd7mraXmEll7UcHi07b0sgXJpYAS8vd5R2Ta6uPOdhQ31SLNs5WNDDfjDRwTwY79PyYmAsQVxQPb+s5c7Zke0w==', '11', '15', 123, 'TestUser', 'American Express', 2, '2015-10-05 07:14:27', '2015-10-05 07:14:27');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE IF NOT EXISTS `driver` (
  `driver_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(30) NOT NULL,
  `mobile_number` varchar(15) NOT NULL,
  `companyname` varchar(255) NOT NULL,
  `abn` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `invitecode` varchar(255) NOT NULL,
  `profile_image` varchar(50) NOT NULL,
  `licensedetails` varchar(255) NOT NULL,
  `status` enum('1','0','2') NOT NULL COMMENT '1=Active, 0=Inactive',
  `add_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`driver_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `first_name`, `last_name`, `email_id`, `username`, `password`, `mobile_number`, `companyname`, `abn`, `address`, `city`, `zipcode`, `invitecode`, `profile_image`, `licensedetails`, `status`, `add_datetime`, `update_datetime`) VALUES
(3, 'test123', 'test', 'test@ifuturz.com', 'test123', '123456', '9876543210', '', '', '', '', '', '', '', '', '1', '2015-08-18 14:37:51', '2015-08-18 14:37:51'),
(4, 'mitul', 'patel', 'metul@gmail.com', 'mitul', '', '0123456789', '', '', '', '', '', '', '', '', '1', '2015-08-18 14:47:21', '2015-08-19 11:53:46'),
(15, 'adad', 'sdfsdf', 'fsfsdf@dsfsd.fsdf', '', '', '9898981111', '', '', '', '', '', '', '', '', '1', '2015-08-22 05:59:25', '2015-08-22 12:19:00'),
(22, 'puni', 'patels', 'punit@email.com', 'punits', 'Test@123', '9879879870', '', '343ee', '', '', '', '', 'Chrysanthemum55e036720bebc.jpg', '', '1', '2015-08-27 14:31:49', '2015-08-28 10:47:02'),
(23, 'Ra', 'Solanki', 'rajesh.solanki@ifuturz.com', 'rajesh', 'Test123', '342342432', '', '', '', '', '', '', '', '', '1', '2015-08-31 11:33:00', '2015-08-31 11:33:00'),
(24, 'manoj', 'gohil', 'manoj@gmail.com', '', '123456', '9879873210', 'testing', 'test', 'mombasha', '9', '232356', '456', 'Desert55e8a1c2e2f97.jpg', '', '1', '2015-09-03 19:38:42', '2015-09-03 19:38:42'),
(25, 'Test', 'Name', 'driverwe@email.com', '', '123456', '1234567892', '', '5689236594', 'Sydney', '1', '2020', '', '', '', '1', '2015-10-02 09:27:03', '2015-10-26 05:29:27'),
(26, 'Php RedStorm', 'undefined', 'rider', '', '', '', '', '', '', '', '', '', '', '', '1', '2015-10-13 05:24:59', '2015-10-13 05:24:59');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(255) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `event_name`) VALUES
(1, 'Wedding'),
(2, 'Fun Party'),
(3, 'Corporate'),
(4, 'Formal'),
(5, 'Airport Transfer'),
(6, 'Door to Door Pickup');

-- --------------------------------------------------------

--
-- Table structure for table `giftcard`
--

CREATE TABLE IF NOT EXISTS `giftcard` (
  `giftcard_id` int(11) NOT NULL AUTO_INCREMENT,
  `giftcard_name` varchar(255) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `pricepercentage` varchar(3) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `add_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1=>Active & 2=>Inactive',
  PRIMARY KEY (`giftcard_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `giftcard`
--

INSERT INTO `giftcard` (`giftcard_id`, `giftcard_name`, `image_name`, `price`, `pricepercentage`, `coupon_code`, `add_datetime`, `update_datetime`, `status`) VALUES
(1, 'On Ride Car With Couple', 'Business_Card_$20056092ffb989dc.png', 200, '10', 'XEQNS2', '2015-08-17 09:25:57', '2015-09-28 12:18:03', '1'),
(2, 'Gift For Your Loved One', 'Business_Card_$5056092fdc11e6e.png', 50, '5', '1GDBEA', '2015-08-23 08:42:17', '2015-09-28 12:17:32', '1'),
(3, 'Travel Hassle Free', 'Business_Card_$10056092fcf22ca2.png', 100, '10', 'UXMP4V', '2015-08-23 08:49:36', '2015-09-28 12:17:19', '1'),
(4, 'Sample Test Gift CARD', 'Business_Card_$50056092fe9437d3.png', 500, '5', '', '2015-09-28 11:29:49', '2015-09-28 12:17:45', '1');

-- --------------------------------------------------------

--
-- Table structure for table `giftcard_coupon`
--

CREATE TABLE IF NOT EXISTS `giftcard_coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_code` varchar(255) NOT NULL,
  `giftcard_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `remaining_amount` float NOT NULL,
  `pricepercentage` float NOT NULL,
  `generate_by` enum('0','1') NOT NULL,
  `rider_id` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  `payment_type` varchar(255) NOT NULL COMMENT '"paypal", "eway", "deposit"',
  `generate_datetime` datetime NOT NULL,
  `use_datetime` datetime NOT NULL,
  PRIMARY KEY (`coupon_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100 ;

--
-- Dumping data for table `giftcard_coupon`
--

INSERT INTO `giftcard_coupon` (`coupon_id`, `coupon_code`, `giftcard_id`, `price`, `remaining_amount`, `pricepercentage`, `generate_by`, `rider_id`, `status`, `payment_type`, `generate_datetime`, `use_datetime`) VALUES
(7, 'LX0K2C', 0, 100, 0, 10, '1', 18, '1', '', '2015-09-15 14:31:22', '0000-00-00 00:00:00'),
(16, 'QGKXZG', 0, 200, 200, 10, '1', 17, '1', '', '2015-09-18 11:14:26', '0000-00-00 00:00:00'),
(17, 'Q5F3E4', 0, 50, 50, 5, '1', 17, '1', '', '2015-09-19 03:12:52', '0000-00-00 00:00:00'),
(18, '0S7DPJ', 0, 50, 50, 5, '1', 17, '1', '', '2015-09-19 03:18:31', '0000-00-00 00:00:00'),
(19, 'MJCUCH', 0, 50, 50, 5, '1', 17, '1', '', '2015-09-19 04:41:36', '0000-00-00 00:00:00'),
(20, 'QD9JOF', 0, 200, 200, 10, '1', 17, '1', '', '2015-09-19 04:43:11', '0000-00-00 00:00:00'),
(21, 'EC4XN2', 0, 200, 200, 10, '1', 17, '1', '', '2015-09-21 06:10:04', '0000-00-00 00:00:00'),
(22, 'MQGPHF', 0, 200, 200, 10, '1', 17, '1', '', '2015-09-21 06:14:44', '0000-00-00 00:00:00'),
(23, 'IGDXYV', 0, 200, 200, 10, '1', 17, '1', '', '2015-09-21 06:26:38', '0000-00-00 00:00:00'),
(24, 'ZNAWVM', 0, 50, 50, 5, '1', 17, '1', '', '2015-09-21 06:29:01', '0000-00-00 00:00:00'),
(25, 'LF6X0G', 0, 200, 200, 10, '1', 17, '1', '', '2015-09-21 06:33:26', '0000-00-00 00:00:00'),
(27, 'C93MQT', 0, 200, 200, 10, '1', 17, '1', '', '2015-09-21 06:45:35', '0000-00-00 00:00:00'),
(28, 'TNZXIS', 0, 50, 50, 5, '1', 17, '1', '', '2015-09-21 06:50:56', '0000-00-00 00:00:00'),
(29, '5NXQSP', 0, 200, 200, 10, '1', 17, '1', '', '2015-09-21 06:52:53', '0000-00-00 00:00:00'),
(30, '8L4ASR', 0, 200, 200, 10, '1', 17, '1', '', '2015-09-21 09:03:23', '0000-00-00 00:00:00'),
(31, 'DQO7YR', 0, 50, 50, 5, '1', 17, '1', '', '2015-09-21 09:05:14', '0000-00-00 00:00:00'),
(54, 'ITEAGN', 0, 100, 100, 10, '1', 19, '1', '', '2015-09-29 12:46:29', '0000-00-00 00:00:00'),
(55, 'MUTKE5', 0, 100, 100, 10, '1', 19, '1', '', '2015-09-29 12:48:53', '0000-00-00 00:00:00'),
(56, 'ADH8ES', 0, 100, 100, 10, '1', 19, '1', '', '2015-10-03 08:25:05', '0000-00-00 00:00:00'),
(57, 'BQUPJ2', 0, 50, 50, 5, '1', 19, '1', '', '2015-10-03 08:26:22', '0000-00-00 00:00:00'),
(61, 'DRADO9', 0, 50, 50, 5, '1', 19, '1', '', '2015-10-05 07:14:48', '0000-00-00 00:00:00'),
(62, 'LUB2FN', 0, 50, 50, 5, '1', 19, '1', '', '2015-10-06 05:12:37', '0000-00-00 00:00:00'),
(63, 'REKE5B', 0, 50, 50, 5, '1', 19, '1', '', '2015-10-06 07:15:24', '0000-00-00 00:00:00'),
(89, 'M2GINT', 2, 50, 50, 5, '0', 19, '1', 'deposit', '2015-10-16 11:55:56', '2015-10-16 11:16:02'),
(90, 'ORLWSB', 2, 50, 50, 5, '0', 19, '1', 'deposit', '2015-10-16 11:54:09', '2015-10-16 11:16:43'),
(91, 'WLERMX', 2, 50, 50, 5, '0', 19, '', 'deposit', '2015-10-20 07:02:58', '2015-10-17 06:05:34'),
(92, 'OLZMOC', 2, 50, 50, 5, '0', 19, '', 'deposit', '2015-10-20 07:03:42', '2015-10-19 12:15:05'),
(95, 'YTNQOA', 2, 50, 8.5, 5, '0', 38, '1', 'deposit', '2015-10-20 06:07:55', '2015-10-20 05:51:32'),
(96, 'QHIPE0', 2, 50, 50, 5, '0', 38, '', 'deposit', '2015-10-20 07:06:43', '2015-10-20 05:53:03'),
(97, 'UASZZG', 2, 50, 50, 5, '0', 38, '', 'deposit', '2015-10-20 07:06:26', '2015-10-20 05:55:03'),
(98, '1DE5TK', 0, 50, 50, 5, '1', 42, '0', 'paypal', '2015-10-26 01:07:41', '0000-00-00 00:00:00'),
(99, 'I4AA92', 2, 50, 50, 5, '0', 42, '0', 'deposit', '2015-10-26 01:08:08', '2015-10-26 01:08:08');

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_details`
--

CREATE TABLE IF NOT EXISTS `inquiry_details` (
  `inquiry_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_email_id` varchar(255) NOT NULL,
  `pick_up_address` varchar(1000) NOT NULL,
  `destination_address` varchar(1000) NOT NULL,
  `pick_up_datetime` datetime NOT NULL,
  `number_of_passengers` int(11) NOT NULL,
  `occasion_type` varchar(1000) NOT NULL,
  `how_to_know_us` varchar(255) NOT NULL,
  `trip_details` varchar(1000) NOT NULL,
  `inquiry_datetime` datetime NOT NULL,
  PRIMARY KEY (`inquiry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `inquiry_details`
--

INSERT INTO `inquiry_details` (`inquiry_id`, `user_name`, `user_phone`, `user_email_id`, `pick_up_address`, `destination_address`, `pick_up_datetime`, `number_of_passengers`, `occasion_type`, `how_to_know_us`, `trip_details`, `inquiry_datetime`) VALUES
(1, 'amit', '1234567890', 'test@email.com', 'test', 'test', '0000-00-00 00:00:00', 0, 'test', 'test', 'test', '2015-08-20 17:19:36'),
(2, 'sdfsdf', '324234', 'test@gmail.com', 'ssdfs', 'sdfsdfs', '0000-00-00 00:00:00', 3, 'sdfdasf', 'asdfasdf', 'sdfsdfsd', '2015-08-24 12:40:28'),
(3, 'Rajesh SOlanki', '23492834', 'rajesh.solanki@ifuturz.com', 'sdfsdf', 'sdfsdf', '0000-00-00 00:00:00', 3, 'dfsdf', 'dfsf', 'sdfsdfsfsdfsd', '2015-08-27 10:34:47');

-- --------------------------------------------------------

--
-- Table structure for table `news_subscriber`
--

CREATE TABLE IF NOT EXISTS `news_subscriber` (
  `id` int(11) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `add_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_subscriber`
--

INSERT INTO `news_subscriber` (`id`, `email_id`, `add_datetime`) VALUES
(0, 'rajni@email.com', '2015-09-02 17:53:59');

-- --------------------------------------------------------

--
-- Table structure for table `payment_details`
--

CREATE TABLE IF NOT EXISTS `payment_details` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `ride_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `payment_type` varchar(255) NOT NULL DEFAULT 'Credit Card' COMMENT 'Credit Card & Deposit & Manual Payment which is received by Driver',
  `txn_id` varchar(255) NOT NULL COMMENT 'paypal txn_id for paypal',
  `coupon_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('0','1','2') NOT NULL COMMENT '0=Not Complete, 1= Successful, 2= Failed',
  `payment_datetime` datetime NOT NULL,
  `payment_complete_datetime` varchar(25) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=182 ;

--
-- Dumping data for table `payment_details`
--

INSERT INTO `payment_details` (`payment_id`, `ride_id`, `amount`, `payment_type`, `txn_id`, `coupon_id`, `status`, `payment_datetime`, `payment_complete_datetime`) VALUES
(2, 1, 18, 'Credit Card', '', 0, '1', '2015-08-24 12:27:24', '0000-00-00 00:00:00'),
(3, 19, 19, 'Credit Card', '', 0, '0', '2015-08-24 12:39:48', '0000-00-00 00:00:00'),
(4, 20, 50, 'Credit Card', '', 0, '1', '2015-08-24 12:52:43', '0000-00-00 00:00:00'),
(5, 21, 50, 'Credit Card', '', 0, '0', '2015-08-24 14:09:21', '0000-00-00 00:00:00'),
(6, 22, 1, 'Credit Card', '', 0, '0', '2015-08-27 06:30:04', '0000-00-00 00:00:00'),
(7, 23, 1, 'Credit Card', '', 0, '1', '2015-08-27 08:59:32', '0000-00-00 00:00:00'),
(8, 24, 1, 'Credit Card', '', 0, '1', '2015-08-27 10:13:44', '0000-00-00 00:00:00'),
(10, 19, 654, 'Credit Card', '', 0, '1', '2015-08-28 09:02:57', '0000-00-00 00:00:00'),
(11, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:47:50', '0000-00-00 00:00:00'),
(12, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:50:15', '0000-00-00 00:00:00'),
(13, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:50:19', '0000-00-00 00:00:00'),
(14, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:50:41', '0000-00-00 00:00:00'),
(15, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:51:24', '0000-00-00 00:00:00'),
(16, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:53:00', '0000-00-00 00:00:00'),
(17, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:53:01', '0000-00-00 00:00:00'),
(18, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:53:04', '0000-00-00 00:00:00'),
(19, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:53:43', '0000-00-00 00:00:00'),
(20, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:53:44', '0000-00-00 00:00:00'),
(21, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:54:18', '0000-00-00 00:00:00'),
(22, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:54:19', '0000-00-00 00:00:00'),
(23, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:54:39', '0000-00-00 00:00:00'),
(24, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:54:39', '0000-00-00 00:00:00'),
(25, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:55:03', '0000-00-00 00:00:00'),
(26, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:55:03', '0000-00-00 00:00:00'),
(27, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:55:53', '0000-00-00 00:00:00'),
(28, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:55:53', '0000-00-00 00:00:00'),
(29, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:56:09', '0000-00-00 00:00:00'),
(30, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:56:09', '0000-00-00 00:00:00'),
(31, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:56:24', '0000-00-00 00:00:00'),
(32, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:56:25', '0000-00-00 00:00:00'),
(33, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:56:40', '0000-00-00 00:00:00'),
(34, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:56:40', '0000-00-00 00:00:00'),
(35, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:57:00', '0000-00-00 00:00:00'),
(36, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:57:01', '0000-00-00 00:00:00'),
(37, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:57:21', '0000-00-00 00:00:00'),
(38, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:57:21', '0000-00-00 00:00:00'),
(39, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:57:41', '0000-00-00 00:00:00'),
(40, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:57:41', '0000-00-00 00:00:00'),
(41, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:57:48', '0000-00-00 00:00:00'),
(42, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:57:49', '0000-00-00 00:00:00'),
(43, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:57:57', '0000-00-00 00:00:00'),
(44, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:57:57', '0000-00-00 00:00:00'),
(45, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:58:10', '0000-00-00 00:00:00'),
(46, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:58:19', '0000-00-00 00:00:00'),
(47, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:58:19', '0000-00-00 00:00:00'),
(48, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:58:29', '0000-00-00 00:00:00'),
(49, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:58:29', '0000-00-00 00:00:00'),
(50, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:58:39', '0000-00-00 00:00:00'),
(51, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:58:39', '0000-00-00 00:00:00'),
(52, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 09:59:52', '0000-00-00 00:00:00'),
(53, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 09:59:52', '0000-00-00 00:00:00'),
(54, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:00:29', '0000-00-00 00:00:00'),
(55, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:00:29', '0000-00-00 00:00:00'),
(56, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:00:37', '0000-00-00 00:00:00'),
(57, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:00:38', '0000-00-00 00:00:00'),
(58, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:02:46', '0000-00-00 00:00:00'),
(59, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:02:46', '0000-00-00 00:00:00'),
(60, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:03:08', '0000-00-00 00:00:00'),
(61, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:03:08', '0000-00-00 00:00:00'),
(62, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:03:31', '0000-00-00 00:00:00'),
(63, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:03:31', '0000-00-00 00:00:00'),
(64, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:03:40', '0000-00-00 00:00:00'),
(65, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:03:40', '0000-00-00 00:00:00'),
(66, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:05:55', '0000-00-00 00:00:00'),
(67, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:05:55', '0000-00-00 00:00:00'),
(68, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:07:50', '0000-00-00 00:00:00'),
(69, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:07:51', '0000-00-00 00:00:00'),
(70, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:07:57', '0000-00-00 00:00:00'),
(71, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:07:58', '0000-00-00 00:00:00'),
(72, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:08:15', '0000-00-00 00:00:00'),
(73, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:08:15', '0000-00-00 00:00:00'),
(74, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:11:31', '0000-00-00 00:00:00'),
(75, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:11:31', '0000-00-00 00:00:00'),
(76, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:11:49', '0000-00-00 00:00:00'),
(77, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:11:49', '0000-00-00 00:00:00'),
(78, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:11:59', '0000-00-00 00:00:00'),
(79, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:11:59', '0000-00-00 00:00:00'),
(80, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:12:09', '0000-00-00 00:00:00'),
(81, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:12:09', '0000-00-00 00:00:00'),
(82, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:12:17', '0000-00-00 00:00:00'),
(83, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:12:17', '0000-00-00 00:00:00'),
(84, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:12:27', '0000-00-00 00:00:00'),
(85, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:12:28', '0000-00-00 00:00:00'),
(86, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:12:53', '0000-00-00 00:00:00'),
(87, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:12:53', '0000-00-00 00:00:00'),
(88, 20, 680, 'Credit Card', '', 0, '0', '2015-08-28 10:40:59', '0000-00-00 00:00:00'),
(89, 20, 0, 'Credit Card', '', 0, '0', '2015-08-28 10:41:00', '0000-00-00 00:00:00'),
(90, 1, 12, 'Manual', '', 0, '1', '2015-08-28 12:23:41', '0000-00-00 00:00:00'),
(115, 34, 47.5, 'Credit Card', '', 0, '1', '2015-09-24 08:47:55', '0000-00-00 00:00:00'),
(117, 32, 49.25, 'PayPal', '', 0, '1', '2015-09-24 14:15:29', '0000-00-00 00:00:00'),
(118, 29, 22.8, 'Manual', '', 0, '1', '2015-09-25 16:20:02', '0000-00-00 00:00:00'),
(119, 29, 22.8, 'Manual', '', 0, '1', '2015-09-25 16:20:29', '0000-00-00 00:00:00'),
(120, 29, 22.8, 'Manual', '', 0, '1', '2015-09-25 16:21:07', '0000-00-00 00:00:00'),
(121, 28, 31.7, 'Manual', '', 0, '1', '2015-09-25 16:22:25', '0000-00-00 00:00:00'),
(128, 35, 47.5, 'Credit Card', '', 0, '1', '2015-09-26 05:39:06', '0000-00-00 00:00:00'),
(129, 38, 47.5, 'Credit Card', '', 0, '1', '2015-09-26 06:33:13', '0000-00-00 00:00:00'),
(130, 26, 167, 'Manual', '', 0, '1', '2015-09-26 07:14:07', '0000-00-00 00:00:00'),
(131, 63, 0, 'Credit Card', '', 0, '0', '2015-09-29 05:30:25', '0000-00-00 00:00:00'),
(132, 63, 0, 'Credit Card', '', 0, '0', '2015-09-29 05:33:01', '0000-00-00 00:00:00'),
(133, 63, 0, 'Credit Card', '', 0, '0', '2015-09-29 05:33:58', '0000-00-00 00:00:00'),
(134, 63, 0, 'Credit Card', '', 0, '0', '2015-09-29 05:37:28', '0000-00-00 00:00:00'),
(135, 39, 62.5, 'PayPal', '', 0, '0', '2015-09-29 05:38:00', '0000-00-00 00:00:00'),
(136, 37, 47.5, 'Credit Card', '', 0, '0', '2015-09-29 06:17:52', '0000-00-00 00:00:00'),
(137, 46, 22, 'Credit Card', '', 0, '1', '2015-09-29 06:21:40', '0000-00-00 00:00:00'),
(138, 39, 62.5, 'Credit Card', '', 0, '0', '2015-09-29 06:37:50', '0000-00-00 00:00:00'),
(139, 39, 62.5, 'PayPal', '', 0, '0', '2015-09-29 06:38:27', '0000-00-00 00:00:00'),
(150, 48, 22, 'PayPal', '', 0, '0', '2015-10-03 05:46:02', '0000-00-00 00:00:00'),
(156, 47, 22, 'PayPal', '', 0, '0', '2015-10-03 07:25:25', '0000-00-00 00:00:00'),
(157, 47, 22, 'PayPal', '9V242389BH520713B', 0, '1', '2015-10-03 07:50:20', '01:12:25 Oct 03, 2015 PDT'),
(158, 48, 22, 'PayPal', '1H998416KX364784F', 0, '1', '2015-10-03 08:22:29', '01:36:16 Oct 03, 2015 PDT'),
(159, 57, 41.5, 'Credit Card', '', 0, '0', '2015-10-05 07:09:56', ''),
(160, 58, 41.5, 'PayPal', '', 0, '0', '2015-10-05 08:12:07', ''),
(161, 59, 41.5, 'PayPal', '', 0, '0', '2015-10-05 08:17:51', ''),
(162, 60, 41.5, 'PayPal', '', 0, '0', '2015-10-05 08:28:02', ''),
(163, 61, 41.5, 'PayPal', '', 0, '0', '2015-10-05 08:29:14', ''),
(164, 58, 41.5, 'PayPal', '', 0, '0', '2015-10-06 04:34:48', ''),
(165, 58, 41.5, 'PayPal', '', 0, '0', '2015-10-06 04:35:35', ''),
(166, 58, 41.5, 'PayPal', '', 0, '0', '2015-10-08 06:41:41', ''),
(167, 58, 41.5, 'PayPal', '', 0, '0', '2015-10-08 06:42:00', ''),
(168, 63, 40.5, 'PayPal', '', 0, '0', '2015-10-14 05:00:39', ''),
(169, 68, 41.5, 'Deposit', '', 0, '1', '2015-10-19 06:31:47', ''),
(175, 69, 41.5, 'Gift Card', '', 88, '1', '2015-10-19 09:17:37', ''),
(176, 69, 41.5, 'Gift Card', '', 88, '1', '2015-10-19 09:18:31', ''),
(177, 69, 41.5, 'Gift Card', '', 88, '1', '2015-10-19 09:19:39', ''),
(178, 69, 41.5, 'Gift Card', '', 88, '1', '2015-10-19 09:20:01', ''),
(179, 69, 41.5, 'Gift Card', '', 88, '1', '2015-10-19 09:20:54', ''),
(180, 73, 41.5, 'Gift Card', '', 95, '1', '2015-10-20 10:08:06', ''),
(181, 75, 41.5, 'PayPal', '', 0, '0', '2015-10-26 05:47:12', '');

-- --------------------------------------------------------

--
-- Table structure for table `rider`
--

CREATE TABLE IF NOT EXISTS `rider` (
  `rider_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `password` varchar(30) NOT NULL,
  `mobile_number` varchar(10) NOT NULL,
  `language` varchar(255) NOT NULL,
  `device_type` enum('0','1','2') NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `status` enum('1','0','2') NOT NULL COMMENT '1=Active, 0=Inactive',
  `add_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`rider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `rider`
--

INSERT INTO `rider` (`rider_id`, `first_name`, `last_name`, `email_id`, `password`, `mobile_number`, `language`, `device_type`, `device_token`, `status`, `add_datetime`, `update_datetime`) VALUES
(17, 'Virag', 'Shah', 'div.patel@ifuturz.com', 'test@123', '1234567890', 'English', '0', '', '1', '2015-07-29 00:00:00', '2015-08-22 12:04:30'),
(18, 'Testing', 'testing', 'sagar.saravaiya@ifuturz.com', '123456', '2546895421', '', '0', '', '1', '2015-07-30 05:07:59', '2015-07-30 05:07:59'),
(19, 'Virag', 'Shah', 'virag.shah@ifuturz.com', '123456', '8956895623', '', '0', '', '1', '2015-07-30 05:14:11', '2015-10-26 05:12:23'),
(21, 'Herry', 'Herry', 'herry@email.com', '123456', '5698754895', '', '0', '', '1', '2015-07-30 05:31:09', '2015-07-30 05:31:16'),
(22, 'Mark', 'Mark', 'mark@email.com', '123456', '5698745698', '', '0', '', '1', '2015-07-30 05:31:47', '2015-07-30 05:31:47'),
(27, 'Rajesh', 'Solanki', 'rajesh.solanki@ifuturz.com', 'Test@123', '342342432', '', '0', '', '1', '2015-08-31 11:37:15', '2015-09-11 14:59:41'),
(28, 'sadsadsa', 'sadsadsa', 'asdsad@email.com', '123456', '2356894556', 'sadasdsa', '0', '', '1', '2015-09-28 13:20:55', '2015-09-28 13:20:55'),
(29, 'sadasd', 'sadasdas', 're@email.com', '123456', '2356894578', 'sadsada', '0', '', '1', '2015-09-28 13:27:21', '2015-09-28 13:27:21'),
(30, 'Anju', 'Sharma', 'anju@hirecarsydneycity.com.au', 'test@123', '1300665711', 'English', '0', '', '1', '2015-09-29 04:28:23', '2015-09-29 04:28:23'),
(31, 'Anju', 'Nyaupane Sharma', 'anjou143@gmail.com', 'anju100', '000', '', '0', '', '1', '2015-10-02 05:44:59', '2015-10-02 05:46:08'),
(38, 'Php', 'RedStorm', 'phpredstorm@gmail.com', '', '5689564562', '', '0', '', '1', '2015-10-13 06:25:23', '2015-10-13 06:37:42'),
(41, 'undefined', 'undefined', 'undefined', '', '', '', '0', '', '1', '2015-10-20 04:39:23', '2015-10-20 04:39:23'),
(42, 'HelloIt''s', 'Sh', 'anjou1984@gmail.com', '', '', '', '0', '', '1', '2015-10-26 01:07:31', '2015-10-26 01:07:31'),
(43, 'Virag', 'Shah', 'virag.shah@email.com', '123564', '5689457812', '', '0', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ride_details`
--

CREATE TABLE IF NOT EXISTS `ride_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rider_id` int(11) NOT NULL COMMENT 'Maintain rider id from Rider Table',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `special_requirement` varchar(1000) NOT NULL,
  `no_of_passenger` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `pickup_location` text NOT NULL,
  `pickup_city` varchar(255) NOT NULL,
  `pickup_postal` varchar(10) NOT NULL,
  `destination_location` text NOT NULL,
  `destination_city` varchar(255) NOT NULL,
  `destination_postal` varchar(10) NOT NULL,
  `pickup_datetime` datetime NOT NULL,
  `car_category_id` int(11) NOT NULL COMMENT 'Maintain car category id from Car Category Table',
  `event_id` int(11) NOT NULL,
  `estimated_distance` float NOT NULL,
  `estimated_fare` float NOT NULL,
  `actual_distance` float NOT NULL,
  `actual_fare` float NOT NULL,
  `pay_by_deposit` enum('0','1') NOT NULL,
  `payment_id` int(11) NOT NULL COMMENT 'Maintain payment id from payment information Table',
  `ride_request_datetime` datetime NOT NULL COMMENT 'Maintain ride requested Date & Time',
  `driver_assign_datetime` datetime NOT NULL COMMENT 'Maintain driver assign requested Date & Time',
  `ride_confirm_datetime` datetime NOT NULL COMMENT 'Maintain driver confirmation for assign ride requested Date & Time',
  `ride_start_datetime` datetime NOT NULL COMMENT 'Maintain ride start Date & Time',
  `ride_finish_datetime` datetime NOT NULL COMMENT 'Maintain ride finish Date & Time',
  `ride_status` enum('0','1','2','3','4','5','6') NOT NULL COMMENT '0 => Waiting for Confirmaition 1=>Assign to driver 2 => Confirm by driver 3 => Ride Started 4 => Ride Finish 5 => Rejected 6 => Cancel',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

--
-- Dumping data for table `ride_details`
--

INSERT INTO `ride_details` (`id`, `rider_id`, `name`, `email`, `phone`, `special_requirement`, `no_of_passenger`, `driver_id`, `pickup_location`, `pickup_city`, `pickup_postal`, `destination_location`, `destination_city`, `destination_postal`, `pickup_datetime`, `car_category_id`, `event_id`, `estimated_distance`, `estimated_fare`, `actual_distance`, `actual_fare`, `pay_by_deposit`, `payment_id`, `ride_request_datetime`, `driver_assign_datetime`, `ride_confirm_datetime`, `ride_start_datetime`, `ride_finish_datetime`, `ride_status`) VALUES
(26, 18, '', '', '', '', 0, 23, 'Sydney Airport', 'Sydney NSW', '2020', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '2000', '2015-09-03 00:30:00', 0, 3, 11.7, 167, 11.7, 167, '0', 1, '2015-09-02 09:00:25', '2015-09-04 10:49:40', '2015-09-04 11:48:15', '2015-09-04 11:03:16', '2015-09-04 11:09:16', '4'),
(27, 0, 'Will Smith', 'smith.will@email.com', '5689421569', '', 0, 0, 'Sydney Airport', 'Sydney NSW', '2020', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '2000', '2015-09-03 04:45:00', 0, 3, 11.7, 167, 11.7, 167, '0', 0, '2015-09-02 09:45:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2'),
(28, 0, 'Will Smith', 'smith.will@email.com', '5689421569', 'BMW', 0, 0, 'Sydney Airport', 'Sydney NSW', '2020', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '2000', '2015-09-11 01:00:00', 0, 0, 11.7, 31.7, 11.7, 31.7, '0', 1, '2015-09-04 11:31:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2'),
(29, 0, 'Will Smith', 'smith.will@email.com', '5689421569', '', 0, 0, '525 New Canterbury Rd', 'Dulwich Hill', '2203', '601 parramatta road', 'leichhardt', '2040', '2015-09-10 15:00:00', 0, 0, 2.8, 22.8, 2.8, 22.8, '0', 1, '2015-09-09 11:29:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(31, 0, 'Will Smith', 'smith.will@email.com', '5689421569', 'Jaguar', 0, 0, '525 New Canterbury Rd', 'Dulwich Hill', '2203', '601 parramatta road', 'Sydney NSW', '2000', '2015-09-10 04:00:00', 0, 0, 2.8, 22.8, 2.8, 22.8, '0', 0, '2015-09-09 11:47:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(32, 18, '', '', '', '', 0, 0, 'Sydney Airport', 'Sydney NSW', '2020', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '2000', '2015-09-21 18:30:00', 0, 2, 11.7, 49.25, 11.7, 49.25, '0', 2, '2015-09-14 08:26:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(33, 0, 'Anju Nyaupane Sharma', 'anjou143@gmail.com', '+61430412888', 'baby sit required', 0, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '601 parramatta road', 'leichhardt', '2204', '2015-09-17 15:00:00', 0, 0, 2.9, 27.25, 2.9, 27.25, '0', 0, '2015-09-17 04:33:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(34, 18, '', '', '', '', 0, 0, '525 New Canterbury Rd', 'Sydney NSW', '', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '', '2015-09-24 00:30:00', 0, 0, 11.4, 47.5, 11.4, 47.5, '0', 2, '2015-09-23 16:09:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(35, 19, '', '', '', '', 0, 0, 'Sydney Airport', 'Sydney NSW', '2020', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '2000', '2015-10-03 00:30:00', 0, 3, 11.7, 47.5, 11.7, 47.5, '0', 2, '2015-09-26 05:11:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '3'),
(36, 19, '', '', '', '', 0, 0, 'Sydney Airport', 'Sydney NSW', '', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '', '2015-09-27 01:15:00', 0, 3, 11.7, 47.5, 11.7, 47.5, '0', 0, '2015-09-26 05:12:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(37, 19, '', '', '', '', 0, 0, 'Sydney Airport', 'Sydney NSW', '', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '', '2015-09-27 01:15:00', 0, 3, 11.7, 47.5, 11.7, 47.5, '0', 1, '2015-09-26 05:16:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(38, 19, '', '', '', '', 0, 0, 'Sydney Airport', 'Sydney NSW', '', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '', '2015-09-27 01:15:00', 0, 3, 11.7, 47.5, 11.7, 47.5, '0', 2, '2015-09-26 05:18:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(39, 19, '', '', '', '', 0, 0, 'Sydney Airport', 'Dulwich Hill', '', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '', '2015-09-27 03:30:00', 0, 0, 17.5, 62.5, 17.5, 62.5, '0', 1, '2015-09-26 07:04:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(41, 0, 'Virag N Shah', 'virag.shah@ifuturz.com', '5689748596', '', 0, 0, 'Sydney Airport', 'Sydney NSW', '2020', 'Sydney Opera House, Bennelong Point', 'Sydney NSW', '2040', '2015-10-03 00:30:00', 0, 4, 24, 44, 24, 44, '0', 0, '2015-09-26 08:09:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(42, 0, 'Anju Sharma', 'anjou143@gmail.com', '+61430412888', 'baby sit required', 1, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '601 parramatta road', 'leichhardt', '2040', '2015-09-28 12:00:00', 1, 0, 2.9, 22, 2.9, 22, '0', 0, '2015-09-28 00:37:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(43, 0, 'Anju Sharma', 'anjou143@gmail.com', '+61430412888', 'baby sit required', 1, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '601 parramatta road', 'leichhardt', '2040', '2015-09-28 12:00:00', 1, 0, 2.9, 22, 2.9, 22, '0', 0, '2015-09-28 00:38:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(44, 0, 'Anju Sharma', 'anjou143@gmail.com', '+61430412888', 'baby sit required', 1, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '601 parramatta road', 'leichhardt', '2040', '2015-09-28 12:00:00', 1, 0, 2.9, 22, 2.9, 22, '0', 0, '2015-09-28 00:38:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(45, 0, 'Anju Sharma', 'anjou143@gmail.com', '+61430412888', 'baby sit required', 1, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '601 parramatta road', 'leichhardt', '2040', '2015-09-28 12:00:00', 1, 0, 2.9, 22, 2.9, 22, '0', 0, '2015-09-28 00:38:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(46, 17, '', '', '', '', 2, 0, '525 New Canterbury Rd', 'Sydney NSW', '2203', '601 parramatta road', 'Sydney NSW', '2000', '2015-09-30 00:30:00', 0, 2, 2.8, 22, 2.8, 22, '0', 1, '2015-09-29 06:20:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(47, 19, '', '', '', '', 5, 0, '525 New Canterbury Rd', 'Sydney NSW', '2020', '601 parramatta road', 'Sydney NSW', '2040', '2015-09-30 02:00:00', 4, 3, 2.8, 22, 2.8, 22, '0', 2, '2015-09-29 12:52:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(48, 19, '', '', '', '', 5, 0, '525 New Canterbury Rd', 'Sydney NSW', '2020', '601 parramatta road', 'Sydney NSW', '2040', '2015-09-30 02:00:00', 2, 3, 2.8, 22, 2.8, 22, '0', 2, '2015-09-29 12:56:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(49, 19, '', '', '', '', 5, 0, '525 New Canterbury Rd', 'Sydney NSW', '2020', '601 parramatta road', 'Sydney NSW', '2040', '2015-09-30 02:00:00', 3, 3, 2.8, 22, 2.8, 22, '0', 1, '2015-09-29 12:58:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(50, 19, '', '', '', '', 5, 0, '525 New Canterbury Rd', 'Sydney NSW', '2020', '601 parramatta road', 'Sydney NSW', '2040', '2015-09-30 02:00:00', 2, 3, 2.8, 22, 2.8, 22, '0', 0, '2015-09-29 12:59:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(51, 0, 'Anju Sharma', 'anjou143@gmail.com', '+61430412888', '', 1, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '100 george street', 'sydney', '2000', '2015-10-02 15:45:00', 8, 0, 10.8, 47, 10.8, 47, '0', 0, '2015-10-02 05:24:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(52, 0, 'Anju Sharma', 'anjou143@gmail.com', '+61430412888', '', 1, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '100 george street', 'sydney', '2000', '2015-10-02 15:45:00', 8, 0, 10.8, 47, 10.8, 47, '0', 0, '2015-10-02 05:25:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(53, 0, 'Anju Sharma', 'anjou143@gmail.com', '+61430412888', '', 1, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '100 george street', 'sydney', '2000', '2015-10-02 15:45:00', 8, 0, 10.8, 47, 10.8, 47, '0', 0, '2015-10-02 05:25:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(54, 0, 'Anju Sharma', 'anjou143@gmail.com', '+61430412888', '', 1, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '100 george street', 'sydney', '2000', '2015-10-02 15:45:00', 8, 0, 10.8, 47, 10.8, 47, '0', 0, '2015-10-02 05:25:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(55, 0, 'test', 'anjou143@gmail.com', '+61430412888', '', 4, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '601 parramatta road', 'leichhardt', '2040', '2015-10-02 16:15:00', 10, 0, 2.9, 27.25, 2.9, 27.25, '0', 0, '2015-10-02 05:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(56, 0, 'test', 'anjou143@gmail.com', '+61430412888', '', 4, 0, '8/525 New Canterbury Road', 'Dulwich Hill', '2203', '601 parramatta road', 'leichhardt', '2040', '2015-10-02 16:15:00', 10, 0, 2.9, 27.25, 2.9, 27.25, '0', 0, '2015-10-02 05:33:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(58, 19, '', '', '', '', 0, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-06 03:30:00', 10, 3, 8.6, 41.5, 8.6, 41.5, '0', 0, '2015-10-05 08:11:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(59, 19, '', '', '', '', 0, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-06 00:45:00', 10, 4, 8.6, 41.5, 8.6, 41.5, '0', 0, '2015-10-05 08:17:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(60, 19, '', '', '', '', 6, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-06 00:30:00', 10, 1, 8.6, 41.5, 8.6, 41.5, '0', 0, '2015-10-05 08:27:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(61, 19, '', '', '', '', 0, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-06 00:15:00', 10, 2, 8.6, 41.5, 8.6, 41.5, '0', 0, '2015-10-05 08:28:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(62, 0, 'test', 'anju@hirecarsydneycity.com.au', '048000', 'ebrstfbfg', 6, 0, '700', 'george street', '2000', '601 parramatta road', 'leichhardt', '2040', '2015-10-14 16:15:00', 11, 2, 8.2, 40.5, 8.2, 40.5, '0', 0, '2015-10-14 04:57:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(68, 19, '', '', '', '', 3, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-19 15:45:00', 52, 2, 8.6, 41.5, 8.6, 41.5, '1', 1, '2015-10-19 05:24:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(69, 19, '', '', '', '', 5, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-20 00:00:00', 51, 3, 8.6, 41.5, 8.6, 41.5, '0', 0, '2015-10-19 08:56:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(70, 19, '', '', '', '', 5, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-20 00:00:00', 51, 3, 8.6, 41.5, 8.6, 41.5, '0', 0, '2015-10-19 09:01:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(71, 19, '', '', '', '', 5, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-20 00:30:00', 46, 4, 8.6, 41.5, 8.6, 41.5, '1', 0, '2015-10-19 10:08:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(72, 38, '', '', '', '', 2, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-21 00:00:00', 52, 1, 8.6, 41.5, 8.6, 41.5, '1', 0, '2015-10-20 10:05:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(73, 38, '', '', '', '', 0, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-21 01:00:00', 42, 3, 8.6, 41.5, 8.6, 41.5, '0', 0, '2015-10-20 10:07:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(74, 42, '', '', '', 'wine', 0, 0, '471 great western highway', 'pendle hill', '2145', '601 parramatta road', 'leichhardt', '2040', '2015-10-26 01:15:00', 52, 0, 22.4, 76, 22.4, 76, '1', 0, '2015-10-26 01:28:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(75, 38, '', '', '', '', 5, 0, 'Sydney Airport', 'Sydney', '', 'Sydney Cricket & Sports Ground Trust', ', Moore Park Road, Moore Park, New South Wales, Australia', '', '2015-10-26 16:00:00', 52, 3, 8.6, 41.5, 8.6, 41.5, '0', 0, '2015-10-26 05:47:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_paypal_email_id` varchar(255) NOT NULL,
  `setting_paypal_api_username` varchar(255) NOT NULL,
  `setting_paypal_password` varchar(255) NOT NULL,
  `setting_paypal_signature` varchar(255) NOT NULL,
  `setting_paypal_mode` enum('test','live') NOT NULL DEFAULT 'test',
  `setting_paypal_email_id_live` varchar(255) NOT NULL,
  `setting_paypal_api_username_live` varchar(255) NOT NULL,
  `setting_paypal_password_live` varchar(255) NOT NULL,
  `setting_paypal_signature_live` varchar(255) NOT NULL,
  `setting_eway_mode` enum('test','live') NOT NULL,
  `setting_eway_api_key` varchar(255) NOT NULL,
  `setting_eway_password` varchar(255) NOT NULL,
  `setting_eway_api_key_live` varchar(255) NOT NULL,
  `setting_eway_password_live` varchar(255) NOT NULL,
  `setting_bank` varchar(255) NOT NULL,
  `setting_account_name` varchar(255) NOT NULL,
  `setting_bsb` varchar(255) NOT NULL,
  `setting_account_number` varchar(255) NOT NULL,
  `setting_abn` varchar(255) NOT NULL,
  `min_fare` float NOT NULL,
  `base_fare` float NOT NULL,
  `per_minute_rate` float NOT NULL,
  `per_km_rate` float NOT NULL,
  `driver_rent` float NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`setting_id`, `setting_paypal_email_id`, `setting_paypal_api_username`, `setting_paypal_password`, `setting_paypal_signature`, `setting_paypal_mode`, `setting_paypal_email_id_live`, `setting_paypal_api_username_live`, `setting_paypal_password_live`, `setting_paypal_signature_live`, `setting_eway_mode`, `setting_eway_api_key`, `setting_eway_password`, `setting_eway_api_key_live`, `setting_eway_password_live`, `setting_bank`, `setting_account_name`, `setting_bsb`, `setting_account_number`, `setting_abn`, `min_fare`, `base_fare`, `per_minute_rate`, `per_km_rate`, `driver_rent`, `update_datetime`) VALUES
(1, 'admin-facilitator@hirecarsydneycity.com.au', 'admin-facilitator_api1.hirecarsydneycity.com.au', 'UNGM4GXJLGMY4T26', 'AFcWxV21C7fd0v3bYYYRCpSSRl31Are2au-thVm1lW2of1OsDWchVfmx', 'live', 'admin@hirecarsydneycity.com.au', 'jitendra.prajapati-facilitator_api1.ifuturz.com', '13849323321', 'AjrLgPvDvvgCrufGFJbdVUCOR1gHAtrXmFfsYxngP-fB8u6hOjzbOHD3', 'live', '60CF3CmF2abuuI9mMLUjnze7pbcb7fQvZT3ne5/a9zonYfsPNsyfe5gUG/LtboBja1LnRL', 'UkIh5kq9', '60CF3CmF2abuuI9mMLUjnze7pbcb7fQvZT3ne5/a9zonYfsPNsyfe5gUG/LtboBja1LnRL', 'UkIh5kq9', 'ANZ', 'Hire Car Sydney City', '012205', '457422035', '42607025294', 0, 0, 0, 0, 30, '2015-10-20 04:21:57');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
