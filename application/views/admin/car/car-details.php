<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-street-view"></i> Car Management</a></li>
            <li class="active">Car Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Car Details</h3>
            </div>
            <div class="box-body">

                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="box-body">

                        <div class="box">
                            <div class="box-header with-border">
                                <b>Car Images</b>
                            </div>
                            <div class="box-body">
                                <div id="images">
                                    <style>
                                        .delete-image {
                                            padding: 3px;
                                            position: absolute;
                                            left: 15px;
                                            top: 0;
                                        }
                                    </style>
                                    <?php $i = 0; ?>
                                    <?php if (count($car_images) > 0): ?>
                                        <?php foreach ($car_images as $image) : ?>
                                            <div class="col-md-3" id="imgBox_<?= $i ?>">
                                                <img src="<?= base_url('assets/uploads/Car/' . $image->car_id . '/' . $image->image_name) ?>" style="height: 150px" id="previewName_<?= $i ?>" class="img img-thumbnail carimage" />
                                                <?php if ($action == "edit"): ?>
                                                <a href="javascript:void(0)" onclick="deleteImage('<?= base_url('admin/car/delete_image/' . $image->car_id . '/' . $image->image_id . '/' . $image->image_name) ?>', 'imgBox_<?= $i ?>')" title="Delete Image" class="delete-image bg-gray text-blue "><i class="fa fa-2x fa-times"></i></a>
                                                <?php endif; ?>
                                            </div>
                                            <?php $i++;
                                        endforeach;
                                        ?>
                                    <?php else: ?>
                                    <?php endif; ?>
                                </div>
                                <?php if ($action == "edit"): ?>
                                    <div class="col-lg-3">
                                        <input type="hidden" id="total-image" value="<?= $i ?>" />
                                        <a id="add-more" href="javascript:void(0);" class="btn btn-primary">
                                            <i class="fa fa-plus"></i> Add New Image
                                        </a>

                                        <script>
                                            $("#add-more").click(function(e) {
                                                var totalvalue = $("#total-image").val();

                                                var previewName = 'imgPreview_' + totalvalue;
                                                var previewName1 = "'" + previewName + "'";
                                                var imgBox = "'imgBox_" + totalvalue + "'";

                                                var data = '<div class="col-md-3" id="imgBox_' + totalvalue + '"><img src="<?= base_url("assets/admin/images/no-image.png") ?>" id="' + previewName + '" style="height: 150px" class="img img-thumbnail carimage" />';
                                                data += '<a href="javascript:void(0)" onclick="deleteImage(0, ' + imgBox + ')" title="Delete Image" class="delete-image bg-gray text-blue"><i class="fa fa-2x fa-times"></i></a>';
                                                data += '<div class="form-group"><input type="file" class="form-control" onchange="previewImage(this, ' + previewName1 + ')" name="image[]" accept="image/*" /></div>';

                                                $("#images").append(data);
                                                $("#total-image").attr('value', parseInt(totalvalue) + 1);
                                            });
                                        </script>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="city" class="col-sm-2 control-label">Select City:</label>
                            <div class="col-sm-4">
                                <select name="city" class="form-control select2" <?= @$disable ?>>
                                    <option value="0">Select city</option>
                                    <?php foreach ($city as $val) : ?>
                                        <option <?php if($car_details->city_id == $val->city_id)echo "selected"; ?> value="<?= $val->city_id ?>"><?= $val->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CATEGORY"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="category" class="col-sm-2 control-label">Select Category:</label>
                            <div class="col-sm-4">
                                <select name="category" class="form-control select2" <?= @$disable ?>>
                                    <option value="0">Select category</option>
                                    <?php foreach ($category as $cat) : ?>
                                        <option <?php if($car_details->category_id == $cat->category_id)echo "selected"; ?> value="<?= $cat->category_id ?>"><?= $cat->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CATEGORY"></p>
                        </div>

                        <div class="form-group">
                            <label for="model_name" class="col-sm-2 control-label">Model Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="model_name" id="model_name" value="<?= @$car_details->model_name ?>" <?= @$disable ?> placeholder="Model Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MODEL_NAME"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="model_name" class="col-sm-2 control-label">Availability:</label>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" name="is_available" value="1" checked="checked"/> Available</label>
                                <label class="radio-inline">
                                    <input type="radio" name="is_available" value="0" <?php if(@$car_details->is_available == 0){ echo "checked"; } ?> /> Not Available</label>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MODEL_NAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="car_number" class="col-sm-2 control-label">Car Number:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="car_number" id="car_number" value="<?= @$car_details->number ?>" <?= @$disable ?> placeholder="Car Number">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_NUMBER"></p>
                        </div>
                        
                          <div class="form-group">
                            <label for="min_fate" class="col-sm-2 control-label">Base Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="base_fare" id="base_fare" value="<?= @$car_details->base_fare ?>" <?= @$disable ?> placeholder="Base Fare">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_BASE_FAR"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="min_fate" class="col-sm-2 control-label">Minimum Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="min_fare" id="min_fare" value="<?= @$car_details->min_fare ?>" <?= @$disable ?> placeholder="Minimum Fare">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MIN_FAR"></p>
                        </div>


                        <div class="form-group">
                            <label for="per_km_fare" class="col-sm-2 control-label">Per Kilometer Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="per_km_fare" id="per_km_fare" value="<?= @$car_details->per_km_rate ?>" <?= @$disable ?> placeholder="Per Kilometer Fare">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="per_min_fare" class="col-sm-2 control-label">Per Minute Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="per_min_fare" id="per_min_fare" value="<?= @$car_details->per_minute_rate ?>" <?= @$disable ?> id="per_min_fare" placeholder="Per Minute Fare">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="driver_rent" class="col-sm-2 control-label">Driver Rent:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="driver_rent" id="driver_rent" value="<?= @$car_details->driver_rent ?>" <?= @$disable ?> placeholder="Driver Rent">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="vehicle_registration" class="col-sm-2 control-label">Vehicle Registration:</label>
                            <div class="col-sm-4">
                                <input type="file" class="form-control" name="vehicle_registration" id="vehicle_registration"  <?= @$disable ?> />
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME"></p>
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                            <?php if ($action == "edit"): ?>
                                <button type="submit" class="btn btn-success" onclick="return validateAddCar();"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                            <?php endif; ?>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/car/view') ?>'"><i class="fa fa-fw fa-angle-left"></i> Cancel</button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->

                </form>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function deleteImage(URL, box)
    {
        if (confirm('Are you sure want to delete?'))
        {
            if (URL == 0)
            {
                $("#" + box).remove();
            }
            else
            {
                $.ajax({
                    type: 'get',
                    url: URL,
                    beforeSend: function() {

                    },
                    success: function(data) {
                        $("#" + box).remove();
                    }
                });
            }
        }
    }
</script>
<script>
    function validateEditRider()
    {
        var err_flag = false;
        //category, model_name, car_number, min_fate, per_km_fare, per_min_fare, driver_rent, vehicle_registration

        // Car Category Validation
        var category = document.getElementById("category").value.trim();
        if (category == 0) {
            document.getElementById("ERR_CATEGORY").innerHTML = "Please Select Category.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_CATEGORY").innerHTML = "";
        }

        // Car Category Validation
        var name = document.getElementById("name").value.trim();
        if (name == 0) {
            document.getElementById("ERR_MODEL_NAME").innerHTML = "Please Enter Car Model Name.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_MODEL_NAME").innerHTML = "";
        }
        //ERR_NUMBER
        /*// Car Number Validation
        var name = document.getElementById("name").value.trim();
        if (name == 0) {
            document.getElementById("ERR_MODEL_NAME").innerHTML = "Please Enter Car Model Name.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_MODEL_NAME").innerHTML = "";
        }*/

//        var re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//
//        // Email Id Validation
//        var email = document.getElementById("email").value.trim();
//        if (email == "") {
//            document.getElementById("ERR_EMAIL").innerHTML = "Please Enter Email Id.";
//            err_flag = true;
//        }
//        else if (!re_email.test(email)) {
//            document.getElementById("ERR_EMAIL").innerHTML = "Please Enter Valid Email Id.";
//        }
//        else {
//            document.getElementById("ERR_EMAIL").innerHTML = "";
//        }
//
//        // Firstname Validation
//        var fname = document.getElementById("fname").value.trim();
//        if (fname == "") {
//            document.getElementById("ERR_FNAME").innerHTML = "Please Enter First Name.";
//            err_flag = true;
//        }
//        else {
//            document.getElementById("ERR_FNAME").innerHTML = "";
//        }
//
//        // Lastname Validation
//        var lname = document.getElementById("lname").value.trim();
//        if (lname == "") {
//            document.getElementById("ERR_LNAME").innerHTML = "Please Enter Last Name.";
//            err_flag = true;
//        }
//        else {
//            document.getElementById("ERR_FNAME").innerHTML = "";
//        }
//
//        // Mobile Number Validation
//        var mobile = document.getElementById("mobile").value.trim();
//        if (mobile == "") {
//            document.getElementById("ERR_MOBILE").innerHTML = "Please Enter Mobile Number.";
//            err_flag = true;
//        }
//        else {
//            document.getElementById("ERR_MOBILE").innerHTML = "";
//        }

        if (err_flag == true)
            return false;
        else
            return true;
    }
</script>
