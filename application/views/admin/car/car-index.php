<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-car"></i> Car Management</a></li>
            <li class="active">View Car</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <?php /*?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Search</h3>
            </div>
            <div class="box-body">
                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <label for="search_category">Select Category:</label>
                        <select name="search_category" id="search_category" class="form-control select2">
                                <option value="0">Select category</option>
                                <?php foreach ($category as $cat) : ?>
                                    <option value="<?= $cat->category_id ?>"><?= $cat->name ?></option>
                                <?php endforeach; ?>
                            </select>
                    </div>

                    <button type="submit" class="btn btn-default" onclick="return searchCarValidation()"><i class="fa fa-search"></i> Search</button>
                    <?php if (isset($_GET['search_city']) && !empty($_GET['search_city'])): ?>
                        <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/city/view') ?>'"><i class="fa fa-refresh"></i> Reset</button>
                    <?php endif; ?>
                    <span class="text-red" id="ERR_SEARCH"></span>
                </form>
            </div><!-- /.box-body -->
        </div>
        <?php */?>
        
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Car</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Name</th>
                                <th>Category</th>
                                <?php /*?><th>City</th>
                                <th>Car Number</th><?php */?>
                                <th>Base Fare</th>
                                <th>Minimum Fare</th>
                                <th>Fare Per Km.</th>
                                <th>Fare Per Minute</th>
                                <?php /*?><th>Driver Rent</th><?php */?>
                                <th>Availability</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($car_details as $car):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $car->model_name ?></td>
                                    <td><?= $car->category_name ?></td>
                                    <?php /*?><td><?= $car->city ?></td>
                                    <td><?= $car->number ?></td><?php */?>
                                    <td>$ <?= $car->base_fare ?></td>
                                    <td>$ <?= $car->min_fare ?></td>
                                    <td>$ <?= $car->per_km_rate ?></td>
                                    <td>$ <?= $car->per_minute_rate ?></td>
                                    <?php /*?><td><?= $car->driver_rent ?></td><?php */?>
                                    <td><?= ($car->is_available == 1) ? "<span class='label label-success'>Available</span>" : "<span class='label label-danger'>Not Available</span>"; ?></td>
                                    <td>
                                        <?php if ($car->status == 1): ?>
                                            <input type="checkbox" checked onchange="return chageStatus('<?= base_url('admin/car/changestatus/' . $car->car_id) ?>', 'status_<?= $car->car_id ?>', 0)" id="status_<?= $car->car_id ?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php else: ?>
                                            <input type="checkbox" onchange="return chageStatus('<?= base_url('admin/car/changestatus/' . $car->car_id) ?>', 'status_<?= $car->car_id ?>', 1)" id="status_<?= $car->car_id ?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="<?= base_url('admin/car/details/' . $car->car_id) ?>" class="btn btn-info" title="View">
                                                <i class="fa fa-fw fa-file-text-o"></i></a>
                                            <a href="<?= base_url('admin/car/edit/' . $car->car_id) ?>" class="btn btn-warning" title="Edit">
                                                <i class="fa fa-fw fa-edit"></i></a>
                                            <a href="<?= base_url('admin/car/delete/' . $car->car_id) ?>" onclick="return deleteConfirm();" class="btn btn-danger" title="Delete">
                                                <i class="fa fa-fw fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function searchCityValidation()
    {
        var search_city = document.getElementById("search_city").value.trim();

        if (search_city == "")
        {
            document.getElementById("ERR_SEARCH").innerHTML = "Please Enter Keyword for Search.";
            return false;
        }
        else {
            return true;
        }
    }
</script>