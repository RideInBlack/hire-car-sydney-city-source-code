<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-car"></i> Car Management</a></li>
            <li class="active">Add Car</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add Car</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="box-body">

                        <div class="box">
                            <div class="box-header">
                                <b>Car Images</b>
                            </div>
                            <div class="box-body">
                                <div id="images">
                                    <style>
                                        .delete-image {
                                            padding: 3px;
                                            position: absolute;
                                            left: 15px;
                                            top: 0;
                                        }
                                    </style>
                                </div>

                                <div class="col-lg-3">
                                    <input type="hidden" id="total-image" value="0" />
                                    <a id="add-more" href="javascript:void(0);" class="btn btn-primary">
                                        <i class="fa fa-plus"></i> Add Image
                                    </a>

                                    <script>
                                        $("#add-more").click(function(e) {
                                            var totalvalue = $("#total-image").val();

                                            var previewName = 'imgPreview_' + totalvalue;
                                            var previewName1 = "'" + previewName + "'";
                                            var imgBox = "'imgBox_" + totalvalue + "'";

                                            var data = '<div class="col-md-3" id="imgBox_' + totalvalue + '"><img src="<?= base_url("assets/admin/images/no-image.png") ?>" id="' + previewName + '" style="height: 150px" class="img img-thumbnail carimage" />';
                                            data += '<a href="javascript:void(0)" onclick="deleteImage(0, ' + imgBox + ')" title="Delete Image" class="delete-image bg-gray text-blue"><i class="fa fa-2x fa-times"></i></a>';
                                            data += '<div class="form-group"><input type="file" class="form-control" onchange="previewImage(this, ' + previewName1 + ')" name="image[]" accept="image/*" /></div>';

                                            $("#images").append(data);
                                            $("#total-image").attr('value', parseInt(totalvalue) + 1);
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="city" class="col-sm-2 control-label">Select City:</label>
                            <div class="col-sm-4">
                                <select name="city" id="city" class="form-control select2">
                                    <option value="0">Select city</option>
                                    <?php foreach ($city as $val) : ?>
                                        <option value="<?= $val->city_id ?>"><?= $val->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="category" class="col-sm-2 control-label">Select Category:</label>
                            <div class="col-sm-4">
                                <select name="category" id="category" class="form-control select2">
                                    <option value="0">Select category</option>
                                    <?php foreach ($category as $cat) : ?>
                                        <option value="<?= $cat->category_id ?>"><?= $cat->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CATEGORY"></p>
                        </div>

                        <div class="form-group">
                            <label for="model_name" class="col-sm-2 control-label">Model Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="model_name" id="model_name" placeholder="Model Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MODEL_NAME"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="model_name" class="col-sm-2 control-label">Availability:</label>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" name="is_available" value="1" checked="checked"/> Available</label>
                                <label class="radio-inline">
                                    <input type="radio" name="is_available" value="0" /> Not Available</label>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MODEL_NAME"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="car_number" class="col-sm-2 control-label">Car Number:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="car_number" id="car_number" placeholder="Car Number">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_NUMBER"></p>
                        </div>

                        <div class="form-group">
                            <label for="min_fate" class="col-sm-2 control-label">Base Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="base_fare" id="base_fare" placeholder="Base Fare">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_BASE_FAR"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="min_fate" class="col-sm-2 control-label">Minimum Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="min_fare" id="min_fare" placeholder="Minimum Fare">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MIN_FAR"></p>
                        </div>

                        <div class="form-group">
                            <label for="per_km_fare" class="col-sm-2 control-label">Per Kilometer Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="per_km_fare" id="per_km_fare" placeholder="Per Kilometer Fare">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME1"></p>
                        </div>

                        <div class="form-group">
                            <label for="per_min_fare" class="col-sm-2 control-label">Per Minute Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="per_min_fare" id="per_min_fare" placeholder="Per Minute Fare">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME2"></p>
                        </div>

                        <div class="form-group">
                            <label for="driver_rent" class="col-sm-2 control-label">Driver Rent:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="driver_rent" id="driver_rent" placeholder="Driver Rent">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME3"></p>
                        </div>

                        <div class="form-group">
                            <label for="vehicle_registration" class="col-sm-2 control-label">Vehicle Registration:</label>
                            <div class="col-sm-4">
                                <input type="file" class="form-control" name="vehicle_registration" id="vehicle_registration">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME"></p>
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success" onclick="return validateAddCar();"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/car/view') ?>'"><i class="fa fa-fw fa-angle-left"></i> Cancel</button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->

                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function deleteImage(URL, box)
    {
        if (confirm('Are you sure want to delete?'))
        {
            if (URL == 0)
            {
                $("#" + box).remove();
            }
            else
            {
                $.ajax({
                    type: 'get',
                    url: URL,
                    beforeSend: function() {

                    },
                    success: function(data) {
                        $("#" + box).remove();
                    }
                });
            }
        }
    }
</script>
<script>
    function validateAddCar()
    {	
	//var a = [0-9]*[.][0-9]+$;
        var err_flag = false;
        
        // City Validation
        var city = document.getElementById("city").value.trim();
        if (city == 0) {
            document.getElementById("ERR_CITY").innerHTML = "Please Select City.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_CITY").innerHTML = "";
        }
        
        // Category Validation
        var category = document.getElementById("category").value.trim();
        if (city == 0) {
            document.getElementById("ERR_CATEGORY").innerHTML = "Please Select Category.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_CATEGORY").innerHTML = "";
        }
        
        // Model Name Validation
        var model_name = document.getElementById("model_name").value.trim();
        if (model_name == "") {
            document.getElementById("ERR_MODEL_NAME").innerHTML = "Please Enter Model Name.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_MODEL_NAME").innerHTML = "";
        }
        
        /*// Car Number Validation
        var car_number = document.getElementById("car_number").value.trim();
        if (car_number == "") {
            document.getElementById("ERR_NUMBER").innerHTML = "Please Enter Car Number.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_NUMBER").innerHTML = "";
        }*/
		
		//base_fare
		var base_fare = document.getElementById("base_fare").value.trim();
        if (base_fare == "") {
            document.getElementById("ERR_BASE_FAR").innerHTML = "Please Enter Base Fare.";
            err_flag = true;
        }
		else if(!base_fare.match(/^\d+/))
		{
			document.getElementById("ERR_BASE_FAR").innerHTML = "Please Enter Numbers only.";
            err_flag = true;
		}
        else {
            document.getElementById("ERR_BASE_FAR").innerHTML = "";
        }
		
		
		//Minimum Fare Validation
		
		var Minimum_Fare = document.getElementById("min_fare").value.trim();
        if (Minimum_Fare == "") {
            document.getElementById("ERR_MIN_FAR").innerHTML = "Please Enter Minimum Fare.";
            err_flag = true;
        }
		else if(!Minimum_Fare.match(/^\d+/))
		{
			document.getElementById("ERR_MIN_FAR").innerHTML = "Please Enter Numbers only.";
            err_flag = true;
		}
        else {
            document.getElementById("ERR_MIN_FAR").innerHTML = "";
        }
		
		
		//Per Kilometer Fare Validation
		
		var per_km_fare = document.getElementById("per_km_fare").value.trim();
        if (per_km_fare == "") {
            document.getElementById("ERR_CITY_NAME1").innerHTML = "Please Enter Per Kilometer Fare.";
            err_flag = true;
        }
		else if(!per_km_fare.match(/^\d+/))
		{
			document.getElementById("ERR_CITY_NAME1").innerHTML = "Please Enter Numbers only.";
            err_flag = true;
		}
        else {
            document.getElementById("ERR_CITY_NAME1").innerHTML = "";
        }
		
		
		//Per Minute Fare Validation
		
		var per_min_fare = document.getElementById("per_min_fare").value.trim();
        if (per_min_fare == "") {
            document.getElementById("ERR_CITY_NAME2").innerHTML = "Please Enter Per Minute Fare.";
            err_flag = true;
        }
		else if(!per_km_fare.match(/^\d+/))
		{
			document.getElementById("ERR_CITY_NAME2").innerHTML = "Please Enter Numbers only.";
            err_flag = true;
		}
        else {
            document.getElementById("ERR_CITY_NAME2").innerHTML = "";
        }
		
		
		//Driver Rent Validation
		
		/*var driver_rent = document.getElementById("driver_rent").value.trim();
        if (driver_rent == "") {
            document.getElementById("ERR_CITY_NAME3").innerHTML = "Please Enter Per Driver Rent.";
            err_flag = true;
        }
		else if(!per_km_fare.match(/^\d+/))
		{
			document.getElementById("ERR_CITY_NAME3").innerHTML = "Please Enter Numbers only.";
            err_flag = true;
		}
        else {
            document.getElementById("ERR_CITY_NAME3").innerHTML = "";
        }*/
		
		
        
        if (err_flag == true)
            return false;
        else
            return true;
    }
</script>
