<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Admin User Management</a></li>
            <li class="active">View Admin Users</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="GET" class="form-inline" role="form">
                            <div class="form-group">
                                <label for="search_nickname">User Name:</label>
                                <input type="text" class="form-control" name="search_nickname" value="<?=@$_GET['search_nickname']?>" id="search_nickname" />
                            </div>
                            <div class="form-group">
                                <label for="pwd">Email Id:</label>
                                <input type="text" class="form-control" name="search_email" value="<?=@$_GET['search_email']?>" id="search_email" />
                            </div>
                            <button type="submit" onclick="return searchUserValidation()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                            <?php if(isset($_GET['search_nickname']) || isset($_GET['search_email'])):?>
                                <button type="button" onclick="location.href = '<?= base_url('admin/user/view') ?>'" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                            <?php endif;?>
                            <span class="text-red" id="ERR_SEARCH"></span>
                        </form>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Admin Users</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <?php if(is_super_admin()): ?>
                                <th>Status</th>
                                <th>Action</th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($user_details as $user):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $user->first_name ?></td>
                                    <td><?= $user->last_name ?></td>
                                    <td><?= $user->nick_name ?></td>
                                    <td><?= $user->email_id ?></td>
                                    <?php if(is_super_admin()): ?>
                                    <td>
                                        <?php if ($user->status == 1): ?>
                                            <input type="checkbox" checked onchange="return chageStatus('<?= base_url('admin/user/changestatus/' . $user->admin_id) ?>', 'status_<?= $user->admin_id ?>', 0)" id="status_<?= $user->admin_id ?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php else: ?>
                                            <input type="checkbox" onchange="return chageStatus('<?= base_url('admin/user/changestatus/' . $user->admin_id) ?>', 'status_<?= $user->admin_id ?>', 1)" id="status_<?= $user->admin_id ?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="<?= base_url('admin/user/details/' . $user->admin_id) ?>" class="btn btn-info" title="View">
                                                <i class="fa fa-fw fa-file-text-o"></i></a>
                                            <a href="<?= base_url('admin/user/edit/' . $user->admin_id) ?>" class="btn btn-warning" title="Edit">
                                                <i class="fa fa-fw fa-edit"></i></a>
                                            <a href="<?= base_url('admin/user/delete/' . $user->admin_id) ?>" onclick="return deleteConfirm();" class="btn btn-danger" title="Delete">
                                                <i class="fa fa-fw fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                    <?php endif; ?>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function searchUserValidation()
    {
        //search_nickname, search_email
        var search_nickname = document.getElementById("search_nickname").value.trim();
        var search_email = document.getElementById("search_email").value.trim();
        
        if(search_nickname == "" && search_email == "")
        {
            document.getElementById("ERR_SEARCH").innerHTML = "Please Enter Keyword to Search.";
            return false;
        }
        else {
            return true;
        }
    }
</script>