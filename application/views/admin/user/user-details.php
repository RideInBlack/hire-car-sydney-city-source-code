<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i>Admin User Management</a></li>
            <li class="active">Admin User Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Admin User Details</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="fname" class="col-sm-2 control-label">First Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="fname" id="fname" value="<?= $user_details->first_name ?>" <?= $disable ?> placeholder="First Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>
                        <div class="form-group">
                            <label for="lname" class="col-sm-2 control-label">Last Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="lname" id="lname" value="<?= $user_details->last_name ?>" <?= $disable ?> placeholder="Last Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_LNAME"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email Id:</label>
                            <div class="col-sm-4">
                                <input type="email" class="form-control" name="email"  id="email" value="<?= $user_details->email_id ?>" <?= $disable ?> placeholder="Email">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_EMAIL"><?=@$_POST['ERR_EMAIL']?></p>
                        </div>

                        <div class="form-group">
                            <label for="nick_name" class="col-sm-2 control-label">User Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="nick_name" id="mobile" value="<?= $user_details->nick_name ?>" <?= $disable ?> placeholder="User Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MOBILE"><?=@$_POST['ERR_NICK_NAME']?></p>
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <?php if ($action == 'edit'): ?>
                                    <button type="submit" class="btn btn-success" onclick="return validateEditRider()"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <?php endif; ?>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/user/view') ?>'">
                                    <i class="fa fa-fw fa-angle-left"></i> Back
                                </button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box body-->    
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript" src="<?=base_url('assets/admin/js')?>/userValidation.js"></script>