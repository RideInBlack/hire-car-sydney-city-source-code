<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-taxi"></i> Driver Management</a></li>
            <li class="active">Driver Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Driver Details</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="fname" class="col-sm-2 control-label">First Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="fname" id="fname" value="<?= $driver_details->first_name ?>" <?= $disable ?> placeholder="First Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>
                        <div class="form-group">
                            <label for="lname" class="col-sm-2 control-label">Last Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="lname" id="lname" value="<?= $driver_details->last_name ?>" <?= $disable ?> placeholder="Last Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_LNAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email Id:</label>
                            <div class="col-sm-4">
                                <input type="email" class="form-control" name="email" id="email" value="<?= $driver_details->email_id ?>" <?= $disable ?> placeholder="Email">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_EMAIL"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="mobile" class="col-sm-2 control-label">Mobile Number:</label>
                            <div class="col-sm-4">
                                <input type="tel" class="form-control" name="mobile" id="mobile" value="<?= $driver_details->mobile_number ?>" <?= $disable ?> placeholder="Mobile Number">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MOBILE"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="username" class="col-sm-2 control-label">Username:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="username" id="username" value="<?= $driver_details->username ?>" <?= $disable ?> placeholder="Username">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_USERNAME"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password :</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="fname" id="fname" value="<?= $driver_details->first_name ?>" <?= $disable ?> placeholder="First Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>
                        <div class="form-group">
                            <label for="companyname" class="col-sm-2 control-label">Company Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="companyname" id="companyname" value="<?= $driver_details->companyname ?>" <?= $disable ?> placeholder="Company Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_COMPANYNAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="abn" class="col-sm-2 control-label">ABN:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="abn" id="abn" value="<?= $driver_details->abn ?>" <?= $disable ?> placeholder="ABN">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_ABN"></p>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Address:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="address" id="address" value="<?= $driver_details->address ?>" <?= $disable ?> placeholder="Address">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_ADDRESS"></p>
                        </div>
                        <div class="form-group">
                            <label for="city" class="col-sm-2 control-label">City:</label>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="city" id="city" placeholder="Select City" <?= $disable ?>>
                                	<option value="">Select City</option>
                                	<?php foreach($city_list as $cities) { ?>
                                    <option value="<?=$cities->city_id;?>" <?php if($driver_details->city == $cities->city_id) { ?> selected="selected" <?php } ?>><?=$cities->name?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY"></p>
                        </div>
                        <div class="form-group">
                            <label for="zipcode" class="col-sm-2 control-label">Zip code:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="zipcode" id="zipcode" value="<?= $driver_details->zipcode ?>" <?= $disable ?> placeholder="Zip code">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_ZIPCODE"></p>
                        </div>
                        <div class="form-group">
                            <label for="invitecode" class="col-sm-2 control-label">Invite Code:</label>
                            <div class="col-sm-4">
                                <input type="tel" class="form-control" name="invitecode" id="invitecode" value="<?= $driver_details->invitecode ?>" <?= $disable ?> placeholder="Invite Code">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_INVIRECODE"></p>
                        </div>
                        
                        <?php /*?><div class="form-group">
                            <label for="mobile" class="col-sm-2 control-label">Driver's Photo:</label>
                            <div class="col-sm-4">
                                <input type="tel" class="form-control" name="mobile" id="mobile" value="<?= $driver_details->profile_image ?>" <?= $disable ?> placeholder="Mobile Number">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MOBILE"></p>
                        </div><?php */?>
                        <?php if ($action == 'edit'): ?>
                        <div class="form-group">
                            <label for="Image" class="col-sm-2 control-label">Driver's Photo:</label>
                            <div class="col-sm-4">
                            	<input type="file" id="driverimage" name="driverimage">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_DRIVERIMAGE"></p>
                        </div>
                        <?php endif; ?>
                        <?php if(!empty($driver_details->profile_image)) { 
								$driverImage = base_url()."/assets/admin/DriverImages/".$driver_details->profile_image;?>
                        <div class="form-group">
                        	<label for="Image" class="col-sm-2 control-label">
							<?php if ($action != 'edit'): ?>
                            Driver's Photo:
                            <?php endif; ?>
                            </label>
                            <div class="col-sm-4">
                            	<img alt="Driver's Image" class="img" src="<?php echo $driverImage; ?>" width="130">
                            </div>
                        </div>
                            <?php } ?>
                        
                        <div class="form-group">
                            <label for="licensedetails" class="col-sm-2 control-label">License Details:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="licensedetails" id="licensedetails" value="<?= $driver_details->licensedetails ?>" <?= $disable ?> placeholder="License Details">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_LICENSEDETAILS"></p>
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <?php if ($action == 'edit'): ?>
                                    <button type="submit" class="btn btn-success" onclick="return validateEditRider()"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <?php endif; ?>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/driver/view') ?>'">
                                    <i class="fa fa-fw fa-angle-left"></i> Back
                                </button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box body -->    
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
function validateEditRider()
{
	var err_flag = false;
	var re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	// Email Id Validation
	var email = document.getElementById("email").value.trim();
	if (email == "") {
		document.getElementById("ERR_EMAIL").innerHTML = "Please Enter Email Id.";
		err_flag = true;
	}
	else if (!re_email.test(email)) {
		document.getElementById("ERR_EMAIL").innerHTML = "Please Enter Valid Email Id.";
	}
	else {
		document.getElementById("ERR_EMAIL").innerHTML = "";
	}

	// Firstname Validation
	var fname = document.getElementById("fname").value.trim();
	if (fname == "") {
		document.getElementById("ERR_FNAME").innerHTML = "Please Enter First Name.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_FNAME").innerHTML = "";
	}

	// Lastname Validation
	var lname = document.getElementById("lname").value.trim();
	if (lname == "") {
		document.getElementById("ERR_LNAME").innerHTML = "Please Enter Last Name.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_FNAME").innerHTML = "";
	}

	// Username Validation
	var username = document.getElementById("username").value.trim();
	if (username == "") {
		document.getElementById("ERR_USERNAME").innerHTML = "Please Enter Username.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_USERNAME").innerHTML = "";
	}

	// Mobile Number Validation
	var mobile = document.getElementById("mobile").value.trim();
	if (mobile == "") {
		document.getElementById("ERR_MOBILE").innerHTML = "Please Enter Mobile Number.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_MOBILE").innerHTML = "";
	}

	if (err_flag == true)
		return false;
	else
		return true;
}
</script>