<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-sliders"></i> CMS Management</a></li>
            <li class="active">View CMS</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View CMS</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Title</th>
                                <!--<th>URL</th>-->
                                <th>Action</th>
                            </tr>
                            <?php
                            foreach ($cms_details as $cms):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $cms->title?></td>
                                    
                                   
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group">
                                           
                                            <a href="<?= base_url('admin/cms/edit/' . $cms->cms_id) ?>" class="btn btn-warning" title="Edit">
                                                <i class="fa fa-fw fa-edit"></i>
                                            </a>
                                           
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
                <?php echo $this->pagination->create_links(); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function deleteConfirm()
    {
        if (confirm('Are you sure want to delete?')) {
            return true;
        }
        return false;
    }
</script>