<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-taxi"></i> CMS Management</a></li>
            <li class="active">Edit CMS</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Edit CMS</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="fname" class="col-sm-2 control-label">Title:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="title" id="title" value="<?= $cms_details->title; ?>" placeholder="Title" disabled="disabled">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>
<!--
                        <div class="form-group">
                            <label for="lname" class="col-sm-2 control-label">URL:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="url" id="url" value="<?= $cms_details->url; ?>" placeholder="Last Name" disabled="disabled">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_LNAME"></p>
                        </div>-->

                        <div class="form-group">
                            <label for="mobile" class="col-sm-2 control-label">Content:</label>
                            <div class="col-sm-8">
                                <textarea cols="80" rows="10" name="editor1" id="editor1"><?= $cms_details->content; ?></textarea>
                            </div>
                           
                            
                        </div>
                        
                        
                       
                           <p class="col-sm-2 text-red" id=""></p>
                           <p class="col-sm-4 text-red" id="ERR_EDITOR1"></p>
                           
                            
                     
                        

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class='row'>
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success" onclick="return validateCMS();"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/cms/view') ?>'"><i class="fa fa-fw fa-angle-left"></i> Back</button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function validateCMS()
    {
        var err_flag = false;

        // Mobile Number Validation
        var content = document.getElementById("editor1").value.trim();
		
        if (CKEDITOR.instances.editor1.getData() == '' )
		{
            document.getElementById("ERR_EDITOR1").innerHTML = "Please Enter CMS Content.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_EDITOR1").innerHTML = "";
        }

        if (err_flag == true)
            return false;
        else
            return true;
    }
</script>
