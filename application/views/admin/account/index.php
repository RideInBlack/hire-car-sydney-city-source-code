<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="#"><i class="fa fa-user"></i> User Profile</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Profile</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="fname" class="col-sm-2 control-label">First Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="fname" id="fname" value="<?= $user_details->first_name ?>" <?= $disable ?> placeholder="First Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>
                        <div class="form-group">
                            <label for="lname" class="col-sm-2 control-label">Last Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="lname" id="lname" value="<?= $user_details->last_name ?>" <?= $disable ?> placeholder="Last Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_LNAME"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email Id:</label>
                            <div class="col-sm-4">
                                <input type="email" class="form-control" name="email"  id="email" value="<?= $user_details->email_id ?>" <?= $disable ?> placeholder="Email">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_EMAIL"><?=@$_POST['ERR_EMAIL']?></p>
                        </div>

                        <div class="form-group">
                            <label for="nick_name" class="col-sm-2 control-label">Nick Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="nick_name" id="mobile" value="<?= $user_details->nick_name ?>" <?= $disable ?> placeholder="Nick Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MOBILE"><?=@$_POST['ERR_NICK_NAME']?></p>
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" name="edit_profile" class="btn btn-success" onclick="return editprofileValidation()" value="Save Details" />
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box body-->    
        </div><!-- /.box -->
        
        <!-- Default box -->
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Change Password</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="oldpassword" class="col-sm-2 control-label">Old Password:</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="oldpassword" id="oldpassword" value="" placeholder="Old Password">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_OLDPASSWORD"><?=@$_POST['ERR_OLDPASSWORD']?></p>
                        </div>
                        <div class="form-group">
                            <label for="newpassword" class="col-sm-2 control-label">New Password:</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="newpassword" id="newpassword" value="" placeholder="New Password">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_NEWPASSWORD"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="newpassword2" class="col-sm-2 control-label">Confirm New Password:</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="newpassword2"  id="newpassword2" value="" placeholder="Confirm New Passwoprd">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_NEWPASSWORD2"></p>
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" name="change_password" class="btn btn-success" onclick="return changePasswordValidation();" value="Change Password" />
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box body-->    
        </div><!-- /.box -->
        
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function editprofileValidation()
    {
        var err_flag = false;
        var re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        // Email Id Validation
        var email = document.getElementById("email").value.trim();
        if (email == "") {
            document.getElementById("ERR_EMAIL").innerHTML = "Please Enter Email Id.";
            err_flag = true;
        }
        else if (!re_email.test(email)) {
            document.getElementById("ERR_EMAIL").innerHTML = "Please Enter Valid Email Id.";
        }
        else {
            document.getElementById("ERR_EMAIL").innerHTML = "";
        }

        // Firstname Validation
        var fname = document.getElementById("fname").value.trim();
        if (fname == "") {
            document.getElementById("ERR_FNAME").innerHTML = "Please Enter First Name.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_FNAME").innerHTML = "";
        }

        // Lastname Validation
        var lname = document.getElementById("lname").value.trim();
        if (lname == "") {
            document.getElementById("ERR_LNAME").innerHTML = "Please Enter Last Name.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_FNAME").innerHTML = "";
        }

        // Nick Name Validation
        var nick_name = document.getElementById("nick_name").value.trim();
        if (nick_name == "") {
            document.getElementById("ERR_NICK_NAME").innerHTML = "Please Enter Nick Name.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_MOBILE").innerHTML = "";
        }

        if (err_flag == true)
            return false;
        else
            return true;
    }
    
    function changePasswordValidation()
    {
        //oldpassword, newpassword, newpassword2
        var oldpassword = document.getElementById("oldpassword").value;
        var newpassword = document.getElementById("newpassword").value;
        var newpassword2 = document.getElementById("newpassword2").value;
        var err_flag = false;
        
        // For old password
        if (oldpassword == "") {
            document.getElementById("ERR_OLDPASSWORD").innerHTML = "Please Enter Old Password.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_OLDPASSWORD").innerHTML = "";
        }
        
        // for new password
        if (newpassword == "") {
            document.getElementById("ERR_NEWPASSWORD").innerHTML = "Please Enter New Password.";
            err_flag = true;
        }
        else {
            if(newpassword != newpassword2)
            {
                document.getElementById("ERR_NEWPASSWORD2").innerHTML = "Confirm password is not match with new password.";
                err_flag = true;
            }
            else
            {
                document.getElementById("ERR_NEWPASSWORD2").innerHTML = "";
            }
            document.getElementById("ERR_NEWPASSWORD").innerHTML = "";
        }
        
        if(err_flag == true)
            return false;
        else
            return true;
    }
</script>