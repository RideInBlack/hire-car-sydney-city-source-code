<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Comment</h4>
</div>
<div class="modal-body">
	<input type="hidden" name="commentGiftCardId" id="commentGiftCardId"  value="<?=$giftcard_id?>" />
	<textarea rows="5" cols="90" id="commentText" name="commentText"><?=@$comment?></textarea>
</div>
<div class="modal-footer">
	<span class="pull-left" id="commentAlertText"></span>
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" id="saveComment" onclick="return saveCommentFun()">Save changes</button>
</div>

<script>
function saveCommentFun() {
	var cardId  = $('#commentGiftCardId').val();
	var commentText = $('#commentText').val();
	
	if(commentText != "") {
		$.ajax({
			method: "POST",
			url: "<?=base_url('admin/giftcard/saveComment/')?>",
			data: { comment: commentText, cardId: cardId}
		})
		.done(function(response) {
			if(response == "Success") {
				$("#commentAlertText").removeClass("text-danger");
				$("#commentAlertText").addClass("text-success");
				$("#commentAlertText").html("Comment saved successfully.");
			}
			else {
				$("#commentAlertText").removeClass("text-success");
				$("#commentAlertText").addClass("text-danger");
				$('#commentAlertText').html("Sorry unable to save comment.");
			}
		});
	}
	else {
		$('#commentAlertText').html("Please enter comment text.");
		return 0;
	}
}
</script>