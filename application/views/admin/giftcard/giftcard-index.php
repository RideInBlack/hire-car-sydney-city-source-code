<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gift"></i> Gift Card Management</a></li>
            <li class="active">View Gift Cards</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
<!--        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <label for="email" class="control-label">Email Id:</label>
                                <input type="text" placeholder=".col-xs-3" class="form-control">
                            </div>
                            <div class="col-xs-4">
                                <label for="email" class="control-label">First Name:</label>
                                <input type="text" placeholder=".col-xs-4" class="form-control">
                            </div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-default" onclick="return validateAddRider();">
                                    <i class="fa fa-fw fa-search"></i> Search
                                </button>&nbsp;&nbsp;
                                <button type="reset" class="btn btn-default" onclick="return validateAddRider();">
                                    <i class="fa fa-fw fa-refresh"></i> Reset
                                </button>&nbsp;&nbsp;
                            </div>
                        </div>
                    </div> /.box-body 
                </div>
            </div>
        </div>-->
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Gift Cards</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($giftcard_details as $details):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $details->giftcard_name ?></td>
                                    <td>$<?= $details->price ?></td>
                                    <td>
                                        <?php if($details->status == 1):?>
                                        <input type="checkbox" checked onchange="return chageStatus('<?=base_url('admin/giftcard/changestatus/'.$details->giftcard_id)?>', 'status_<?=$details->giftcard_id?>', 0)" id="status_<?=$details->giftcard_id?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php else:?>
                                        <input type="checkbox" onchange="return chageStatus('<?=base_url('admin/giftcard/changestatus/'.$details->giftcard_id)?>', 'status_<?=$details->giftcard_id?>', 1)" id="status_<?=$details->giftcard_id?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group">
                                        <a href="<?= base_url('admin/giftcard/details/'.$details->giftcard_id) ?>" class="btn btn-info" title="View">
                                            <i class="fa fa-fw fa-file-text-o"></i></a>
                                        <a href="<?= base_url('admin/giftcard/edit/'.$details->giftcard_id) ?>" class="btn btn-warning" title="Edit">
                                            <i class="fa fa-fw fa-edit"></i></a>
                                        <a href="<?= base_url('admin/giftcard/delete/'.$details->giftcard_id) ?>" onclick="return deleteConfirm();" class="btn btn-danger" title="Delete">
                                            <i class="fa fa-fw fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->