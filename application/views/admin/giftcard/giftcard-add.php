<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gift"></i> Gift Card Management</a></li>
            <li class="active">Add Gift Card</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add Gift Card</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_NAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="imageFile" class="col-sm-2 control-label">Image:</label>
                            <div class="col-sm-4">
                                <input type="file" id="imageFile" name="imageFile">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_IMAGE"></p>
                        </div>

                        <div class="form-group">
                            <label for="pricepercentage" class="col-sm-2 control-label">Price Percentage:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="pricepercentage" id="pricepercentage" placeholder="Price Percentage">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_PRICEPERCENTAGE"></p>
                        </div>

                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">Price:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="price" id="price" placeholder="Price" />
                                    <span class="input-group-addon">$</span>
                                </div>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_PRICE"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="coupon_code" class="col-sm-2 control-label">Coupon Code:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="coupon_code" id="coupon_code" placeholder="Coupon Code" />
                                    <div class="input-group-btn"><button type="button" class="btn btn-default" id="btnGetCoupon"><i class="glyphicon glyphicon-repeat"></i></button></div>
                                </div>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_PRICE"></p>
                        </div>

                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success" onclick="return validateAddGiftcard();"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/giftcard/view') ?>'"><i class="fa fa-fw fa-angle-left"></i> Cancel</button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
$("#btnGetCoupon").click(function(){
    $.get("<?=base_url('admin/giftcard/generateCoupon')?>", function(data, status){
        //alert("Data: " + data + "\nStatus: " + status);
		$('#coupon_code').val(data);
    });
});
</script>
<script type="text/javascript" src="<?= base_url('assets/admin/js') ?>/giftcardValidation.js"></script>
