<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gift"></i> Gift Card Management</a></li>
            <li class="active">View Coupons (Pay via EFT)</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    	<?php /*?><div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="GET" class="form-inline" role="form">
                            <div class="form-group">
                                <label for="pwd">Rider Email Id:</label>
                                <input type="text" class="form-control" name="search_email" value="<?= @$_GET['search_email'] ?>" id="search_email" />
                            </div>
                            <button type="submit" onclick="return searchRiderValidation()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                            <?php if (isset($_GET['search_email'])): ?>
                                <button type="button" onclick="location.href = '<?= base_url('admin/giftcard/registered') ?>'" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                            <?php endif; ?>
                            <span class="text-red" id="ERR_SEARCH"></span>
                        </form>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div><?php */?>
    	
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Coupons (Pay via EFT)</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Rider Name</th>
                                 <th>Rider Email</th>
                                <th>Rider Contact</th>
                                <th>Register Date</th>
                                <th>Price</th>
                                <th>Coupon Code</th>
                                <th >Remaining Amount</th>
                                <th>Payment Method</th>
                                <th>Used Date and Time</th>
                                <th>Status</th>
                                <th  style="width: 200px;">Action</th>
                                     <th>Gift card usages</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($gift_cards as $gift_card):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?=$gift_card->first_name?> <?=$gift_card->last_name?></td>
                                     <td><?=$gift_card->email_id?></td>
                                    <td><?=$gift_card->mobile_number?></td>
                                    <td><?=date('d/m/Y, h:i A', strtotime($gift_card->generate_datetime))?></td>
                                    <td>$ <?=$gift_card->price?></td>
                                    <td><?=$gift_card->coupon_code?></td>
                                    <td>$<?=$gift_card->remaining_amount?></td>
                                    <td><?=$gift_card->payment_type?></td>
                                    <td><?=date('d/m/Y, h:i A', strtotime($gift_card->use_datetime))?></td>
                                    <td>
                                    	<?php if($gift_card->status == 1):?>
											<span class="label label-success">Active</span>
                                        <?php elseif($gift_card->status == 0):?>
                                        	<span class="label label-danger">Pending</span>
										<?php endif;?>
										
                                    </td>
                                    <td>
                                    	<a href="<?=base_url('admin/giftcard/sendReminderEmail/'.$gift_card->coupon_id)?>" class="btn btn-xs btn-warning <?=($gift_card->status == 1) ? 'disabled' : '' ?>" onclick="return confirmSend()" title="Send reminder email"><i class="fa fa-envelope"></i></a>
                                        <a href="<?=base_url('admin/giftcard/chageStatus/'.$gift_card->coupon_id.'/1')?>" class="btn btn-xs btn-success" onclick="return confirmAccept()" title="Active"><i class="fa fa-check"></i></a>
                                        <a href="<?=base_url('admin/giftcard/chageStatus/'.$gift_card->coupon_id.'/0')?>" class="btn btn-xs btn-danger" onclick="return confirmReject()" title="Inactive"><i class="fa fa-times"></i></a>
										<a data-toggle="modal" data-target="#commentModal" href="<?=base_url('admin/giftcard/getComment/'.$gift_card->coupon_id)?>" class="btn btn-xs btn-primary" onclick="" ><i class="fa fa-comment" title="Comment"></i></a>
                                       
                                    </td>
                                    <td>
                                     <a data-toggle="modal" data-target="#codeusesModal" href="<?=base_url('admin/giftcard/getCodeuses/'.$gift_card->coupon_id)?>" class="btn btn-xs btn-primary" onclick="" >View Usage</a>
                                    </td>
                                    <script>
									function confirmSend() {
										if(confirm('Are you sure wnat to send reminder email ?')) {
											return true;	
										}
										else {
											return false;
										}
									}
									function confirmAccept() {
										if(confirm('Are you sure wnat to enable this coupon ?')) {
											return true;	
										}
										else {
											return false;
										}
									}
									function confirmReject() {
										if(confirm('Are you sure want to reject this coupon ?')) {
											return true;	
										}
										else {
											return false;
										}
									}
									</script>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<!-- Comment Box Modal -->
<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
		  <?php /*?><div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Comment</h4>
		  </div>
		  
		  <div class="modal-body">
			<textarea rows="5" cols="90" id="comment" name="comment"></textarea>
		  </div>
		  
		  <div class="modal-footer">
		  	<span class="pull-left">Comment saved successfully.</span>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary" id="saveComment" onclick="return saveCommentFun()">Save changes</button>
		  </div><?php */?>
	</div>
  </div>
</div>

<!--******************** -->

<!-- Get Coupan Code uses Box Modal -->
<div class="modal fade" id="codeusesModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
		  <?php /*?><div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Comment</h4>
		  </div>
		  
		  <div class="modal-body">
			<textarea rows="5" cols="90" id="comment" name="comment"></textarea>
		  </div>
		  
		  <div class="modal-footer">
		  	<span class="pull-left">Comment saved successfully.</span>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary" id="saveComment" onclick="return saveCommentFun()">Save changes</button>
		  </div><?php */?>
	</div>
  </div>
</div>

<script>
$('#commentModal').on('hide.bs.modal', function() {
	$('#commentModal').removeData();
})
$('#codeusesModal').on('hide.bs.modal', function() {
	$('#codeusesModal').removeData();
})
</script>