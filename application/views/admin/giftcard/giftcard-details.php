<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gift"></i> Gift Card Management</a></li>
            <li class="active">Gift Card Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Gift Card Details</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="fname" class="col-sm-2 control-label">Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="name" id="name" value="<?= $giftcard_details->giftcard_name ?>" <?= $disable ?> placeholder="Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_NAME"></p>
                        </div>

                        <?php if ($action == 'edit'): ?>
                            <div class="form-group">
                                <label for="Image" class="col-sm-2 control-label">Image:</label>
                                <div class="col-sm-4">
                                    <input type="file" id="imageFile" name="imageFile" onchange="previewImage(this, 'imgPreview')">
                                </div>
                                <p class="col-sm-4 text-red" id="ERR_IMAGE"></p>
                            </div>
                        <?php endif; ?>
                        <?php
                        if (!empty($giftcard_details->image_name)) {
                            $giftcardImage = base_url() . "/assets/admin/GiftCardImages/" . $giftcard_details->image_name;
                            ?>
                            <div class="form-group">
                                <label for="Image" class="col-sm-2 control-label">
                                    <?php if ($action != 'edit'): ?>
                                        Image:
                                    <?php endif; ?>
                                </label>
                                <div class="col-sm-4">
                                    <img alt="Gift Card Image" class="img" id="imgPreview" src="<?php echo $giftcardImage; ?>" height="120">
                                </div>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <label for="pricepercentage" class="col-sm-2 control-label">Price Percentage:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="pricepercentage" id="pricepercentage" value="<?= $giftcard_details->pricepercentage ?>" <?= $disable ?> placeholder="Price Percentage">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_PRICEPERCENTAGE"></p>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">Price:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="<?= $giftcard_details->price ?>" <?= $disable ?> />
                                    <span class="input-group-addon">$</span>
                                </div>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_PRICE"></p>
                        </div>
                        
                         <div class="form-group">
                            <label for="coupon_code" class="col-sm-2 control-label">Coupon Code:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="coupon_code" id="coupon_code" value="<?= $giftcard_details->coupon_code ?>" <?= $disable ?> placeholder="Coupon Code" />
                                    <div class="input-group-btn"><button type="button" <?= $disable ?> class="btn btn-default" id="btnGetCoupon"><i class="glyphicon glyphicon-repeat"></i></button></div>
                                </div>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_COUPON"></p>
                        </div>
                        
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <?php if ($action == 'edit'): ?>
                                    <button type="submit" class="btn btn-success" onclick="return validateEditRider()"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <?php endif; ?>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/giftcard/view') ?>'">
                                    <i class="fa fa-fw fa-angle-left"></i> Back
                                </button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
$("#btnGetCoupon").click(function(){
    $.get("<?=base_url('admin/giftcard/generateCoupon')?>", function(data, status){
        //alert("Data: " + data + "\nStatus: " + status);
		$('#coupon_code').val(data);
    });
});
</script>
<script type="text/javascript" src="<?= base_url('assets/admin/js') ?>/giftcardValidation.js"></script>
