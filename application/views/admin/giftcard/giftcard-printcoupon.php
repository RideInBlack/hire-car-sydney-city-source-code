<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(document).ready(function()
{
    $("#gift").change(function(){
        var gid=$('#gift').val();
			$.ajax({
			type:'POST',	
			url:"<?php echo site_url('admin/Giftcard/pricepercentage'); ?>",
			data: $('#formdata').serialize(),//$("#gift").val(),
			success:function(data) 
			{
				//var obj = jQuery.parseJSON(data);
				//document.getElementById('dd').innerHTML=obj[''];
				$('#pricepercentage').attr('value',data);
				//$('#price').attr('value',$("#gift").val());
				//$('#coupon_code').attr('value',$("#gift").val());
			}
  			});	
    });
	$("#gift").change(function(){
        var gid=$('#gift').val();
			$.ajax({
			type:'POST',	
			url:"<?php echo site_url('admin/Giftcard/price'); ?>",
			data: $('#formdata').serialize(),//$("#gift").val(),
			success:function(data) 
			{
				$('#price').attr('value',data);
			}
  			});	
    });
	/*$("#gift").change(function(){
        var gid=$('#gift').val();
			$.ajax({
			type:'POST',	
			url:"<?php echo site_url('admin/Giftcard/coupon_code'); ?>",
			data: $('#formdata').serialize(),//$("#gift").val(),
			success:function(data) 
			{
				$('#coupon_code').attr('value',data);
			}
  			});	
    });*/
});
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gift"></i> Gift Card Management</a></li>
            <li class="active">Print Coupons</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Print Coupons</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="formdata" >
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name:</label>
                            <div class="col-sm-4">
							<div id="dd"></div>
                                <select class="form-control select2" id="gift" name="giftcard">
                                    <option>Select Gift Card</option>
                                    <?php foreach ($dift_cards as $key) : ?>
                                    <option value="<?=$key->giftcard_id	?>"><?=$key->giftcard_name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_NAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="pricepercentage" class="col-sm-2 control-label">Price Percentage:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="pricepercentage" id="pricepercentage" disabled="" placeholder="Price Percentage" value="">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_PRICEPERCENTAGE"></p>
                        </div>

                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">Price:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="price" id="price" disabled="" placeholder="Price" />
                                    <span class="input-group-addon">$</span>
                                </div>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_PRICE"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="coupon_code" class="col-sm-2 control-label">Coupon Code:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="coupon_code" id="coupon_code" placeholder="Coupon Code" />
                                    <div class="input-group-btn"><button type="button" class="btn btn-default" id="btnGetCoupon"><i class="glyphicon glyphicon-repeat"></i></button></div>
                                </div>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_PRICE"></p>
                        </div>

                         <div class="form-group">
                            <label for="coupon_code" class="col-sm-2 control-label">Rider Name:</label>
                            <div class="col-sm-4">
                                
                                    <input type="text" class="form-control" name="rider_name" id="rider_name" placeholder="Rider Name" />
                    
                               
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_rider_name"></p>
                        </div>

                          <div class="form-group">
                            <label for="coupon_code" class="col-sm-2 control-label">Address:</label>
                            <div class="col-sm-4">
                               
                                    <input type="text" class="form-control" name="Address" id="Address" placeholder="Address" />
                                   
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_Address"></p>
                        </div>

                          <div class="form-group">
                            <label for="coupon_code" class="col-sm-2 control-label">Rider Phone:</label>
                            <div class="col-sm-4">
                        
                                    <input type="text" class="form-control" name="rider_phone" id="rider_phone" placeholder="Rider Phone" />
                                
                              
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_rider_phone"></p>
                        </div>

                         <div class="form-group">
                            <label for="coupon_code" class="col-sm-2 control-label">Rider Email:</label>
                            <div class="col-sm-4">
                      
                                    <input type="text" class="form-control" name="rider_email" id="rider_email" placeholder="Rider Email" />
                                 
                         
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_rider_email"></p>
                        </div>
                         <div class="form-group">
                            <label for="coupon_code" class="col-sm-2 control-label">Admin Comment :</label>
                            <div class="col-sm-4">
                     
                                    <input type="text" class="form-control" name="admin_comment" id="admin_comment" placeholder="Admin Comment" />
                                  
                         
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_rider_comment"></p>
                        </div>

                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                               <!-- <button type="submit" class="btn btn-success " onclick="return validateAddGiftcard();"><i class="fa fa-fw fa-check"></i> Save</button>-->
                                 <button type="button" class="btn btn-success saveandprint" ><i class="fa fa-fw fa-check"></i> Save & Print</button>
                                &nbsp;&nbsp;
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/giftcard/view') ?>'"><i class="fa fa-fw fa-angle-left"></i> Cancel</button>&nbsp;&nbsp;
                                
                                
                                 
                                 
                              <!--  <button type="button" class="btn btn-success" onclick="return validateAddGiftcard();"><i class="fa fa-fw fa-check"></i> Save & Print</button>-->
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box -->
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
$("#btnGetCoupon").click(function(){
    $.get("<?=base_url('admin/giftcard/generateAdminCoupon')?>", function(data, status){
        //alert("Data: " + data + "\nStatus: " + status);
		$('#coupon_code').val(data);
    });
});


$(document).ready(function(){
	$(".saveandprint").click(function(){
		

		var gid=$('#gift').val();
		var coupon_code=$('#coupon_code').val();
        var rider_name=$('#rider_name').val();
        var Address=$('#Address').val();
        var rider_phone=$('#rider_phone').val();
        var rider_email=$('#rider_email').val();
        var admin_comment=$('#admin_comment').val();

		if(gid == "Select Gift Card" ) {
			alert('Please select gift card');
			return false;
		}
		$.ajax({
			type:'POST',	
			url:"<?php echo site_url('admin/Giftcard/makecoupanimage'); ?>",
			data: {
				gid:gid,
				coupon_code:coupon_code,
                rider_name:rider_name,
                Address:Address,
                rider_phone:rider_phone,
                rider_email:rider_email,
                admin_comment:admin_comment
				},
			success:function(data)  {
				if(data == 0) {
					alert('Coupon Code is already Genereted Before. Please Enter Diferent.'); 
					return 0;
				}
				
				$('#coupon_code').val('');
				//alert(data); return false;
				var printWindow = window.open('', '', 'height=370,width=630');
				printWindow.document.write('<html><head><title>Coupan Code</title>');
				printWindow.document.write('</head><body >');
				printWindow.document.write('<img src="'+data+'" style="display: block;  width: 600px;">');
				printWindow.document.write('</body></html>');
				
				//printWindow.location.reload();
				setTimeout(function(){printWindow.print(); printWindow.document.close(); }, 1000);
				return false;
			}
		});	
	});
});	
</script>
<script type="text/javascript" src="<?= base_url('assets/admin/js') ?>/giftcardValidation.js"></script>
