
<style>
.ribUse{
  background-color:#99FFFF ;
}
</style>
<?php //echo "<pre>"; print_r($alldetail); //die;?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Gift Card Uses</h4>
</div>
<div class="modal-body">
<table class="table">
  
    <?php  
    if(!empty($alldetail)) { ?>
    <thead>
      <tr>
        <th>Rider Id</th>
        <th>Pickup Location </th>
         <th>Destination Location   </th>
        <th>Used Amount</th>
        <th>Date/Time</th>
      </tr>
    </thead>
    <tbody>
  <?php   foreach($alldetail as $used_detail){ ?>

        <tr <?php if($used_detail->usageFrom == '1'){ ?> class="ribUse" <?php } ?> >
        <td><?php  if($used_detail->usageFrom == '1'){ echo 'R'.str_pad($used_detail->riderId, 4, "0", STR_PAD_LEFT); } ?> </td>      
        <td><?php echo $used_detail->pickup_location; ?> </td>
        
        <td> <?php echo $used_detail->destination_location ; ?></td>
        <td>$<?php echo $used_detail->amount;?></td>        
        <td><?php echo $used_detail->payment_datetime ;?></td>

      </tr>
      <?php } }else{?>
      <tr>
        <td><strong style="color:red;">No record found. </strong><hr></td>
      </tr>
      
      <?php } ?>
      
   </tbody>
  </table>
<!-- <div class=""><strong> Remaining Amount :- </strong> <?php echo $used_detail->remaining_amount; ?> </div> -->
</div>
<div class="modal-footer">
	<span class="pull-left" id="commentAlertText"></span>
	NOTE  :  Row highlighted with <b>Sky blue color</b> indicates usages from the <b>Ride In Black app</b>.<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

</div>

