<!-- Content Wrapper. Contains page content -->
<script>
function chageStatusOnlineStatus(url, ele_id, status,id)
{
     // dissable element upto ajax return
    $("#"+ele_id).prop('disabled', true);
    
    $.ajax({
        type: "POST",
        url: url,
        data: {"status": status},
        success: function(data) {
            //alert(data);
            var chStatus;
            if(status == 1)
                chStatus = 0;
            else
                chStatus = 1;
             
            // Successfully change status
            if(data == "success")
            {
                document.getElementById(ele_id).onchange = function(){chageStatusOnlineStatus(url, ele_id, chStatus,id); };
            }
            else
            {
                if(status == 1)
                    document.getElementById(ele_id).checked = true;
                else
                    document.getElementById(ele_id).checked = false;
            }
            // enable elemet 
            $("#"+ele_id).prop('disabled', false);
        }
    });
    
    return false;
}
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-gift"></i> Gift Card Management</a></li>
            <li class="active">View Printed Coupons</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    	<?php /*?><div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="GET" class="form-inline" role="form">
                            <div class="form-group">
                                <label for="pwd">Rider Email Id:</label>
                                <input type="text" class="form-control" name="search_email" value="<?= @$_GET['search_email'] ?>" id="search_email" />
                            </div>
                            <button type="submit" onclick="return searchRiderValidation()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                            <?php if (isset($_GET['search_email'])): ?>
                                <button type="button" onclick="location.href = '<?= base_url('admin/giftcard/registered') ?>'" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                            <?php endif; ?>
                            <span class="text-red" id="ERR_SEARCH"></span>
                        </form>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div><?php */?>
    	
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Printed Coupons</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Coupon Code</th>
                                <th>Price</th>
                                <th>Remaining Amount</th>
                                <th>Giftcard</th>
                                <th>Generated Date&Time</th>
                                <th>Rider</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Admin Comments</th>
                                <th>View usages</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($gift_cards as $gift_card):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?=$gift_card->coupon_code?></td>
                                    <td>$ <?=$gift_card->price?></td>
                                    <td>$<?=$gift_card->remaining_amount?></td>
                                    <td><?=$gift_card->giftcard_name?></td>
                                    <td><?=date('d/m/Y, h:i A', strtotime($gift_card->generate_datetime))?></td>
                                    <td><?=$gift_card->rider_name?></td>
                                    <td><?=$gift_card->rider_phone?></td>
                                    <td><?=$gift_card->rider_email?></td>
                                    <td><?=$gift_card->Address?></td>
                                    <td><?=$gift_card->admin_comment?></td>
                                    
                                    <td>
                                     <a data-toggle="modal" data-target="#codeusesModal" href="<?=base_url('admin/giftcard/getCodeuses/'.$gift_card->coupon_id)?>" class="btn btn-xs btn-primary" onclick="" >View Usage</a>
                                    </td>
                                     <td>
                                        <?php if($gift_card->statusPrinted == 1):?>
                                        <input type="checkbox" checked onchange="return chageStatusOnlineStatus('<?=base_url('admin/giftcard/changestatusPrintedGift/'.$gift_card->coupon_id)?>', 'status_<?=$gift_card->coupon_id?>', 0,'<?=$gift_card->coupon_id?>')" id="status_<?=$gift_card->coupon_id?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php else:?>
                                        <input type="checkbox" onchange="return chageStatusOnlineStatus('<?=base_url('admin/giftcard/changestatusPrintedGift/'.$gift_card->coupon_id)?>', 'status_<?=$gift_card->coupon_id?>', 1,'<?=$gift_card->coupon_id?>')" id="status_<?=$gift_card->coupon_id?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php endif;?>
                                    </td>
                                    
                                  

                                    <script>
									function confirmSend() {
										if(confirm('Are you sure wnat to send reminder email ?')) {
											return true;	
										}
										else {
											return false;
										}
									}
									function confirmAccept() {
										if(confirm('Are you sure wnat to enable this coupon ?')) {
											return true;	
										}
										else {
											return false;
										}
									}
									function confirmReject() {
										if(confirm('Are you sure want to reject this coupon ?')) {
											return true;	
										}
										else {
											return false;
										}
									}
									</script>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<!-- Comment Box Modal -->
<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
		  <?php /*?><div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Comment</h4>
		  </div>
		  
		  <div class="modal-body">
			<textarea rows="5" cols="90" id="comment" name="comment"></textarea>
		  </div>
		  
		  <div class="modal-footer">
		  	<span class="pull-left">Comment saved successfully.</span>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary" id="saveComment" onclick="return saveCommentFun()">Save changes</button>
		  </div><?php */?>
	</div>
  </div>
</div>

<!--******************** -->

<!-- Get Coupan Code uses Box Modal -->
<div class="modal fade" id="codeusesModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
		  <?php /*?><div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Comment</h4>
		  </div>
		  
		  <div class="modal-body">
			<textarea rows="5" cols="90" id="comment" name="comment"></textarea>
		  </div>
		  
		  <div class="modal-footer">
		  	<span class="pull-left">Comment saved successfully.</span>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary" id="saveComment" onclick="return saveCommentFun()">Save changes</button>
		  </div><?php */?>
	</div>
  </div>
</div>

<script>
$('#commentModal').on('hide.bs.modal', function() {
	$('#commentModal').removeData();
})
$('#codeusesModal').on('hide.bs.modal', function() {
	$('#codeusesModal').removeData();
})
</script>