<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-sliders"></i> Inquiry Management</a></li>
            <li class="active">View Inquiry</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View CMS</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>User Name</th>
                                <th>User Contact Number</th>
                                <th>User Email</th>
                                <th>User Pickup</th>
                                <th>User Destination</th>
                                <th>Inquiry Date & Time</th>
                                <th>Action</th>
                            </tr>
                            <?php
                            foreach ($inquirydetails as $inquiry):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $inquiry->user_name?></td>
                                    <td><?= $inquiry->user_phone ?></td>
                                    <td><?= $inquiry->user_email_id?></td>
                                    <td><?= $inquiry->pick_up_address ?></td>
                                    <td><?= $inquiry->destination_address?></td>
                                    <td><?= $inquiry->inquiry_datetime ?></td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="<?= base_url('admin/inquiry/details/'.$inquiry->inquiry_id) ?>" class="btn btn-warning" data-toggle="modal" data-target="#detailModel" title="View Details">
                                                <i class="fa fa-fw fa-file-text-o"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
                <?php echo $this->pagination->create_links(); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal fade"  id="detailModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">

     </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    function deleteConfirm()
    {
        if (confirm('Are you sure want to delete?')) {
            return true;
        }
        return false;
    }
</script>