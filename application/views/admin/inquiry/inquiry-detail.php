<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Inquiry Details</h4>
</div>
<div class="modal-body">
    <div class="form-horizontal">
        <div class="box-body">
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">User Name</label>
            <div class="col-sm-9">
                <span><?= @$inquiry->user_name?></span>
            </div>
          </div>
            
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">Contact Number</label>
            <div class="col-sm-9">
                <span><?= @$inquiry->user_phone?></span>
            </div>
          </div>
         
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">User Email</label>
            <div class="col-sm-9">
                <span><?= @$inquiry->user_email_id?></span>
            </div>
          </div> 
          
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">Pickup</label>
            <div class="col-sm-9">
                <span><?= @$inquiry->pick_up_address?></span>
            </div>
          </div>  
          
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">Destination</label>
            <div class="col-sm-9">
                <span><?= @$inquiry->destination_address?></span>
            </div>
          </div> 
            
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">Pickup Date & Time</label>
            <div class="col-sm-9">
                <span><?= @$inquiry->pick_up_datetime?></span>
            </div>
          </div>
            
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">No of Passengers</label>
            <div class="col-sm-9">
                <span><?= @$inquiry->number_of_passengers?></span>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">Occasion Type</label>
            <div class="col-sm-9">
                <span><?= @$inquiry->occasion_type?></span>
            </div>
          </div> 
            
         <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">How did you hear about us?</label>
            <div class="col-sm-9">
                <span><?= @$inquiry->how_to_know_us?></span>
            </div>
          </div> 
            
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">Trip Details / Additional Information</label>
            <div class="col-sm-9">
                <span><?= @$inquiry->trip_details?></span>
            </div>
          </div>   
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>