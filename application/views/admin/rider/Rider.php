<?php 
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Rider extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        // Check Admin login from function from (h: function)
        check_admin_login();
        
        // Load Rider Model
        $this->load->model('rider_model', 'model');
    }
    
    // View All riders List
    public function view($page = 1) 
    {
        $search_where = "";
     
	 	// Get nSearch Element and prepare query
        if(isset($_GET['search_firstname']) && !empty($_GET['search_firstname']))
        {
            $search_where = "first_name LIKE '%".$_GET['search_firstname']."%'";
        }
        if(isset($_GET['search_email']) && !empty($_GET['search_email']))
        {
           // $search_where = (where == "") ? "email_id LIKE '%".$_GET['search_emailid']."%'" : $where." AND email_id LIKE '%".$_GET['search_emailid']."%'";
			
			$search_where = "email_id LIKE '%".$_GET['search_email']."%'";
        }
	    
		
		// Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;
        
        // Fetch details from model
        $rider = $this->model->list_all($record_per_page, ($page-1)*$record_per_page, $search_where);
        
        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url().'admin/rider/view/', $this->model->count_total($search_where), $record_per_page);
        
        // Store details of rider
        $data['rider_details'] = $rider;
        $data['sr'] = (($page-1)*PER_PAGE_RECORD)+1;
        
        $page_details['title'] = "View Rider";
        
        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/rider/rider-index', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Add new rider
    public function add() 
    {
        if($this->input->post())
        {
            $fields = array("first_name" => trim($this->input->post('fname')),
                "last_name" => trim($this->input->post('lname')),
                "email_id" => trim($this->input->post('email')),
                "password" => $this->input->post('password'),
                "mobile_number" => trim($this->input->post('mobile')),
                "add_datetime" => date('Y-m-d H:i:s', time()),
                "update_datetime" => date('Y-m-d H:i:s', time())
                );
            
            $rider_id = $this->model->add($fields);
            
            if($rider_id)
            {
				$message = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
<a href='" . base_url() . "'>";

                    $message .= '<img src="http://192.168.1.102/hirecar/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';

                    $message .= "</a></div>
					<div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'>Register Successfully.</div>
					<div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
					<div style='padding-left:10px;'>
					<p>Dear " . $fields['first_name'] . " " . $fields['last_name'] . ",</p>
					<p>Your have successfully register for Hire Care Sydney City. Your login details are as below.</p>
					<p><b>Email:</b> " . $fields['email_id'] . " <br>
					<b>Password:</b> " . $fields['password'] . " <br>
					</p>
					<p>Regards,<br />
					Hire Care Sydney City Team</p>
					</div>
					</div>
					</div>";
                    $this->load->library('email');
                    $this->email->from('admin@hirecarsydneycity.com', "Admin Team");
                    $this->email->to($fields['email_id']);
                    //$this->email->cc("testcc@domainname.com");
                    $this->email->subject("Sign Up By Rider For Hire Care Sydney City");
                    $this->email->message($message);

                    $this->email->send();
				
                $this->session->set_flashdata('SUCC_MESSAGE', 'Successfully Created New Rider');
                redirect('admin/rider/view');
            }
            else
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to create new Rider.');
            }
        }
        
        $page_details['title'] = "Add Rider";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/rider/rider-add');
        $this->load->view('admin/template/footer');
    }
    
    // View rider details
    public function details($id)
    {
        $data['disable'] = "readonly disabled";
        
        $data['rider_details'] = $this->model->fetch_details($id);
        $data['action'] = 'view';
        
		$this->load->model('region_model');
		$data['city_list']  = $this->region_model->get_all_city();
		$data['state_list'] = $this->region_model->get_all_state();
		
        $page_details['title'] = "View Rider Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/rider/rider-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Edit rider details
    public function edit($id)
    {
        if($this->input->post() && $this->input->post())
        {
            $fields = array("first_name" => trim($this->input->post('fname')),
                "last_name" => trim($this->input->post('lname')),
                "email_id" => trim($this->input->post('email')),
				
				"address" => trim($this->input->post('address')),
				"suburb" => trim($this->input->post('suburb')),
				"city" => trim($this->input->post('city')),
				"state" => trim($this->input->post('state')),
				"zipcode" => trim($this->input->post('postcode')),
				
                "mobile_number" => trim($this->input->post('mobile')),
                "update_datetime" => date('Y-m-d H:i:s', time())
                );
            
            $rider_id = $this->model->edit($fields, $id);
            
            if($rider_id)
            {
                $this->session->set_flashdata('SUCC_MESSAGE', 'Rider Details Saved Successfully.');
                redirect('admin/rider/edit/'.$id);
            }
            else
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to edit Rider details.');
            }
        }
        
        $data['disable'] = "";
        
		$this->load->model('region_model');
		$data['city_list']  = $this->region_model->get_all_city();
		$data['state_list'] = $this->region_model->get_all_state();
		
        $data['rider_details'] = $this->model->fetch_details($id);
        $data['action'] = 'edit';
        
        $page_details['title'] = "Edit Rider Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/rider/rider-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Delete Ridder
    public function delete($id)
    {
        $result = $this->model->delete($id);
        
        if ($result) 
        {
            $this->session->set_flashdata('SUCC_MESSAGE', 'Rider Deleted Successfully.');
        } 
        else 
        {
            $this->session->set_flashdata('ERR_MESSAGE', 'Error to Delete Rider.');
        }
        
        redirect('admin/rider/view');
    }
    
    public function changestatus($id) 
    {
        //echo $_POST['status'];exit;
        $data = array('status' => $this->input->post('status'));
        
        $result = $this->model->edit($data, $id);

        if ($result) 
        {
            echo 'success';
        } 
        else 
        {
            echo 'error';
        }
        exit();
    }
}