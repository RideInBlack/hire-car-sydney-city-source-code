<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-street-view"></i> Rider Management</a></li>
            <li class="active">View Riders</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="GET" class="form-inline" role="form">
                            <div class="form-group">
                                <label for="search_firstname">First Name:</label>
                                <input type="text" class="form-control" name="search_firstname" value="<?= @$_GET['search_firstname'] ?>" id="search_firstname" />
                            </div>
                            <div class="form-group">
                                <label for="pwd">Email Id:</label>
                                <input type="text" class="form-control" name="search_email" value="<?= @$_GET['search_email'] ?>" id="search_email" />
                            </div>
                            <button type="submit" onclick="return searchRiderValidation()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                            <?php if (isset($_GET['search_firstname']) || isset($_GET['search_email'])): ?>
                                <button type="button" onclick="location.href = '<?= base_url('admin/rider/view') ?>'" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                            <?php endif; ?>
                            <span class="text-red" id="ERR_SEARCH"></span>
                        </form>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Riders</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Mobile Number</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($rider_details as $rider):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $rider->first_name ?></td>
                                    <td><?= $rider->last_name ?></td>
                                    <td><?= $rider->email_id ?></td>
                                    <td><?= $rider->mobile_number ?></td>
                                    <td>
                                        <?php if ($rider->status == 1): ?>
                                            <input type="checkbox" checked onchange="return chageStatus('<?= base_url('admin/rider/changestatus/' . $rider->rider_id) ?>', 'status_<?= $rider->rider_id ?>', 0)" id="status_<?= $rider->rider_id ?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php else: ?>
                                            <input type="checkbox" onchange="return chageStatus('<?= base_url('admin/rider/changestatus/' . $rider->rider_id) ?>', 'status_<?= $rider->rider_id ?>', 1)" id="status_<?= $rider->rider_id ?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="<?= base_url('admin/rider/details/' . $rider->rider_id) ?>" class="btn btn-info" title="View">
                                                <i class="fa fa-fw fa-file-text-o"></i></a>
                                            <a href="<?= base_url('admin/rider/edit/' . $rider->rider_id) ?>" class="btn btn-warning" title="Edit">
                                                <i class="fa fa-fw fa-edit"></i></a>
                                            <a href="<?= base_url('admin/rider/delete/' . $rider->rider_id) ?>" onclick="return deleteConfirm();" class="btn btn-danger" title="Delete">
                                                <i class="fa fa-fw fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function searchRiderValidation()
    {
        //search_nickname, search_email
        var search_firstname = document.getElementById("search_firstname").value.trim();
        var search_email = document.getElementById("search_email").value.trim();

        if (search_firstname == "" && search_email == "")
        {
            document.getElementById("ERR_SEARCH").innerHTML = "Please Enter Keyword to Search.";
            return false;
        }
        else {
            return true;
        }
    }
</script>