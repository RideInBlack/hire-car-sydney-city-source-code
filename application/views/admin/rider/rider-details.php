<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-street-view"></i> Rider Management</a></li>
            <li class="active">Rider Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Rider Details</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email Id:</label>
                            <div class="col-sm-4">
                                <input type="email" class="form-control" name="email"  id="email" value="<?= $rider_details->email_id ?>" <?= $disable ?> placeholder="Email">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_EMAIL"></p>
                        </div>

                        <div class="form-group">
                            <label for="fname" class="col-sm-2 control-label">First Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="fname" id="fname" value="<?= $rider_details->first_name ?>" <?= $disable ?> placeholder="First Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>
                        <div class="form-group">
                            <label for="lname" class="col-sm-2 control-label">Last Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="lname" id="lname" value="<?= $rider_details->last_name ?>" <?= $disable ?> placeholder="Last Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_LNAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="mobile" class="col-sm-2 control-label">Mobile Number:</label>
                            <div class="col-sm-4">
                                <input type="tel" class="form-control" name="mobile" id="mobile" value="<?= $rider_details->mobile_number ?>" <?= $disable ?> placeholder="Mobile Number">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MOBILE"></p>
                        </div>
						
						<!-- Address Details -->
						<h4>Address Details</h4>
						<!-- Street Number & Name -->
						<div class="form-group">
							<label for="address" class="col-sm-2 control-label">Street Number & Name:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="address" id="address" value="<?= $rider_details->address ?>" <?= $disable ?> placeholder="Street Number & Name">
							</div>
							<p class="col-sm-4 text-red" id="ERR_ADDRESS"></p>
						</div>
						<!-- Suburb -->
						<div class="form-group">
							<label for="address" class="col-sm-2 control-label">Suburb:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="suburb" id="suburb" value="<?= $rider_details->suburb ?>" <?= $disable ?> placeholder="Suburb">
							</div>
							<p class="col-sm-4 text-red" id="ERR_SUBURB"></p>
						</div>
						<!-- State -->
						<div class="form-group">
							<label for="address" class="col-sm-2 control-label">State:</label>
							<div class="col-sm-4">
								<select class="form-control select2" name="state" id="state" <?= $disable ?> placeholder="Select State">
									<option value="">Select State</option>
									<?php foreach($state_list as $states) { ?>
									<option value="<?=$states->state_id;?>" <?php if($rider_details->state == $states->state_id) { ?> selected="selected" <?php } ?>>
									<?=$states->name?>
									</option>
									<?php } ?>
								</select>
							</div>
							<p class="col-sm-4 text-red" id="ERR_STATE"></p>
						</div>
						<?php /*?><div class="form-group">
							<label for="city" class="col-sm-2 control-label">City:</label>
							<div class="col-sm-4">
								<select class="form-control select2" name="city" id="city" placeholder="Select City" <?= $disable ?>>
									<option value="">Select City</option>
									<?php foreach($city_list as $cities) { ?>
									<option value="<?=$cities->city_id;?>" <?php if($rider_details->city == $cities->city_id) { ?> selected="selected" <?php } ?>>
									<?=$cities->name?>
									</option>
									<?php } ?>
								</select>
							</div>
							<p class="col-sm-4 text-red" id="ERR_CITY"></p>
						</div><?php */?>
						<!-- Postal Code -->
						<div class="form-group">
							<label for="zipcode" class="col-sm-2 control-label">Post code:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="postcode" id="postcode" value="<?= $rider_details->zipcode ?>" <?= $disable ?> placeholder="Post code">
							</div>
							<p class="col-sm-4 text-red" id="ERR_POSTCODE"></p>
						</div>
						<!-- */End Address Details --> 

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <?php if ($action == 'edit'): ?>
                                    <button type="submit" class="btn btn-success" onclick="return validateEditRider()"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <?php endif; ?>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/rider/view') ?>'">
                                    <i class="fa fa-fw fa-angle-left"></i> Back
                                </button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript" src="<?=base_url('assets/admin/js')?>/riderValidation.js"></script>
