<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-street-view"></i> Rider Management</a></li>
            <li class="active">Add Rider</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add Rider</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email Id:</label>
                            <div class="col-sm-4">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_EMAIL"></p>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password:</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_PASSWORD"></p>
                        </div>
                        <div class="form-group">
                            <label for="confirmPassword" class="col-sm-2 control-label">Confirm Password:</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="confirmPassword" id="password2" placeholder="Confirm Password">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_PASSWORD2"></p>
                        </div>

                        <div class="form-group">
                            <label for="fname" class="col-sm-2 control-label">First Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="lname" class="col-sm-2 control-label">Last Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_LNAME"></p>
                        </div>

                        <div class="form-group">
                            <label for="mobile" class="col-sm-2 control-label">Mobile Number:</label>
                            <div class="col-sm-4">
                                <input type="tel" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_MOBILE"></p>
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success" onclick="return validateAddRider();"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/rider/view') ?>'"><i class="fa fa-fw fa-angle-left"></i> Cancel</button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript" src="<?=base_url('assets/admin/js')?>/riderValidation.js"></script>
