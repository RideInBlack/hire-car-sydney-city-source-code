<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-street-view"></i> Book History Management</a></li>
            <li class="active">View Completed Rides</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="GET" class="form-inline" role="form">
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label for="ridername">Rider Name:</label>
                                <input type="text" class="form-control" name="ridername" value="<?= @$_GET['ridername'] ?>" id="ridername" />
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label for="rideremail">Rider Email:</label>
                                <input type="text" class="form-control" name="rideremail" value="<?= @$_GET['rideremail'] ?>" id="rideremail" />
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label for="drivername">Driver Name:</label>
                                <input type="text" class="form-control" name="drivername" value="<?= @$_GET['drivername'] ?>" id="drivername" />
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label for="driveremail">Driver Email:</label>
                                <input type="text" class="form-control" name="driveremail" value="<?= @$_GET['driveremail'] ?>" id="driveremail" />
                            </div>
                            <div style="margin-top: 15px;float: left;width: 100%">
                                <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                    <label>Status:</label>
                                    <select class="form-control select2" name="ridestatus" id="ridestatus">
                                        <option value="" selected >Select Status</option>
                                        <option <?php if(isset($_REQUEST['ridestatus']) && $_REQUEST['ridestatus'] == '4') { ?> selected <?php } ?> value="4">Completed</option>
                                        <option <?php if(isset($_REQUEST['ridestatus']) && $_REQUEST['ridestatus'] == '5') { ?> selected <?php } ?> value="5">Rejected</option>
                                    </select>
                                </div><!-- /.form-group -->
                                <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                  <button type="submit" onclick="return searchRideValidation()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                                  <?php if (isset($_GET['ridername']) || isset($_GET['rideremail']) || isset($_GET['ridestatus'])): ?>
                                      <button type="button" onclick="location.href = '<?= base_url('admin/bookhistory/completed') ?>'" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                                  <?php endif; ?>
                                  <p class="text-red" id="ERR_SEARCH"></p>
                                </div>
                            </div>
                        </form>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Rides</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Rider Name</th>
                                <th>Rider Email</th>
                                <th>Driver Name</th>
                                <th>Driver Email</th>
                                <th>Pickup</th>
                                <th>Destination</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($completedridedetails as $rides):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $rides->first_name.' '.$rides->last_name ?></td>
                                    <td><?= $rides->email_id ?></td>
                                    <td><?= $rides->d_first_name.' '.$rides->d_last_name ?></td>
                                    <td><?= $rides->d_email_id ?></td>
                                    <td><?= $rides->pickup_location ?></td>
                                    <td><?= $rides->destination_location ?></td>
                                    <td>
                                        <?php if ($rides->ride_status == 4): ?>
                                            <span class="label label-success">Completed</span>
                                        <?php else: ?>
                                            <span class="label label-danger">Rejected</span>    
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script>
    function searchRiderValidation()
    {
        //search_nickname, search_email
        var search_firstname = document.getElementById("search_firstname").value.trim();
        var search_email = document.getElementById("search_email").value.trim();

        if (search_firstname == "" && search_email == "")
        {
            document.getElementById("ERR_SEARCH").innerHTML = "Please Enter Keyword to Search.";
            return false;
        }
        else {
            return true;
        }
    }
</script>