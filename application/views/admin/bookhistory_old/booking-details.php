<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Ride Details</h4>
</div>
<!-- /modal-header -->
<div class="modal-body">
    <?php
//echo '<pre>'; print_r($ride_details); echo '</pre>';
    ?>
    <div class="form-group">
        <label>Rider Name: </label>
        <input type="text" readonly="readonly" name="rider_name" value="<?= $ride_details->name ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label>Rider Phone: </label>
        <input type="text" readonly="readonly" name="rider_name" value="<?= $ride_details->phone ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label>Rider Email: </label>
        <input type="text" readonly="readonly" name="rider_name" value="<?= $ride_details->email ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label>Pick-up Details: </label>
        <input type="text" readonly="readonly" name="rider_name" value="<?= $ride_details->pickup_location ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label>Drop-off Details: </label>
        <input type="text" readonly="readonly" name="rider_name" value="<?= $ride_details->destination_location ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label>Pick-up Date-time: </label>
        <input type="text" readonly="readonly" name="rider_name" value="<?= $ride_details->pickup_datetime ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label>Estimated Distance: </label>
        <input type="text" readonly="readonly" name="rider_name" value="<?= $ride_details->estimated_distance ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label>Estimated Fare: </label>
        <input type="text" readonly="readonly" name="rider_name" value="<?= $ride_details->estimated_fare ?>" class="form-control" />
    </div>

    <?php if ($ride_details->ride_status == 0): ?>
        <a href="<?= base_url('admin/bookhistory/change_status/' . $ride_details->id . '/2') ?>" class="btn btn-primary">Assign Driver</a>
    <?php elseif ($ride_details->ride_status == 2): ?>
        <a href="<?= base_url('admin/bookhistory/change_status/' . $ride_details->id . '/3') ?>" class="btn btn-primary">Start Ride</a>
    <?php elseif ($ride_details->ride_status == 3): ?>
        <a href="<?= base_url('admin/bookhistory/change_status/') ?>" class="btn btn-primary">Ride Finished</a>
    <?php endif; ?>
</div>
<!-- /modal-footer --> 
