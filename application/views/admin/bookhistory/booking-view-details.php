<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Payment Details</h4>
</div>
<!-- /modal-header -->
<div class="modal-body">
    <?php //echo '<pre>'; print_r($ride_details); echo '</pre>'; ?>
    <label>Estimated Fare: </label> $<?= $ride_details->estimated_fare ?>
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <?php if (count($payment_details) > 0): ?>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Payment type</th>
                        <th>Amount</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($payment_details AS $payment): ?>
                        <tr>
                            <td><?= $i++ ?></td>
                            <td><?= $payment->payment_type ?></td>
                            <td>$<?= $payment->amount ?></td>
                            <td><?= date('d/m/Y @ h:i A', strtotime($payment->payment_datetime)) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            <?php else: ?>
                <thead>
                    <tr>
                        <th class="text-center">Payment Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>No Payment Received Yet.</td>
                    </tr>
                </tbody>
            <?php endif; ?>
        </table>
    </div>
</div>
<div class="modal-footer">
	<?php if ($paid_amount < $ride_details->estimated_fare): ?>
    	<?php if ($ride_details->pay_by_deposit == '1'): ?>
        	<a href="<?= base_url('admin/bookhistory/bankDeposit/' . $ride_details->id . '/'.$ride_details->estimated_fare) ?>" class="btn btn-primary makepayment" >Received Bank Deposit</a>
        <?php endif;?>
    
        <a href="<?= base_url('admin/bookhistory/manual_payment/' . $ride_details->id . '/'.$ride_details->estimated_fare) ?>" class="btn btn-primary makepayment" >Received Manual Payment</a>
    <?php endif; ?>
</div>
<!-- /modal-footer --> 
