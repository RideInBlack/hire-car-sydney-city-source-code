<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-street-view"></i> Book History Management</a></li>
            <li class="active">View Running Rides</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="GET" class="form-inline" role="form">
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label for="ridername">Rider Name:</label>
                                <input type="text" class="form-control" name="ridername" value="<?= @$_GET['ridername'] ?>" id="ridername" />
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label for="rideremail">Rider Email:</label>
                                <input type="text" class="form-control" name="rideremail" value="<?= @$_GET['rideremail'] ?>" id="rideremail" />
                            </div>
                            <?php /*?><div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label for="drivername">Driver Name:</label>
                                <input type="text" class="form-control" name="drivername" value="<?= @$_GET['drivername'] ?>" id="drivername" />
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label for="driveremail">Driver Email:</label>
                                <input type="text" class="form-control" name="driveremail" value="<?= @$_GET['driveremail'] ?>" id="driveremail" />
                            </div><?php */?>
                            <div style="margin-top: 15px;float: left;width: 100%">
                                <?php /*?><div class="form-group col-xs-12 col-sm-6 col-md-3">
                                    <label>Status:</label>
                                    <select class="form-control select2" name="ridestatus" id="ridestatus">
                                        <option value="" selected >Select Status</option>
                                        <option <?php if(isset($_REQUEST['ridestatus']) && $_REQUEST['ridestatus'] == '2') { ?> selected <?php } ?> value="2">Confirmed</option>
                                        <option <?php if(isset($_REQUEST['ridestatus']) && $_REQUEST['ridestatus'] == '3') { ?> selected <?php } ?> value="3">Started</option>
                                    </select>
                                </div><!-- /.form-group --><?php */?>
                                <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                  <button type="submit" onclick="return searchRideValidation()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                                  <?php if (isset($_GET['ridername']) || isset($_GET['rideremail']) || isset($_GET['ridestatus'])): ?>
                                      <button type="button" onclick="location.href = '<?= base_url('admin/bookhistory/running') ?>'" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                                  <?php endif; ?>
                                  <p class="text-red" id="ERR_SEARCH"></p>
                                </div>
                            </div>
                        </form>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Rides</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Rider Name</th>
                                <th>Rider Email</th>
                                <th>Driver Name</th>
                                <th>Driver Email</th>
                                <th>Pickup</th>
                                <th>Destination</th>
                                <th>Payment</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($runningridedetails as $rides):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $rides->first_name.' '.$rides->last_name ?></td>
                                    <td><?= $rides->email_id ?></td>
                                    <td><?= $rides->d_first_name.' '.$rides->d_last_name ?></td>
                                    <td><?= $rides->d_email_id ?></td>
                                    <td><?= $rides->pickup_location ?></td>
                                    <td><?= $rides->destination_location ?></td>
                                    <td>
                                        <a href="<?= base_url('admin/bookhistory/paymentdetails/' . $rides->id) ?>" data-toggle="modal" data-target="#view-details" title="View Payment Details"><i class="fa fa-fw fa-file-text"></i></a> 
                                        
                                    </td>
                                    <td>
                                        <?php if ($rides->ride_status == 2): ?>
                                            <span class="label label-info">Confirmed</span>
                                        <?php else: ?>
                                            <span class="label label-default">Started</span>    
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                            <?php /* if ($rides->ride_status == 0): ?>
                                              <a href="Javascript:void(0);" data-toggle="modal" data-target="" title="" onclick="showmodel('<?= $rides->id; ?>')">Assign</a>
                                              <?php endif; */ ?>
                                            <a href="<?= base_url('admin/bookhistory/details/' . $rides->id) ?>" data-toggle="modal" data-target="#booking-details" title="View Details"><i class="fa fa-fw fa-file-text-o"></i></a> 
                                        </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<div id="booking-details" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>

<div id="view-details" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<script>
	$('#booking-details').on('hide.bs.modal', function() {
        $('#booking-details').removeData();
    })
	
    $('#view-details').on('hide.bs.modal', function() {
        $('#view-details').removeData();
    })
</script>
<script>
    function searchRideValidation()
    {
        //search_nickname, search_email
        var ridername = document.getElementById("ridername").value.trim();
        var rideremail = document.getElementById("rideremail").value.trim();
        var drivername = document.getElementById("drivername").value.trim();
        var driveremail = document.getElementById("driveremail").value.trim();
        var ridestatus = document.getElementById("ridestatus").value.trim();

        if (ridername == "" && rideremail == "" && ridestatus == "" && drivername == "" && driveremail == "")
        {
            document.getElementById("ERR_SEARCH").innerHTML = "Please Enter Keyword to Search.";
            return false;
        }
        else {
            return true;
        }
    }
</script>