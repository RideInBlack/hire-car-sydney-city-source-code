<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-street-view"></i> Book History Management</a></li>
            <li class="active">View Waiting Rides</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="GET" class="form-inline" role="form">
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label for="ridername">Rider Name:</label>
                                <input type="text" class="form-control" name="ridername" value="<?= @$_GET['ridername'] ?>" id="ridername" />
                            </div>
                            <div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label for="rideremail">Rider Email:</label>
                                <input type="text" class="form-control" name="rideremail" value="<?= @$_GET['rideremail'] ?>" id="rideremail" />
                            </div>

                            <?php /*?><div class="form-group col-xs-12 col-sm-6 col-md-3">
                                <label>Status:</label>
                                <select class="form-control select2" name="ridestatus" id="ridestatus">
                                    <option value="" selected >Select Status</option>
                                    <option <?php if (isset($_REQUEST['ridestatus']) && $_REQUEST['ridestatus'] == '0') { ?> selected <?php } ?> value="0">Waiting</option>
                                    <option <?php if (isset($_REQUEST['ridestatus']) && $_REQUEST['ridestatus'] == '1') { ?> selected <?php } ?> value="1">Requested</option>
                                </select>
                            </div><!-- /.form-group --><?php */?>
                            <button type="submit" onclick="return searchRideValidation()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                            <?php if (isset($_GET['ridername']) || isset($_GET['rideremail']) || isset($_GET['ridestatus'])): ?>
                                <button type="button" onclick="location.href = '<?= base_url('admin/bookhistory/waiting') ?>'" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                            <?php endif; ?>
                            <p class="text-red" id="ERR_SEARCH"></p>
                        </form>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Rides</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Rider Name</th>
                                <th>Email</th>
                                <th>Pickup</th>
                                <th>Destination</th>
                                <th>Pickup Datetime</th>
                                <th>Status</th>
                                <th>Payment</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($waitingridedetails) > 0): ?>
                                <?php foreach ($waitingridedetails as $rides): ?>
                                    <tr>
                                        <td><?= $sr++; ?></td>
                                        <?php if ($rides->rider_id == 0): ?>
                                            <td><?= $rides->name ?></td>
                                            <td><?= $rides->email ?></td>
                                        <?php else: ?>
                                            <td><?= $rides->first_name . ' ' . $rides->last_name ?></td>
                                            <td><?= $rides->email_id ?> <i class="fa fa-check text-green"></i></td>
                                        <?php endif; ?>
                                        <td><?= $rides->pickup_location ?></td>
                                        <td><?= $rides->destination_location ?></td>
                                        <td><?= $rides->pickup_datetime ?></td>
                                        <td>
                                            <?php if ($rides->ride_status == 0): ?>
                                                <span class="label label-primary">Waiting</span>
                                            <?php elseif ($rides->ride_status == 1): ?>
                                                <span class="label label-warning">Requested</span>
                                            <?php else: ?>
                                                <span class="label label-warning">Assigned</span>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <a href="<?= base_url('admin/bookhistory/paymentdetails/' . $rides->id) ?>" data-toggle="modal" data-target="#view-details" title="View Payment Details"><i class="fa fa-fw fa-file-text"></i></a> 
<!--                                            <a href="javascript:oid(0)" title="Send Invoice"><i class="fa fa-fw fa-envelope"></i></a>-->
                                        </td>
                                        <td>
                                            <?php /* if ($rides->ride_status == 0): ?>
                                              <a href="Javascript:void(0);" data-toggle="modal" data-target="" title="" onclick="showmodel('<?= $rides->id; ?>')">Assign</a>
                                              <?php endif; */ ?>
                                            <a href="<?= base_url('admin/bookhistory/details/' . $rides->id) ?>" data-toggle="modal" data-target="#booking-details" title="View Details"><i class="fa fa-fw fa-file-text-o"></i></a> 
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="8">No Record Available</td> 
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal fade"  id="assignModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= base_url(); ?>admin/bookhistory/assign" id="register-form" method="post" name="assignDriverForm" id="assignDriverForm" enctype="multipart/form-data">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Assign Driver</h4>
                </div>
                <input type="hidden" id="rideid" name="rideid" value="" />
                <div class="modal-body">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="driverTable">
                                <thead>
                                    <tr>
                                        <th style="width: 10px"><input type="checkbox" name="checkAll" id="checkAll" value=""></th>
                                        <th style="width: 10px">#</th>
                                        <th>Driver Name</th>
                                        <th>Driver Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($driverdetails as $driver):
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" name="driver[]" id="" value="<?= $driver->driver_id; ?>" class="drivercheckbox"></td>
                                            <td><?= $i; ?></td>
                                            <td><?= $driver->first_name . ' ' . $driver->last_name ?></td>
                                            <td><?= $driver->email_id ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" name="assigndriver" id="assigndriver" class="btn btn-primary" onclick='' value="Assign" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal HTML -->
<div id="booking-details" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<div id="view-details" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>

<script>
    $('#booking-details').on('hide.bs.modal', function() {
        $('#booking-details').removeData();
    })

    $('#view-details').on('hide.bs.modal', function() {
        $('#view-details').removeData();
    })


</script>

<script>
    function searchRideValidation()
    {
        //search_nickname, search_email
        var ridername = document.getElementById("ridername").value.trim();
        var rideremail = document.getElementById("rideremail").value.trim();
        var ridestatus = document.getElementById("ridestatus").value.trim();

        if (ridername == "" && rideremail == "" && ridestatus == "")
        {
            document.getElementById("ERR_SEARCH").innerHTML = "Please Enter Keyword to Search.";
            return false;
        }
        else {
            return true;
        }
    }
    function showmodel(id)
    {
        $('#rideid').val(id);
        $('#assignModel').modal('show');
    }

    $(document).ready(function() {

        $('#checkAll').click(function(event) {  //on click
            if (this.checked) { // check select status
                $('.drivercheckbox').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"              
                });
            } else {
                $('.drivercheckbox').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                      
                });
            }
        });



    });



</script>