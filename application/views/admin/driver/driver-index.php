<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-taxi"></i> Driver Management</a></li>
            <li class="active">View Drivers</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    
    	<div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="GET" class="form-inline" role="form">
                            <div class="form-group">
                                <label for="search_firstname">First Name:</label>
                                <input type="text" class="form-control" name="search_firstname" value="<?=@$_GET['search_firstname']?>" id="search_firstname" />
                            </div>
                            <div class="form-group">
                                <label for="pwd">Email Id:</label>
                                <input type="text" class="form-control" name="search_email" value="<?=@$_GET['search_email']?>" id="search_email" />
                            </div>
                            <button type="submit" onclick="return searchDriverValidation()" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                            <?php if(isset($_GET['search_firstname']) || isset($_GET['search_email'])):?>
                                <button type="button" onclick="location.href = '<?= base_url('admin/driver/view') ?>'" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                            <?php endif;?>
                            <span class="text-red" id="ERR_SEARCH"></span>
                        </form>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
        
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Drivers</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Mobile Number</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <?php
                            foreach ($driver_details as $driver):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $driver->first_name ?></td>
                                    <td><?= $driver->last_name ?></td>
                                    <td><?= $driver->email_id ?></td>
                                    <td><?= $driver->mobile_number ?></td>
                                    <td>
                                        <?php if($driver->status == 1):?>
                                        <input type="checkbox" checked onchange="return chageStatus('<?=base_url('admin/driver/changestatus/'.$driver->driver_id)?>', 'status_<?=$driver->driver_id?>', 0)" id="status_<?=$driver->driver_id?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php else:?>
                                        <input type="checkbox" onchange="return chageStatus('<?=base_url('admin/driver/changestatus/'.$driver->driver_id)?>', 'status_<?=$driver->driver_id?>', 1)" id="status_<?=$driver->driver_id?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="<?= base_url('admin/driver/details/' . $driver->driver_id) ?>" class="btn btn-info" title="View">
                                                <i class="fa fa-fw fa-file-text-o"></i>
                                            </a>
                                            <a href="<?= base_url('admin/driver/edit/' . $driver->driver_id) ?>" class="btn btn-warning" title="Edit">
                                                <i class="fa fa-fw fa-edit"></i>
                                            </a>
                                            <a href="<?= base_url('admin/driver/delete/' . $driver->driver_id) ?>" class="btn btn-danger" onclick="return deleteConfirm();" title="Delete">
                                                <i class="fa fa-fw fa-trash-o"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
                <?php echo $this->pagination->create_links(); ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function searchDriverValidation()
    {
        //search_nickname, search_email
        var search_firstname = document.getElementById("search_firstname").value.trim();
        var search_email = document.getElementById("search_email").value.trim();
        
        if(search_firstname == "" && search_email == "")
        {
            document.getElementById("ERR_SEARCH").innerHTML = "Please Enter Keyword to Search.";
            return false;
        }
        else {
            return true;
        }
    }
</script>