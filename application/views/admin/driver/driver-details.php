<?php
$rib_upload_path = DRIVER_UPLOAD_SERVER_PATH;
?>
<style>
.delete-image {
	padding: 3px;
	position: absolute;
	left: 15px;
	top: 0;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-taxi"></i> Driver Management</a></li>
            <li class="active">Driver Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Driver Details</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
					<div class="box-body"> 
						<!-- First Name -->
						<div class="form-group">
							<label for="fname" class="col-sm-2 control-label">First Name:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="fname" id="fname" value="<?= $driver_details->first_name ?>" <?= $disable ?> placeholder="First Name">
							</div>
							<p class="col-sm-4 text-red" id="ERR_FNAME"></p>
						</div>
						<!-- Last Name -->
						<div class="form-group">
							<label for="lname" class="col-sm-2 control-label">Last Name:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="lname" id="lname" value="<?= $driver_details->last_name ?>" <?= $disable ?> placeholder="Last Name">
							</div>
							<p class="col-sm-4 text-red" id="ERR_LNAME"></p>
						</div>
						<!-- Driver Photo -->
						<?php if(!empty($driver_details->profile_image)) :
								if($driver_details->profile_image_server_path == 'rib') {
									$driverImage = $rib_upload_path."assets/uploads/driver/".$driver_details->profile_image;
								}
								else {
									$driverImage = base_url()."assets/uploads/driver/".$driver_details->profile_image;
								}?>
								<div class="form-group">
									<label for="Image" class="col-sm-2 control-label">
										<?php if ($action != 'edit'): ?>
										Driver's Photo:
										<?php endif; ?>
									</label>
									<div class="col-sm-4"> <img alt="Driver's Image" class="img img-thumbnail" src="<?php echo $driverImage; ?>" width="130"> </div>
								</div>
						<?php endif; ?>
						<?php if ($action == 'edit'): ?>
						<div class="form-group">
							<label for="Image" class="col-sm-2 control-label">Driver's Photo:</label>
							<div class="col-sm-4">
								<input type="file" id="driverimage" name="driverimage" class="form-control" accept="image/*"/>
							</div>
							<p class="col-sm-4 text-red" id="ERR_DRIVERIMAGE"></p>
						</div>
						<?php endif; ?>
						
						<!-- Email Id -->
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">Email Id:</label>
							<div class="col-sm-4">
								<input type="email" class="form-control" name="email" id="email" value="<?= $driver_details->email_id ?>" <?= $disable ?> placeholder="Email">
							</div>
							<p class="col-sm-4 text-red" id="ERR_EMAIL"></p>
						</div>
						<!-- Mobile Number -->
						<div class="form-group">
							<label for="mobile" class="col-sm-2 control-label">Mobile Number:</label>
							<div class="col-sm-4">
								<input type="tel" class="form-control" name="mobile" id="mobile" value="<?= $driver_details->mobile_number ?>" <?= $disable ?> placeholder="Mobile Number">
							</div>
							<p class="col-sm-4 text-red" id="ERR_MOBILE"></p>
						</div>
						<?php /*?><!-- Username -->
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">Username:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="username" id="username" value="<?= $driver_details->username ?>" <?= $disable ?> placeholder="Username">
							</div>
							<p class="col-sm-4 text-red" id="ERR_USERNAME"></p>
						</div><?php */?>
						<!-- Password -->
						<div class="form-group">
							<label for="password" class="col-sm-2 control-label">Password :</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="password" id="password" value="<?= $driver_details->password ?>" readonly="readonly" placeholder="Password" redonly>
							</div>
							<p class="col-sm-4 text-red" id="ERR_PASSWORD"></p>
						</div>
						<!-- Company Name -->
						<div class="form-group">
							<label for="companyname" class="col-sm-2 control-label">Company Name:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="companyname" id="companyname" value="<?= $driver_details->companyname ?>" <?= $disable ?> placeholder="Company Name">
							</div>
							<p class="col-sm-4 text-red" id="ERR_COMPANYNAME"></p>
						</div>
						<!-- ABN -->
						<div class="form-group">
							<label for="abn" class="col-sm-2 control-label">ABN:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="abn" id="abn" value="<?= $driver_details->abn ?>" <?= $disable ?> placeholder="ABN">
							</div>
							<p class="col-sm-4 text-red" id="ERR_ABN"></p>
						</div>
						<!-- ==================================================================================================================== --> 
						<!-- Address Details -->
						<h4>Address Details</h4>
						<!-- Street Number & Name -->
						<div class="form-group">
							<label for="address" class="col-sm-2 control-label">Street Number & Name:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="address" id="address" value="<?= $driver_details->address ?>" <?= $disable ?> placeholder="Street Number & Name">
							</div>
							<p class="col-sm-4 text-red" id="ERR_ADDRESS"></p>
						</div>
						<!-- Suburb -->
						<div class="form-group">
							<label for="address" class="col-sm-2 control-label">Suburb:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="suburb" id="suburb" value="<?= $driver_details->address ?>" <?= $disable ?> placeholder="Suburb">
							</div>
							<p class="col-sm-4 text-red" id="ERR_SUBURB"></p>
						</div>
						<!-- State -->
						<div class="form-group">
							<label for="address" class="col-sm-2 control-label">State:</label>
							<div class="col-sm-4">
								<select class="form-control select2" name="state" id="state" <?= $disable ?> placeholder="Select State">
									<option value="">Select State</option>
									<?php foreach($state_list as $states) { ?>
									<option value="<?=$states->state_id;?>" <?php if($driver_details->state == $states->state_id) { ?> selected="selected" <?php } ?>>
									<?=$states->name?>
									</option>
									<?php } ?>
								</select>
							</div>
							<p class="col-sm-4 text-red" id="ERR_STATE"></p>
						</div>
						<!-- Postal Code -->
						<div class="form-group">
							<label for="zipcode" class="col-sm-2 control-label">Post code:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="postcode" id="postcode" value="<?= $driver_details->zipcode ?>" <?= $disable ?> placeholder="Post code">
							</div>
							<p class="col-sm-4 text-red" id="ERR_POSTCODE"></p>
						</div>
						<?php /*?><div class="form-group">
							<label for="city" class="col-sm-2 control-label">City:</label>
							<div class="col-sm-4">
								<select class="form-control select2" name="city" id="city" placeholder="Select City" <?= $disable ?>>
									<option value="">Select City</option>
									<?php foreach($city_list as $cities) { ?>
									<option value="<?=$cities->city_id;?>" <?php if($driver_details->city == $cities->city_id) { ?> selected="selected" <?php } ?>>
									<?=$cities->name?>
									</option>
									<?php } ?>
								</select>
							</div>
							<p class="col-sm-4 text-red" id="ERR_CITY"></p>
						</div><?php */?>
						<!-- */End Address Details --> 
						<!-- ==================================================================================================================== --> 
						<!-- Licence Details -->
						<h4>Licence Details</h4>
						<!-- Licence Number -->
						<?php $i = 1 ?>
						<?php $itr = 0 ?>
						<div class="form-group">
							<label for="Image" class="col-sm-2 control-label"></label>
							<?php if(count($licence_uploads) > 0): ?>
								
								<?php foreach($licence_uploads as $row): ?>
									<div class="col-sm-2" id="imgBox_<?=$i?>">
										<?php if($row->server_reference == 'rib'):  ?>
											<img alt="Licence Image" class="img img-thumbnail" src="<?= $rib_upload_path."assets/uploads/driver/license/".$row->file_name; ?>" width="150">
										<?php else: ?>
											<img alt="Licence Image" class="img img-thumbnail" src="<?= base_url()."assets/uploads/driver/license/".$row->file_name; ?>" width="150">
										<?php endif;?>
										<a href="javascript:void(0)" onclick="deleteImage('<?= base_url('admin/driver/delete_uploads/' . $row->driverupload_id) ?>', 'imgBox_<?= $i ?>', 'licensedetails')" title="Delete Image" class="delete-image bg-gray text-blue "><i class="fa fa-2x fa-times"></i></a>
									</div>
									<?php $i++; ?>
									<?php $itr++; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
						
						<?php if ($action == 'edit'): ?>
						<div class="form-group">
							<label for="licensedetails" class="col-sm-2 control-label">Driver's Licence Copy:</label>
							<div class="col-sm-4">
								<input type="file" name="licensedetails[]" multiple id="licensedetails" class="form-control multi" maxlength="<?=4-$itr?>" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG"/>
							</div>
							<p class="col-sm-4 text-red" id="ERR_LICENSEDETAILS"></p>
						</div>
						<?php endif; ?>
						<!-- HC Authority Card -->
						<div class="form-group">
							<label for="Image" class="col-sm-2 control-label"></label>
							<?php $itr = 0;?>
							<?php if(count($hc_authority_uploads) > 0): ?>
								<?php foreach($hc_authority_uploads as $row): ?>
									<div class="col-sm-2" id="imgBox_<?=$i?>">
										<?php if($row->server_reference == 'rib'):  ?>
											<img alt="HC Authority Image" class="img img-thumbnail" src="<?= $rib_upload_path."assets/uploads/driver/hcauthoritycertificate/".$row->file_name; ?>" width="150">
										<?php else: ?>
											<img alt="HC Authority Image" class="img img-thumbnail" src="<?= base_url()."assets/uploads/driver/hcauthoritycertificate/".$row->file_name; ?>" width="150">
										<?php endif; ?>
										<a href="javascript:void(0)" onclick="deleteImage('<?= base_url('admin/driver/delete_uploads/' . $row->driverupload_id) ?>', 'imgBox_<?= $i ?>', 'hc_authority_card')" title="Delete Image" class="delete-image bg-gray text-blue "><i class="fa fa-2x fa-times"></i></a>
									</div>
									<?php $i++;?>
									<?php $itr++; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
						<?php if ($action == 'edit'): ?>
						<div class="form-group">
							<label for="hc_authority_card" class="col-sm-2 control-label">HC Authority Card:</label>
							<div class="col-sm-4">
								<input type="file" name="hc_authority_card[]" multiple id="hc_authority_card" class="form-control multi" maxlength="<?=4-$itr?>" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG"/>
							</div>
							<p class="col-sm-4 text-red" id="ERR_HCCARD"></p>
						</div>
						<?php endif; ?>
						<!-- */End Licence Details --> 
						<!-- ==================================================================================================================== --> 
						<!-- Car Details -->
						<h4>Car Details</h4>
						<!-- Registration Number -->
						<div class="form-group">
							<label for="Image" class="col-sm-2 control-label"></label>
							<?php $itr = 0; ?>
							<?php if(count($registration_uploads) > 0): ?>
								<?php foreach($registration_uploads as $row): ?>
									<div class="col-sm-2" id="imgBox_<?=$i?>">
										<?php if($row->server_reference == 'rib'):  ?>
											<img alt="Registration Image" class="img img-thumbnail" src="<?= $rib_upload_path."assets/uploads/driver/registrationnumber/".$row->file_name; ?>" width="150"> 
										<?php else: ?>
											<img alt="Registration Image" class="img img-thumbnail" src="<?= base_url()."assets/uploads/driver/registrationnumber/".$row->file_name; ?>" width="150"> 
										<?php endif; ?>
										<a href="javascript:void(0)" onclick="deleteImage('<?= base_url('admin/driver/delete_uploads/' . $row->driverupload_id) ?>', 'imgBox_<?= $i ?>', 'registrationnumber')" title="Delete Image" class="delete-image bg-gray text-blue "><i class="fa fa-2x fa-times"></i></a>
									</div>
									<?php $i++; ?>
									<?php $itr++; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
						<?php if ($action == 'edit'): ?>
						<div class="form-group">
							<label for="registrationnumber" class="col-sm-2 control-label">Vehicle Registration Document:</label>
							<div class="col-sm-4">
								<input type="file" name="registrationnumber[]" multiple id="registrationnumber" class="form-control multi" maxlength="<?=4-$itr?>" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG"/>
							</div>
							<p class="col-sm-4 text-red" id="ERR_REGISTRATIONNUMBEr"></p>
						</div>
						<?php endif; ?>
						<!-- Make -->
						<div class="form-group">
							<label for="carmake" class="col-sm-2 control-label">Make:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="carmake" id="carmake" value="<?= $driver_details->car_make ?>" <?= $disable ?> placeholder="Make">
							</div>
							<p class="col-sm-4 text-red" id="ERR_CARMAKE"></p>
						</div>
						<!-- Model & Year -->
						<div class="form-group">
							<label for="modelnyear" class="col-sm-2 control-label">Model & Year:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="modelnyear" id="modelnyear" value="<?= $driver_details->car_yearmodel ?>" <?= $disable ?> placeholder="Model & Year">
							</div>
							<p class="col-sm-4 text-red" id="ERR_MODELNYEAR"></p>
						</div>
						<!-- */End Card Details --> 
						<!-- ==================================================================================================================== --> 
						<!-- Insurance Details -->
						<h4>Insurance Details</h4>
						<!-- Licence Number -->
						<div class="form-group">
							<label for="insurancecompany" class="col-sm-2 control-label">Company:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="insurancecompany" id="insurancecompany" value="<?= $driver_details->insurance_company ?>" <?= $disable ?> placeholder="Company">
							</div>
							<p class="col-sm-4 text-red" id="ERR_INSURANCECOMPANY"></p>
						</div>
						<!-- HC Authority Card -->
						<div class="form-group">
							<label for="Image" class="col-sm-2 control-label"></label>
							<?php $itr = 0;?>
							<?php if(count($insurance_uploads) > 0): ?>
								<?php foreach($insurance_uploads as $row): ?>
									<div class="col-sm-2" id="imgBox_<?=$i?>"> 
										<?php if($row->server_reference == 'rib'):  ?>
											<img alt="Insurance Image" class="img img-thumbnail" src="<?= $rib_upload_path."assets/uploads/driver/insurance/".$row->file_name; ?>" width="150"> 
										<?php else: ?>
											<img alt="Insurance Image" class="img img-thumbnail" src="<?= base_url()."assets/uploads/driver/insurance/".$row->file_name; ?>" width="150"> 
										<?php endif; ?>
										<a href="javascript:void(0)" onclick="deleteImage('<?= base_url('admin/driver/delete_uploads/' . $row->driverupload_id) ?>', 'imgBox_<?=$i?>', 'insurancetype')" title="Delete Image" class="delete-image bg-gray text-blue "><i class="fa fa-2x fa-times"></i></a>
									</div>
									<?php $i++; ?>
									<?php $itr++; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
						
						<div class="form-group">
							<label for="insurancetype" class="col-sm-2 control-label">Insurance Document:</label>
							<div class="col-sm-4">
								<input type="file" name="insurancetype[]" multiple id="insurancetype" class="form-control multi" maxlength="<?=4-$itr?>" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG" />
							</div>
							<p class="col-sm-4 text-red" id="ERR_INSURANCETYPE"></p>
						</div>
						<!-- */End Insurance Details --> 
						<!-- ==================================================================================================================== --> 
						<!-- Invite Code -->
						<div class="form-group">
							<label for="invitecode" class="col-sm-2 control-label">Invite Code:</label>
							<div class="col-sm-4">
								<input type="tel" class="form-control" name="invitecode" id="invitecode" value="<?= $driver_details->invitecode ?>" <?= $disable ?> placeholder="Invite Code">
							</div>
							<p class="col-sm-4 text-red" id="ERR_INVITECODE"></p>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<div class="row">
							<div class="col-sm-offset-2 col-sm-10">
								<?php if ($action == 'edit'): ?>
								<button type="submit" class="btn btn-success" onclick="return validateEditDriver()"><i class="fa fa-fw fa-check"></i> Save</button>
								&nbsp;&nbsp;
								<?php endif; ?>
								<button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/driver/view') ?>'"> <i class="fa fa-fw fa-angle-left"></i> Back </button>
							</div>
						</div>
					</div>
					<!-- /.box-footer -->
				</form>
            </div><!-- /.box body -->    
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript" src="<?=base_url('assets/admin/plugins/multiple-file-upload')?>/jquery.MultiFile.js"></script> 
<script type="text/javascript" src="<?=base_url('assets/admin/js')?>/driverValidation.js"></script>
<script>
function deleteImage(URL, box, uploadName)
{
	if (confirm('Are you sure want to delete?')) {
		if (URL == 0) {
			$("#" + box).remove();
		}
		else {
			$.ajax({
				type: 'get',
				url: URL,
				beforeSend: function() {
				},
				success: function(data) {
					//console.log(data); return 0;
					$("#" + box).remove();
					var num = parseInt(document.getElementById(uploadName).getAttribute('maxlength'));
					document.getElementById(uploadName).setAttribute('maxlength', num + 1);
				}
			});
		}
	}
}
</script>
