<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-car"></i> Car Category</a></li>
            <li class="active">View Car Category</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <?php /*?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Search</h3>
            </div>
            <div class="box-body">
                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <label for="search_category">Select Category:</label>
                        <select name="search_category" id="search_category" class="form-control select2">
                                <option value="0">Select category</option>
                                <?php foreach ($category as $cat) : ?>
                                    <option value="<?= $cat->category_id ?>"><?= $cat->name ?></option>
                                <?php endforeach; ?>
                            </select>
                    </div>

                    <button type="submit" class="btn btn-default" onclick="return searchCarValidation()"><i class="fa fa-search"></i> Search</button>
                    <?php if (isset($_GET['search_city']) && !empty($_GET['search_city'])): ?>
                        <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/city/view') ?>'"><i class="fa fa-refresh"></i> Reset</button>
                    <?php endif; ?>
                    <span class="text-red" id="ERR_SEARCH"></span>
                </form>
            </div><!-- /.box-body -->
        </div>
        <?php */?>
        
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Car Category</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($car_category_details as $car):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $car->name ?></td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="<?= base_url('admin/carcategory/details/'.$car->category_id) ?>" class="btn btn-info" title="View">
                                                <i class="fa fa-fw fa-file-text-o"></i></a>
                                            <a href="<?= base_url('admin/carcategory/edit/'.$car->category_id) ?>" class="btn btn-warning" title="Edit">
                                                <i class="fa fa-fw fa-edit"></i></a>
                                            <?php /* ?><a href="<?= base_url('admin/carcategory/view') ?>" onclick="return deleteConfirm();" class="btn btn-danger" title="Delete">
                                                <i class="fa fa-fw fa-trash-o"></i></a><?php */ ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

