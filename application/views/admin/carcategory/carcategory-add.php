<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-car"></i> Car Management</a></li>
            <li class="active">Add Car Category</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add Car Category</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        
                        <div class="form-group">
                            <label for="model_name" class="col-sm-2 control-label">Category Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="category_name" id="category_name" placeholder="Category Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_NAME"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="default_image" class="col-sm-2 control-label">Select Default Car Image:</label>
                            <div class="col-sm-4">
                                <img src="<?= base_url("assets/admin/images/no-image.png") ?>" id="default_image_preview" style="height: 150px" class="img img-thumbnail" />
                                <input type="file" class="form-control" name="default_image" onchange="previewImage(this,'default_image_preview')" id="default_image"/>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="category_logo" class="col-sm-2 control-label">Select Logo:</label>
                            <div class="col-sm-4">
                                <img src="<?= base_url("assets/admin/images/no-image.png") ?>" id="category_logo_preview" style="height: 100px; background-position: center center;" class="img img-thumbnail" />
                                <input type="file" class="form-control" name="category_logo" onchange="previewImage(this,'category_logo_preview')" id="category_logo"/>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME"></p>
                        </div>

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success" onclick=""><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/car/view') ?>'"><i class="fa fa-fw fa-angle-left"></i> Cancel</button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->

                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function validateAddCar()
    {	
	//var a = [0-9]*[.][0-9]+$;
        var err_flag = false;
        
        // Model Name Validation
        var model_name = document.getElementById("model_name").value.trim();
        if (model_name == "") {
            document.getElementById("ERR_MODEL_NAME").innerHTML = "Please Enter Model Name.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_MODEL_NAME").innerHTML = "";
        }
        
        if (err_flag == true)
            return false;
        else
            return true;
    }
</script>
