<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-bank"></i> Company Management</a></li>
            <li class="active">View Company</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View Companies</h3>
            </div>
            <div class="box-body">
                <!--                <div class="box box-info">
                                    <div class="box-body">
                                        <form>
                                            <div class="row">
                                            <div class="form-group-sm col-md-3">
                                                <label>Name: </label>
                                                <input type="text" name="search_name" id="search_name" class="form-control" />
                                            </div>
                                            <div class="form-group-sm col-md-3">
                                                <label for="city_id">City:</label>
                                                <select class="form-control" name="search_city" id="search_city" placeholder="Select City">
                                                    <option value="">Select City</option>
                <?php foreach ($city_list as $city) : ?>
                                                            <option value="<?= $city->city_id ?>"><?= $city->name ?></option>
                <?php endforeach; ?>
                                                </select>
                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>-->
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Added Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($company_details as $compnay):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $compnay->name ?></td>
                                    <td><?= $compnay->address ?></td>
                                    <td><?= $compnay->city ?></td>
                                    <td><?= $compnay->add_datetime ?></td>
                                    <td>
                                        <?php if ($compnay->status == 1): ?>
                                            <input type="checkbox" checked onchange="return chageStatus('<?= base_url('admin/company/changestatus/' . $compnay->company_id) ?>', 'status_<?= $compnay->company_id ?>', 0)" id="status_<?= $compnay->company_id ?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php else: ?>
                                            <input type="checkbox" onchange="return chageStatus('<?= base_url('admin/company/changestatus/' . $compnay->company_id) ?>', 'status_<?= $compnay->company_id ?>', 1)" id="status_<?= $compnay->company_id ?>" data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" data-on="Active" data-off="Inactive"/>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="<?= base_url('admin/company/details/' . $compnay->company_id) ?>" class="btn btn-info" title="View">
                                                <i class="fa fa-fw fa-file-text-o"></i>
                                            </a>
                                            <a href="<?= base_url('admin/company/edit/' . $compnay->company_id) ?>" class="btn btn-warning" title="Edit">
                                                <i class="fa fa-fw fa-edit"></i>
                                            </a>
                                            <a href="<?= base_url('admin/company/delete/' . $compnay->company_id) ?>" onclick="return deleteConfirm();" class="btn btn-danger" title="Delete">
                                                <i class="fa fa-fw fa-trash-o"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

