<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-bank"></i> Company Management</a></li>
            <li class="active">Company Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add Company</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Company Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="name" value="<?= $company_details->name ?>" <?= @$disable ?> id="name" placeholder="Name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Address:</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" name="address" id="address" placeholder="Address" <?= @$disable ?>><?= $company_details->address ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">City:</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="city_id" id="city_id" placeholder="Select City" <?= @$disable ?>>
                                    <option value="0">Select City</option>
                                    <?php foreach ($city_list as $city) : ?>
                                        <option <?php if ($company_details->city_id == $city->city_id) echo "selected" ?> value="<?= $city->city_id ?>"><?= $city->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="zipcode" class="col-sm-2 control-label">Zip Code:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="zipcode" value="<?= $company_details->zipcode ?>" id="zipcode" <?= @$disable ?> placeholder="Zip Code"/>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <?php if ($action == 'edit'): ?>
                                    <button type="submit" class="btn btn-success" onclick="return editCompnayValidation();"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <?php endif; ?>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/company/view') ?>'"><i class="fa fa-fw fa-angle-left"></i> Back</button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function editCompnayValidation()
    {
        var err_flag = false;

        // Compnay Name Validation
        var name = document.getElementById("name").value.trim();
        if (name == "") {
            document.getElementById("ERR_NAME").innerHTML = "Please Enter Company Name.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_NAME").innerHTML = "";
        }

        // Compnay Address Validation
        var address = document.getElementById("address").value.trim();
        if (address == "") {
            document.getElementById("ERR_ADDRESS").innerHTML = "Please Enter Address.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_ADDRESS").innerHTML = "";
        }

        // City Validation
        var city_id = document.getElementById("city_id").value;
        if (city_id == "") {
            document.getElementById("ERR_CITY").innerHTML = "Please Select City.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_CITY").innerHTML = "";
        }

        // Compnay Address Validation
        var zipcode = document.getElementById("zipcode").value.trim();
        if (zipcode == "") {
            document.getElementById("ERR_ZIPCODE").innerHTML = "Please Enter Zip Code.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_ZIPCODE").innerHTML = "";
        }

        if (err_flag == true)
            return false;
        else
            return true;
        return true;
    }
</script>