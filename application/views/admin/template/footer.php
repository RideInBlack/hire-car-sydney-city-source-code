<?php
/**
 * @Developer Virag Shah
 */
?>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <!--<b>Version</b> 1.0-->
    </div>
    <strong>Copyright &copy; <?= date("Y", time()) ?> <a href="#">Hire Car sydney City</a>.</strong>
</footer>

</div><!-- ./wrapper -->


<!-- Bootstrap 3.3.2 JS -->
<script type="text/javascript" src="<?= base_url('assets/admin') ?>/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?= base_url('assets/admin') ?>/js/bootbox.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/admin') ?>/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- Select2 JS -->
<script type="text/javascript" src="<?= base_url('assets/admin') ?>/js/select2.full.min.js"></script>
<!-- Theme App -->
<script type="text/javascript" src="<?= base_url('assets/admin') ?>/js/app.min.js"></script>

<!-- Custom JS -->
<script type="text/javascript" src="<?= base_url('assets/admin') ?>/js/custom.js"></script>
<!-- Custom JS -->
<script type="text/javascript" src="<?= base_url('assets/admin') ?>/js/common.js"></script>

<!-- Bootstrap Toggle -->
<script type="text/javascript" src="<?= base_url('assets/admin') ?>/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>

<!-- CK Editor -->
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js" type="text/javascript"></script>
<?php /* ?><!-- SlimScroll -->
  <script type="text/javascript" src="<?= base_url('assets/admin') ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script type="text/javascript" src="<?= base_url('assets/admin') ?>/plugins/fastclick/fastclick.js"></script>\
  <!-- AdminLTE for demo purposes -->
  <script type="text/javascript" src="<?= base_url('assets/admin') ?>/js/demo.js"></script>
  <?php */ ?>
  
<script type="text/javascript">
     $(document).ready( function () {
        $('#driverTable').DataTable();
     } );
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
		
		// Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
		CKEDITOR.replace('editor1');

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                  ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                  },
                  startDate: moment().subtract(29, 'days'),
                  endDate: moment()
                },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
      });
    </script>
    
</body>
</html>
