<?php
/**
 * @Developer Virag Shah
 */

$controller = $this->router->fetch_class();
$action = $this->router->fetch_method();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title><?= @SITE_TITLE ?> | <?= @$title ?></title>

        <!-- Bootstrap 3.3.4 -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin') ?>/css/bootstrap.min.css"/>
        <!-- DATA TABLES -->
        <link href="<?= base_url('assets/admin') ?>/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin') ?>/css/font-awesome.min.css"/>
        <!-- Select 2 -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin') ?>/css/select2.min.css"/>
        <!-- Bootstrap Toggle -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin') ?>/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css"/>

        <!-- Theme style -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin') ?>/css/theme.min.css"/>
        <!-- Theme Skins. -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin') ?>/css/skins/skin-blue.min.css"/>
        
        <!-- Custom style -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin') ?>/css/style.css"/>
        
        <!-- jQuery 2.1.4 -->
        <script type="text/javascript" src="<?= base_url('assets/admin') ?>/js/jQuery-2.1.4.min.js"></script>
        
    </head>
    <body class="skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="javascript:void(0);" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>HC</b>SC</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Hire Car</b> Sydney City</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-gears"></i>
                                    <span class="hidden-xs">Administrator</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?= base_url('assets/admin') ?>/images/admin.png" class="img-circle" alt="User Image" />
                                        <p>
                                            Administrator - Hire Car Sydney City
                                        </p>
                                    </li>

                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?= base_url('admin') ?>/account" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?= base_url('admin/logout') ?>" onclick="return logoutConfirm();" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </nav>
            </header>
            <!-- =============================================== -->
            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">

                        <!-- Dashboard -->
                        <li <?php if ($controller == "dashboard") echo 'class="active"'; ?>>
                            <a href="<?= base_url('admin/dashboard') ?>">
                                <i class="fa  fa-th-large"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <!-- For Company Management -->
                        <?php /* ?><li class="treeview <?php if ($controller == "company") echo 'active'; ?>">
                          <a href="#">
                          <i class="fa fa-bank"></i> <span>Company Management</span><i class="fa fa-angle-left pull-right"></i>
                          </a>
                          <ul class="treeview-menu">
                          <li <?php if ($controller == 'company' && $action == 'view') echo 'class="active"'; ?>>
                          <a href="<?= base_url('admin/company/view') ?>"><i class="fa fa-circle-o"></i> View Companies</a>
                          </li>
                          <li <?php if ($controller == 'company' && $action == 'add') echo 'class="active"'; ?>>
                          <a href="<?= base_url('admin/company/add') ?>"><i class="fa fa-circle-o"></i> Add Company</a>
                          </li>
                          </ul>
                          </li><?php */ ?>
                        <!-- Admin User Management -->
                        <li class="treeview <?php if ($controller == 'user') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-user"></i> <span>Admin User Management</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li <?php if ($controller == 'user' && $action == 'view') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/user/view') ?>"><i class="fa fa-circle-o"></i> View Admin Users</a>
                                </li>
                                <?php if(is_super_admin()): ?>
                                <li <?php if ($controller == 'user' && $action == 'add') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/user/add') ?>"><i class="fa fa-circle-o"></i> Add Admin User</a>
                                </li>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <!-- Driver Management -->
                        <li class="treeview <?php if ($controller == 'driver') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-taxi"></i> <span>Driver Management</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li <?php if ($controller == 'driver' && $action == 'view') echo 'class="active"'; ?>><a href="<?= base_url('admin/driver/view') ?>">
                                        <i class="fa fa-circle-o"></i> View Drivers</a>
                                </li>
                                <li <?php if ($controller == 'driver' && $action == 'add') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/driver/add') ?>"><i class="fa fa-circle-o"></i> Add Driver</a>
                                </li>
                            </ul>
                        </li>
                        <!-- Rider Management -->
                        <li class="treeview <?php if ($controller == 'rider') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-street-view"></i> <span>Rider Management</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li <?php if ($controller == 'rider' && $action == 'view') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/rider/view') ?>"><i class="fa fa-circle-o"></i> View Riders</a>
                                </li>
                                <li <?php if ($controller == 'rider' && $action == 'add') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/rider/add') ?>"><i class="fa fa-circle-o"></i> Add Rider</a>
                                </li>
                            </ul>
                        </li>
                        <!-- City Management -->
                        <li class="treeview <?php if ($controller == 'city') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-map-signs"></i> <span>City Management</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li <?php if ($controller == 'city' && $action == 'view') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/city/view') ?>"><i class="fa fa-circle-o"></i> View City</a>
                                </li>
                                <li <?php if ($controller == 'city' && $action == 'add') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/city/add') ?>"><i class="fa fa-circle-o"></i> Add City</a>
                                </li>
                            </ul>
                        </li>
                        <!-- Gift Card Management -->
                        <li class="treeview <?php if ($controller == 'giftcard') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-gift"></i> <span>Gift Card Management</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li <?php if ($controller == 'giftcard' && $action == 'view') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/giftcard/view') ?>"><i class="fa fa-circle-o"></i> View Gift Card</a>
                                </li>
                                <li <?php if ($controller == 'giftcard' && $action == 'add') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/giftcard/add') ?>"><i class="fa fa-circle-o"></i> Add Gift Card</a>
                                </li>
                                <li <?php if ($controller == 'giftcard' && $action == 'printcoupon') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/giftcard/printcoupon') ?>"><i class="fa fa-circle-o"></i> Print Coupon</a>
                                </li>
                                
                                <li <?php if ($controller == 'giftcard' && $action == 'registered') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/giftcard/registered') ?>"><i class="fa fa-circle-o"></i> View Registered Gift Card</a>
                                </li>
                                 <li <?php if ($controller == 'giftcard' && $action == 'printed') echo 'class="active"'; ?>>
                                <a href="<?= base_url('admin/giftcard/printed') ?>"><i class="fa fa-circle-o"></i>View Printed Coupons</a>
                            </li>
                            </ul>
                        </li>
                        <!-- Car Management -->
                        <li class="treeview <?php if ($controller == 'car' || $controller == 'carcategory') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-car"></i> <span>Car Management</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li <?php if ($controller == 'car' && $action == 'view') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/car/view') ?>"><i class="fa fa-circle-o"></i> View Car</a>
                                </li>
                                <li <?php if ($controller == 'car' && $action == 'add') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/car/add') ?>"><i class="fa fa-circle-o"></i> Add Car</a>
                                </li>
                                <li <?php if ($controller == 'carcategory' && $action == 'view') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/carcategory/view') ?>"><i class="fa fa-circle-o"></i> View Car Category</a>
                                </li>
                                <li <?php if ($controller == 'carcategory' && $action == 'add') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/carcategory/add') ?>"><i class="fa fa-circle-o"></i> Add Car Category</a>
                                </li>
                            </ul>
                        </li>
                        <!-- CMS Management -->
                        <li class="treeview <?php if ($controller == 'cms') echo 'active'; ?>">
                            <a href="<?= base_url('admin/cms/view') ?>">
                                <i class="fa fa-sliders"></i> <span>CMS Management</span></i>
                            </a>
                        </li>
                        <!-- Book History Management -->
                        <li class="treeview <?php if ($controller == 'bookhistory') echo 'active'; ?>">
                            <a href="#">
                                <i class="fa fa-street-view"></i> <span>Book History Management</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li <?php if ($controller == 'bookhistory' && $action == 'waiting') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/bookhistory/waiting') ?>"><i class="fa fa-circle-o"></i> Waiting Rides</a>
                                </li>
                                <li <?php if ($controller == 'bookhistory' && $action == 'running') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/bookhistory/running') ?>"><i class="fa fa-circle-o"></i> Running Rides</a>
                                </li>
                                <li <?php if ($controller == 'bookhistory' && $action == 'completed') echo 'class="active"'; ?>>
                                    <a href="<?= base_url('admin/bookhistory/completed') ?>"><i class="fa fa-circle-o"></i> Completed Rides</a>
                                </li>
                            </ul>
                        </li>
                        <!-- Setting Management -->
                        <li class="treeview <?php if ($controller == 'setting') echo 'active'; ?>">
                            <a href="<?= base_url('admin/setting/') ?>">
                                <i class="fa fa-gear"></i> <span>Setting</span></i>
                            </a>
                        </li>
                        <?php /* ?>
                        <!-- Feedback Management -->
                        <li <?php if ($controller == 'feedback') echo 'class="active"'; ?>">
                            <a href="<?= base_url('admin/feedback/view') ?>">
                                <i class="fa fa-commenting "></i> <span>Feedback </span></i>
                            </a>
                        </li>
                        <?php */?>
                        <?php /* ?>
                        <!-- Inquiry Management -->
                        <li class="treeview <?php if ($controller == 'Inquiry') echo 'active'; ?>">
                            <a href="<?= base_url('admin/Inquiry/view') ?>">
                                <i class="fa fa-sliders"></i> <span>Inquiry Management</span></i>
                            </a>
                        </li>
                        <?php */ ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <?php if ($this->session->flashdata('ERR_MESSAGE') != ""): ?>
                <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_MESSAGE') ?>
                </div>
            <?php elseif ($this->session->flashdata('SUCC_MESSAGE') != ""): ?>
                <div class="alert alert-success alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <i class="icon fa fa-check-circle"></i> <?= $this->session->flashdata('SUCC_MESSAGE') ?>
                </div>
            <?php endif; ?>
