<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-street-view"></i> City Management</a></li>
            <li class="active">City Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Rider Details</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="city" class="col-sm-2 control-label">City Name:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="city" id="city" value="<?= $city_details->name ?>" <?= $disable ?> placeholder="City Name">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY"><?=@$_POST['ERR_CITY']?></p>
                        </div>
                        
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <?php if ($action == 'edit'): ?>
                                    <button type="submit" class="btn btn-success" onclick="return validateEditRider()"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <?php endif; ?>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/city/view') ?>'">
                                    <i class="fa fa-fw fa-angle-left"></i> Back
                                </button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript" src="<?=base_url('assets/admin/js')?>/cityValidation.js"></script>