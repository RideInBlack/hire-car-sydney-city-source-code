<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-map-signs"></i> City Management</a></li>
            <li class="active">View City</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Search</h3>
            </div>
            <div class="box-body">
                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <label for="city">City Name:</label>
                        <input type="text" class="form-control" name="search_city" id="search_city" value="<?=@$_GET['search_city']?>" placeholder="City Name">
                    </div>

                    <button type="submit" class="btn btn-default" onclick="return searchCityValidation()"><i class="fa fa-search"></i> Search</button>
                    <?php if (isset($_GET['search_city']) && !empty($_GET['search_city'])): ?>
                        <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/city/view') ?>'"><i class="fa fa-refresh"></i> Reset</button>
                    <?php endif; ?>
                    <span class="text-red" id="ERR_SEARCH"></span>
                </form>
            </div><!-- /.box-body -->
        </div>
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">View City</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($city_details as $city):
                                ?>
                                <tr>
                                    <td><?= $sr++; ?></td>
                                    <td><?= $city->name ?></td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="<?= base_url('admin/city/edit/' . $city->city_id) ?>" class="btn btn-warning" title="Edit">
                                                <i class="fa fa-fw fa-edit"></i></a>
                                            <a href="<?= base_url('admin/city/delete/' . $city->city_id) ?>" onclick="return deleteConfirm();" class="btn btn-danger" title="Delete">
                                                <i class="fa fa-fw fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function searchCityValidation()
    {
        var search_city = document.getElementById("search_city").value.trim();
        
        if(search_city == "")
        {
            document.getElementById("ERR_SEARCH").innerHTML = "Please Enter Keyword to Search.";
            return false;
        }
        else {
            return true;
        }
    }
</script>