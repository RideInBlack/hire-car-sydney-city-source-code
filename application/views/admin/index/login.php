<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <title><?=SITE_TITLE?> | Log in</title>

        <!-- Bootstrap 3.3.4 -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets') ?>/admin/css/bootstrap.min.css"/>
        <!-- Font Awesome Icons -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets') ?>/admin/css/font-awesome.min.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets') ?>/admin/css/theme.min.css"/>
    </head>
    <body class="login-page">
        <div class="login-box">
            <div class="login-logo"> <b></b>Hire Car Sydney City</div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg"><b>Admin</b> : Sign in</p>
                <form action="" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" name="username" id="username" class="form-control" placeholder="Username" />
                        <span class="glyphicon glyphicon-user form-control-feedback"></span> </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" />
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span> </div>
                    <p class="text-center" id="ERR_MSG"></p>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <a href="<?=base_url('admin/forgotpassword')?>">I forgot my password</a><br>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" onclick="return formValidation();" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                        <!-- /.col --> 
                    </div>
                </form>

                <!-- /.login-box-body --> 
            </div>
            <!-- /.login-box --> 
            
            <?php //Print Error Message  ?>
            <?php if($this->session->flashdata('ERR_MESSAGE') != ""): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="icon fa fa-warning"></i> <?=$this->session->flashdata('ERR_MESSAGE')?>
            </div>
            <?php elseif($this->session->flashdata('SUC_MESSAGE') != ""): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="icon fa fa-check-circle"></i> <?=$this->session->flashdata('SUC_MESSAGE')?>
            </div>
            <?php endif; ?>
            
        </div>
        <!-- jQuery 2.1.4 --> 
        <script type="text/javascript" src="<?= base_url('assets') ?>/admin/js/jQuery-2.1.4.min.js"></script> 
        <!-- Bootstrap 3.3.2 JS --> 
        <script type="text/javascript" src="<?= base_url('assets') ?>/admin/js/bootstrap.min.js"></script>
    </body>
</html>

<script>
    function formValidation()
    {
        var err_flag = false;
        var username = document.getElementById("username").value.trim();
        var password = document.getElementById("password").value;
        
        if(username == "" && password == "")
        {
            document.getElementById("ERR_MSG").innerHTML = "<code>Please Enter Username and Password.</code>";
            err_flag = true;
        }
        else
        {
            if(username == "")
            {
                document.getElementById("ERR_MSG").innerHTML = "<code>Please Enter Username.</code>";
                err_flag = true;
            }
            else if(password == "")
            {
                document.getElementById("ERR_MSG").innerHTML = "<code>Please Enter Password.</code>";
                err_flag = true;
            }
            else
            {
                document.getElementById("ERR_MSG").innerHTML = "";
            }
        }
        
        if(err_flag == true)
            return false;
        else
            return true;
    }
    
    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").alert('close');
    });
</script>