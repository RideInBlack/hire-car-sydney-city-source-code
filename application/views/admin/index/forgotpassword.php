<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <title><?= SITE_TITLE ?> | Forgot Password</title>

        <!-- Bootstrap 3.3.4 -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets') ?>/admin/css/bootstrap.min.css"/>
        <!-- Font Awesome Icons -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets') ?>/admin/css/font-awesome.min.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets') ?>/admin/css/theme.min.css"/>
    </head>
    <body class="login-page">
        <div class="login-box">
            <div class="login-logo"> <b></b>Hire Car Sydney City</div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg"><b>Admin</b> : Forgot Password</p>
                <form action="" method="post">
                    <div class="form-group has-feedback">
                        <input type="email" name="email" id="email" class="form-control" placeholder="Email" />
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span> </div>
                    <p class="text-center" id="ERR_MSG"></p>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <a href="<?= base_url('admin/') ?>">Back to Login</a><br>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" onclick="return formValidation()" class="btn btn-primary btn-block btn-flat">Submit</button>
                        </div>
                        <!-- /.col --> 
                    </div>
                </form>
                <!-- /.login-box-body --> 
            </div>
            <!-- /.login-box --> 

            <?php //Print Error Message  ?>
            <?php if ($this->session->flashdata('ERR_MESSAGE') != ""): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_MESSAGE') ?>
                </div>
            <?php endif; ?>
        </div>
        <!-- jQuery 2.1.4 -->
        <script type="text/javascript" src="<?= base_url('assets') ?>/admin/js/jQuery-2.1.4.min.js"></script> 
        <!-- Bootstrap 3.3.2 JS --> 
        <script type="text/javascript" src="<?= base_url('assets') ?>/admin/js/bootstrap.min.js"></script> 

    </body>
</html>

<script>
    function formValidation()
    {
        var err_flag = false;
        var email = document.getElementById("email").value.trim();
        
        if (email == "")
        {
            document.getElementById("ERR_MSG").innerHTML = "<code>Please Enter Email.</code>";
            err_flag = true;
        }
        else
        {
            
        }
        
        if (err_flag == true)
            return false;
        else
            return true;
    }
    
    $(".alert").fadeTo(2000, 500).slideUp(500, function() {
        $(".alert").alert('close');
    });
</script>