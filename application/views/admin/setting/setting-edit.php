<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-taxi"></i> Setting</a></li>
            <li class="active">Update Setting</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Setting</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="setting_paypal_email_id" class="col-sm-2 control-label">PayPal Email ID:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_email_id" id="setting_paypal_email_id" value="<?= $setting_details->setting_paypal_email_id; ?>" placeholder="PayPal Email Id" disabled="disabled">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>
                        <div class="form-group">
                            <label for="setting_paypal_api_username	" class="col-sm-2 control-label">PayPal API Username:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_api_username	" id="setting_paypal_api_username	" value="<?= $setting_details->setting_paypal_api_username; ?>" placeholder="PayPal API Username" disabled="disabled">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>
                        <div class="form-group">
                            <label for="setting_paypal_password" class="col-sm-2 control-label">PayPal API Password:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_password" id="setting_paypal_password" value="<?= $setting_details->setting_paypal_password; ?>" placeholder="PayPal API Password" disabled="disabled">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>
                        <div class="form-group">
                            <label for="setting_paypal_signature" class="col-sm-2 control-label">PayPal API Signature:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_signature" id="setting_paypal_signature" value="<?= $setting_details->setting_paypal_signature; ?>" placeholder="PayPal Signature" disabled="disabled">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_FNAME"></p>
                        </div>
                        
                        
                        
                       
                           <p class="col-sm-2 text-red" id=""></p>
                           <p class="col-sm-4 text-red" id="ERR_EDITOR1"></p>
                           
                            
                     
                        

                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class='row'>
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success" onclick="return validateCMS();"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-default" onclick="location.href = '<?= base_url('admin/setting') ?>'"><i class="fa fa-fw fa-angle-left"></i> Back</button>
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function validateCMS()
    {
        var err_flag = false;

        // Mobile Number Validation
        var content = document.getElementById("editor1").value.trim();
		
        if (CKEDITOR.instances.editor1.getData() == '' )
		{
            document.getElementById("ERR_EDITOR1").innerHTML = "Please Enter CMS Content.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_EDITOR1").innerHTML = "";
        }

        if (err_flag == true)
            return false;
        else
            return true;
    }
</script>
