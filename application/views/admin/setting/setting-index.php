<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-taxi"></i> Setting</a></li>
            <li class="active">Update Setting</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Setting</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST">
                    <div class="box-body">
                    
                     <div class="form-group">
                            <label for="abn" class="col-md-2 control-label">ABN:</label>
                        
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="abn" id="abn" value="<?= $setting_details->setting_abn; ?>" placeholder="ABN" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_ABN"></p>
                    </div>
                     
                     <div class="form-group">
                            <label for="paypal mode" class="col-md-2 control-label">PayPal and eWay Mode:</label>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="setting_paypal_mode" id="setting_paypal_mode">
                                    <option value="test" <?php if($setting_details->setting_paypal_mode == 'test') { ?> selected="selected" <?php } ?>>Test</option>
                                    <option value="live" <?php if($setting_details->setting_paypal_mode == 'live') { ?> selected="selected" <?php } ?>>Live</option>
                                </select>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY"></p>
                    </div>
                    
                    <h4 class="box-title">Test Mode </h4>
                    
                        <div class="form-group">
                            <label for="setting_paypal_email_id" class="col-sm-2 control-label">PayPal Email ID:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_email_id" id="setting_paypal_email_id" value="<?= $setting_details->setting_paypal_email_id; ?>" placeholder="PayPal Email Id" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_setting_paypal_email_id"></p>
                        </div>
                        
						<div class="form-group">
                            <label for="setting_paypal_api_username" class="col-sm-2 control-label">eWay API Key:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_eway_api_key" id="setting_paypal_api_username" value="<?= $setting_details->setting_eway_api_key; ?>" placeholder="eWay API Key">
                            </div>
                            <p class="col-sm-4 text-red" id=""></p>
                        </div>
						
						<div class="form-group">
                            <label for="setting_paypal_api_username" class="col-sm-2 control-label">eWay API Pasword:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_eway_password" id="setting_paypal_api_username" value="<?= $setting_details->setting_eway_password; ?>" placeholder="eWay API Password">
                            </div>
                            <p class="col-sm-4 text-red" id=""></p>
                        </div>
						
						<?php /*?><div class="form-group">
                            <label for="setting_paypal_api_username" class="col-sm-2 control-label">PayPal API Username:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_api_username" id="setting_paypal_api_username" value="<?= $setting_details->setting_paypal_api_username; ?>" placeholder="PayPal API Username">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_setting_paypal_api_username"></p>
                        </div>
                        <div class="form-group">
                            <label for="setting_paypal_password" class="col-sm-2 control-label">PayPal API Password:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_password" id="setting_paypal_password" value="<?= $setting_details->setting_paypal_password; ?>" placeholder="PayPal API Password" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_setting_paypal_password"></p>
                        </div>
                        <div class="form-group">
                            <label for="setting_paypal_signature" class="col-sm-2 control-label">PayPal API Signature:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_signature" id="setting_paypal_signature" value="<?= $setting_details->setting_paypal_signature; ?>" placeholder="PayPal Signature" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_setting_paypal_signature"></p>
							
                        </div><?php */?>
                        
                        <!--<div class="form-group">
                            <label for="paypal mode" class="col-sm-2 control-label">PayPal Mode:</label>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="setting_paypal_mode" id="setting_paypal_mode">
                                    <option value="<?=$cities->setting_paypal_mode;?>" <?php if($setting_details->setting_paypal_mode == $cities->setting_paypal_mode) { ?> selected="selected" <?php } ?>>test</option>
                                </select>
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY"></p>
                   	 	</div>-->
                      
                      
                    <h4 class="box-title">Live Mode </h4>
                    
                        <div class="form-group">
                            <label for="setting_paypal_email_id_live" class="col-sm-2 control-label">PayPal Email ID:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_email_id_live" id="setting_paypal_email_id_live" value="<?= $setting_details->setting_paypal_email_id_live; ?>" placeholder="PayPal Email Id" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_setting_paypal_email_id_live"></p>
                        </div>
                        
						<div class="form-group">
                            <label for="setting_paypal_api_username" class="col-sm-2 control-label">eWay API Key:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_eway_api_key_live" id="setting_paypal_api_username" value="<?= $setting_details->setting_eway_api_key_live; ?>" placeholder="e-Way API Key Live">
                            </div>
                            <p class="col-sm-4 text-red" id=""></p>
                        </div>
						
						<div class="form-group">
                            <label for="setting_paypal_api_username" class="col-sm-2 control-label">eWay API Pasword:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_eway_password_live" id="setting_paypal_api_username" value="<?= $setting_details->setting_eway_password_live; ?>" placeholder="e-Way API Password Live">
                            </div>
                            <p class="col-sm-4 text-red" id=""></p>
                        </div>
						
						<?php /*?><div class="form-group">
                            <label for="setting_paypal_api_username_live" class="col-sm-2 control-label">PayPal API Username:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_api_username_live" id="setting_paypal_api_username_live" value="<?= $setting_details->setting_paypal_api_username_live; ?>" placeholder="PayPal API Username">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_setting_paypal_api_username_live"></p>
                        </div>
                        <div class="form-group">
                            <label for="setting_paypal_password_live" class="col-sm-2 control-label">PayPal API Password:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_password_live" id="setting_paypal_password_live" value="<?= $setting_details->setting_paypal_password_live; ?>" placeholder="PayPal API Password" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_setting_paypal_password_live"></p>
                        </div>
                        <div class="form-group">
                            <label for="setting_paypal_signature_live" class="col-sm-2 control-label">PayPal API Signature:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="setting_paypal_signature_live" id="setting_paypal_signature_live" value="<?= $setting_details->setting_paypal_signature_live; ?>" placeholder="PayPal Signature" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_setting_paypal_signature_live"></p>
                        </div><?php */?>
                        
                        <?php /*?><h4 class="box-title">General Fare</h4>
                         <div class="form-group">
                            <label for="min_fate" class="col-sm-2 control-label">Base Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="base_fare" id="base_fare" placeholder="Base Fare" value="<?= $setting_details->base_fare; ?>">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_base_fare"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="min_fate" class="col-sm-2 control-label">Minimum Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="min_fare" id="min_fare" placeholder="Minimum Fare" value="<?= $setting_details->min_fare; ?>">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_min_fare"></p>
                        </div>

                        <div class="form-group">
                            <label for="per_km_fare" class="col-sm-2 control-label">Per Kilometer Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="per_km_fare" id="per_km_fare" placeholder="Per Kilometer Fare" value="<?= $setting_details->per_minute_rate; ?>">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_per_km_fare"></p>
                        </div>

                        <div class="form-group">
                            <label for="per_min_fare" class="col-sm-2 control-label">Per Minute Fare:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="per_min_fare" id="per_min_fare" placeholder="Per Minute Fare" value="<?= $setting_details->	per_km_rate; ?>">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_per_min_fare"></p>
                        </div><?php */?>

                        <!--<div class="form-group">
                            <label for="driver_rent" class="col-sm-2 control-label">Driver Rent:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="driver_rent" id="driver_rent" placeholder="Driver Rent" value="<?= $setting_details->driver_rent; ?>">
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_CITY_NAME3"></p>
                        </div>-->
                      
                    <h4 class="box-title">Bank Details</h4>
                    
                        <div class="form-group">
                            <label for="bank" class="col-sm-2 control-label">Bank:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="bank" id="bank" value="<?= $setting_details->setting_bank; ?>" placeholder="Bank" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_BANK"></p>
                        </div>  
                        
                        <div class="form-group">
                            <label for="account_name" class="col-sm-2 control-label">Account Name :</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="account_name" id="account_name" value="<?= $setting_details->setting_account_name; ?>" placeholder="Account Name" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_ACCOUNT_NAME"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="bsb" class="col-sm-2 control-label">BSB:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="bsb" id="bsb" value="<?= $setting_details->setting_bsb; ?>" placeholder="BSB" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_BSB"></p>
                        </div>
                        
                        <div class="form-group">
                            <label for="account_number" class="col-sm-2 control-label">Account Number:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="account_number" id="account_number" value="<?= $setting_details->setting_account_number; ?>" placeholder="Account Number" >
                            </div>
                            <p class="col-sm-4 text-red" id="ERR_ACCOUNT_NUMBER"></p>
                        </div>
                     
                    <div class="box-footer">
                        <div class='row'>
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success" onclick="return validateSetting();"><i class="fa fa-fw fa-check"></i> Save</button>&nbsp;&nbsp;
                            </div>
                        </div>
                    </div><!-- /.box-footer -->
                </form>
            </div><!-- /.box body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    function validateSetting()
    {
		
        var err_flag = false;
		var re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        // Mobile Number Validation
		
        var setting_paypal_email_id = document.getElementById("setting_paypal_email_id").value.trim();
		var setting_paypal_api_username = document.getElementById("setting_paypal_api_username").value.trim();
		var setting_paypal_password = document.getElementById("setting_paypal_password").value.trim();
		var setting_paypal_signature = document.getElementById("setting_paypal_signature").value.trim();
		
		var setting_paypal_email_id_live = document.getElementById("setting_paypal_email_id_live").value.trim();
		var setting_paypal_api_username_live = document.getElementById("setting_paypal_api_username_live").value.trim();
		var setting_paypal_password_live = document.getElementById("setting_paypal_password_live").value.trim();
		var setting_paypal_signature_live = document.getElementById("setting_paypal_signature_live").value.trim();
		
		var base_fare = document.getElementById("base_fare").value.trim();
		var min_fare = document.getElementById("min_fare").value.trim();
		var per_km_fare = document.getElementById("per_km_fare").value.trim();
		var per_min_fare = document.getElementById("per_min_fare").value.trim();
		
		
		//Email
		if (setting_paypal_email_id == "") 
		{
            document.getElementById("ERR_setting_paypal_email_id").innerHTML = "Please Enter Email Id.";
            err_flag = true;
        }
        else if (!re_email.test(setting_paypal_email_id)) {
            document.getElementById("ERR_setting_paypal_email_id").innerHTML = "Please Enter Valid Email Id.";
			err_flag = true;
        }
        else {
            document.getElementById("ERR_setting_paypal_email_id").innerHTML = "";
        }
		
		if (setting_paypal_email_id_live == "") 
		{
            document.getElementById("ERR_setting_paypal_email_id_live").innerHTML = "Please Enter Email Id.";
            err_flag = true;
        }
        else if (!re_email.test(setting_paypal_email_id_live)) {
            document.getElementById("ERR_setting_paypal_email_id_live").innerHTML = "Please Enter Valid Email Id.";
			err_flag = true;
        }
        else {
            document.getElementById("ERR_setting_paypal_email_id_live").innerHTML = "";
        }
		
			
		
		//Username
        if (setting_paypal_api_username == "") {
            document.getElementById("ERR_setting_paypal_api_username").innerHTML = "Please Enter PayPal API Username.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_setting_paypal_api_username").innerHTML = "";
        }
		
        if (setting_paypal_api_username_live == "") {
            document.getElementById("ERR_setting_paypal_api_username_live").innerHTML = "Please Enter PayPal API Username.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_setting_paypal_api_username_live").innerHTML = "";
        }
	
		// Password
        if (setting_paypal_password == "") {
            document.getElementById("ERR_setting_paypal_password").innerHTML = "Please Enter PayPal API Password.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_setting_paypal_password").innerHTML = "";
        }
		
		if (setting_paypal_password_live == "") {
            document.getElementById("ERR_setting_paypal_password_live").innerHTML = "Please Enter PayPal API Password.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_setting_paypal_password_live").innerHTML = "";
        }
		
		//Signature
		if (setting_paypal_signature == "") {
            document.getElementById("ERR_setting_paypal_signature").innerHTML = "Please Enter PayPal API Signature.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_setting_paypal_signature").innerHTML = "";
        }
		
		if (setting_paypal_signature_live == "") {
            document.getElementById("ERR_setting_paypal_signature_live").innerHTML = "Please Enter PayPal API Signature.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_setting_paypal_signature_live").innerHTML = "";
        }
		
		// Base Fare
		if (base_fare == "") {
            document.getElementById("ERR_base_fare").innerHTML = "Please Enter Base Fare.";
            err_flag = true;
        }
        else 
		{
			if (isNaN(base_fare)) 
			{
				document.getElementById("ERR_base_fare").innerHTML = "Please Enter Valid Base Fare.";
            	err_flag = true;
			}
			else
			{
            	document.getElementById("ERR_base_fare").innerHTML = "";
			}
        }
		
		// Munimum Fare
		if (min_fare == "") {
            document.getElementById("ERR_min_fare").innerHTML = "Please Enter Munimum Fare.";
            err_flag = true;
        }
        else 
		{
			if (isNaN(min_fare)) 
			{
				document.getElementById("ERR_min_fare").innerHTML = "Please Enter Valid Munimum Fare.";
            	err_flag = true;
			}
			else
			{
            	document.getElementById("ERR_min_fare").innerHTML = "";
			}
        }
		
		// Per Kilometer Fare
		if (per_km_fare == "") {
            document.getElementById("ERR_per_km_fare").innerHTML = "Please Enter Per Kilometer Fare.";
            err_flag = true;
        }
        else 
		{
			if (isNaN(per_km_fare)) 
			{
				document.getElementById("ERR_per_km_fare").innerHTML = "Please Enter Valid Per Kilometer Fare.";
            	err_flag = true;
			}
			else
			{
            	document.getElementById("ERR_per_km_fare").innerHTML = "";
			}
        }
		
		// Per Minute Fare	
		if (per_min_fare == "") {
            document.getElementById("ERR_per_min_fare").innerHTML = "Please Enter Per Minute Fare.";
            err_flag = true;
        }
        else 
		{
			if (isNaN(per_min_fare)) 
			{
				document.getElementById("ERR_per_min_fare").innerHTML = "Please Enter Valid Per Minute Fare.";
            	err_flag = true;
			}
			else
			{
            	document.getElementById("ERR_per_min_fare").innerHTML = "";
			}
        }
		
		
		
        if (err_flag == true)
            return false;
        else
            return true;
    }
</script>
