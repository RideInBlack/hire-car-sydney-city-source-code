<?php
/*
 * For Encode Data and than put after en in 2) 
 1) https://developers.google.com/maps/documentation/javascript/examples/geometry-encodings
 2) https://developers.google.com/maps/documentation/staticmaps/intro?hl=en
 * e.g: https://maps.googleapis.com/maps/api/staticmap?size=400x400&path=fillcolor:0xAA000033%7Ccolor:0xFFFFFF00%7Cenc:encoded_data
 
 * 
 * 
 * Final https://maps.googleapis.com/maps/api/staticmap?size=400x400&path=fillcolor:0xAA000033|color:0xFFFFFF00|enc:urspEdckaPuElTsd@cHbBiKhDwIjGiKvLcOrIuIpFNtTr@eMhKmCfXaBdBy@vBsAdA
 */
?>

<html> 
    <head> 
        <title>Test</title> 
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&amp;sensor=false"></script>
        <style type="text/css"> 
            #map {width:670px;height:600px;}
        </style> 
        <script type='text/javascript'>
            function initialize() {
                var myLatlng = new google.maps.LatLng(51.65905179951626, 7.3835928124999555);
                var myOptions = {
                    zoom: 8,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                var map = new google.maps.Map(document.getElementById("map"), myOptions);

                var decodedPath = google.maps.geometry.encoding.decodePath('}~kvHmzrr@ba\\hnc@jiu@r{Zqx~@hjp@pwEhnc@zhu@zflAbxn@fhjBvqHroaAgcnAp}gAeahAtqGkngAinc@_h|@r{Zad\\y|_D}_y@swg@ysg@}llBpoZqa{@xrw@~eBaaX}{uAero@uqGadY}nr@`dYs_NquNgbjAf{l@|yh@bfc@}nr@z}q@i|i@zgz@r{ZhjFr}gApob@ff}@laIsen@dgYhdPvbIren@');
                var decodedLevels = decodeLevels("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");

                var setRegion = new google.maps.Polyline({
                    path: decodedPath,
                    levels: decodedLevels,
                    strokeColor: "#FF0000",
                    strokeOpacity: 1.0,
                    strokeWeight: 2,
                    map: map
                });
            }

            function decodeLevels(encodedLevelsString) {
                var decodedLevels = [];

                for (var i = 0; i < encodedLevelsString.length; ++i) {
                    var level = encodedLevelsString.charCodeAt(i) - 63;
                    decodedLevels.push(level);
                }
                return decodedLevels;
            }
        </script> 
    </head> 
    <body onload="initialize()"> 
        <div id="map"></div>
    </body> 
</html>