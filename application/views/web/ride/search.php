<section class="profile-list">
    <div class="container" style="max-height: 600px; overflow-y: scroll"> 
        <div class="col-lg-12">
                <h3>Select Ride</h3>
                <hr class="style-two">
            </div>
        <?php if (count($rides) > 0): ?>
            <?php foreach ($rides AS $ride): ?>
                <!-- /Profile Items BOC -->
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                        <?php if($ride->image_name != ""): ?>
                        <img alt="Modle Car" src="<?= base_url('assets/uploads') ?>/Car/<?= $ride->car_id ?>/<?= $ride->image_name ?>" class="img-border"> 
                        <?php else: ?>
                        <img alt="Modle Car" src="<?= base_url('assets/web') ?>/images/no-image.gif" class="img-border"> 
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h4 class="text-left">
                            <?= $ride->model_name ?>
                        </h4>
                        <div class="content">
                            <h5>Base Fare: </h5>
                            <h6><?= $ride->base_fare ?></h6>
                        </div>
                        <div class="content">
                            <h5>Per Minute Rate: </h5>
                            <h6><?= $ride->per_minute_rate ?></h6>
                        </div>
                        <div class="content">
                            <h5>Per Kilometer Rate: </h5>
                            <h6><?= $ride->per_km_rate ?></h6>
                        </div>
                        <label><input type="radio" name="selected-ride-id" value="<?=$ride->car_id?>"> Select Ride</label>
                    </div>
                </div>
                <hr/>
                <!-- /Profile Items EOC --> 
            <?php endforeach; ?>
        <?php else: ?>
            <div class="row">
                <div class="col-md-12">
                    No Ride Available.
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
<!-- /Profile Listing Section EOC -->
