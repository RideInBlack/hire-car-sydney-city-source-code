<!DOCTYPE HTML>
<html>
    <head>
        <!-- =================== META ==================== -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <title>:: Welcome To Hire Car Sydney City ::</title>

        <!-- =================== FAVICON ==================== -->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">

        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/web') ?>/css/bootstrap.min.css" />
        
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/web') ?>/css/font-awesome.min.css" />
    </head>
    <body>

        <div class="container center-block text-center text-black" style="margin-top: 10%;">
            <i class="fa fa-3x fa-circle-o-notch fa-pulse text-warning"></i><br/>
            <span class="text-center"><b>Please wait... </b></span><br>
            <span class="text-center">We are Processing Your Payment.</span>
        </div>
        <script>
            window.onload = function() {
                document.getElementById('frmPayPal1').submit();
            }
        </script>
    </body>
</html>