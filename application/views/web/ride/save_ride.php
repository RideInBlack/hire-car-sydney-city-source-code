<?php
// For Pay Pal Integration
// http://stackoverflow.com/questions/20783050/how-to-integrate-paypal-with-codeigniter
// 
//echo '<pre>'; print_r($_SESSION);
//echo '</pre>';
?>

<section class="inquiry">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Ride Booked</h3>
                <hr class="style-two"/>
            </div>
        </div>

        <div class="row">
            <p class="text-bold text-center" style="font-size: 36px"><i class="fa fa-check-circle-o text-success"></i> Ride Booked Successfully.</p>
        </div>
    </div>
    <!-- /Form EOC --> 
</section>
<!-- /Contact Us Section EOC -->