<!DOCTYPE HTML>
<html>
    <head>
        <!-- =================== META ==================== -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <title>:: Welcome To Hire Car Sydney City ::</title>

        <!-- =================== FAVICON ==================== -->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">

        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/web') ?>/css/bootstrap.min.css" />
        
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/web') ?>/css/font-awesome.min.css" />
    </head>
    <body>

        <div class="container center-block text-center text-black" style="margin-top: 10%;">
            <i class="fa fa-3x fa-circle-o-notch fa-pulse text-warning"></i><br/>
            <span class="text-center"><b>Please wait... </b></span><br>
            <span class="text-center">We are Redirecting  You to Payment Gateway.</span>
        </div>

        <form action='<?= $paypal_url ?>' method='post' name='frmPayPal1' id="frmPayPal1">
            <input type='hidden' name='business' value='<?= $buisness_id ?>'>
            <input type='hidden' name='cmd' value='_xclick'>
            <input type='hidden' name='item_name' value='Book Ride[Hire Car Sydney City]'>
            <!-- <input type='hidden' name='item_number' value='<?php echo $planData['id']; ?>'> -->
            <input type='hidden' name='amount' value='<?= $amount ?>'>
            <input type='hidden' name='no_shipping' value='1'>
            <input type='hidden' name='currency_code' value='AUD'>
            <input type='hidden' name='handling' value='0'>
            <input type="hidden" name="custom" value="<?= $ride_id ?>"/>
            
            <input type="hidden" name="return" value="<?= base_url('ride/paypalReturn/1') ?>">
            <input type="hidden" name="cancel_return" value="<?= base_url('ride/paypalReturn/0') ?>">
            <input type="hidden" name="notify_url" value="<?= base_url('ride/paypal_ipn?coupon_payment_id='.$coupon_payment_id.'&paymentId=' . $payment_id . '&rideId=' . $ride_id) ?>">
            
			<?php /*?>
			<input type="hidden" name="return" value="<?= base_url('ride/payment_result_new/1?coupon_payment_id='.$coupon_payment_id.'&paymentId=' . $payment_id . '&rideId=' . $ride_id) ?>">
            <input type="hidden" name="cancel_return" value="<?= base_url('ride/payment_result_new/0?paymentId=' . $payment_id) ?>">
            <input type="hidden" name="notify_url" value="<?= base_url('ride/paypal_ipn?coupon_payment_id='.$coupon_payment_id.'&paymentId=' . $payment_id . '&rideId=' . $ride_id) ?>"><?php */?>
			<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>

        <script>
            window.onload = function() {
                document.getElementById('frmPayPal1').submit();
            }
        </script>
    </body>
</html>