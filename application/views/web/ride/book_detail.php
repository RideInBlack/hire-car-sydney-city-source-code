<?php
// For Pay Pal Integration
// http://stackoverflow.com/questions/20783050/how-to-integrate-paypal-with-codeigniter
// 
//echo '<pre>'; print_r($_SESSION);
//echo '</pre>';
?>

<?php if ($ride_id != 0): ?>
    <section class="inquiry">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Booking Details</h3>
                    <hr class="style-two"/>
                </div>
            </div>

            <div class="row">
                <div class="common-form">
                    <!-- Pick up location details -->
                  <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <h6>Pick Up Details</h6>
                    </div>

                    <div class="col-lg-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <input type="text" name="pick-address" id="pick-address" value="<?= @$ride_details->pickup_location ?>" disabled="disabled" readonly="" placeholder="Pick up Address" class="form-control"/>
                            <p id="ERR_ADDRESS" class="text-red"></p>  
                        </div>
                    </div>

                    <?php /*?><div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-city" id="pick-city" value="<?= @$ride_details->pickup_city ?>" readonly="" placeholder="Pick up City" class="form-control"/>
                            <p id="ERR_CITY" class="text-red"></p>  
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-postal" id="pick-postal" value="<?= @$ride_details->pickup_postal ?>" readonly="" placeholder="Pick up Postal Code" class="form-control"/>
                            <p id="ERR_POSTAL" class="text-red"></p>  
                        </div>
                    </div><?php */?>
                    <!-- Drop off location details -->
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <h6>Drop Off Details</h6>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <input type="text" name="drop-address" id="drop-address" value="<?= @$ride_details->destination_location ?>" disabled="disabled" placeholder="Drop Off Address" readonly="" class="form-control"/>
                            <p id="ERR_ADDRESS" class="text-red"></p>  
                        </div>
                    </div>

                    <?php /*?><div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="drop-city" id="drop-city" value="<?= @$ride_details->destination_city ?>" placeholder="Drop Off City" readonly="" class="form-control"/>
                            <p id="ERR_CITY" class="text-red"></p>  
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="drop-postal" id="drop-postal" value="<?= @$ride_details->destination_postal ?>" placeholder="Drop Off Postal Code" readonly="" class="form-control"/>
                            <p id="ERR_POSTAL" class="text-red"></p>  
                        </div>
                    </div><?php */?>
                    <!-- Other Trip Details location details -->
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-up-date" id="pick-up-date" value="<?= date('d/m/Y', strtotime(@$ride_details->pickup_datetime)) ?>" placeholder="Pick-Up Date" readonly="" disabled="disabled" class="form-control datepicker"/>
                            <i class="fa fa-calendar-o"></i> 
                            <span id="ERR_pick-up-date" class="text-red"></span>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-up-time" id="pick-up-time" value="<?= date('H:i A', strtotime(@$ride_details->pickup_datetime)) ?>" placeholder="Pick-Up Time" readonly="" disabled="disabled" class="form-control timepicker" />
                            <i class="fa fa-clock-o"></i>
                            <span id="ERR_pick-up-time" class="text-red"></span>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <select id="event" name="event" readonly="" disabled="disabled" class="form-control text-black">
                                <option value="0">Type of Occasion</option>
                                <?php foreach ($event_details AS $val): ?>
                                    <option value="<?= $val->event_id ?>" <?php if (@$ride_details->event_id == $val->event_id) echo 'selected'; ?>><?= $val->event_name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span id="ERR_event" class="text-red"></span>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="number_passengers" id="number_passengers" value="<?= @$ride_details->no_of_passenger ?>" placeholder="No of Passengers" readonly="readonly" class="form-control" />
                            <p id="ERR_NO_PESSENGER" class="text-red"></p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="special_requirement" id="special_requirement" value="<?= @$ride_details->special_requirement ?>" placeholder="Special Requirement" readonly="readonly" class="form-control" />
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group" id="car-category-block">
                            <?php /* ?> onchange="getRides()" <?php */ ?>
                            <select onchange="" id="selected-car" name="selected-car" readonly="readonly" disabled="disabled" class="form-control">
                                <?php foreach ($cars AS $val): ?>
                                    <option value="<?= $val->car_id ?>" <?php
                                    if (@$ride_details->car_category_id == $val->car_id) {
                                        echo "selected";
                                    }
                                    ?> ><?= $val->model_name ?> (<?= $val->category_name ?>)</option>
                                        <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="container"> 
            <div class="row">
                <div class="col-lg-12">
                    <h3>Fare Details</h3>
                    <hr class="style-two">
                </div>
            </div>
            <!-- /Profile Items BOC -->

            <?php if (count($payment_details) > 0) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.</th>
                                        <th>Paid Amount</th>
                                        <th>Payment Type</th>
                                        <th>Payment Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($payment_details as $payment) : ?>
                                    <tr>
                                        <td><?=$i++?></td>
                                        <td><?=$payment->amount?></td>
                                        <td><?=$payment->payment_type?></td>
                                        <td><?=date('d/m/Y', strtotime($payment->payment_datetime))?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <form action="<?=  base_url('ride/payForRideNew/'.$ride_details->id)?>" method="POST">
            <div class="row">
                
                <div class="common-form">
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label>Estimated Distance</label>
                            <input type="text" name="estimated_distance" id="estimated_distance" value="<?= @$ride_details->estimated_distance ?> km" placeholder="Estimated Distance" class="form-control" readonly="" />
                            <p id="ERR_NO_PESSENGER" class="text-red"></p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label>Estimated Fare</label>
                            <input type="text" name="estimated_fare" id="estimated_fare" value="<?= @round($ride_details->estimated_fare,2) ?> AUD" placeholder="No of Passengers" class="form-control" readonly="" />
                            <p id="ERR_NO_PESSENGER" class="text-red"></p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label>Due Amount</label>
                            <?php $paid_amount = ($ride_details->paid_amount != "") ? $ride_details->paid_amount : 0 ?>
                            <?php $due_amount = $ride_details->estimated_fare - $paid_amount ?>
                            <input type="text" name="due_amount" id="due_amount" value="<?= @$due_amount ?> AUD" placeholder="No of Passengers" class="form-control" readonly="" />
                            <p id="ERR_NO_PESSENGER" class="text-red"></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php if($due_amount < $ride_details->estimated_fare): ?>
            
                <?php if($ride_details->pay_by_deposit == 1): ?>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    Bank Details
                                </div>
                                <div class="panel-body">
                                    <p><b>Bank:</b> <?=$setting_details->setting_bank?></p>
                                    <p><b>Account Name:</b> <?=$setting_details->setting_account_name?></p>
                                    <p><b>BSB:</b> <?=$setting_details->setting_bsb?></p>
                                    <p><b>Account Number:</b> <?=$setting_details->setting_account_number?></p>
                                    <p>Please deposit giftcard amount in bank for complete booking of you ride.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                <div class="row">
                    <div class="common-form">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-8 col-sm-12 col-md-8">
                                    <div class="form-group" style="margin-bottom: 0px">
                                        <input type="text" name="coupon_code" id="coupon_code" value="" placeholder="Have a Coupon Code?" class="form-control" />
                                        <p id="ERR_NO_PESSENGER" class="text-red"></p>
                                    </div>
                                </div>
                                <input type="hidden" id="isValidCoupon" value="" />
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <button type="button" id="validateCoupon" class="btn" title="Validate">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                </div>
                            </div>
                            <span id="couponCodeMessage"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                        <label class="radio-inline"><input type="radio" name="payOption" checked="checked" value="paypal" id="paypal">Pay Via PayPal</label>
                        <label class="radio-inline"><input type="radio" name="payOption" value="cc" id="cc">Pay Via Cradit Card</label>
                        <label class="radio-inline"><input type="radio" name="payOption" value="deposit" id="deposit">Deposit in bank account</label>
                        <br/>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-3 col-xs-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-full" value="Submit" onclick="paymentRedirect()" name="paymentSubmit">
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            <?php endif; ?>
            
            </form>
        </div>

        <script type="text/javascript">
            function paymentRedirect() {
                if (document.getElementById("paypal").checked == true) {
                    window.location.assign("<?= base_url('ride/payment/0/' . $ride_details->id . '/' . $due_amount) ?>");
                }
                //window.location.assign("<?= base_url('ride/save_ride') ?>");
                //document.getElementById("frmPayPal1").submit();
            }
        </script>
            
            

            <div class="row">
                <div class="col-md-12">
                    <span class="text-center" style="font-size:24px"><b>Simply call us on 1300665711 or message us on 0431722784</b></span>
                    <div class="location-map">
                        <div id="map" class="google-map"></div>
                    </div>
                </div>

                <input type="hidden" value="" id="estimated-distance" />

                <div class="clearfix"></div>
            </div>
        </div>
        <!-- /Form EOC --> 
    </section>
    <!-- /Contact Us Section EOC --> 

    <section class="profile-list" id="fare-details">
        
    </section>

<?php else: ?>
    <form method="POST" action="<?= base_url('ride/save_ride') ?>" id="select-ride-form">
        <section class="inquiry">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12">
                        <h3>Booking Details</h3>
                        <hr class="style-two"/>
                    </div>
                </div>

                <div class="row">
                    <div class="common-form">
                        <!-- Personal Details -->
                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <h6>Personal Details</h6>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <?php if(isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)) : ?>
                                    <input type="text" name="name" id="firstname" value="<?= @$_SESSION['HIRE_CAR']['RIDER']->first_name.' '.$_SESSION['HIRE_CAR']['RIDER']->last_name  ?>" placeholder="Enter Name" class="form-control" disabled="disabled"  />
                                <?php else: ?>
                                    <input type="text" name="name" id="firstname" value="<?= @$_SESSION['RIDE']['name'] ?>" placeholder="Enter Name" class="form-control" />
                                <?php endif; ?>
                                <p id="ERR_FIRSTNAME" class="text-red"></p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <?php if(isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)) : ?>
                                    <input type="tel" name="phone" id="phone" value="<?= @$_SESSION['HIRE_CAR']['RIDER']->mobile_number?>" placeholder="Enter Phone" class="form-control" disabled="disabled" />    
                                <?php else: ?>
                                    <input type="tel" name="phone" id="phone" value="<?= @$_SESSION['RIDE']['phone'] ?>" placeholder="Enter Phone" class="form-control" />  
                                <?php endif; ?>
                                <p id="ERR_PHONE" class="text-red"></p> 
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                 <?php if(isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)) : ?>
                                    <input type="email" name="email" id="email" value="<?= @$_SESSION['HIRE_CAR']['RIDER']->email_id?>" placeholder="Enter Email Address" class="form-control" disabled="disabled"  />  
                                <?php else: ?>
                                    <input type="email" name="email" id="email" value="<?= @$_SESSION['RIDE']['email'] ?>" placeholder="Enter Email Address" class="form-control" /> 
                                <?php endif; ?>
                                
                                <p id="ERR_EMAIL" class="text-red"></p>
                            </div>
                        </div>

                        <!-- Pick up location details -->
                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <h6>Pick Up Details</h6>
                        </div>

                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <input type="text" name="pick-address" id="pick-address" value="<?= @$_SESSION['RIDE']['pickup_location'] ?>" placeholder="Pick up Address" class="form-control"/>
                                <p id="ERR_ADDRESS" class="text-red"></p>  
                            </div>
                        </div>

                        <?php /*?><div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="pick-city" id="pick-city" value="<?= @$_SESSION['RIDE']['pickup_city'] ?>" placeholder="Pick up City" class="form-control"/>
                                <p id="ERR_CITY" class="text-red"></p>  
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="pick-postal" id="pick-postal" value="<?= @$_SESSION['RIDE']['pickup_postal'] ?>" placeholder="Pick up Postal Code" class="form-control"/>
                                <p id="ERR_POSTAL" class="text-red"></p>  
                            </div>
                        </div><?php */?>
                        <!-- Drop off location details -->
                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <h6>Drop Off Details</h6>
                        </div>
                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <input type="text" name="drop-address" id="drop-address" value="<?= @$_SESSION['RIDE']['destination_location'] ?>" placeholder="Drop Off Address" class="form-control"/>
                                <p id="ERR_ADDRESS" class="text-red"></p>  
                            </div>
                        </div>

                        <?php /*?><div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="drop-city" id="drop-city" value="<?= @$_SESSION['RIDE']['destination_city'] ?>" placeholder="Drop Off City" class="form-control"/>
                                <p id="ERR_CITY" class="text-red"></p>  
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="drop-postal" id="drop-postal" value="<?= @$_SESSION['RIDE']['destination_postal'] ?>" placeholder="Drop Off Postal Code" class="form-control"/>
                                <p id="ERR_POSTAL" class="text-red"></p>  
                            </div>
                        </div><?php */?>
                        <!-- Other Trip Details location details -->
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="pick-up-date" id="pick-up-date" value="<?= @$_SESSION['RIDE']['pickup_date'] ?>" placeholder="Pick-Up Date" class="form-control homedatepicker"/>
                                <i class="fa fa-calendar-o"></i> 
                                <span id="ERR_pick-up-date" class="text-red"></span>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group" id="timepick">
                                <input type="text" name="pick-up-time" id="pick-up-time" value="<?= @$_SESSION['RIDE']['pickup_time'] ?>" placeholder="Pick-Up Time" class="form-control hometimepicker" />
                                <i class="fa fa-clock-o"></i>
                                <span id="ERR_pick-up-time" class="text-red"></span>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <select id="event" name="event" class="form-control text-black">
                                    <option value="0">Type of Occasion</option>
                                    <?php foreach ($event_details AS $val): ?>
                                        <option value="<?= $val->event_id ?>" <?php if (@$_SESSION['RIDE']['event_type'] == $val->event_id || @$_GET['event'] == $val->event_id ) echo 'selected'; ?>><?= $val->event_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span id="ERR_event" class="text-red"></span>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="number_passengers" id="number_passengers" value="<?= @$_SESSION['RIDE']['number_passengers'] ?>" placeholder="No of Passengers" class="form-control" />
                                <p id="ERR_NO_PESSENGER" class="text-red"></p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="special_requirement" id="special_requirement" value="<?= @$_SESSION['RIDE']['special_requirement'] ?>" placeholder="Special Requirement" class="form-control" />
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group" id="car-category-block">
                                <?php /* ?> onchange="getRides()" <?php */ ?>
                                <select onchange="changeCarFare()" id="selected-car" name="selected-car" class="form-control">
                                    <?php foreach ($cars AS $val): ?>
                                        <option value="<?= $val->car_id ?>" id="<?=$val->min_fare?>-<?=$val->per_km_rate?>" <?php
                                        if (base64_decode(@$_GET['selected_ride']) == $val->car_id) {
                                            echo "selected";
                                        }
                                        ?>><?= $val->model_name ?> (<?= $val->category_name ?>)</option>
                                            <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                         <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                              <input type="button" name="book-now" value="Calculate Fare" id="find" class="btn btn-full" onclick="">
                            </div>
                        </div>
                        
                 
                        
                    </div>
                </div>
                
                
                
                <div class="row" id="fare-details" style="display: none">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-md-12">
                                <h3>Fare Details</h3>
                                <hr class="style-two">
                            </div>
                        </div>
                        <!-- /Profile Items BOC -->
        
                        <div class="row">
                            <div class="common-form">
                                <div class="col-lg-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label>Estimated Distance</label>
                                        <input type="text" name="estimated_distance" id="estimated_distance" value="<?= @$_SESSION['RIDE']['estimated_distance'] ?>" placeholder="Estimated Distance" class="form-control" readonly="" />
                                        <p id="ERR_NO_PESSENGER" class="text-red"></p>
                                    </div>
                                </div>
        
                                <div class="col-lg-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label>Estimated Fare</label>
                                        <input type="text" name="estimated_fare" id="estimated_fare" value="<?= @($_SESSION['RIDE']['estimated_distance'] * $setting->per_km_rate) + $setting->base_fare ?> AUD" placeholder="No of Passengers" class="form-control" readonly="" />
                                        <p id="ERR_NO_PESSENGER" class="text-red"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        
                    <div class="col-md-12">
                        <?php if (isset($_SESSION['HIRE_CAR']['RIDER'])): ?>
                            <div class="row">
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-full checkCoupon" value="Book Ride" onclick="return bookingDetailValidation()" name="bookride">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                    <p class="text-center"> <b>OR</b> </p>
                                </div>
                            </div>
                            <?php if (isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)): ?>
                                <div class="row">
                                    <div class="common-form">
                                        <div class="col-lg-12 col-sm-12 col-md-6">
                                            <div class="row">
                                                <div class="col-lg-4 col-sm-12 col-md-8">
                                                    <div class="form-group" style="margin-bottom: 0px">
                                                        <input type="text" name="coupon_code" id="coupon_code" value="" placeholder="Have a Coupon Code?" class="form-control" />
                                                        <p id="ERR_NO_PESSENGER" class="text-red"></p>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="isValidCoupon" value="" />
                                                <div class="col-lg-1 col-md-2 col-sm-2">
                                                    <button type="button" id="validateCoupon" class="btn" title="Validate">
                                                        <i class="fa fa-refresh"></i>
                                                    </button>
                                                </div>
                                                <div class="col-lg-4 col-sm-12 col-md-12">
                                                    <p style="font-size:18px">Click here to validate gift coupon.</p>
                                                </div>
                                            </div>
                                            <span id="couponCodeMessage"></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12" style="margin-bottom: 15px;">
                                    <label class="radio-inline"><input type="radio" name="payOption" checked="checked" value="paypal" id="paypal">Pay Via PayPal</label>
                                    <label class="radio-inline"><input type="radio" name="payOption" value="cc" id="cc">Pay Via Cradit Card</label>
                                    <label class="radio-inline"><input type="radio" name="payOption" value="deposit" id="deposit">Deposit in bank account</label>
                                    <br/>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-full" id="book-n-pay" value="Book & Pay" onclick="" name="paymentSubmit">
                                    </div>
                                </div>
        
                                <!--                    <div class="col-lg-4 col-sm-4 col-md-4">
                                                        <div class="form-group">
                                                            <input type="submit" class="btn btn-full checkCoupon" value="Book and Pay via PayPal" onclick="paymentRedirect()" name="book-n-pay">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                                        <div class="form-group">
                                                            <input type="submit" class="btn btn-full checkCoupon" value="Book and Pay via Cradit Card" onclick="paymentRedirectCC()" name="book-n-pay-cc">
                                                        </div>
                                                    </div>-->
                            </div>
                        </div>
        
        
                        <?php /* ?><div class="col-lg-3 col-sm-3 col-md-3">
                          <div class="form-group">
                          <input type="button" class="btn btn-full" data-toggle="modal" data-target="#paymentOption" value="Book and Pay2" name="book-n-pay2">
                          </div>
                          </div><?php */ ?>
        
                        <script type="text/javascript">
                            function paymentRedirect()
                            {
                                window.location.assign("<?= base_url('ride/save_ride') ?>");
                                //document.getElementById("frmPayPal1").submit();
                            }
                        </script>
        
                    <?php else: ?>
                        <div class="row">
                            <div class="col-lg-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-full" value="Book Ride" onclick="return bookingDetailValidation()" name="bookride">
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <button data-toggle="modal" class="btn btn-full" data-target="#loginFrom" title="Signup By Rider">Log in</button>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <button data-toggle="modal" class="btn btn-full" data-target="#myModal1" title="Signup By Rider">Register Now</button>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    </div>
                </div>
                
                <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <span class="text-center" style="font-size:20px"><b>Simply call us on 1300665711 or message us on 0431722784</b></span>
                        <div class="location-map">
                            <div id="map" class="google-map"></div>
                        </div>
                    </div>

                    <?php /* ?><div class="col-md-3">
                        

                        <div id="directions_panel" style="padding-top: 15px; padding-bottom: 15px"></div>

                        <div class="" id="select-ride" style="visibility: hidden">
                            <input type="hidden" name="start" id="form-start" value="<?= @$_SESSION['pickup_location'] ?>"/>
                              <input type="hidden" name="end" id="form-end" value="<?= @$_SESSION['destination_location'] ?>"/>
                              <input type="hidden" name="estimated-distance" id="form-estimated-distance" value=""/>
                        </div>
                    </div><?php */ ?>
                    <input type="hidden" value="" id="estimated-distance" />

                    <div class="clearfix"></div>
                </div>
                </div>

            </div>
            <!-- /Form EOC --> 
        </section>
        <!-- /Contact Us Section EOC --> 

        <?php /*?><section class="profile-list" id="fare-details" style="display: none">
            <div class="container"> 
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Fare Details</h3>
                        <hr class="style-two">
                    </div>
                </div>
                <!-- /Profile Items BOC -->

                <div class="row">
                    <div class="common-form">
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <label>Estimated Distance</label>
                                <input type="text" name="estimated_distance" id="estimated_distance" value="<?= @$_SESSION['RIDE']['estimated_distance'] ?>" placeholder="Estimated Distance" class="form-control" readonly="" />
                                <p id="ERR_NO_PESSENGER" class="text-red"></p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <label>Estimated Fare</label>
                                <input type="text" name="estimated_fare" id="estimated_fare" value="<?= @($_SESSION['RIDE']['estimated_distance'] * $setting->per_km_rate) + $setting->base_fare ?> AUD" placeholder="No of Passengers" class="form-control" readonly="" />
                                <p id="ERR_NO_PESSENGER" class="text-red"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <?php if (isset($_SESSION['HIRE_CAR']['RIDER'])): ?>
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form-group">
                                <input type="submit" class="btn btn-full checkCoupon" value="Book Ride" name="bookride">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3">
                            <p class="text-center"> <b>OR</b> </p>
                        </div>
                    </div>
                    <?php if (isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)): ?>
                        <div class="row">
                            <div class="common-form">
                                <div class="col-lg-4 col-sm-4 col-md-4">
                                    <div class="row">
                                        <div class="col-lg-10 col-sm-10 col-md-10">
                                            <div class="form-group" style="margin-bottom: 0px">
                                                <input type="text" name="coupon_code" id="coupon_code" value="" placeholder="Have a Coupon Code?" class="form-control" />
                                                <p id="ERR_NO_PESSENGER" class="text-red"></p>
                                            </div>
                                        </div>
                                        <input type="hidden" id="isValidCoupon" value="" />
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                            <button type="button" id="validateCoupon" class="btn" title="Validate">
                                                <i class="fa fa-refresh"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <span id="couponCodeMessage"></span>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12" style="margin-bottom: 15px;">
                            <label class="radio-inline"><input type="radio" name="payOption" checked="checked" value="paypal" id="paypal">Pay Via PayPal</label>
                            <label class="radio-inline"><input type="radio" name="payOption" value="cc" id="cc">Pay Via Cradit Card</label><br/>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form-group">
                                <input type="submit" class="btn btn-full" value="Submit" onclick="payment()" name="paymentSubmit">
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    function paymentRedirect()
                    {
                        window.location.assign("<?= base_url('ride/save_ride') ?>");
                        //document.getElementById("frmPayPal1").submit();
                    }
                </script>

            <?php else: ?>
                <div class="row">
                    <div class="col-lg-3 col-sm-3 col-md-3">
                        <div class="form-group">
                            <input type="submit" class="btn btn-full" value="Book Ride" name="bookride">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-md-3">
                        <div class="form-group">
                            <button data-toggle="modal" class="btn btn-full" data-target="#loginFrom" title="Signup By Rider">Log in</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-md-3">
                        <div class="form-group">
                            <button data-toggle="modal" class="btn btn-full" data-target="#myModal1" title="Signup By Rider">Register Now</button>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            </div>
        </section><?php */?>
    </form>
    <!-- /Profile Listing Section EOC -->
    
    <script>
    function bookingDetailValidation() 
    {
        <?php if(!isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)) :?>
        var bookingName = document.getElementById('firstname').value;
        var bookingPhone = document.getElementById('phone').value;
        var bookingEmail = document.getElementById('email').value;
        
        if(bookingName == "" || bookingEmail == "" || bookingPhone == "")
        {
            if(bookingName == "") {
                alert("Please Enter Name.");
                document.getElementById("firstname").focus();
                return false;
            }
            
            else if(bookingPhone == "") {
                alert("Please Enter Phone.");
                document.getElementById("phone").focus();
                return false;
            }
            else if(bookingEmail == "") {
                alert("Please Enter Email.");
                document.getElementById("email").focus();
                return false;
            }
        }
        <?php endif; ?>
        
        var bookingPickAddr = document.getElementById('pick-address').value;
        if(bookingPickAddr == "")
        {
            alert("Please Enter Pick up Address.");
            document.getElementById("pick-address").focus();
            return false;
        }
            
        var bookingDropaddr = document.getElementById('drop-address').value;
        if(bookingDropaddr == "")
        {
            alert("Please Enter Drop off Address.");
            document.getElementById("drop-address").focus();
            return false;
        }
        
        var bookingDate = document.getElementById('pick-up-date').value;
        if(bookingDate == "")
        {
            alert("Please Enter Booking Date.");
            document.getElementById("pick-up-date").focus();
            return false;
        }
        
        var bookingTime = document.getElementById('pick-up-time').value;
        if(bookingTime == "")
        {
            alert("Please Enter Booking Date.");
            document.getElementById("pick-up-time").focus();
            return false;
        }
        
        if(document.getElementById('event').selectedIndex == 0) {
            alert("Please Select Event Type.");
            document.getElementById("event").focus();
            return false;
        }
        return true;
    }
    </script>
    <script>
    /*function bookingDetailValidation() 
    {
        var bookingDate = document.getElementById('pick-up-date').value;
        var bookingTime = document.getElementById('pick-up-time').value;
        
        if(bookingDate == "")
        {
            alert("Please Enter Booking Date.");
            document.getElementById("pick-up-date").focus();
            return false;
        }
        
        if(bookingTime == "")
        {
            alert("Please Enter Booking Date.");
            document.getElementById("pick-up-time").focus();
            return false;
        }
        
        if(document.getElementById('event').selectedIndex == 0) {
            alert("Please Select Event Type.");
            document.getElementById("event").focus();
            return false;
        }
        return true;
    }*/
    </script>
    
<?php endif; ?>


<script>
$("#find").click(function (e) {
    var data = $('#select-ride-form').serializeArray();
    $.post("<?=base_url('ride/storeRideData')?>", data);
    console.log(data);
});
</script>


<script>
    $(document).ready(function() {
        $("#validateCoupon").click(function(event) {
            var coupon = $("#coupon_code").val();
            if (coupon != "") {
                var URL = "<?= base_url('giftcard/validateCoupon/') ?>";
                URL = URL + '/' + coupon;
                $.ajax({
                    url: URL,
                    success: function(response) {
                        //alert(response);
                        var json = JSON.parse(response);
                        if (json['status'] == "success") {
                            var estimatedFare = parseInt($('#estimated_fare').val());
                            if(json['payableAmount'] < estimatedFare) {
                                $("#couponCodeMessage").html("<p class='text-success'><b>Coupon code is vaild. Coupon value is $" + json['payableAmount'] + ".</b></p><p style='font-size:18px'><b>Note: Remaining Amount need to pay by the following options.</b> </p>");
                            }
                            else {
                                $("#couponCodeMessage").html("<p class='text-success'><b>Coupon code is vaild. Coupon value is $" + json['payableAmount'] + ".</b></p>");
                            }
                            
                            $("#isValidCoupon").val(json['couponId']);
                        }
                        else {
                            $("#couponCodeMessage").html("<p class='text-danger'>Coupon Code is Invalid/Exipred.</p>");
                            $("#isValidCoupon").val(0);
                        }

                        //alert(json['status']);
                    }
                });
            }
            else {
                $("#couponCodeMessage").html("<p class='text-danger'>Please Enter Coupon Code.</p>");
            }
        });
    });</script>
<script>
    $(document).ready(function() {
        $(".checkCoupon").click(function(event) {
            var couponTxt = $("#coupon_code").val();
            var couponId = $("#isValidCoupon").val();
            if (couponId == 0 && couponTxt != "") {
                return false;
            }
        });
    });</script>

<!-- ====== LOGIN MODAL  ====== -->
<div class="modal fade" id="loginFrom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Login</h4>
                    <hr class="style-two"/>
                </div>

                <div class="modal-body">
                    <form id="userlogin" action="<?= base_url('login') ?>?redirect=<?=uri_string()?>" method="post" name="login-form">
                          <div class="common-form">
                            <div class="row">

                                <div class="col-lg-12 text-center text-red"id="ERR_LOGIN1"></div>

                                <?php if ($this->session->flashdata('ERR_LOGIN') != ""): ?>
                                    <div class="col-lg-12">
                                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_LOGIN') ?>
                                        </div>
                                    </div>

                                    <script>
                                        // for open login modal if password is invalid
                                        jQuery(document).ready(function($) {
                                            $('#login').modal('show')
                                        });</script>

                                <?php endif; ?> 

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="user-selection">
                                            <div class="radio-inline">
                                                <label><input type="radio" name="select" value="rider" checked="checked" />Rider</label>
                                            </div>
                                            <?php /*?><div class="radio-inline">
                                                <label><input type="radio" name="select" value="driver" />Driver</label>
                                            </div><?php */?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="emailadd" id="emailadd1" placeholder="Enter Email" class="form-control" value="<?php
                                        if ($this->input->cookie('emailadd') != '') {
                                            echo $this->input->cookie('emailadd');
                                        } else if ($this->input->post('emailadd') != '') {
                                            echo $this->input->post('emailadd');
                                        }
                                        ?>">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password1" placeholder="Enter Password" class="form-control" value="<?php
                                        if ($this->input->cookie('password') != '') {
                                            echo $this->input->cookie('password');
                                        } else if ($this->input->post('password') != '') {
                                            echo $this->input->post('password');
                                        }
                                        ?>">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="user-selection">
                                            <label class="radio" for="rememberme">
                                                <input type="checkbox" name="rememberme" value="rememberme" id="rememberme" <?php if ($this->input->cookie('emailadd') != '') { ?> checked <?php } ?>> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="submit" id="loginBtn" onclick="return loginFormValidation();" value="Login" class="btn btn-full">
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="Javascript:void(0);" onclick="return after_login_button()" class="btn btn-full facebook" style="padding-right:15px;">Login With Facebook</a>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group"> <a class="pull-right font-medium" href="Javascript:void(0);" title="Forgot Password" data-toggle="modal" data-target="#forgotpassword">Forgot Password ?</a> </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ====== LOGIN MODAL END  ====== -->

<?php if ($ride_id != 0): ?>
<script>
    function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: {lat: -33.8674869, lng: 151.2069902},
            scrollwheel: false,
        });
        
        
        
        directionsDisplay.setMap(map);
        var onChangeHandler = function() {

            // Add Value of start and end city
            var start = $("#start").find('option:selected').attr('id');
            $("#form-start").val(start);
            var end = $("#end").find('option:selected').attr('id');
            $("#form-end").val(end);
            //---------------------------------------

            calculateAndDisplayRoute(directionsService, directionsDisplay);
        };
        //document.getElementById('start').addEventListener('change', onChangeHandler);
        //document.getElementById('end').addEventListener('change', onChangeHandler);
        window.addEventListener('load', onChangeHandler);
        //document.getElementById('find').addEventListener('click', onChangeHandler);
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var startl = document.getElementById('pick-address').value;
        var endl = document.getElementById('drop-address').value;
        directionsService.route({
            origin: startl,
            destination: endl,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                //-----------------------------------------
                var route = response.routes[0];
                var summaryPanel = document.getElementById('directions_panel');
                summaryPanel.innerHTML = '';
                // For first route, display summary information.
                summaryPanel.innerHTML = '<b>Estimated Distance: </b>' + route.legs[0].distance.text;
                //-----------------------------------------
                var distance = route.legs[0].distance.text;
                $("#form-estimated-distance").val(distance);
                // Chang as remve the middle step and search page is booking details page
                $("#estimated_distance").val(distance);

                var distance=distance.replace(/\,/g,''); 
                distance=parseFloat(distance,10);

                var fare = (parseInt(distance) * <?= $setting->per_km_rate ?>);
                fare = fare + <?= $setting->base_fare ?>;
				
				var fare = fare.toFixed(2);
				
                $("#estimated_fare").val(fare + " AUD");
                $("#fare-details").removeAttr("style")
                // ------------------------------------------------------------------------
                // Vissible Select Car Category Div
                //document.getElementById("car-category-block").style.visibility = 'visible';
                document.getElementById("select-ride").style.visibility = 'visible';
                //getRides();

            } else {
                //window.alert('Directions request failed due to ' + status);
                window.alert('Invalid Source or Destination Address might be incorect.');
            }
        });
    }
</script>
<?php else: ?>
<script>
    function changeCarFare() {
        var selectedCarFare = $( "#selected-car" ).find('option:selected').attr('id');
        var fares = selectedCarFare.split('-');
        
        var distance = $("#estimated_distance").val();
        //alert(parseFloat(distance));
        var distance=distance.replace(/\,/g,''); 
        distance=parseFloat(distance,10);

        var fare = (parseFloat(distance) * parseFloat(fares[1]));
        //alert(fare);
        fare += parseFloat(fares[0]);
        
		//alert(fare);
		
		var fare = fare.toFixed(2); 
		
        $("#estimated_fare").val(fare + " AUD");
    }
</script>
<script>
    function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: {lat: -33.8674869, lng: 151.2069902},
            scrollwheel: false,
        });
        directionsDisplay.setMap(map);
        var onChangeHandler = function() {

            // Add Value of start and end city
            var start = $("#start").find('option:selected').attr('id');
            $("#form-start").val(start);
            var end = $("#end").find('option:selected').attr('id');
            $("#form-end").val(end);
            //---------------------------------------

            calculateAndDisplayRoute(directionsService, directionsDisplay);
        };
        //document.getElementById('start').addEventListener('change', onChangeHandler);
        //document.getElementById('end').addEventListener('change', onChangeHandler);
        <?php if(isset($_SESSION['RIDE']['pickup_location']) && !empty($_SESSION['RIDE']['pickup_location']) && isset($_SESSION['RIDE']['destination_location']) && !empty($_SESSION['RIDE']['destination_location'])): ?>
            window.addEventListener('load', onChangeHandler);
        <?php endif; ?>
        document.getElementById('find').addEventListener('click', onChangeHandler);
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var startl = document.getElementById('pick-address').value;
        
        /*+ ' ' + document.getElementById('pick-city').value + ' ' + document.getElementById('pick-postal').value;*/
        var endl = document.getElementById('drop-address').value ;
        
        /*+ ' ' + document.getElementById('drop-city').value + ' ' + document.getElementById('drop-postal').value;*/
        
        directionsService.route({
            origin: startl,
            destination: endl,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                //-----------------------------------------
                var route = response.routes[0];
                //var summaryPanel = document.getElementById('directions_panel');
                //summaryPanel.innerHTML = '';
                // For first route, display summary information.
                //summaryPanel.innerHTML = '<b>Estimated Distance: </b>' + route.legs[0].distance.text;
                //-----------------------------------------
                var distance = route.legs[0].distance.text;
                $("#form-estimated-distance").val(distance);
                // Chang as remve the middle step and search page is booking details page
                
                var selectedCarFare = $( "#selected-car" ).find('option:selected').attr('id');
                var fares = selectedCarFare.split('-');
                //alert(fares[0]);
                
                $("#estimated_distance").val(distance);

                var distance=distance.replace(/\,/g,''); 
                distance=parseFloat(distance,10);

                var fare = (parseFloat(distance) * parseFloat(fares[1]));
                //alert(fare);
                fare += parseFloat(fares[0]);
				
				var fare = fare.toFixed(2);
				
                $("#estimated_fare").val(fare + " AUD");
                $("#fare-details").removeAttr("style")
                // ------------------------------------------------------------------------
                // Vissible Select Car Category Div
                //document.getElementById("car-category-block").style.visibility = 'visible';
                document.getElementById("select-ride").style.visibility = 'visible';
                //getRides();

            } else {
                //window.alert('Directions request failed due to ' + status);
                window.alert('Invalid Source or Destination Address might be incorect.');
            }
        });
    }
</script>
<?php endif; ?>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap"
         ></script>

<script>
$(document).ready(function() {
  var inputS = document.getElementById('pick-address');
        var searchBoxS = new google.maps.places.SearchBox(inputS);
  
        var inputD = document.getElementById('drop-address');
        var searchBoxD = new google.maps.places.SearchBox(inputD);
});
</script>

<?php /*?><script src="https://maps.googleapis.com/maps/api/js?&callback=initMap" 
async defer></script><?php */?>

<script>
    function loginFormValidation()
    {
        var err_flag = false;
        var emailadd = document.getElementById("emailadd1").value.trim();
        var password = document.getElementById("password1").value;
        if (emailadd == "" && password == "") {
            document.getElementById("ERR_LOGIN").innerHTML = "Please Enter Email  and Password.";
            err_flag = true;
        }
        else {
            if (emailadd == "") {
                document.getElementById("ERR_LOGIN1").innerHTML = "Please Enter Email.";
                err_flag = true;
            }
            else if (password == "") {
                document.getElementById("ERR_LOGIN1").innerHTML = "Please Enter Password.";
                err_flag = true;
            }
            else {
                document.getElementById("ERR_LOGIN1").innerHTML = "";
            }
        }

        if (err_flag == true)
            return false;
        else
            return true;
    }

    $(".alert").fadeTo(2000, 500).slideUp(500, function() {
        $(".alert").alert('close');
    });
</script>

<div class="modal fade" id="progressbar" tabindex="-1" role="dialog" aria-labelledby="progressbarLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php /*?><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php */?>
                    <h4>Please wait...</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="text-center text-black">
                        <i class="fa fa-3x fa-circle-o-notch fa-pulse text-warning"></i><br/>
                        <span class="text-center"><b>Please wait... </b></span><br>
                        <span class="text-center">We are processing your payment.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ====== Payment Option MODAL END  ====== -->.
<div class="modal fade" id="myModalCreditcard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Creadit Card Details</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <div class="alert-danger alert-dismissable" id="" style="padding-top: 10px; padding-bottom: 10px;">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <i class="icon fa fa-warning"></i> Please insert your creditcard details first before making payment.
                    </div>
                    
                    <form action="<?= base_url(); ?>riderprofile/updatecreditcard" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <input type="hidden" id="gift" name="gift" value="<?php echo base64_encode($gift_card_details->giftcard_id);?>" />
                        <div class="common-form">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Card Type</b></label><br />
                                        <select name="card_type" id="card_type" class="form-control">
                                            <option value="">Select a type</option>
                                            <option <?php if (isset($card_details->card_type) && $card_details->card_type == "American Express") { ?> selected <?php } ?> value="American Express">American Express</option>
                                            <option <?php if (isset($card_details->card_type) && $card_details->card_type == "Master") { ?> selected <?php } ?> value="Master">Master</option>
                                            <option <?php if (isset($card_details->card_type) && $card_details->card_type == "Visa") { ?> selected <?php } ?> value="Visa">Visa</option>
                                        </select>
                                        <p class="text-red" id="ERR_CARD_TYPE"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Card No.</b></label>
                                        <input type="text" name="cardno" id="cardno" placeholder="Card Number" class="form-control" value="<?php
                                        if (isset($card_details->card_no) && $card_details->card_no != '') {
                                            echo decrypt($card_details->card_no);
                                        }
                                        ?>">
                                        <p class="text-red" id="ERR_CARD_NO"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Expiry Date</b></label><br />
                                        <select name="month" id="month" class="form-control" style="width:120px;margin-right:25px;float:left;">
                                            <option value="">Month</option>
                                            <?php
                                            $current = 01;
                                            for ($i = $current; $i <= 12; $i++) {
                                                if($i < 10) {?>
                                                <option value="0<?= $i ?>" <?php if (isset($card_details->exp_month) && $card_details->exp_month == $i) { ?> selected="selected" <?php } ?>>0<?= $i ?></option>
                                                <?php } else { ?>
                                                <option value="<?= $i ?>" <?php if (isset($card_details->exp_month) && $card_details->exp_month == $i) { ?> selected="selected" <?php } ?>><?= $i ?></option>
                                                <?php }?>
                                            <?php } ?>
                                        </select>

                                        <select name="year" id="year" class="form-control" style="width:120px;">
                                            <option value="">Year</option>
                                            <?php
                                            $current = date('y');
                                            for ($i = $current; $i <= $current + 35; $i++) {
                                                ?>
                                                <option value="<?= $i ?>"  <?php if (isset($card_details->exp_year) && $card_details->exp_year == $i) { ?> selected="selected" <?php } ?>><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                        <p class="text-red" id="ERR_EXP"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>CCV No.</b></label><br />
                                        <input type="text" name="ccvno" id="ccvno" placeholder="CCV Number" class="form-control" value="<?php
                                        if (isset($card_details->ccv_no) && $card_details->ccv_no != '') {
                                            echo $card_details->ccv_no;
                                        }
                                        ?>" maxlength="3">
                                        <p class="text-red" id="ERR_CCV"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Name of Card</b></label><br />
                                        <input type="text" name="name_of_card" id="name_of_card" placeholder="Name of Card" class="form-control" value="<?php
                                        if (isset($card_details->name_of_card) && $card_details->name_of_card != '') {
                                            echo $card_details->name_of_card;
                                        }
                                        ?>">
                                        <p class="text-red" id="ERR_NAME_CARD"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="Save" value="Save & Make Payment" class="btn btn-full" onclick="return validateCard()">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#book-n-pay").click(function (e){
            
            //e.preventDefault();
            
            if(!bookingDetailValidation()) {
                return false;
            }
            
            $('#progressbar').modal('show');
            
            //$('#progressbar').modal('show');
        });
    });
</script>

<?php
/*
  <?php if (isset($car_details)): ?>
  <div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6">
  <?php if ($car_images[0]->image_name != ""): ?>
  <img alt="Modle Car" src="<?= base_url('assets/uploads') ?>/Car/<?= $car_details->car_id ?>/<?= $car_images[0]->image_name ?>" class="img-border">
  <?php else: ?>
  <img alt="Modle Car" src="<?= base_url('assets/web') ?>/images/no-image.gif" class="img-border">
  <?php endif; ?>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6">
  <h4 class="text-left">
  <?= $car_details->model_name ?>
  </h4>
  <div class="content">
  <h5>Base Fare: </h5>
  <h6><?= $car_details->base_fare ?></h6>
  </div>
  <div class="content">
  <h5>Per Minute Rate: </h5>
  <h6><?= $car_details->per_minute_rate ?></h6>
  </div>
  <div class="content">
  <h5>Per Kilometer Rate: </h5>
  <h6><?= $car_details->per_km_rate ?></h6>
  </div>
  <div class="content">
  <h5>Ride Detail: </h5>
  <h6><?= @$source ?> to <?= @$destination ?></h6>
  </div>
  <div class="content">
  <h5>Pick-up Date-Time: </h5>
  <h6><?= @$_SESSION['RIDE']['pickup_date'] ?> at <?= @$_SESSION['RIDE']['pickup_time'] ?></h6>
  </div>

  </div>
  </div>

  <hr/>
  <?php endif; ?>
  <!-- /Profile Items EOC -->
 */
?>