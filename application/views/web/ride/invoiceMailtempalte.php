<?php
//echo '<pre>'; print_r($ride_details); echo '</pre>';
?>

<!DOCTYPEhtml>
<html>
    <head>
    <metacharset="UTF-8">
        <title></title>

    </head>
    <body onLoad="">
        <div  id="container" style="width:800px;margin:0auto">
            <div  id="header" style="background-color:#000000!important;padding:10px 10px 10px 10px;webkit-print-color-adjust:exact;"><img src="<?= base_url('assets/web/images/AK-logo.jpg') ?>" height="130" style="margin-right:70px"/><img src="<?= base_url('assets/web/images/logo-lg.png') ?>" height="140" style="text-align:center;"/> <br/>
                <p style="color:#ffffff;font-family:fantasy;font-size:xx-large;text-align:center;padding-top:5px;
                   padding-bottom:5px; 
                   margin:0px;">ABN <?= $setting_details->setting_abn; ?></p>
            </div>
            <div id="details">
                <div id="bill-details" style="border-bottom:2px solid;padding:5px;">
                    <div id="detail">
                        <h2 style="display:inline;font-family:sans-serif;padding:0px;
                            margin:0px;font-weight:bold;font-size:xx-large;">Bill To: <?=$rider['name'] ?>
                        </h2>
                        <p style="font-family:sans-serif;padding-top:5px;margin:0px;padding-bottom:5px;font-size:x-large;">
                            Invoice No: <?=$ride_details->id?> </p>
                        <p style="font-family:sans-serif;padding-top:5px;margin:0px;padding-bottom:5px;font-size:x-large;">
                            Date: <?=date('d/m/Y', strtotime($ride_details->ride_request_datetime))?></p>
                            
                            <?php /* if (count($payment_details) > 0) : ?>
								<?php $payment = $payment_details[0]; ?>
                                Date: <?=date('d/m/Y', strtotime($payment->payment_datetime))?></p>
                            <?php else: ?>
                            	Date: <?=date('d/m/Y', strtotime($ride_details->ride_request_datetime))?></p>
                            <?php endif; */ ?>
                    </div>
                </div>
                <!--ItemTable-->
                <table id="item-details" style="width:100%;padding:0px;
                       margin:0px;border-collapse:collapse;margin-top:25px;font-family:sans-serif;
                       font-size:large;min-height:200px">
                    <thead style="font-weight:bold;padding:0px;margin:0px;background-color:#d3d3d3;-webkit-print-color-adjust:exact;">
                        <tr>
                            <td style="width:600px;padding:0px;margin:0px;padding:5px 5px;border:2px solid;word-spacing:2px">Description </td>
                            <td style="padding:5px 5px;margin:0px;border:2px solid;word-spacing:2px">Amount Ex GST </td>
                            <td style="padding:5px 5px;margin:0px;border:2px solid;word-spacing:2px">Amount Inc GST </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total_amount = 0 ?>
                        <?php if (count($payment_details) > 0) : ?>
                        	<tr>
                            	<td style="padding:2px 2px;margin:0px;border:2px solid;word-spacing:2px">Ride From <?=$ride_details->pickup_location?> to <?=$ride_details->destination_location?>
                                </td>
                                <td style="padding:2px 2px;border:2px solid;word-spacing:2px"></td>
                                <td style="padding:2px 2px;border:2px solid;word-spacing:2px"></td>
                            </tr>
							<?php foreach ($payment_details as $value) : ?>
                            <tr>
                                <td style="padding:2px 2px;margin:0px;border:2px solid;word-spacing:2px">
                                    <?= $value->payment_type ?>
                                </td>
                                <td style="padding:2px 2px;border:2px solid;word-spacing:2px">
                                    $<?= $value->amount ?>
                                </td>
                                <td style="padding:2px 2px;border:2px solid;word-spacing:2px">
                                    $<?= $value->amount ?>
                                </td>
                                <?php $total_amount += $value->amount ?>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                        	<tr>
                            	<td style="padding:2px 2px;margin:0px;border:2px solid;word-spacing:2px">Ride From "<?=$ride_details->pickup_location?>" to "<?=$ride_details->destination_location?>"
                                </td>
                                <td style="padding:2px 2px;border:2px solid;word-spacing:2px">
                                	$<?=$ride_details->estimated_fare?>
                                </td>
                                <td style="padding:2px 2px;border:2px solid;word-spacing:2px">
                                	$<?=$ride_details->estimated_fare?>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                <!--ItemTableTotalDetails-->
                <div id="details-footer1" style="background-color:#d3d3d3;padding:0px 10px; text-align:right;-webkit-print-color-adjust:exact;">
                    <?php /*?><p style="font-family:sans-serif;padding-top:5px;padding-bottom:5px;font-size:x-large; margin:0;">Subtotal ex GST  $<?=$ride_details->estimated_fare?> </p><?php */?>
                    <p style="font-family:sans-serif;padding-top:5px;padding-bottom:5px;font-size:x-large;  margin:0;">Total $<?=$ride_details->estimated_fare?></p>
                </div>
                <div id="details-footer2" style="	background-color:#a9a9a9;	text-align:right;	padding:0px;
                     margin:0px;padding:3px 10px;	-webkit-print-color-adjust:exact;">
                    <p style="font-family:sans-serif;padding-top:5px;padding-bottom:5px;font-size:x-large;  margin:0;"><b>Amount Paid</b> $<?=$total_amount?> </p>
                    <p style="font-family:sans-serif;padding-top:5px;padding-bottom:5px; font-size:x-large;
                       margin:0px;"><b>Balance Due </b> $<?= $ride_details->estimated_fare - $total_amount?> </p>
                </div>
                <div id="bank-details" style="padding:5px 5px;
                     margin:0px;">
                    <h2  style="font-family: sans-serif;
                         font-weight: bold; padding:0px; margin:10px 0 5px 0">Bank Details</h2>
                    <p style="font-family:sans-serif;font-size:14px;margin:0px;">
                    	Bank Name: <?= $setting_details->setting_bank; ?> </p>
                    <p style="font-family:sans-serif;font-size:14px;margin:0px;">
                    	Account Name: <?= $setting_details->setting_account_name; ?> </p>
                    <p style="font-family:sans-serif;margin:0px;">
                    	BSB: <?= $setting_details->setting_bsb; ?> </p>
                    <p style="font-family:sans-serif;font-size:14px;margin:0px;">
                    	Account: <?= $setting_details->setting_account_number; ?> </p>
                    <h2 style="font-size:18px; margin:2px 0px;">Please Use Invoice Number as a reference </h2>
                </div>
                <div id="contact-details" style="float:left; width:100%; background-color:#000000;	color:#FFFFFF; padding:0px;
                     margin:0px;text-align:center;font-family:sans-serif; font-size:16px;font-weight:bold;-webkit-print-color-adjust:exact;">
                    <div style="width:250px; float:left; padding:4px 0px;margin-right:5px;">PO BOX 450 DULWICH HILL NSW 2203 </div>
                    <div style="width:180px;float:left;padding:4px 0px;	
                         margin:0px;margin-right:5px;text-align:right">Phone:1300665711<br/>
                        0431722784 </div>
                    <div style="width:320px;padding:4px 0px;margin-right:5px;float:right;
                         margin:0px;text-align:right;margin-right:10px; color:#fff; " >Email:admin@hirecarsydneycity.com.au<br/>
                        www.hirecarsydneycity.com.au </div>
                </div>
            </div>
        </div>
    </body>
</html>
