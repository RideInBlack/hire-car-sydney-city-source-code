<?php
//echo '<pre>'; print_r($gift_card_details); 
//print_r($_SESSION);
//echo '</pre>';
?>

<div class="page-wrapper">
    <!-- ====== CONTENT BOC ====== --> 
    <!-- /Booknow Section BOC -->
    <section class="inquiry">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Gift Card Details</h3>
                    <hr class="style-two"/>
                </div>
            </div>
        </div>
        <!-- /Form EOC --> 
    </section>
    <section class="profile-list">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <img alt="Gift Card" src="<?= base_url() ?>assets/admin/GiftCardImages/<?= $gift_card_details->image_name ?>" class="img-border"> 
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="text-left"><?= $gift_card_details->giftcard_name ?></h4>
                    <hr>
                    <div class="content">
                     <span class="test-center">
Looking for the Gift and you can't think of anything?? Why don't you get one of our Gift Card for your loved one and get them to Ride in Style... We are here to pick your loved one. <a style="font-weight:bold;" href="<?=base_url()?>cms/view/TermsAndCondition">Read more</a></span><br /><br />
                        <h6><b>Price: </b>$<?= $gift_card_details->price ?></h6>
                    </div>
                    <?php if (isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)): ?>
                        <a href="javascript:void(0);" class="btn" data-toggle="modal" data-target="#paymentOption"> Pay for gift card</a>
					<?php elseif (isset($_SESSION['HIRE_CAR']['DRIVER']->driver_id)): ?>
                        <a href="javascript:void(0);" class="btn" data-toggle="modal" data-target="#driverMessage"> Pay for gift card</a>
					<?php else: ?>
                        <a href="javascript:void(0);" class="btn" data-toggle="modal" data-target="#loginGiftCard"> Pay for gift card</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
    <!-- /Booknow Section EOC --> 
</div>

<!-- ====== Payment Option MODAL  ====== -->
<div class="modal fade" id="paymentOption" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4>Select Payment Option</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body text-center">
                    <?php /*?><a href="<?=  base_url('giftcard/payment/?gift='.base64_encode($gift_card_details->giftcard_id))?>" onclick="return sureToPaymeny('<?= base_url(''); ?>" class="btn btn-full btn-gray"> Pay via Paypal</a>
                    
                    <p class="text-center">OR</p>
                    <a href="Javascript:void(0);" id="payCreditcard" class="btn btn-full btn-gray"> Pay via Credit Card</a><?php */?>
					
					<a href="<?=  base_url('giftcard/payment/?gift='.base64_encode($gift_card_details->giftcard_id))?>" onclick="return sureToPaymeny('<?= base_url(''); ?>" class="btn btn-full btn-gray"><img src="<?=base_url('assets/web/images/paypal-logo.png')?>" style="height:60px !important" /></a>
                    
                    <p class="text-center">OR</p>
                    <a href="Javascript:void(0);" id="payCreditcard" class="btn btn-full btn-gray"> <img src="<?=base_url('assets/web/images/eway-logo.png')?>" style="height:60px !important" /></a>
                    
                    <p class="text-center">OR</p>
                    <a href="Javascript:void(0);" id="depositToBank" class="btn btn-full"> Deposit in Bank Account</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ====== Driver Message MODAL  ====== -->
<div class="modal fade" id="driverMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    
                    <hr class="style-two"/>
                </div>
                <div class="modal-body text-center">
                    Please Login as a rider.
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ====== Payment Option MODAL END  ====== -->.
<div class="modal fade" id="myModalCreditcard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Creadit Card Details</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <div class="alert-danger alert-dismissable" id="" style="padding-top: 10px; padding-bottom: 10px;">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <i class="icon fa fa-warning"></i> Please insert your creditcard details first before making payment.
                    </div>
                    
                    <form action="<?= base_url(); ?>riderprofile/updatecreditcard" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                    	<input type="hidden" id="gift" name="gift" value="<?php echo base64_encode($gift_card_details->giftcard_id);?>" />
                        <div class="common-form">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Card Type</b></label><br />
                                        <select name="card_type" id="card_type" class="form-control">
                                            <option value="">Select a type</option>
                                            <option <?php if (isset($card_details->card_type) && $card_details->card_type == "American Express") { ?> selected <?php } ?> value="American Express">American Express</option>
                                            <option <?php if (isset($card_details->card_type) && $card_details->card_type == "Master") { ?> selected <?php } ?> value="Master">Master</option>
                                            <option <?php if (isset($card_details->card_type) && $card_details->card_type == "Visa") { ?> selected <?php } ?> value="Visa">Visa</option>
                                        </select>
                                        <p class="text-red" id="ERR_CARD_TYPE"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Card No.</b></label>
                                        <input type="text" name="cardno" id="cardno" placeholder="Card Number" class="form-control" value="<?php
                                        if (isset($card_details->card_no) && $card_details->card_no != '') {
                                            echo decrypt($card_details->card_no);
                                        }
                                        ?>">
                                        <p class="text-red" id="ERR_CARD_NO"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Expiry Date</b></label><br />
                                        <select name="month" id="month" class="form-control" style="width:120px;margin-right:25px;float:left;">
                                            <option value="">Month</option>
                                            <?php
                                            $current = 01;
                                            for ($i = $current; $i <= 12; $i++) {
                                                if($i < 10) {?>
                                                <option value="0<?= $i ?>" <?php if (isset($card_details->exp_month) && $card_details->exp_month == $i) { ?> selected="selected" <?php } ?>>0<?= $i ?></option>
                                                <?php } else { ?>
                                                <option value="<?= $i ?>" <?php if (isset($card_details->exp_month) && $card_details->exp_month == $i) { ?> selected="selected" <?php } ?>><?= $i ?></option>
                                                <?php }?>
                                            <?php } ?>
                                        </select>

                                        <select name="year" id="year" class="form-control" style="width:120px;">
                                            <option value="">Year</option>
                                            <?php
                                            $current = date('y');
                                            for ($i = $current; $i <= $current + 35; $i++) {
                                                ?>
                                                <option value="<?= $i ?>"  <?php if (isset($card_details->exp_year) && $card_details->exp_year == $i) { ?> selected="selected" <?php } ?>><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                        <p class="text-red" id="ERR_EXP"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>CCV No.</b></label><br />
                                        <input type="text" name="ccvno" id="ccvno" placeholder="CCV Number" class="form-control" value="<?php
                                        if (isset($card_details->ccv_no) && $card_details->ccv_no != '') {
                                            echo $card_details->ccv_no;
                                        }
                                        ?>" maxlength="3">
                                        <p class="text-red" id="ERR_CCV"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Name of Card</b></label><br />
                                        <input type="text" name="name_of_card" id="name_of_card" placeholder="Name of Card" class="form-control" value="<?php
                                        if (isset($card_details->name_of_card) && $card_details->name_of_card != '') {
                                            echo $card_details->name_of_card;
                                        }
                                        ?>">
                                        <p class="text-red" id="ERR_NAME_CARD"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="Save" value="Save & Make Payment" class="btn btn-full" onclick="return validateCard()">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ====== Payment Option MODAL  ====== -->
<div class="modal fade" id="progressbar" tabindex="-1" role="dialog" aria-labelledby="progressbarLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php /*?><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php */?>
                    <h4>Please wait...</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="text-center text-black">
                        <i class="fa fa-3x fa-circle-o-notch fa-pulse text-warning"></i><br/>
                        <span class="text-center"><b>Please wait... </b></span><br>
                        <span class="text-center">We are processing your payment.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ====== Register Coupon MODAL  ====== -->
<div class="modal fade" id="registerProgressbar" tabindex="-1" role="dialog" aria-labelledby="progressbarLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php /*?><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php */?>
                    <h4>Please wait...</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="text-center text-black">
                        <i class="fa fa-3x fa-circle-o-notch fa-pulse text-warning"></i><br/>
                        <span class="text-center"><b>Please wait... </b></span><br>
                        <span class="text-center">We are registering your coupon.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ====== LOGIN MODAL  ====== -->
<div class="modal fade" id="loginGiftCard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Login as Rider</h4>
                    <hr class="style-two"/>
                </div>

                <div class="modal-body">
                    <form id="userlogin" action="<?= base_url('login') ?>?redirect=<?=uri_string()?>" method="post" name="login-form">
                        <div class="common-form">
                            <div class="row">

                                <div class="col-lg-12 text-center text-red"id="ERR_LOGIN1"></div>

                                <?php if ($this->session->flashdata('ERR_LOGIN') != ""): ?>
                                    <div class="col-lg-12">
                                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_LOGIN') ?>
                                        </div>
                                    </div>

                                    <script>
                                        // for open login modal if password is invalid
                                        jQuery(document).ready(function($) {
                                            $('#login').modal('show')
                                        });
                                    </script>

                                <?php endif; ?> 
								
								<input type="hidden" name="select" value="rider"  />
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="emailadd" id="emailadd1" placeholder="Enter Email" class="form-control" value="<?php
                                        if ($this->input->cookie('emailadd') != '') {
                                            echo $this->input->cookie('emailadd');
                                        } else if ($this->input->post('emailadd') != '') {
                                            echo $this->input->post('emailadd');
                                        }
                                        ?>">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password1" placeholder="Enter Password" class="form-control" value="<?php
                                        if ($this->input->cookie('password') != '') {
                                            echo $this->input->cookie('password');
                                        } else if ($this->input->post('password') != '') {
                                            echo $this->input->post('password');
                                        }
                                        ?>">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="user-selection">
                                            <label class="radio" for="rememberme">
                                                <input type="checkbox" name="rememberme" value="rememberme" id="rememberme" <?php if ($this->input->cookie('emailadd') != '') { ?> checked <?php } ?>> Remember Me
                                            </label>
                                        </div>
                                    </div>
								</div>
								<div class="col-lg-6">
									<div class="form-group pull-right">
                                        <div class="form-group"> <a class="pull-right font-medium" id="registerASRider" href="Javascript:void(0);" title="Register as Rider" >Not yet registered ?</a> </div>
                                    </div>
									<script>
									$("#registerASRider").click(function (e) {
										$('#loginGiftCard').modal('hide');
									});
									</script>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="submit" onclick="return loginFormValidation();" value="Login" class="btn btn-full">
                                    </div>
                                </div>
								
								
								<script>
								function loginFormValidation()
								{
									var err_flag = false;
									var emailadd = document.getElementById("emailadd1").value.trim();
									var password = document.getElementById("password1").value;
									if (emailadd == "" && password == "") {
										document.getElementById("ERR_LOGIN1").innerHTML = "Please Enter Email  and Password.";
										err_flag = true;
									}
									else {
										if (emailadd == "") {
											document.getElementById("ERR_LOGIN1").innerHTML = "Please Enter Email.";
											err_flag = true;
										}
										else if (password == "") {
											document.getElementById("ERR_LOGIN1").innerHTML = "Please Enter Password.";
											err_flag = true;
										}
										else {
											document.getElementById("ERR_LOGIN1").innerHTML = "";
										}
									}
							
									if (err_flag == true)
										return false;
									else
										return true;
								}
							
								$(".alert").fadeTo(2000, 500).slideUp(500, function() {
									$(".alert").alert('close');
								});
							</script>
								
								<div class="col-lg-12">
                                    <div class="form-group">
                                		<a href="Javascript:void(0);" onclick="return after_login_button()" class="btn btn-full facebook" style="padding-right:15px;">Login With Facebook</a>
                                    </div>
                                </div>
								
                                <?php /*?><div class="col-lg-12">
                                    <div class="form-group">
                                		<a href="Javascript:void(0);" onclick="return after_login_button()"><img src="<?=base_url('assets/web/images/fb-btn.png')?>" style="width: 420px; height: 47px;"></a>
                                    </div>
                                </div><?php */?>
                                
                                <div class="col-lg-12">
                                    <div class="form-group"> <a class="pull-right font-medium" href="Javascript:void(0);" title="Forgot Password" data-toggle="modal" data-target="#forgotpassword">Forgot Password ?</a> </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ====== LOGIN MODAL END  ====== -->

<script type="application/javascript">
// ====================================================================================
// Pay By Gift Card Button Click
// ====================================================================================

$("#registerASRider").click(function(){
	

		$('#loginGiftCard').modal('hide');
		$(".modal-backdrop").remove();
		$("#myModal1").modal("show");
	});


$( document ).ready(function() {
	$('#payCreditcard').click(function(){
		
		$.ajax({
		  url: '<?php echo base_url('giftcard/checkforcreditcard');?>',
		  type: 'GET',
		  success: function(data) {
			//called when successful
			if(data == 'no')
			{
				$('#paymentOption').modal('hide');
				$('#myModalCreditcard').modal('show');
			} else {
				$('#paymentOption').modal('hide');
				$('#progressbar').modal('show');
				window.location.href = "<?php echo base_url('giftcard/paybycreditcard/?gift='.base64_encode($gift_card_details->giftcard_id));?>";
			}
		  },
		  error: function(e) {
			//called when there is an error
			//console.log(e.message);
		  }
		});
		
	});
});
// ====================================================================================
// Deposit in Bank Button Click
// ====================================================================================
$( document ).ready(function() {
	$('#depositToBank').click(function(){
		
		$('#paymentOption').modal('hide');
		$('#registerProgressbar').modal('show');
		
		$.ajax({
		  url: '<?php echo base_url('giftcard/depositToBank/'.$gift_card_details->giftcard_id);?>',
		  type: 'GET',
		  success: function(data) {
			var result = $.parseJSON(data);
			//called when successful
			
			if(result['status'] == 1)
			{
				$('#registerProgressbar').modal('hide');
				//alert(result['message']);
				window.location.href = "<?=base_url('pages/our_products')?>";
			}
			else if(result['status'] == 0)
			{
				$('#registerProgressbar').modal('hide');
				alert(result['message']);
				window.location.href = "<?=base_url('pages/our_products')?>";
			} 
			else if(result['status'] == -1)
			{
				$('#registerProgressbar').modal('hide');
				alert(result['message']);
				window.location.href = "<?=base_url('pages/our_products')?>";
			}
		  },
		  error: function(e) {
			//called when there is an error
			//console.log(e.message);
		  }
		});
		
	});
});
// ====================================================================================

function validateCard()
{
	var err_flag = false;
	var re_ccv = /^[0-9]{3,4}$/;

	var cardType = document.getElementById("card_type").value.trim();
	if (cardType == "" || cardType == 0) {
		document.getElementById("ERR_CARD_TYPE").innerHTML = "Please Select Valid Card Type.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_CARD_TYPE").innerHTML = "";
	}

	// Card Number Validation
	var cardnumber = document.getElementById("cardno").value.trim();
	if (cardnumber == "" || isNaN(cardnumber))
	{
		document.getElementById("ERR_CARD_NO").innerHTML = "Please Enter your Card Number.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_CARD_NO").innerHTML = "";
	}

	var ccv = document.getElementById("ccvno").value.trim();
	if (ccv == "" || isNaN(ccv))
	{
		document.getElementById("ERR_CCV").innerHTML = "Please Enter CCV Number.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_CCV").innerHTML = "";
	}

	var expiremonth = document.getElementById("month").value.trim();
	var expireyear = document.getElementById("year").value.trim();

	if (expiremonth == "" || expireyear == "")
	{
		document.getElementById("ERR_EXP").innerHTML = "Please Enter Expiry Date.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_EXP").innerHTML = "";
	}
	//alert(err_flag);
	if (err_flag == true) {
		return false;
	} else {
		$('#myModalCreditcard').modal('hide');
		$('#progressbar').modal('show');
		return true;
	}
}
</script>