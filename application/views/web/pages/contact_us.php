<!-- /Contact Us Section BOC -->
<section class="contact-us">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>contact us</h3>
                <hr class="style-two"/>
                <?php if ($this->session->flashdata('SUCC_MESSAGE') != ""): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('SUCC_MESSAGE') ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <!-- /Form BOC -->
        <div class="row">
            <form id="" action="" method="post" name="booknow-form">
                <div class="common-form">
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="firstname" id="firstname" placeholder="Enter Name" class="form-control" value="">
                            <p id="ERR_FIRSTNAME" class="text-red"></p>
                        </div>

                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="tel" name="phone" id="phone" placeholder="Enter Phone" class="form-control" value="">
                            <p id="ERR_PHONE" class="text-red"></p>
                        </div>

                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="email" id="email" placeholder="Enter Email Address" class="form-control" value="">
                            <p id="ERR_EMAIL" class="text-red"></p>
                        </div>

                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <textarea class="form-control" name="trip" id="trip" placeholder="Trip Details / Additional Information"></textarea>
                            <p id="ERR_TRIP" class="text-red"></p>
                        </div>

                    </div>
                    <div class="col-lg-12">
                        <span class="text-center " style="font-size:24px;font-weight:bold;"><b>Simply call us on 1300665711 or message us on 0431722784</b></span>
                        <div class="user-action pull-right">
                            <div class="form-group">
                                <input type="reset" name="submit" value="Reset" class="btn">
                                <input type="submit" name="submit" value="Submit" class="btn" onclick="return validateContact()">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- /Form EOC --> 

</section>
<!-- /Contact Us Section EOC --> 
<script type="text/javascript" src="<?= base_url('assets/web/js') ?>/contactValidation.js"></script>


<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="<?= base_url('assets/web') ?>/js/jquery-map.js"></script>