<!-- /Booknow Section BOC -->
<style>
.text-red {
    color:red;
}
</style>
<section class="inquiry">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Inquiry</h3>
                <hr class="style-two"/>
                
                 <?php if ($this->session->flashdata('SUCC_MESSAGE') != ""): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa fa-success"></i> <?= $this->session->flashdata('SUCC_MESSAGE') ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <!-- /Form BOC -->
        <div class="row">
            <form id="" action="" method="post" name="booknow-form">
                <div class="common-form">
                    <div class="col-lg-12">
                        <h6>Personal Details</h6>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="firstname" id="firstname" placeholder="Enter Name" class="form-control" value="">
                             <p id="ERR_FIRSTNAME" class="text-red"></p>
                        </div>
                       
                        
                    </div>
                    
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="tel" name="phone" id="phone" placeholder="Enter Phone" class="form-control" value="">	
                            <p id="ERR_PHONE" class="text-red"></p>	
                        </div>
                        
                    </div>
                    
                    
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="email" name="email" id="email" placeholder="Enter Email Address" class="form-control" value=""> 
                             <p id="ERR_EMAIL" class="text-red"></p>
                        </div>
                       
                              
                    </div>
                    <div class="col-lg-12">
                        <h6> Trip Details </h6>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-up-date" id="pick-up-date" placeholder="Pick-Up Date" class="form-control" value=""> 
                            <i class="fa fa-calendar-o"></i> 
                             <p id="ERR_PICK_UP_DATE" class="text-red"></p>
                            </div>
                    </div>
                    
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-address" id="pick-address" placeholder="Pick up Address" class="form-control" value="">
                             <p id="ERR_ADDRESS" class="text-red"></p>  
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="drop-address" id="drop-address" placeholder="Drop Off Address" class="form-control" value="">
                             <p id="ERR_DROP_ADDRESS" class="text-red"></p>
                        </div>
                        
                    </div>
                   
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                        <input type="text" name="number_passengers" id="number_passengers" placeholder="No of Passengers" class="form-control" value="">
                        <p id="ERR_NO_PESSENGER" class="text-red"></p>
                           
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="occassion" id="occassion" placeholder="Type of Occasion" class="form-control" value="">
                            <p id="ERR_OCCASSIN" class="text-red"></p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="find-us" id="find-us" placeholder="How did you hear about us?" class="form-control" value="">
                            <p id="ERR_FIND_US" class="text-red"></p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <textarea placeholder="Trip Details / Additional Information" class="form-control" name="trip" id="trip"></textarea>
                            <p id="ERR_TRIP" class="text-red"></p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="user-action pull-right">
                            <div class="form-group">
                                <input type="reset" class="btn" value="Reset" name="submit">
                                <input type="submit" class="btn" value="Submit" name="submit" onclick="return validateInquiry()">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- /Form EOC --> 
		
</section>
   <script type="text/javascript" src="<?=base_url('assets/web/js')?>/inquiryValidation.js"></script>

<!-- /Booknow Section EOC --> 