<!-- /Giftcard Section BOC -->
<section class="gift-gallery">
    <div class="container-fluid">
        <div class="row">
            <?php if($this->session->flashdata('SUCC_GIFTCARD') != "") : ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('SUCC_GIFTCARD')?>
            </div>
            <?php elseif($this->session->flashdata('ERR_GIFTCARD') != ""):?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('ERR_GIFTCARD')?>
            </div>
            <?php endif;?>
            
            <?php if($this->session->flashdata('GIFT_CARD_DEPOSIT') != "") : ?>
            <div class="col-md-12">
            <div class="panel panel-success">
  				<div class="panel-heading">
    				Gift card is register successfully.
  				</div>
                <div class="panel-body">
                	<?=$this->session->flashdata('GIFT_CARD_DEPOSIT');?>
  				</div>
			</div>
            </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h4>choose gift card</h4>
                <hr class="style-two"/>
            </div>
        </div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<p>Let's talk about what isn't just a gift card, THE Ride in Black gift card. We know that there are a myriad of choices and types of gift cards in the market but none of them is like ours. Here's a list of what you can do with this little card:</p>
			<ul class="ul-bullet">
				<li>Buy a Ride in Black</li>
				<li>Ride in luxury with our top cars</li>
				<li>Have a Limo Ride on world class limousines</li>
				<li>Get your packages delivered, from pizza to furniture</li>
			</ul>
			<p>All that said, there is no person who wouldn't like receiving a Ride in Black gift card. Give it to your loved ones, to reward your employees, as a treat for yourself – why not?. A Ride in Black Gift Card is its own reward.</p>
			</div>
		</div>
		
        <!-- /Gift Package BOC -->
        <div class="row">
            <ul class="gift-package">
                <?php foreach ($giftCardData as $getgiftCardData): ?>
                    <li class="col-lg-4 col-sm-4 col-md-4"> <a href="<?=  base_url('giftcard/details/'.$getgiftCardData->giftcard_id)?>" title="<?= $getgiftCardData->giftcard_name ?>"> <img src="<?= base_url('assets') ?>/admin/GiftCardImages/<?= $getgiftCardData->image_name ?>" alt="Drive Couple"/>
                            <div class="items">
                                <div class="items-content">
                                    <div class="price-tag text-center">$<?= $getgiftCardData->price ?></div>
                                    <h5><?= $getgiftCardData->giftcard_name ?></h5>
                                </div>
                            </div>
                        </a> </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <!-- /Gift Package EOC --> 
    </div>
</section>
<!-- /Giftcard Section EOC --> 