<!-- /Giftcard Section BOC -->

<section class="gift-gallery">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h4>Support </h4>
        <hr class="style-two"/>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <p><strong>ACCOUNT:</strong></p>
        <p>
          <?php if(isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)): ?>
            <a href="<?=base_url()?>riderprofile">Update my profile information</a> 
          <?php elseif(isset($_SESSION['HIRE_CAR']['DRIVER']->driver_id)): ?>
            <a href="<?=base_url()?>driverprofile">Update my profile information</a> 
          <?php else: ?>
            <a href="Javascript:void(0);" data-toggle="modal" data-target="#login">Update my profile information</a>
          <?php endif; ?>
		</p>
        <p>
          <?php if(isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)): ?>
            <a href="<?=base_url()?>riderprofile">Reset my password</a> 
          <?php elseif(isset($_SESSION['HIRE_CAR']['DRIVER']->driver_id)): ?>
            <a href="<?=base_url()?>driverprofile">Reset my password</a> 
          <?php else: ?>
            <a href="Javascript:void(0);" data-toggle="modal" data-target="#login">Reset my password</a> 
          <?php endif; ?>
        </p>
        <p>
          <?php if(isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)): ?>
            <a href="<?=base_url()?>riderprofile">Confirm my mobile number</a> 
          <?php elseif(isset($_SESSION['HIRE_CAR']['DRIVER']->driver_id)): ?>
            <a href="<?=base_url()?>driverprofile">Confirm my mobile number</a> 
          <?php else: ?>
            <a href="Javascript:void(0);" data-toggle="modal" data-target="#login">Confirm my mobile number</a> 
          <?php endif; ?>
        </p>
        <p>Email us at admin@hirecarsydneycity.com.au</p>
        <p><b>PAYMENT</b></p>
        <p>Promotions</p>
        <p>Payment options</p>
        <p>Update payment information</p>
        <p>For Refund email us at admin@hirecarsydneycity.com.au</p>
      </div>
    </div>
  </div>
</section>
<style>
.mytext
{
	text-align:left;
	color:#ffbf00;
}

</style>
