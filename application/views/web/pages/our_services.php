<!-- /Service Section BOC -->
<?php
    $active1 = '';
    $active2 = '';
    $active3 = '';
    $active4 = '';
    $active5 = '';
    $active6 = '';
    if($type == '' || $type == 'wedding') {
        $active1 = 'active';
    } elseif($type == 'fun') {
        $active2 = 'active';
    } elseif($type == 'corporate') {
        $active3 = 'active';
    } elseif($type == 'formal') {
        $active4 = 'active';
    } elseif($type == 'airport') {
        $active5 = 'active';
    } elseif($type == 'door') {
        $active6 = 'active';
    } else {
        $active1 = 'active';
    }
?>
<section class="gift-gallery">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h3>our-services</h3>
                <hr class="style-two"/>
            </div>
        </div>

        <!-- /Service Listing BOC -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div data-example-id="togglable-tabs" class="common-tab"> 

                        <!-- /Tab Navigation BOC -->
                        <div class="row">
                            <ul role="tablist" class="nav nav-tabs" id="myTabs">
                                <li class="<?= $active1;?> col-lg-2 col-sm-2 col-md-2 col-xs-2" role="presentation"><a aria-expanded="true" aria-controls="tab-one" data-toggle="tab" 
                                                                                                              role="tab" id="tab-one" href="#one"><span>wedding</span><i class="fa fa-car"></i></a></li>
                                <li class="<?= $active2;?> col-lg-2 col-sm-2 col-md-2 col-xs-2" role="presentation"><a aria-controls="tab-two" data-toggle="tab" id="tab-two" role="tab" href="#two">
                                        <span>fun party</span><i class="fa fa-music"></i></a></li>
                                <li class="<?= $active3;?> col-lg-2 col-sm-2 col-md-2 col-xs-2" role="presentation"><a aria-controls="tab-three" data-toggle="tab" id="tab-three" 
                                                                                                       role="tab" href="#three"><span>Corporate</span><i class="fa fa-briefcase"></i></a></li>
                                <li class="<?= $active4;?> col-lg-2 col-sm-2 col-md-2 col-xs-2" role="presentation"><a aria-controls="tab-four" data-toggle="tab" id="tab-four" role="tab" href="#four">
                                        <span>formal</span><i class="fa fa-user"></i></a></li>
                                <li class="<?= $active5;?> col-lg-2 col-sm-2 col-md-2 col-xs-2" role="presentation">
                                    <a aria-controls="tab-five" data-toggle="tab" id="tab-five" role="tab" href="#five">
                                        <span>Airport Transfer</span>
                                        <i class="fa fa-user"></i>
                                    </a>
                                </li>
                                <li class="<?= $active6;?> col-lg-2 col-sm-2 col-md-2 col-xs-2" role="presentation">
                                    <a aria-controls="tab-six" data-toggle="tab" id="tab-six" role="tab" href="#six">
                                        <span>Door to Door Pickup</span>
                                        <i class="fa fa-user"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /Tab Navigation EOC --> 

                        <!-- /Tab Content BOC -->
                        <div class="tab-content" id="myTabContent">
                            <div aria-labelledby="tab-one" id="one" class="tab-pane fade in <?= $active1;?>" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12">
                                        <h6>Wedding Car Hire</h6>
                                        <p>The wedding of your dreams isn't complete without seamless transport that ensures everything runs like clockwork.</p>
                                        
                                        <p>Are you in the process of planning a wedding? With so much to think about the dress, the venue, the seating plan it's easy to feel stressed out and overwhelmed. Luckily, you no longer need to worry about finding the right transport for your special day, because Hire Car Sydney City is on hand to help.</p>
                                                                                
                                        <p>When you rent a vehicle with Hire Car Sydney City you don't just get a beautiful, quality wedding car to enjoy on your big day, you also get the full VIP treatment from the moment you step aboard. Your own personal chauffeur will arrive to collect you at a location of your choice, with plenty of time to spare to ensure you arrive promptly in comfort and style. </p>
                                                                                
                                        <p>All of our vehicles are available to hire for just a few hours, a whole day or as long as you need. So if you want to enjoy a really special and unique vehicle on your wedding day, give us a call or email us at info@hirecarsydneycity.com.au or <b>click the link below for a quote</b></p>
                                        <p>Price Starts From $500/Day</p>
                                        <div class="user-action pull-right">
                                            <a class="btn" href="<?= base_url('') ?>ride/book_details?event=1" title=" BOOK NOW"> BOOK NOW</a>
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            <div aria-labelledby="tab-two" id="two" class="tab-pane fade in <?= $active2;?>" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12">
                                        <h6>Casual or Fun Party in Luxury Car withchauffeur Service</h6>
                                        <p>Our Service includes airport transfers, private tours, formals, hens party, and any other special occasions such as Birthdays, anniversaries, theatre trips or a special fine dining with the love one - making a special occasion that much more luxurious, indulgent and enjoyable calls for Luxury Chauffeur Drive's ultimate service.With Luxury Chauffeur Drive you are assured the comfort and convenience of travelling in the preferred marque of the most discerning travellers, executives and dignitaries. Just click the button and Book our Luxury cars with chauffeur. Our entire chauffeur is qualified to Drive HC Cars and hold Hire Car Authority Licence.</p>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <h6>You can count on us for:</h6>
                                                <ul class="common-listing">
                                                    <li>Reliable service.</li>
                                                    <li>Professional chauffeur Service</li>
                                                    <li>Luxury and latest model Cars</li>
                                                    <li>Unbeatable Price</li>
                                                    <li>Secure online payment</li>
                                                    <li>Easy Online Booking system</li>
                                                    <li>Satisfaction Guaranteed</li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                        <p>All of our vehicles are available to hire for just a few hours, a whole day or as long as you need. So if you want to enjoy a really special and unique vehicle on your wedding day, give us a call or email us at info@hirecarsydneycity.com.au or <b>click the link below for a quote</b></p>
                                        <div class="user-action pull-right">
                                            <a class="btn" href="<?= base_url('') ?>ride/book_details?event=2" title=" BOOK NOW"> BOOK NOW</a>
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            
                            <div aria-labelledby="tab-three" id="three" class="tab-pane fade in <?= $active3;?>" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12">
                                        <h6>CorporateTransfer Service</h6>
                                        <p>We offer a luxurious chauffeur driven service in luxury cars for corporate executives, VIPs and others in Sydney. With Luxury Chauffeur Drive you are assured the comfort and convenience of travelling in the preferred marque of the most discerning travellers, executives and dignitaries. Just click the button and Book our Luxury cars with chauffeur. Our entire chauffeur is qualified to Drive HC Cars and hold Hire Car Authority Licence.</p>

                                        <div class="row">
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <h6>You can count on us for:</h6>
                                                <ul class="common-listing">
                                                    <li>Reliable service.</li>
                                                    <li>Professional chauffeur Service</li>
                                                    <li>Luxury and latest model Cars</li>
                                                    <li>Unbeatable Price</li>
                                                    <li>Secure online payment</li>
                                                    <li>Easy Online Booking system</li>
                                                    <li>Satisfaction Guaranteed</li>
                                                </ul>
                                            </div>
                                        </div> 
                                        <p>All of our vehicles are available to hire for just a few hours, a whole day or as long as you need. So if you want to enjoy a really special and unique vehicle on your wedding day, give us a call or email us at info@hirecarsydneycity.com.au or <b>click the link below for a quote</b></p>  
                                        <div class="user-action pull-right">
                                            <a class="btn" href="<?= base_url('') ?>ride/book_details?event=3" title=" BOOK NOW"> BOOK NOW</a>
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            <div aria-labelledby="tab-four" id="four" class="tab-pane fade in <?= $active4;?>" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12">
                                        <h6>Formal Car Hire with chauffeur Service</h6>
                                        <p>We have several different chauffeur packages and deals that are specially designed according to a person's needs or personalised tours. For any particular requirements or orders, as in, specific destinations and routes, you should make sure those requirements are transparent at an early stage so that any additions can be taken care of to avoid any disappointments.With Luxury Chauffeur Drive you are assured the comfort and convenience of travelling in the preferred marque of the most discerning travellers, executives and dignitaries. Just click the button and Book our Luxury cars with chauffeur. Our entire chauffeur is qualified to Drive HC Cars and hold Hire Car Authority Licence.</p>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <h6>facilities</h6>
                                                <ul class="common-listing">
                                                    <li>Reliable service.</li>
                                                    <li>Professional chauffeur Service</li>
                                                    <li>Luxury and latest model Cars</li>
                                                    <li>Unbeatable Price</li>
                                                    <li>Secure online payment</li>
                                                    <li>Easy Online Booking system</li>
                                                    <li>Satisfaction Guaranteed</li>
                                                </ul>
                                            </div>
                                        </div>   
                                        <p>All of our vehicles are available to hire for just a few hours, a whole day or as long as you need. So if you want to enjoy a really special and unique vehicle on your wedding day, give us a call or email us at info@hirecarsydneycity.com.au or <b>click the link below for a quoted</b></p>
                                        <div class="user-action pull-right">
                                            <a class="btn" href="<?= base_url('') ?>ride/book_details?event=4" title=" BOOK NOW"> BOOK NOW</a>
                                            
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            
                            <div aria-labelledby="tab-five" id="five" class="tab-pane fade in <?= $active5;?>" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12">
                                        <h6>Sydney Airport Transfer With Chauffeur Service</h6>
                                        <p>Looking for a ride from or to Sydney Airport? Are you looking for professional, luxurious airport transfers for you or your family?  Make your visit to Sydney more memorable than ever and enjoy complete peace of mind with our premium Sydney Airport Transfer service! We offer door-to-door airport transfer service to and from Sydney CBD at the most cost effective rates in Hire Car Sydney City chauffeured cars. </p>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <h6>You can count on us for:</h6>
                                                <ul class="common-listing">
                                                    <li>Premium Airport Transfers</li>
                                                    <li>Reliable service</li>
                                                    <li>Professional chauffeur Service</li>
                                                    <li>Luxury and latest model Cars</li>
                                                    <li>Unbeatable Price</li>
                                                    <li>Secure online payment</li>
                                                    <li>Easy Online Booking system</li>
                                                     <li>Satisfaction Guaranteed </li>
                                                </ul>
                                            </div>
                                        </div>   
                                        <p>All of our vehicles are available to hire for just a few hours, a whole day or as long as you need. So if you want to enjoy a really special and unique vehicle on your wedding day, give us a call or email us at <a href="mailto:info@hirecarsydneycity.com.au">info@hirecarsydneycity.com.au</a> or <b>click the link below for a quoted</b></p>
                                       
                                        <?php /*?><h6>Formal Car Hire with chauffeur Service</h6>
                                        <p>We have several different chauffeur packages and deals that are specially designed according to a person's needs or personalised tours. For any particular requirements or orders, as in, specific destinations and routes, you should make sure those requirements are transparent at an early stage so that any additions can be taken care of to avoid any disappointments.With Luxury Chauffeur Drive you are assured the comfort and convenience of travelling in the preferred marque of the most discerning travellers, executives and dignitaries. Just click the button and Book our Luxury cars with chauffeur. Our entire chauffeur is qualified to Drive HC Cars and hold Hire Car Authority Licence.</p>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <h6>facilities</h6>
                                                <ul class="common-listing">
                                                    <li>Reliable service.</li>
                                                    <li>Professional chauffeur Service</li>
                                                    <li>Luxury and latest model Cars</li>
                                                    <li>Unbeatable Price</li>
                                                    <li>Secure online payment</li>
                                                    <li>Easy Online Booking system</li>
                                                    <li>Satisfaction Guaranteed</li>
                                                </ul>
                                            </div>
                                        </div>   
                                        <p>All of our vehicles are available to hire for just a few hours, a whole day or as long as you need. So if you want to enjoy a really special and unique vehicle on your wedding day, give us a call or email us at info@hirecarsydneycity.com.au or <b>click the link below for a quoted</b></p><?php */?>
                                        <div class="user-action pull-right">
                                            <a class="btn" href="<?= base_url('') ?>ride/book_details?event=5" title=" BOOK NOW"> BOOK NOW</a>
                                            
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            
                            <div aria-labelledby="tab-six" id="six" class="tab-pane fade in <?= $active6;?>" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12">
                                        <h6>Door to Door Pick Up and Drop Off Chauffeur Service</h6>
                                        <p>We offer a luxurious chauffeur driven service in luxury cars for corporate executives, VIPs and others. Just click the button and Book our Luxury cars with chauffeur. Our entire chauffeur is qualified to Drive HC Cars and hold Hire Car Authority Licence.</p>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-6 col-md-6">
                                                <h6>facilities</h6>
                                                <ul class="common-listing">
                                                    <li>Reliable service.</li>
                                                    <li>Professional chauffeur Service</li>
                                                    <li>Luxury and latest model Cars</li>
                                                    <li>Unbeatable Price</li>
                                                    <li>Secure online payment</li>
                                                    <li>Easy Online Booking system</li>
                                                    <li>Satisfaction Guaranteed</li>
                                                </ul>
                                            </div>
                                        </div>   
                                        <p>All of our vehicles are available to hire for just a few hours, a whole day or as long as you need. So if you want to enjoy a really special and unique vehicle on your wedding day, give us a call or email us at info@hirecarsydneycity.com.au or <b>click the link below for a quoted</b></p>
                                        <div class="user-action pull-right">
                                            <a class="btn" href="<?= base_url('') ?>ride/book_details?event=6" title=" BOOK NOW"> BOOK NOW</a>
                                            
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!-- /Tab Content EOC --> 

                    </div>
                </div>
            </div>
        </div>
        <!-- /Service Listing EOC --> 

    </div>
</section>
<!-- /Service Section EOC --> 