<!-- /Contact Us Section BOC -->

<?php
//echo base64_decode($_GET['selected_ride']);
//echo '<pre>'; print_r($_SESSION); echo '</pre>';
?>

<section class="inquiry">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Book Ride</h3>
                <hr class="style-two"/>
            </div>
        </div>

        <form method="POST" action="<?= base_url('Home/book_now') ?>" id="select-ride-form">
            <div class="row">
                <div class="common-form">
                    <!-- Personal Details -->
                    <div class="col-lg-12">
                        <h6>Personal Details</h6>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="name" id="firstname" value="<?= @$_SESSION['RIDE']['name'] ?>" placeholder="Enter Name" class="form-control" />
                            <p id="ERR_FIRSTNAME" class="text-red"></p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="tel" name="phone" id="phone" value="<?= @$_SESSION['RIDE']['phone'] ?>" placeholder="Enter Phone" class="form-control" />	
                            <p id="ERR_PHONE" class="text-red"></p>	
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="email" name="email" id="email" value="<?= @$_SESSION['RIDE']['email'] ?>" placeholder="Enter Email Address" class="form-control" /> 
                            <p id="ERR_EMAIL" class="text-red"></p>
                        </div>
                    </div>

                    <!-- Pick up location details -->
                    <div class="col-lg-12">
                        <h6>Pick Up Details</h6>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-address" id="pick-address" value="<?= @$_SESSION['RIDE']['pickup_location'] ?>" placeholder="Pick up Address" class="form-control"/>
                            <p id="ERR_ADDRESS" class="text-red"></p>  
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-city" id="pick-city" value="<?= @$_SESSION['RIDE']['pickup_city'] ?>" placeholder="Pick up City" class="form-control"/>
                            <p id="ERR_CITY" class="text-red"></p>  
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-postal" id="pick-postal" value="<?= @$_SESSION['RIDE']['pickup_postal'] ?>" placeholder="Pick up Postal Code" class="form-control"/>
                            <p id="ERR_POSTAL" class="text-red"></p>  
                        </div>
                    </div>
                    <!-- Drop off location details -->
                    <div class="col-lg-12">
                        <h6>Drop Off Details</h6>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="drop-address" id="drop-address" value="<?= @$_SESSION['RIDE']['destination_location'] ?>" placeholder="Drop Off Address" class="form-control"/>
                            <p id="ERR_ADDRESS" class="text-red"></p>  
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="drop-city" id="drop-city" value="<?= @$_SESSION['RIDE']['destination_city'] ?>" placeholder="Drop Off City" class="form-control"/>
                            <p id="ERR_CITY" class="text-red"></p>  
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="drop-postal" id="drop-postal" value="<?= @$_SESSION['RIDE']['destination_postal'] ?>" placeholder="Drop Off Postal Code" class="form-control"/>
                            <p id="ERR_POSTAL" class="text-red"></p>  
                        </div>
                    </div>
                    <!-- Other Trip Details location details -->
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-up-date" id="pick-up-date" value="<?= @$_SESSION['RIDE']['pickup_date'] ?>" placeholder="Pick-Up Date" class="form-control datepicker"/>
                            <i class="fa fa-calendar-o"></i> 
                            <span id="ERR_pick-up-date" class="text-red"></span>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="pick-up-time" id="pick-up-time" value="<?= @$_SESSION['RIDE']['pickup_time'] ?>" placeholder="Pick-Up Time" class="form-control timepicker" />
                            <i class="fa fa-clock-o"></i>
                            <span id="ERR_pick-up-time" class="text-red"></span>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <select id="event" name="event" class="form-control text-black">
                                <option value="0">Type of Occasion</option>
                                <?php foreach ($event_details AS $val): ?>
                                    <option value="<?= $val->event_id ?>" <?php if (@$_SESSION['RIDE']['event_type'] == $val->event_id || @$event_type == $val->event_id) echo 'selected'; ?>><?= $val->event_name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span id="ERR_event" class="text-red"></span>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="number_passengers" id="number_passengers" value="<?= @$_SESSION['RIDE']['number_passengers'] ?>" placeholder="No of Passengers" class="form-control" />
                            <p id="ERR_NO_PESSENGER" class="text-red"></p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <input type="text" name="special_requirement" id="special_requirement" value="<?= @$_SESSION['RIDE']['special_requirement'] ?>" placeholder="Special Requirement" class="form-control" />
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <div class="form-group" id="car-category-block">
                            <?php /* ?> onchange="getRides()" <?php */ ?>
                            <select onchange="" id="car-category" name="selected-car" class="form-control">
                                <option id="0" value="0">Select Car</option>
                                <?php foreach ($cars AS $val): ?>
                                    <option value="<?= $val->car_id ?>" <?php
                                    if (base64_decode(@$_GET['selected_ride']) == $val->car_id) {
                                        echo "selected";
                                    }
                                    ?>><?= $val->model_name ?> (<?= $val->category_name ?>)</option>
                                        <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <p class="text-center"><b>Simply call us on 1300665711 or message us on 0431722784</b></p>
            </div>

            <!-- /Form BOC -->
            <div class="row">
                <div class="col-md-9">
                    <div class="location-map">
                        <div id="map" class="google-map"></div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <input type="button" name="book-now" value="Submit" id="find" class="btn btn-full" onclick="return validateAddBook()">
                    </div>

                    <div id="directions_panel" style="padding-top: 15px; padding-bottom: 15px"></div>

                    <div class="" id="select-ride" style="visibility: hidden">
                        <input type="hidden" name="start" id="form-start" value="<?= @$_SESSION['pickup_location'] ?>"/>
                        <input type="hidden" name="end" id="form-end" value="<?= @$_SESSION['destination_location'] ?>"/>
                        <input type="hidden" name="estimated-distance" id="form-estimated-distance" value=""/>
                        <div class="form-group">
                            <input type="submit" name="select-ride" value="Continue" id="select-ride" class="btn btn-full"/>
                        </div>
                    </div>

                    <?php /* ?>
                      <div class="">
                      <div class="form-group">
                      <select id="start" onchange="calcRoute();" class="form-control select2">
                      <option id="0" value="0">Fom</option>
                      <?php foreach ($city AS $val): ?>
                      <option id="<?= $val->city_id ?>" <?php if ($_SESSION['RIDE']['pickup_location'] == $val->city_id) echo 'selected'; ?> value="<?= strtolower($val->name) ?>"><?= $val->name ?></option>
                      <?php endforeach; ?>
                      </select>
                      </div>
                      <div class="form-group">
                      <select id="end" onchange="calcRoute();" class="form-control select2">
                      <option id="0" value="0">To</option>
                      <?php foreach ($city AS $val): ?>
                      <option id="<?= $val->city_id ?>" <?php if ($_SESSION['RIDE']['destination_location'] == $val->city_id) echo 'selected'; ?> value="<?= strtolower($val->name) ?>"><?= $val->name ?></option>
                      <?php endforeach; ?>
                      </select>
                      </div>
                      <div class="form-group">
                      <input type="submit" class="btn btn-full" id="find" value="Find" name="submit">
                      </div>

                      <div id="directions_panel" style="padding-top: 15px; padding-bottom: 15px"></div>

                      <div class="form-group" id="car-category-block" style="visibility: collapse">
                      <select onchange="getRides()" id="car-category" class="form-control select2">
                      <option id="0" value="0">Select Car Category</option>
                      <?php foreach ($car_category AS $val): ?>
                      <option value="<?= $val->category_id ?>"><?= $val->name ?></option>
                      <?php endforeach; ?>
                      </select>
                      </div>
                      </div>
                      <?php /* */ ?>
                </div>
                <input type="hidden" value="" id="estimated-distance" />

                <div class="clearfix"></div>
            </div>

            <div class="row">

                <div class="col-md-12" id="available-rides">

                </div>
                <div class="row">

                </div>

            </div>
        </form>
    </div>
</div>
<!-- /Form EOC --> 
</section>
<!-- /Contact Us Section EOC --> 

<script>
    $("#start").on('change', function() {
        //alert($(this).find('option:selected').attr('id'));
    });
</script>
<script>
    function getRides()
    {
        //document.getElementById("select-ride").style.visibility = 'hidden';
        //alert('hi');
        //var car_category = $("#car-category").val();
        //var city = $("#start").find('option:selected').attr('id');

        //var URL = "<?= base_url() ?>ride/search_ride/" + car_category + "/" + city;
        var URL = "<?= base_url() ?>ride/search_ride";
        //alert(URL);

        $("#available-rides").load(URL, function(data) {
            if (data != "")
            {
                document.getElementById("select-ride").style.visibility = 'visible';
            }
            else
            {
                $("#available-rides").html("<div class='alert alert-danger'>Sorry, No Ride Available.</div>");
            }
        });
    }
</script>
<script>
    function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: {lat: -33.8674869, lng: 151.2069902}
        });
        directionsDisplay.setMap(map);

        var onChangeHandler = function() {

            // Add Value of start and end city
            var start = $("#start").find('option:selected').attr('id');
            $("#form-start").val(start);
            var end = $("#end").find('option:selected').attr('id');
            $("#form-end").val(end);

            //---------------------------------------

            calculateAndDisplayRoute(directionsService, directionsDisplay);
        };
        //document.getElementById('start').addEventListener('change', onChangeHandler);
        //document.getElementById('end').addEventListener('change', onChangeHandler);
        document.getElementById('find').addEventListener('click', onChangeHandler);
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var startl = document.getElementById('pick-address').value + ' ' + document.getElementById('pick-city').value + ' ' + document.getElementById('pick-postal').value;
        var endl = document.getElementById('drop-address').value + ' ' + document.getElementById('drop-city').value + ' ' + document.getElementById('drop-postal').value;

        directionsService.route({
            origin: startl,
            destination: endl,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);

                //-----------------------------------------
                var route = response.routes[0];
                var summaryPanel = document.getElementById('directions_panel');
                summaryPanel.innerHTML = '';
                // For first route, display summary information.
                summaryPanel.innerHTML = '<b>Estimated Distance: </b>' + route.legs[0].distance.text;
                //-----------------------------------------
                var distance = route.legs[0].distance.text;
                $("#form-estimated-distance").val(distance);
                // Vissible Select Car Category Div
                //document.getElementById("car-category-block").style.visibility = 'visible';
                document.getElementById("select-ride").style.visibility = 'visible';
                //getRides();

            } else {
                //window.alert('Directions request failed due to ' + status);
                window.alert('Invalid Source or Destination Address might be incorect.');
            }
        });
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?&callback=initMap" 
async defer></script>