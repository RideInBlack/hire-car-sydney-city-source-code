<?php
$rib_upload_path = DRIVER_UPLOAD_SERVER_PATH;
?>
<!-- /Profile Section BOC -->

<?php 
$driverId = $_SESSION['HIRE_CAR']['DRIVER']->driver_id;
$this->load->helpers('encryption_helper');
//echo "<pre>";
//print_r($_SESSION['HIRE_CAR']['DRIVER']);die;

if ($this->session->flashdata('ERR_PASSWORD_MESSAGE') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_PASSWORD_MESSAGE') ?>
                        </div>
                    <?php endif; ?>

<section class="profile grey-color-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Profile</h3>
                <hr class="style-two"/>
            </div>
        </div>

        <!-- User Info BOC -->
        <div class="row">
            <div class="user-profile">
                <div class="col-lg-2 col-sm-2 col-md-2">
					<?php 
					if($_SESSION['HIRE_CAR']['DRIVER']->profile_image != '')
					{
						if($_SESSION['HIRE_CAR']['DRIVER']->profile_image_server_path == 'rib') { 
							$url = $rib_upload_path."assets/uploads/driver/".$_SESSION['HIRE_CAR']['DRIVER']->profile_image; 
						}
						else {
							$url = base_url()."assets/uploads/driver/".$_SESSION['HIRE_CAR']['DRIVER']->profile_image; 
						}
					}
					else
					{ 
						$url = base_url('assets/web')."/images/img-privew-user.png"; 
					}?>
					<img src="<?=$url?>" alt="Privew" class="img-center"/>
				</div>
                <div class="col-lg-6">
                    <h4 class="text-left"><?php echo $_SESSION['HIRE_CAR']['DRIVER']->first_name.' '.$_SESSION['HIRE_CAR']['DRIVER']->last_name; ?></h4>
                    <ul class="user-info">
                        <li><i class="fa fa-map-marker"></i><?php echo $_SESSION['HIRE_CAR']['DRIVER']->address;?> </li>
                        <li><i class="fa fa-envelope"></i><a href="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->email_id;?>" title="megan1611@gmail.com"><?php echo $_SESSION['HIRE_CAR']['DRIVER']->email_id;?></a> </li>
                        <li><i class="fa fa-phone"></i><a href="tel:<?php echo $_SESSION['HIRE_CAR']['DRIVER']->mobile_number;?>" title="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->mobile_number;?>"><?php echo $_SESSION['HIRE_CAR']['DRIVER']->mobile_number;?></a></li>
                        <li class="info-action"> 
                        	<a href="Javascript:void(0);" data-toggle="modal" data-target="#myModalEdit" title="Edit Profile">Edit Profile</a> 
                            <?php if(@$_SESSION['HIRE_CAR']['DRIVER']->LOGIN_TYPE != 'FB') : ?>
                            <a href="Javascript:void(0);" data-toggle="modal" data-target="#myModalChangePass" title="Change Password">Change Password</a>
                            <?php endif; ?>
                            <a href="Javascript:void(0);" data-toggle="modal" data-target="#myModalCreditcard" title="Setting">Setting</a> </li>
                            
                    </ul>
                </div>
            </div>
        </div>
        <!-- User Info EOC --> 

    </div>
</section>
<!-- /Profile Section EOC --> 
<?php /*?>
<!-- /Profile Listing Section BOC -->
<section class="profile-list">
    <div class="container"> 
        <?php $ci =&get_instance();
        $ci->load->model('ride_model');
        $ci->load->model('region_model');
        $ci->load->model('event_model');
        foreach ($ride_details as $rides):
            $paymentDetails = $ci->ride_model->fetch_paymentdetails($rides->ride_id);
            $pickup = $rides->pickup_location.', '.$rides->pickup_city.', '.$rides->pickup_postal;
            $destination = $rides->destination_location.', '.$rides->destination_city.', '.$rides->destination_postal;
            $eventType = $ci->event_model->get_event_name($rides->event_id);
           
            //$carDetails = $ci->car_model->get_carcategory_name($rides->category_id);
            $rideStatus = '';
            $rideButton = '';
            $actualFare = '';
            $paidFare = '';
            $remainingFare = '';
            if($rides->ride_status == 0) {
                $rideStatus = 'Waiting for Confirmation';
            } elseif($rides->ride_status == 1 && $rides->assign_driver_id != '0' ) {
                $rideStatus = 'Assign to you';
                $rideButton = '<a class="btn" title="Accept Ride" href="'.base_url().'driverprofile/status/accept/'.$rides->id.'" data-toggle="modal" data-target="#messageModel">Accept</a>';
            } elseif($rides->ride_status == 2) {
                $rideStatus = 'Confirm';
                if($rides->assign_driver_id == $driverId) {
                    $rideButton = '<a class="btn" title="Start Ride" href="'.base_url().'driverprofile/status/start/'.$rides->id.'" data-toggle="modal" data-target="#messageModel">Start Ride</a>';
                } 
            } elseif($rides->ride_status == 3) {
                $rideStatus = 'Ride Started';
                if($rides->assign_driver_id == $driverId) {
                    $rideButton = '<a class="btn" title="Finish Ride" href="'.base_url().'driverprofile/status/finish/'.$rides->id.'" data-toggle="modal" data-target="#messageModel">Finish Ride</a>';
                }
            } elseif($rides->ride_status == 4) {
                $rideStatus = 'Ride Finish';
                $actualFare = $rides->estimated_fare;
                if(!empty($paymentDetails)) {
                    $paidFare = $paymentDetails->amount;
                } else {
                    $paidFare = '0';
                }
                $remainingFare = ($rides->estimated_fare - $paidFare);
                if($rides->assign_driver_id == $driverId && $remainingFare > 0 ) {
                    $rideButton = '<a class="btn" title="Manual Payment" href="'.base_url().'driverprofile/status/payment/'.$rides->id.'" data-toggle="modal" data-target="#messageModel">Manual Payment</a>';
                } else {
                    $rideStatus = 'Completed';
                }
            } elseif($rides->ride_status == 5) {
                $rideStatus = 'Rejected';
            } elseif($rides->ride_status == 6) {
                $rideStatus = 'Canceled';
            }
                                ?>
        <!-- /Profile Items BOC -->
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6"> 
                <?php if ($rides->image_name != ""): ?>
                        <img alt="Modle Car" src="<?= base_url('assets/') ?>/web/images/car_image/<?= $rides->car_id ?>/<?= $rides->image_name ?>" class="img-border"> 
                    <?php else: ?>
                        <img alt="Modle Car" src="<?= base_url('assets/web') ?>/images/no_image.gif" class="img-border"> 
                    <?php endif; ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <?php if($rides->name != '' && $rides->model_name != '') { ?>
                <h4 class="text-left"><?php echo $rides->name . " " . $rides->model_name;?></h4> <?php } ?>
                <div class="content">
                    <h5>Used for</h5>
                    <h6><?php echo $eventType;?></h6>
                </div>
                <div class="content">
                    <h5>Pick up</h5>
                    <h6><?php echo $pickup;?></h6>
                </div>
                <div class="content">
                    <h5>Destination</h5>
                    <h6><?php echo $destination;?></h6>
                </div>
                <div class="content">
                    <h5>Trip Status</h5>
                    <h6><?php echo $rideStatus;?></h6>
                </div>
                <?php if($rides->ride_status == 4) { ?>
                <div class="content">
                    <h5>Payment Details of Rider</h5>
                    <h6><b>Estimated Fare</b>: <?= $actualFare;?></h6>
                    <h6><b>Paid Fare</b>: <?= $paidFare;?></h6>
                    <h6><b>Remaining Fare</b>: <?= $remainingFare;?></h6>
                </div>
                <?php } if($rideButton != '') { ?>
                <div class="content">
                    <?php echo $rideButton;?>
                </div>
                <?php }?>
            </div>
        </div>
        <hr/>
        <!-- /Profile Items EOC --> 
        <?php endforeach; ?>
    </div>
</section>
<?php */?>
<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_EDIT_DRIVER_EMAILDUPLICATION') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_EDIT_DRIVER_EMAILDUPLICATION') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url(); ?>driverprofile/editprofile/<?=$driver_details->driver_id;?>" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <div class="common-form">
                            <div class="row">
                            	
								<?php if($_SESSION['HIRE_CAR']['DRIVER']->LOGIN_TYPE == 'FB'):
                                	$readonly = "readonly";
                            	endif;?>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="d_fname" id="d_fname" placeholder="First Name" class="form-control" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->first_name;?>" <?=@$readonly?>>
                                        <p class="col-sm-12 text-red" id="D_ERR_FNAME"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="d_lname" id="d_lname" placeholder="Last Name" class="form-control" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->last_name;?>" <?=@$readonly?>>
                                        <p class="col-sm-12 text-red" id="D_ERR_LNAME"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12">
                                    <div class="form-group">
                                        Driver's Photo :
										<?php if(!empty($_SESSION['HIRE_CAR']['DRIVER']->profile_image)) { 
												if($_SESSION['HIRE_CAR']['DRIVER']->profile_image_server_path == 'rib') { 
													$driverImage = $rib_upload_path."assets/uploads/driver/".$_SESSION['HIRE_CAR']['DRIVER']->profile_image;
												}
												else {
													$driverImage = base_url()."assets/uploads/driver/".$_SESSION['HIRE_CAR']['DRIVER']->profile_image;
												}
											?>
										<div>
											<img alt="Driver's Image" class="img img-thumbnail" src="<?php echo $driverImage; ?>" width="130">
										</div>
										<?php } ?>
										<input type="file" id="d_driverimage" name="d_driverimage">
										<p class="col-sm-12 text-red" id="D_ERR_DRIVERIMAGE"></p>
                                    </div>
                                </div>
								
                                <div class="col-lg-12">
                                    <div class="form-group">

										<input type="tel" name="d_mobile" id="d_mobile" placeholder="Mobile No" class="form-control" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->mobile_number;?>">
                                        <p class="col-sm-12 text-red" id="D_ERR_MOBILE"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="d_email" id="d_email" placeholder="Email Address" class="form-control" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->email_id;?>" <?=@$readonly?>>
                                        <p class="col-sm-12 text-red" id="D_ERR_EMAIL"></p>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
									<div class="form-group">
                                        <input type="text" class="form-control" name="d_companyname" id="d_companyname" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->companyname;?>" placeholder="Company Name">
                                        <p class="col-sm-12 text-red" id="D_ERR_COMPANYNAME"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="d_abn" id="d_abn" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->abn;?>"  placeholder="ABN">
                                        <p class="col-sm-12 text-red" id="D_ERR_ABN"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12"><p><b>Address Details</b></p></div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="d_address" id="d_address" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->address;?>"  placeholder="Address">
                                        <p class="col-sm-12 text-red" id="D_ERR_ADDRESS"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select class="form-control " name="d_state" id="d_state" placeholder="Select State">
                                            <option value="">Select State</option>
                                            <?php foreach ($state_list as $state) { ?>
                                                <option value="<?= $state->state_id; ?>" <?php if($_SESSION['HIRE_CAR']['DRIVER']->state != '' && $_SESSION['HIRE_CAR']['DRIVER']->state == $state->state_id){ ?> selected="selected" <?php } ?>><?= $state->name ?></option>
                                            <?php } ?>
                                        </select>
                                        <p class="col-sm-12 text-red" id="D_ERR_STATE"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
										<input type="text" class="form-control" name="d_zipcode" id="d_zipcode" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->zipcode;?>" placeholder="Zip code">
										<p class="col-sm-12 text-red" id="D_ERR_ZIPCODE"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12"><p><b>Licence Details</b></p></div>
								<div class="col-lg-12">
									<?php $i=1; ?>
									<?php $itr = 0 ?>
                                    <div class="form-group">
										Upload Copy of Driver's Licence : 
										<?php if(count($licence_uploads) > 0): ?>
											<?php foreach($licence_uploads as $row): ?>
												<div class="col-sm-12" id="imgBox_<?=$i?>"> 
													<a href="javascript:void(0);" title="Delete Upload"><i class="fa fa-fw fa-times" style="position:static" onclick="deleteImage('<?= base_url('driverprofile/delete_uploads/' . $row->driverupload_id) ?>', 'imgBox_<?=$i?>', , 'licensedetails')"></i></a>
													
													<?php if($row->server_reference == 'rib'):  ?>
														<a href="<?= $rib_upload_path."assets/uploads/driver/license/".$row->file_name; ?>" download="<?=$row->file_name; ?>"><?=$row->file_name; ?></a>
													<?php else: ?>
														<a href="<?= base_url()."assets/uploads/driver/license/".$row->file_name; ?>" download="<?=$row->file_name; ?>"><?=$row->file_name; ?></a>
													<?php endif; ?>
													
													<?php /*?><a href="javascript:void(0);" title="Delete Upload"><i class="fa fa-fw fa-times" style="position:static" onclick="deleteImage('<?= base_url('driverprofile/delete_uploads/' . $row->driverupload_id) ?>', 'imgBox_<?=$i?>', , 'licensedetails')"></i></a>
													<a href="<?= base_url()."assets/uploads/driver/license/".$row->file_name; ?>" download="<?=$row->file_name; ?>"><?=$row->file_name; ?></a><?php */?>
												</div>
												<?php $i++; ?>
												<?php $itr++; ?>
											<?php endforeach; ?>
										<?php endif; ?>
                                        <input type="file" id="licensedetails" name="licensedetails[]" multiple class="multi" maxlength="<?=4-$itr?>" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG">
										
										<p class="col-sm-12 text-red" id=""></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12">
                                    <div class="form-group">
										<?php $itr = 0 ?>
										Upload Copy of Driver's HC Authority Card :
										<?php if(count($hc_authority_uploads) > 0): ?>
											<?php foreach($hc_authority_uploads as $row): ?>
												<div class="col-sm-12" id="imgBox_<?=$i?>"> 
													<a href="javascript:void(0);" title="Delete Upload"><i class="fa fa-fw fa-times" style="position:static" onclick="deleteImage('<?= base_url().'driverprofile/delete_uploads/' . $row->driverupload_id ?>', 'imgBox_<?=$i?>', , 'hc_authority_card')"></i></a>
													<?php if($row->server_reference == 'rib'):  ?>
														<a href="<?= $rib_upload_path."assets/uploads/driver/hcauthoritycertificate/".$row->file_name; ?>" download="<?=$row->file_name; ?>"><?=$row->file_name; ?></a>
													<?php else: ?>
														<a href="<?= base_url()."assets/uploads/driver/hcauthoritycertificate/".$row->file_name; ?>" download="<?=$row->file_name; ?>"><?=$row->file_name; ?></a>
													<?php endif; ?>
													
													<?php /*?><a href="javascript:void(0);" title="Delete Upload"><i class="fa fa-fw fa-times" style="position:static" onclick="deleteImage('<?= base_url('driverprofile/delete_uploads/' . $row->driverupload_id) ?>', 'imgBox_<?=$i?>', , 'hc_authority_card')"></i></a>
													<a href="<?= base_url()."assets/uploads/driver/hcauthoritycertificate/".$row->file_name; ?>" download="<?=$row->file_name; ?>"><?=$row->file_name; ?></a><?php */?>
												</div>
												<?php $i++; ?>
												<?php $itr++; ?>
											<?php endforeach; ?>
										<?php endif; ?>
										
										<input type="file" id="hc_authority_card" name="hc_authority_card[]" multiple class="multi" maxlength="<?=4-$itr?>" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG">
										
										<p class="col-sm-12 text-red" id=""></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12">
                                    <div class="form-group">
										<input type="text" class="form-control" name="d_licensedetails" id="d_licensedetails" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->licensedetails;?>" placeholder="License Details">
                                        <p class="col-sm-12 text-red" id="D_ERR_LICENSEDETAILS"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12"><p><b>Car Details</b></p></div>
								<div class="col-lg-12">
                                    <div class="form-group">
										<?php $itr = 0 ?>
                                         Upload Copy of Vehicle Registration Document : 
										<?php if(count($registration_uploads) > 0): ?>
											<?php foreach($registration_uploads as $row): ?>
												<div class="col-sm-12" id="imgBox_<?=$i?>"> 
													
													<a href="javascript:void(0);" title="Delete Upload"><i class="fa fa-fw fa-times" style="position:static" onclick="deleteImage('<?= base_url('driverprofile/delete_uploads/' . $row->driverupload_id) ?>', 'imgBox_<?=$i?>', 'registrationnumber')"></i></a>
													
													<?php if($row->server_reference == 'rib'):  ?>
														<a href="<?= $rib_upload_path."assets/uploads/driver/registrationnumber/".$row->file_name; ?>" download="<?=$row->file_name; ?>"><?=$row->file_name; ?></a>
													<?php else: ?>
														<a href="<?= base_url()."assets/uploads/driver/registrationnumber/".$row->file_name; ?>" download="<?=$row->file_name; ?>"><?=$row->file_name; ?></a>
													<?php endif;?>
													
												</div>
												<?php $i++; ?>
												<?php $itr++; ?>
											<?php endforeach; ?>
										<?php endif; ?>
										<input type="file" id="registrationnumber" name="registrationnumber[]" multiple class="multi" maxlength="<?=4-$itr?>" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG">
										
										<p class="col-sm-12 text-red" id=""></p>
                                    </div>
                                </div>
								<div class="col-lg-12">
                                    <div class="form-group">
										<input type="text" class="form-control" name="d_carmake" id="d_carmake" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->car_make;?>" placeholder="Make">
										<p class="col-sm-12 text-red" id="D_ERR_CARMAKE"></p>
                                    </div>
                                </div>
								<div class="col-lg-12">
                                    <div class="form-group">
										<input type="text" class="form-control" name="d_carYearModel" id="d_carYearModel" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->car_yearmodel;?>" placeholder="Year & Model">
										<p class="col-sm-12 text-red" id="D_ERR_CARYEARMODEL"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12"><p><b>Insurance Details</b></p></div>
								<div class="col-lg-12">
                                    <div class="form-group">
										<input type="text" class="form-control" name="d_insurancecompany" id="d_company" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->insurance_company;?>" placeholder="Company">
										<p class="col-sm-12 text-red" id="D_ERR_INSURANCECOMPANY"></p>
                                    </div>
                                </div>
								<div class="col-lg-12">
                                    <div class="form-group">
										<?php $itr = 0; ?>
                                        Upload Copy of Insurance Document: 
										<?php if(count($insurance_uploads) > 0): ?>
											<?php foreach($insurance_uploads as $row): ?>
												<div class="col-sm-12" id="imgBox_<?=$i?>"> 
													<a href="javascript:void(0);" title="Delete Upload"><i class="fa fa-fw fa-times" style="position:static" onclick="deleteImage('<?= base_url().'driverprofile/delete_uploads/' . $row->driverupload_id ?>', 'imgBox_<?=$i?>', 'insurancetype')"></i></a>
													
													<?php if($row->server_reference == 'rib'):  ?>
														<a href="<?= $rib_upload_path."assets/uploads/driver/insurance/".$row->file_name; ?>" download="<?=$row->file_name; ?>"><?=$row->file_name; ?></a>
													<?php else: ?>
														<a href="<?= base_url()."assets/uploads/driver/insurance/".$row->file_name; ?>" download="<?=$row->file_name; ?>"><?=$row->file_name; ?></a>
													<?php endif; ?>
												</div>
												<?php $i++; ?>
												<?php $itr++; ?>
											<?php endforeach; ?>
										<?php endif; ?>
										
										<input type="file" id="insurancetype" name="insurancetype[]" multiple class="multi" maxlength="<?=4-$itr?>" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG">
										
										<p class="col-sm-12 text-red" id=""></p>
                                    </div>
                                </div>
								

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="register" value="Save" class="btn btn-full" onclick="return validateEditDriver()">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
function deleteImage(URL, box, uploadName)
{
	if (confirm('Are you sure want to delete?')) {
		if (URL == 0) {
			$("#" + box).remove();
		}
		else {
			$.ajax({
				type: 'get',
				url: URL,
				beforeSend: function() {
				},
				success: function(data) {
					//console.log(data); return 0;
					$("#" + box).remove();
					var num = parseInt(document.getElementById(uploadName).getAttribute('maxlength'));
					document.getElementById(uploadName).setAttribute('maxlength', num + 1);
				}
			});
		}
	}
}
</script>


<div class="modal fade" id="myModalChangePass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_PASSWORD_MESSAGE') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_PASSWORD_MESSAGE') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url(); ?>driverprofile/changepassword/<?=$driver_details->driver_id;?>" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <div class="common-form">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="oldpassword" id="oldpassword" placeholder="Old Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_OLD_PASSWORD"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="newpassword" id="newpassword" placeholder="New Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_NEW_PASSWORD"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="confpassword" id="confpassword" placeholder="Confirm Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_CONF_PASSWORD"></p>
                                    </div>
                                </div>
                               

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="Save" value="Save" class="btn btn-full" onclick="return validateChangePass()">
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModalCreditcard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Creadit Card Details</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_PASSWORD_MESSAGE') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_PASSWORD_MESSAGE') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url(); ?>driverprofile/updatecreditcard/<?=$driver_details->driver_id;?>" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <div class="common-form">
                            <div class="row">
                            	
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<label><b>Card Type</b></label><br />
                                        <select name="card_type" id="card_type" class="form-control">
                                        	<option value="">Select a type</option>
                                            <option value="American Express" <?php if(isset($card_details->card_type) && $card_details->card_type == 'American Express'){ ?> selected="selected" <?php } ?>>American Express</option>
                                            <option value="Master" <?php if(isset($card_details->card_type) && $card_details->card_type == 'Master'){ ?> selected="selected" <?php } ?>>Master</option>
                                            <option value="Visa" <?php if(isset($card_details->card_type) && $card_details->card_type == 'Visa'){ ?> selected="selected" <?php } ?>>Visa</option>
                                        </select>
                                        
                                         
                                        <p class="text-red" id="ERR_CARD_TYPE"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<label><b>Card No.</b></label>
                                        <input type="text" name="cardno" id="cardno" placeholder="Card Number" class="form-control" value="<?php if(isset($card_details->card_no) && $card_details->card_no != ''){ echo decrypt($card_details->card_no); } ?>">
                                        <p class="text-red" id="ERR_CARD_NO"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<label><b>Expiry Date</b></label><br />
                                        <select name="month" id="month" class="form-control" style="width:120px;margin-right:25px;float:left;">
                                        	<option value="">Month</option>
                                            <option value="01" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '01'){ ?> selected="selected" <?php } ?>>01</option>
                                            <option value="02" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '02'){ ?> selected="selected" <?php } ?>>02</option>
                                            <option value="03" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '03'){ ?> selected="selected" <?php } ?>>03</option>
                                            <option value="04" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '04'){ ?> selected="selected" <?php } ?>>04</option>
                                            <option value="05" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '05'){ ?> selected="selected" <?php } ?>>05</option>
                                            <option value="06" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '06'){ ?> selected="selected" <?php } ?>>06</option>
                                            <option value="07" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '07'){ ?> selected="selected" <?php } ?>>07</option>
                                            <option value="08" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '08'){ ?> selected="selected" <?php } ?>>08</option>
                                            <option value="09" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '09'){ ?> selected="selected" <?php } ?>>09</option>
                                            <option value="10" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '10'){ ?> selected="selected" <?php } ?>>10</option>
                                            <option value="11" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '11'){ ?> selected="selected" <?php } ?>>11</option>
                                            <option value="12" <?php if(isset($card_details->exp_month) && $card_details->exp_month == '12'){ ?> selected="selected" <?php } ?>>12</option>
                                        </select>
                                        
                                         <select name="year" id="year" class="form-control" style="width:120px;">
                                        	<option value="">Year</option>
                                            <?php
											$current = date('Y');
											for($i=$current;$i<=$current + 35;$i++)
											{
											?>
                                            	<option value="<?=$i?>" <?php if(isset($card_details->exp_year) && $card_details->exp_year == $i ){ ?> selected="selected" <?php } ?>><?=$i?></option>
                                            <?php
											}
											?>
                                           
                                        </select>
                                        <p class="text-red" id="ERR_EXP"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<label><b>CCV No.</b></label><br />
                                        <input type="text" name="ccvno" id="ccvno" placeholder="CCV Number" class="form-control" value="<?php if(isset($card_details->ccv_no) && $card_details->ccv_no != ''){ echo $card_details->ccv_no; } ?>" maxlength="3">
                                        <p class="text-red" id="ERR_CCV"></p>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<label><b>Name of Card</b></label><br />
                                        <input type="text" name="name_of_card" id="name_of_card" placeholder="Name of Card" class="form-control" value="<?php if(isset($card_details->name_of_card) && $card_details->name_of_card != ''){ echo $card_details->name_of_card; } ?>">
                                        <p class="text-red" id="ERR_NAME_CARD"></p>
                                    </div>
                                </div>
                               

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="Save" value="Save" class="btn btn-full" onclick="return validateCard();">
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Profile Listing Section EOC -->
<div class="modal fade"  id="messageModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  
</div><!-- /.modal -->
<script type="text/javascript">


function validateCard()
{	
	var err_flag = false;
	var re_ccv = /^[0-9]{3,4}$/;
	
	var cardType = document.getElementById("card_type").value.trim();
	if (cardType == "" || cardType == 0) {
		document.getElementById("ERR_CARD_TYPE").innerHTML = "Please Select Valid Card Type.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_CARD_TYPE").innerHTML = "";
	}

	// Card Number Validation
	var cardnumber = document.getElementById("cardno").value.trim();
	if (cardnumber == "" || isNaN(cardnumber)) 
	{
		document.getElementById("ERR_CARD_NO").innerHTML = "Please Enter your Card Number.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_CARD_NO").innerHTML = "";
	}
	
	var ccv = document.getElementById("ccvno").value.trim();
	if (ccv == "" || isNaN(ccv)) 
	{
		document.getElementById("ERR_CCV").innerHTML = "Please Enter CCV Number.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_CCV").innerHTML = "";
	}
	
	var expiremonth = document.getElementById("month").value.trim();
	var expireyear = document.getElementById("year").value.trim();
	
	if (expiremonth == "" || expireyear == "") 
	{
		document.getElementById("ERR_EXP").innerHTML = "Please Enter Expiry Date.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_EXP").innerHTML = "";
	}
	//alert(err_flag);
	if (err_flag == true)
	return false;
	else
	return true;
}


function validateEditDriver()
{
	var err_flag = false;
	var re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	
	// Firstname Validation
	var fname = document.getElementById("d_fname").value.trim();
	if (fname == "") {
		document.getElementById("D_ERR_FNAME").innerHTML = "Please Enter First Name.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_FNAME").innerHTML = "";
	}
	
	// Lastname Validation
	var lname = document.getElementById("d_lname").value.trim();
	if (lname == "") {
		document.getElementById("D_ERR_LNAME").innerHTML = "Please Enter Last Name.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_LNAME").innerHTML = "";
	}
	
	// Mobile Number Validation
	var mobile = document.getElementById("d_mobile").value.trim();
	if (mobile == "" || isNaN(mobile)) {
		document.getElementById("D_ERR_MOBILE").innerHTML = "Please Enter Mobile Number.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_MOBILE").innerHTML = "";
	}
	
	// Email Id Validation
	var email = document.getElementById("d_email").value.trim();
	if (email == "") {
		document.getElementById("D_ERR_EMAIL").innerHTML = "Please Enter Email Address.";
		err_flag = true;
	}
	else if (!re_email.test(email)) {
		document.getElementById("D_ERR_EMAIL").innerHTML = "Please Enter Valid Email Address.";
	}
	else {
		document.getElementById("D_ERR_EMAIL").innerHTML = "";
	}
	
	// Address Validation
	var address = document.getElementById("d_address").value.trim();
	if (address == "") {
		document.getElementById("D_ERR_ADDRESS").innerHTML = "Please Enter Address.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_ADDRESS").innerHTML = "";
	}
	
	// State Validation
	var state = document.getElementById("d_state").value;
	if (state == "") {
		document.getElementById("D_ERR_STATE").innerHTML = "Please Select State.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_STATE").innerHTML = "";
	}
	
	// Zipcode Validation
	var zipcode = document.getElementById("d_zipcode").value;
	if(zipcode != '') {
		if (isNaN(zipcode))  {
			document.getElementById("D_ERR_ZIPCODE").innerHTML = "Please Enter valid zipcode.";
			err_flag = true;
		}
		else {
			document.getElementById("D_ERR_ZIPCODE").innerHTML = "";	
		}
	}
	
	var fup = document.getElementById('d_driverimage');
	var fileName = fup.value;
	var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
	if(fileName != '') {
		if(ext =="GIF" || ext=="gif" || ext =="PNG" || ext=="png" || ext =="JPG" || ext=="jpg" || ext =="JPEG" || ext=="jpeg") {
			document.getElementById("D_ERR_DRIVERIMAGE").innerHTML = "";	
		}
		else {
			document.getElementById("D_ERR_DRIVERIMAGE").innerHTML = "Please upload only png,jpg,jpeg and gif image file.";
			err_flag = true;
		}
	}
	
	/*// Validation For Licence Details
	var licenceUpload = document.getElementById("licensedetails").value.trim();
	if (licenceUpload == "") {
		document.getElementById("D_ERR_LICENSEUPLOAD").innerHTML = "Please Upload Licence Image.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_LICENSEUPLOAD").innerHTML = "";
	}*/
	
	/*// HC Authority Details
	var hcAuthory = document.getElementById("hc_authority_card").value.trim();
	if (hcAuthory == "") {
		document.getElementById("D_ERR_HCAUTHORITY").innerHTML = "Please Upload HC Authoirity Image.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_HCAUTHORITY").innerHTML = "";
	}*/
	
	// HC Authority Details
	/*var regNumber = document.getElementById("registrationnumber").value.trim();
	if (regNumber == "") {
		document.getElementById("D_ERR_REGNUMBER").innerHTML = "Please Upload Registration Detail Image.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_REGNUMBER").innerHTML = "";
	}*/
	
	// Car Make
	var carMake = document.getElementById("d_carmake").value.trim();
	if (carMake == "") {
		document.getElementById("D_ERR_CARMAKE").innerHTML = "Please Enter Car Make and Year.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_CARMAKE").innerHTML = "";
	}
	
	// Car Year Model
	var carMake = document.getElementById("d_carYearModel").value.trim();
	if (carMake == "") {
		document.getElementById("D_ERR_CARYEARMODEL").innerHTML = "Please Enter Car Make and Year.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_CARYEARMODEL").innerHTML = "";
	}
	
	// Insurance Company
	var carMake = document.getElementById("d_company").value.trim();
	if (carMake == "") {
		document.getElementById("D_ERR_INSURANCECOMPANY").innerHTML = "Please Enter Insurance Company.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_INSURANCECOMPANY").innerHTML = "";
	}
	
	/*// Insurance Type
	var carMake = document.getElementById("insurancetype").value.trim();
	if (carMake == "") {
		document.getElementById("D_ERR_INSURANCETYPE").innerHTML = "Please Upload Insurence Type Image.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_INSURANCETYPE").innerHTML = "";
	}*/

	//alert(err_flag);
	if (err_flag == true)
		return false;
	else
		return true;
}


function validateChangePass()
{	
	var err_flag = false;

	// Password Validation confirmPassword
	var oldpassword = document.getElementById("oldpassword").value;
	var newpassword = document.getElementById("newpassword").value;
	var confpassword = document.getElementById("confpassword").value;
	
	if(oldpassword == '')
	{
		document.getElementById("ERR_OLD_PASSWORD").innerHTML = "Please Enter Old Password.";
		err_flag = true;
	}
	else
	{
		document.getElementById("ERR_OLD_PASSWORD").innerHTML = "";
	}
	
	if(newpassword == '')
	{
		document.getElementById("ERR_NEW_PASSWORD").innerHTML = "Please Enter New Password.";
		err_flag = true;
	}
	else
	{
		document.getElementById("ERR_NEW_PASSWORD").innerHTML = "";
	}
	
	if(confpassword == '')
	{
		document.getElementById("ERR_CONF_PASSWORD").innerHTML = "Please Enter Confirm Password.";
		err_flag = true;
	}
	else
	{
		document.getElementById("ERR_CONF_PASSWORD").innerHTML = "";
	}
	
	if (newpassword != confpassword) 
	{
		document.getElementById("ERR_CONF_PASSWORD").innerHTML = "Confirm Password is not match.";
		err_flag = true;
	}
	else 
	{
		document.getElementById("ERR_CONF_PASSWORD").innerHTML = "";
	}


	//alert(err_flag);
	if (err_flag == true)
	return false;
	else
	return true;
}


$('#messageModel').on('hidden.bs.modal', function () {
  location.reload();
})


</script>

<?php 
	
	if ($this->session->flashdata('ERR_EDIT_DRIVER_EMAILDUPLICATION') != "") 
	{
	?>
    	<script type="application/javascript">
			$( document ).ready(function() {
			$('#myModalEdit').modal('show');
			});
		</script>
    <?php	
	}
	
	if ($this->session->flashdata('ERR_PASSWORD_MESSAGE') != "") 
	{
	?>
    	<script type="application/javascript">
			$( document ).ready(function() {
			$('#myModalChangePass').modal('show');
			});
		</script>
    <?php	
	}
	
	
?>

