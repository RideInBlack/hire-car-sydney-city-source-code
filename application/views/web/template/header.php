<!DOCTYPE HTML>
<html>
    <head>
        <!-- =================== META ==================== -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta name="keywords" content="<?=$meta_keyword?>">
        <meta name="description" content="<?=$meta_description?>">
		<title><?=@$title?> :: Welcome To Hire Car Sydney City ::</title>
        <!-- =================== FAVICON ==================== -->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <!-- =================== WEB CSS ==================== -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/web') ?>/css/bootstrap.min.css" />
		<? //Bootstrap and font awesomev, custom font, alider ?>
		<!-- Plugin CSS -->
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/web') ?>/css/plugin.css" />
        <? //Select 2, fancy select, datepicker, jquery timepicker ?>
        
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/web') ?>/css/style.css" />
        <?php /*?><link rel="stylesheet" type="text/css" href="<?= base_url('assets/web') ?>/css/media.css" /><?php */?>
        <?php /*?><link rel="stylesheet" type="text/css" href="<?= base_url('assets/web') ?>/css/responsive.css" /><?php */?>
    </head>
    <body>
    <!-- ====== WRAPPER BOC ====== -->
	<div class="page-wrapper">
    <!-- ====== HEADER BOC ====== -->
    <header>
        <!-- /Banner BOC -->
        <div id="carousel-example-generic" class="myCarousel carousel slide" data-ride="carousel"> 
            <?php $this->load->view('web/template/menu'); ?>
            <!-- Indicators BOC -->
            <ol class="carousel-indicators hidden">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            </ol>
            <!-- Indicators EOC --> 
            <!-- Banner Content BOC  -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="carousel-content">
                        <h1>Hire Car Sydney City </h1>
                        <hr class="style-one"/>
                        <h5> We will pick you up </h5>
                    </div>
                </div>
                <div class="item item-1">
                    <div class="carousel-content">
                        <h1>Hire Car Sydney City </h1>
                        <hr class="style-one"/>
                        <h5> We will pick you up </h5>
                    </div>
                </div>
                <div class="item item-2">
                    <div class="carousel-content">
                        <h1>Hire Car Sydney City </h1>
                        <hr class="style-one"/>
                        <h5> We will pick you up </h5>
                    </div>
                </div>
                <div class="item item-3">
                    <div class="carousel-content">
                        <h1>Hire Car Sydney City </h1>
                        <hr class="style-one"/>
                        <h5> We will pick you up </h5>
                    </div>
                </div>
                <div class="item item-4">
                    <div class="carousel-content">
                        <h1>Hire Car Sydney City </h1>
                        <hr class="style-one"/>
                        <h5> We will pick you up </h5>
                    </div>
                </div>
            </div>
            <!-- Banner Content EOC  --> 
            <!-- Controls BOC --> 
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> 
            	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><i class="fa fa-angle-left img-circle"></i></span> 
            </a> 
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> 
            	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><i class="fa fa-angle-right img-circle"></i></span> 
            </a> 
            <!-- Controls EOC --> 
        </div>
        <!-- /Banner EOC --> 
    </header>
    <!-- ====== HEADER EOC ====== --> 
	<!-- ================ Web Js =================== -->
	<script type="text/javascript" src="<?= base_url('assets/web') ?>/js/jquery.min.js"></script> 
	<?php /*?><script type="text/javascript" src="<?= base_url('assets/web') ?>/js/bootstrap.min.js"></script><?php */?>