<!-- /Navigation BOC -->
<div class="header-nav">
    <div class="brand">
        <a href="Javascript:void(0);" title="Hire Car Sydney City">
            <img src="<?= base_url('assets/web') ?>/images/logo.png" alt="Logo"/>
        </a>
    </div>
    <nav>
        <!-- /Slideout Menu BOC -->
        <div class="slideout-menu">
            <h3><a href="#" class="slideout-menu-toggle"><i class="fa fa-close"></i></a></h3>
            <ul>
                <li>
                    <a href="<?= base_url() ?>" title="Home">Home </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>cms/view/WhatWeAreAbout" title="About Us">About Us </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>cms/view/OurHistory" title="Our History">Our Services </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>cms/view/Help" title="Help">Help</a>
                </li>
                <li>
                    <a href="Javascript:void(0);" title="Payment">Book Now </a>
                </li>
                <li>
                    <a href="<?= base_url('inquiry') ?>" title="Inquiry">Inquiry </a>
                </li>
                <li>
                    <a href="<?= base_url('contact-us') ?>" title="Contact Us">Contact Us </a>
                </li>
            </ul>
        </div>
        <!-- /Slideout Menu EOC --> 
        <?php if(isset($_SESSION['HIRE_CAR']['DRIVER'])) { ?>
        <!-- /Button BOC -->
        <ul class="user-item">
            <li>
                <a href="<?=  base_url('driverprofile')?>" title="Dashboard"><i class="fa fa-th-large"></i></a>
            </li>
            <li>
                <a href="<?=  base_url('logout')?>" title="Log Out"><i class="fa fa-sign-out"></i></a>
            </li>
            <li>
                <a href="Javascript:void(0);" title="Navigation" class="slideout-menu-toggle">
                    <i class="fa fa-bars"></i></a>
            </li>
        </ul>
        <!-- /Button EOC --> 
        <?php } else if(isset($_SESSION['HIRE_CAR']['RIDER'])) { ?>
        <!-- /Button BOC -->
        <ul class="user-item">
            <li>
                <a href="<?=  base_url('riderprofile')?>" title="Dashboard"><i class="fa fa-th-large"></i></a>
            </li>
            <li>
                <a href="<?=  base_url('logout')?>" title="Log Out"><i class="fa fa-sign-out"></i></a>
            </li>
            <li>
                <a href="Javascript:void(0);" title="Navigation" class="slideout-menu-toggle">
                    <i class="fa fa-bars"></i></a>
            </li>
        </ul>
        <!-- /Button EOC --> 
        <?php } else { ?>
        <!-- /Button BOC -->
        <ul class="user-item">
            <li><a href="Javascript:void(0);" data-toggle="modal" data-target="#myModal1" title="Signup By Rider"><i class="fa fa-user"></i></a></li>
            <li><a href="Javascript:void(0);" data-toggle="modal" data-target="#myModal2" title="Signup By Driver"><i class="fa fa-user"></i></a></li>
            <?php /*?><li>
                <a href="Javascript:void(0);" title="Add to cart"><i class="fa fa-shopping-cart"></i></a>
            </li><?php */?>
            <li>
                <a href="Javascript:void(0);" title="Login" data-toggle="modal" data-target="#login"><i class="fa fa-lock"></i></a>
            </li>
            <li>
                <a href="Javascript:void(0);" title="Navigation" class="slideout-menu-toggle"><i class="fa fa-bars"></i></a>
            </li>
        </ul>
        <!-- /Button EOC --> 
        <?php } ?>
    </nav>
    
    <?php if ($this->session->flashdata('SUCC_DRIVER_MESSAGE') != ""): ?>
    <div class="alert alert-success alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;margin-top:15px;">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('SUCC_DRIVER_MESSAGE') ?>
    </div>
    <?php endif; ?>
</div>
<!-- /Navigation EOC --> 
