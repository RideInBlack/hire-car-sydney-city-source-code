<?php
//echo '<pre>'; print_r($gift_card_details); 
//print_r($_SESSION);
//echo '</pre>';
?>

<?php $this->load->view('web/riderprofile/profile-header'); ?>
<!-- /Profile Listing Section BOC -->
<section class=".profile-list">
    
    <div class="container"> 
        <div class="row">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Coupon Code</th>
                        <th>Price</th>
                        <th>Amount remaining</th>
                        <th>Status</th>
                        <th>Purchase Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($coupon_details as $coupon) : ?>
                        <tr>
                            <td><?= $i++; ?></td>
                            <td><?= $coupon->coupon_code ?></td>
                            <td>$<?= $coupon->price ?></td>
                            <td>$<?= $coupon->remaining_amount ?></td>
                            <td>
                            	<?php if(intval($coupon->remaining_amount < 0)): ?>
                                	Used
                            	<?php elseif($coupon->payment_type == 'deposit' && intval($coupon->status) == 0): ?>
                                	Payment Deposit Due
                                <?php elseif(intval($coupon->status) == 1): ?>
                                	Active
                                <?php else: ?>
                                	Inactive
								<?php endif; ?>
                            </td>
                            <td><?= date('d/m/Y @ h:i:s A', strtotime($coupon->generate_datetime)) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /Profile Items BOC -->
    </div>
</section>
<!-- /Profile Listing Section EOC -->