
<section class="profile grey-color-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Profile</h3>
                <hr class="style-two"/>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php if ($this->session->flashdata('PAYMENT_SUCC') != ""): ?>
                    <div class="alert alert-success">
                        <p><?= $this->session->flashdata('PAYMENT_SUCC') ?></p>
                    </div>
                <?php elseif ($this->session->flashdata('PAYMENT_ERR') != ""): ?>
                    <div class="alert alert-danger">
                        <p><?= $this->session->flashdata('PAYMENT_ERR') ?></p>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <!-- User Info BOC -->
        <div class="row">
            <div class="user-profile">
                <div class="col-lg-2 col-sm-2 col-md-2"><img src="<?php echo base_url('assets/web') . "/images/img-privew-user.png"; ?>" alt="Privew" class="img-center"/></div>
                <div class="col-lg-6">
                    <h4 class="text-left"><?php echo $rider_details->first_name . ' ' . $rider_details->last_name; ?></h4>
                    <ul class="user-info">
                        <?php /* ?><li><i class="fa fa-map-marker"></i><?php echo $_SESSION['HIRE_CAR']['DRIVER']->address;?> </li><?php */ ?>
                        <li><i class="fa fa-envelope"></i><a href="Javascript:void(0);"  title="<?php echo $rider_details->email_id; ?>"><?php echo $rider_details->email_id; ?></a> </li>
                        <li><i class="fa fa-phone"></i><a href="tel:<?php echo $rider_details->mobile_number; ?>" title="<?php echo $rider_details->mobile_number; ?>"><?php echo $rider_details->mobile_number; ?></a></li>
                        <li class="info-action"> 
                            <a href="Javascript:void(0);" data-toggle="modal" data-target="#myModalEdit" title="Edit Profile">Edit Profile</a> 
                            <?php if(@$_SESSION['HIRE_CAR']['RIDER']->LOGIN_TYPE != 'FB') : ?>
                            <a href="Javascript:void(0);" data-toggle="modal" data-target="#myModalChangePass" title="Change Password">Change Password</a>
                            <?php endif; ?>
                            <a href="Javascript:void(0);" data-toggle="modal" data-target="#myModalCreditcard" title="Setting">Setting</a> 
                            <a href="<?=  base_url('riderprofile/couponhistory')?>" title="Coupon History">Coupon History</a> 
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- User Info EOC --> 
    </div>
</section>
<!-- /Profile Section EOC -->

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_EDIT_RIDER_EMAILDUPLICATION') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_EDIT_RIDER_EMAILDUPLICATION') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url(); ?>riderprofile/editprofile" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <div class="common-form">
                            <div class="row">
                            	<?php if($_SESSION['HIRE_CAR']['RIDER']->LOGIN_TYPE == 'FB'):
                                	$readonly = "readonly";
                            	endif;?>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="r_email" id="r_email" placeholder="Email Address" class="form-control" value="<?php echo $rider_details->email_id; ?>" <?=@$readonly?>>
                                        <p class="text-red" id="R_ERR_EMAIL"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="r_fname" id="r_fname" placeholder="First Name" class="form-control" value="<?php echo $rider_details->first_name; ?>" <?=@$readonly?>>
                                        <p class="text-red" id="R_ERR_FNAME"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="r_lname" id="r_lname" placeholder="Last Name" class="form-control" value="<?php echo $rider_details->last_name; ?>" <?=@$readonly?>>
                                        <p class="text-red" id="R_ERR_LNAME"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="tel" name="r_mobile" id="r_mobile" placeholder="Mobile No" class="form-control" value="<?php echo $rider_details->mobile_number; ?>">
                                        <p class="text-red" id="R_ERR_MOBILE"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="address" id="address" placeholder="Street Number and Name" class="form-control" value="<?php echo $rider_details->address; ?>">
                                        <p class="text-red" id="R_ERR_ADDRESS"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="suburb" id="suburb" placeholder="Suburb" class="form-control" value="<?php echo $rider_details->suburb; ?>">
                                        <p class="text-red" id="R_ERR_SUBURB"></p>
                                    </div>
                                </div>
								
								<?php /*?><div class="col-lg-12">
                                    <div class="form-group">
                                        <select id="city" name="city" class="form-control text-black">
											<option value="0">Select City</option>
											<?php foreach ($city_list AS $val): ?>
												<option id="<?= $val->city_id ?>" value="<?= $val->city_id ?>" <?php if($rider_details->city == $val->city_id) { ?> selected="selected" <?php } ?>><?= $val->name ?></option>
											<?php endforeach; ?>
										</select>
                                        <p class="text-red" id="R_ERR_CITY"></p>
                                    </div>
                                </div><?php */?>
								
								<div class="col-lg-12">
                                    <div class="form-group">
										<select id="state" name="state" class="form-control text-black">
											<option value="0">Select State</option>
											<?php foreach ($state_list AS $val): ?>
												<option id="<?= $val->state_id ?>" value="<?= $val->state_id ?>" <?php if($rider_details->state == $val->state_id) { ?> selected="selected" <?php } ?>><?= $val->name ?></option>
											<?php endforeach; ?>
										</select>
										<p class="text-red" id="R_ERR_STATE"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="postal" id="postal" placeholder="Postal Code" class="form-control" value="<?php echo $rider_details->zipcode; ?>">
                                        <p class="text-red" id="R_ERR_POSTAL"></p>
                                    </div>
                                </div>
								
                                <?php /*?><div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="r_language" id="r_language" placeholder="Language" class="form-control" value="<?php echo $rider_details->language; ?>">
                                    </div>
                                </div><?php */?>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="register" value="Save" class="btn btn-full" onclick="return validateEditDriver()">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalChangePass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_PASSWORD_MESSAGE') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_PASSWORD_MESSAGE') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url(); ?>riderprofile/changepassword" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <div class="common-form">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="oldpassword" id="oldpassword" placeholder="Old Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_OLD_PASSWORD"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="newpassword" id="newpassword" placeholder="New Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_NEW_PASSWORD"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="confpassword" id="confpassword" placeholder="Confirm Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_CONF_PASSWORD"></p>
                                    </div>
                                </div>


                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="Save" value="Save" class="btn btn-full" onclick="return validateChangePass()">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalCreditcard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Creadit Card Details</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_PASSWORD_MESSAGE') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_PASSWORD_MESSAGE') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url(); ?>riderprofile/updatecreditcard" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <div class="common-form">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Card Type</b></label><br />
                                        <select name="card_type" id="card_type" class="form-control">
                                            <option value="">Select a type</option>
                                            <option <?php if (isset($card_details->card_type) && $card_details->card_type == "American Express") { ?> selected <?php } ?> value="American Express">American Express</option>
                                            <option <?php if (isset($card_details->card_type) && $card_details->card_type == "Master") { ?> selected <?php } ?> value="Master">Master</option>
                                            <option <?php if (isset($card_details->card_type) && $card_details->card_type == "Visa") { ?> selected <?php } ?> value="Visa">Visa</option>
                                        </select>
                                        <p class="text-red" id="ERR_CARD_TYPE"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Card No.</b></label>
                                        
                                        <?php if (isset($card_details->card_no) && $card_details->card_no != ''): ?>
										
                                        <a href="javascript:void(0)" class="text-primary" id="editCardNumber"> [Edit] </a>
                                        <input type="text" name="cardno" id="cardno" placeholder="Card Number" class="form-control" disabled="disabled" value="<? $card_number = decrypt($card_details->card_no);
											echo "XXXXXXXXXXXX".substr($card_number, 12, strlen($card_number));
                                          ?>">
                                        <p class="text-red" id="ERR_CARD_NO"></p>
										
                                        <script>
										$(document).ready(function(e){
											$('#editCardNumber').click (function(e) {
												var cardNumber = <?=$card_number?>;
												//alert(cardNumber);
												$('#cardno').val(cardNumber);
												$('#cardno').removeAttr( "disabled" );
												$('#saveCardBtn').removeAttr( "disabled" );
											});
										});
										</script>
										
										<?php else :?>
										
                                        <input type="text" name="cardno" id="cardno" placeholder="Card Number" class="form-control" disabled="disabled" value="">
                                        <p class="text-red" id="ERR_CARD_NO"></p>
                                        
										<?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Expiry Date</b></label><br />
                                        <select name="month" id="month" class="form-control" style="width:120px;margin-right:25px;float:left;">
                                            <option value="">Month</option>
                                            <?php
                                            $current = 01;
                                            for ($i = $current; $i <= 12; $i++) {
                                                ?>
                                                <option value="<?= $i ?>" <?php if (isset($card_details->exp_month) && $card_details->exp_month == $i) { ?> selected="selected" <?php } ?>><?= $i ?></option>
                                            <?php } ?>
                                        </select>

                                        <select name="year" id="year" class="form-control" style="width:120px;">
                                            <option value="">Year</option>
                                            <?php
                                            $current = date('y');
                                            for ($i = $current; $i <= $current + 35; $i++) {
                                                ?>
                                                <option value="<?= $i ?>"  <?php if (isset($card_details->exp_year) && $card_details->exp_year == $i) { ?> selected="selected" <?php } ?>><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                        <p class="text-red" id="ERR_EXP"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>CCV No.</b></label><br />
                                        <input type="text" name="ccvno" id="ccvno" placeholder="CCV Number" class="form-control" value="<?php
                                        if (isset($card_details->ccv_no) && $card_details->ccv_no != '') {
                                            echo $card_details->ccv_no;
                                        }
                                        ?>" maxlength="3">
                                        <p class="text-red" id="ERR_CCV"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label><b>Name of Card</b></label><br />
                                        <input type="text" name="name_of_card" id="name_of_card" placeholder="Name of Card" class="form-control" value="<?php
                                        if (isset($card_details->name_of_card) && $card_details->name_of_card != '') {
                                            echo $card_details->name_of_card;
                                        }
                                        ?>">
                                        <p class="text-red" id="ERR_NAME_CARD"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<?php if (isset($card_details->card_no) && $card_details->card_no != ''): ?>
                                    
                                    	<input type="submit" name="Save" value="Save" class="btn btn-full" onclick="return validateCard()" disabled="disabled" id="saveCardBtn">
                                        <?php else: ?>
                                        <input type="submit" name="Save" value="Save" class="btn btn-full" onclick="return validateCard()" >
                                        <?php endif;?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function validateEditDriver()
    {
        var err_flag = false;
        var re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        // Email Id Validation
        var email = document.getElementById("r_email").value.trim();
        if (email == "") {
            document.getElementById("R_ERR_EMAIL").innerHTML = "Please Enter Email Address.";
            err_flag = true;
        }
        else if (!re_email.test(email)) {
            document.getElementById("R_ERR_EMAIL").innerHTML = "Please Enter Valid Email Address.";
        }
        else {
            document.getElementById("R_ERR_EMAIL").innerHTML = "";
        }

        // Firstname Validation
        var fname = document.getElementById("r_fname").value.trim();
        if (fname == "") {
            document.getElementById("R_ERR_FNAME").innerHTML = "Please Enter First Name.";
            err_flag = true;
        }
        else {
            document.getElementById("R_ERR_FNAME").innerHTML = "";
        }

        // Lastname Validation
        var lname = document.getElementById("r_lname").value.trim();
        if (lname == "") {
            document.getElementById("R_ERR_LNAME").innerHTML = "Please Enter Last Name.";
            err_flag = true;
        }
        else {
            document.getElementById("R_ERR_LNAME").innerHTML = "";
        }

        // Mobile Number Validation
        var mobile = document.getElementById("r_mobile").value.trim();
        if (mobile == "" || isNaN(mobile))
        {
            document.getElementById("R_ERR_MOBILE").innerHTML = "Please Enter Mobile Number.";
            err_flag = true;
        }
        else {
            document.getElementById("R_ERR_MOBILE").innerHTML = "";
        }

        //alert(err_flag);
        if (err_flag == true)
            return false;
        else
            return true;
    }


    function validateChangePass()
    {
        var err_flag = false;

        // Password Validation confirmPassword
        var oldpassword = document.getElementById("oldpassword").value;
        var newpassword = document.getElementById("newpassword").value;
        var confpassword = document.getElementById("confpassword").value;

        if (oldpassword == '')
        {
            document.getElementById("ERR_OLD_PASSWORD").innerHTML = "Please Enter Old Password.";
            err_flag = true;
        }
        else
        {
            document.getElementById("ERR_OLD_PASSWORD").innerHTML = "";
        }

        if (newpassword == '')
        {
            document.getElementById("ERR_NEW_PASSWORD").innerHTML = "Please Enter New Password.";
            err_flag = true;
        }
        else
        {
            document.getElementById("ERR_NEW_PASSWORD").innerHTML = "";
        }

        if (confpassword == '')
        {
            document.getElementById("ERR_CONF_PASSWORD").innerHTML = "Please Enter Confirm Password.";
            err_flag = true;
        }
        else
        {
            document.getElementById("ERR_CONF_PASSWORD").innerHTML = "";
        }

        if (newpassword != confpassword)
        {
            document.getElementById("ERR_CONF_PASSWORD").innerHTML = "Confirm Password is not match.";
            err_flag = true;
        }
        else
        {
            document.getElementById("ERR_CONF_PASSWORD").innerHTML = "";
        }


        //alert(err_flag);
        if (err_flag == true)
            return false;
        else
            return true;
    }

    function validateCard()
    {
        var err_flag = false;
        var re_ccv = /^[0-9]{3,4}$/;

        var cardType = document.getElementById("card_type").value.trim();
        if (cardType == "" || cardType == 0) {
            document.getElementById("ERR_CARD_TYPE").innerHTML = "Please Select Valid Card Type.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_CARD_TYPE").innerHTML = "";
        }

        // Card Number Validation
        var cardnumber = document.getElementById("cardno").value.trim();
        if (cardnumber == "" || isNaN(cardnumber))
        {
            document.getElementById("ERR_CARD_NO").innerHTML = "Please Enter your Card Number.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_CARD_NO").innerHTML = "";
        }

        var ccv = document.getElementById("ccvno").value.trim();
        if (ccv == "" || isNaN(ccv))
        {
            document.getElementById("ERR_CCV").innerHTML = "Please Enter CCV Number.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_CCV").innerHTML = "";
        }

        var expiremonth = document.getElementById("month").value.trim();
        var expireyear = document.getElementById("year").value.trim();

        if (expiremonth == "" || expireyear == "")
        {
            document.getElementById("ERR_EXP").innerHTML = "Please Enter Expiry Date.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_EXP").innerHTML = "";
        }
        //alert(err_flag);
        if (err_flag == true)
            return false;
        else
            return true;
    }


</script>

<?php
if ($this->session->flashdata('ERR_EDIT_DRIVER_EMAILDUPLICATION') != "") {
    ?>
    <script type="application/javascript">
        $( document ).ready(function() {
        $('#myModalEdit').modal('show');
        });
    </script>
    <?php
}

if ($this->session->flashdata('ERR_PASSWORD_MESSAGE') != "") {
    ?>
    <script type="application/javascript">
        $( document ).ready(function() {
        $('#myModalChangePass').modal('show');
        });
    </script>
    <?php
}
?>
