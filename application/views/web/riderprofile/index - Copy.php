<!-- /Profile Section BOC -->
<section class="profile grey-color-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Profile</h3>
                <hr class="style-two"/>
            </div>
        </div>

        <!-- User Info BOC -->
        <div class="row">
            <div class="user-profile">
                <div class="col-lg-2 col-sm-2 col-md-2"><img src="<?php echo base_url('assets/web')."/images/img-privew-user.png"; ?>" alt="Privew" class="img-center"/></div>
                <div class="col-lg-6">
                    <h4 class="text-left"><?php echo $rider_details->first_name.' '.$rider_details->last_name; ?></h4>
                    <ul class="user-info">
                        <?php /*?><li><i class="fa fa-map-marker"></i><?php echo $_SESSION['HIRE_CAR']['DRIVER']->address;?> </li><?php */?>
                        <li><i class="fa fa-envelope"></i><a href="<?php echo $rider_details->email_id;?>" title="<?php echo $rider_details->email_id;?>"><?php echo $rider_details->email_id;?></a> </li>
                        <li><i class="fa fa-phone"></i><a href="tel:<?php echo $rider_details->mobile_number;?>" title="<?php echo $rider_details->mobile_number;?>"><?php echo $rider_details->mobile_number;?></a></li>
                        <li class="info-action"> 
                        	<a href="Javascript:void(0);" data-toggle="modal" data-target="#myModalEdit" title="Edit Profile">Edit Profile</a> 
                            <a href="Javascript:void(0);" data-toggle="modal" data-target="#myModalChangePass" title="Change Password">Change Password</a>
                            <a href="Javascript:void(0);" data-toggle="modal" data-target="#myModalCreditcard" title="Setting">Setting</a> </li>
                            
                    </ul>
                </div>
            </div>
        </div>
        <!-- User Info EOC --> 

    </div>
</section>
<!-- /Profile Section EOC --> 

<!-- /Profile Listing Section BOC -->
<section class="profile-list">
    <div class="container"> 

        <!-- /Profile Items BOC -->
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6"> <img alt="Modle Car" src="<?=base_url('assets/web')?>/images/img-models-car.jpg" class="img-border"> </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h4 class="text-left">tesla s 70d</h4>
                <div class="content">
                    <h5>Used for</h5>
                    <h6>Wedding, <span>(on 1st Aug 2015)</span></h6>
                </div>
                <div class="content">
                    <h5>Held At</h5>
                    <h6>Bathurst  - To -  Blue Mountains</h6>
                </div>
                <div class="content">
                    <h5>Trip Status</h5>
                    <h6>Complete</h6>
                </div>
                <div class="content">
                    <h5>Description</h5>
                    <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</h6>
                </div>
            </div>
        </div>
        <hr/>
        <!-- /Profile Items EOC --> 

    </div>
</section>
<!-- /Profile Listing Section EOC -->


<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_EDIT_RIDER_EMAILDUPLICATION') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_EDIT_RIDER_EMAILDUPLICATION') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url(); ?>riderprofile/editprofile" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <div class="common-form">
                            <div class="row">

								<div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="r_email" id="r_email" placeholder="Email Address" class="form-control" value="<?php echo $rider_details->email_id;?>">
                                        <p class="text-red" id="R_ERR_EMAIL"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="r_fname" id="r_fname" placeholder="First Name" class="form-control" value="<?php echo $rider_details->first_name;?>">
                                        <p class="text-red" id="R_ERR_FNAME"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="r_lname" id="r_lname" placeholder="Last Name" class="form-control" value="<?php echo $rider_details->last_name;?>">
                                        <p class="text-red" id="R_ERR_LNAME"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="tel" name="r_mobile" id="r_mobile" placeholder="Mobile No" class="form-control" value="<?php echo $rider_details->mobile_number;?>">
                                        <p class="text-red" id="R_ERR_MOBILE"></p>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="r_language" id="r_language" placeholder="Language" class="form-control" value="<?php echo $rider_details->language;?>">
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="register" value="Save" class="btn btn-full" onclick="return validateEditDriver()">
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalChangePass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_PASSWORD_MESSAGE') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_PASSWORD_MESSAGE') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url(); ?>riderprofile/changepassword" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <div class="common-form">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="oldpassword" id="oldpassword" placeholder="Old Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_OLD_PASSWORD"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="newpassword" id="newpassword" placeholder="New Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_NEW_PASSWORD"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="confpassword" id="confpassword" placeholder="Confirm Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_CONF_PASSWORD"></p>
                                    </div>
                                </div>
                               

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="Save" value="Save" class="btn btn-full" onclick="return validateChangePass()">
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalCreditcard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Creadit Card Details</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_PASSWORD_MESSAGE') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_PASSWORD_MESSAGE') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url(); ?>riderprofile/updatecreditcard" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <div class="common-form">
                            <div class="row">
                            	
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<label><b>Card Type</b></label><br />
                                        <select name="card_type" id="card_type" class="form-control">
                                        	<option value="">Select a type</option>
                                            <option value="Ae">American Express</option>
                                            <option value="Master">Master</option>
                                            <option value="Visa">Visa</option>
                                        </select>
                                        <p class="text-red" id="ERR_CARD_TYPE"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<label><b>Card No.</b></label>
                                        <input type="text" name="cardno" id="cardno" placeholder="Card Number" class="form-control" value="">
                                        <p class="text-red" id="ERR_CARD_NO"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<label><b>Expiry Date</b></label><br />
                                        <select name="month" id="month" class="form-control" style="width:120px;margin-right:25px;float:left;">
                                        	<option id="">Month</option>
                                            <option id="01">01</option>
                                            <option id="02">02</option>
                                            <option id="03">03</option>
                                            <option id="04">04</option>
                                            <option id="05">05</option>
                                            <option id="06">06</option>
                                            <option id="07">07</option>
                                            <option id="08">08</option>
                                            <option id="09">09</option>
                                            <option id="10">10</option>
                                            <option id="11">11</option>
                                            <option id="12">12</option>
                                        </select>
                                        
                                         <select name="year" id="year" class="form-control" style="width:120px;">
                                        	<option id="">Year</option>
                                            <?php $current = date('Y');
                                                for($i=$current;$i<=$current + 35;$i++) { ?>
                                                <option id="<?=$i?>"><?=$i?></option>
                                            <?php } ?>
                                        </select>
                                        <p class="text-red" id="ERR_EXP"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<label><b>CCV No.</b></label><br />
                                        <input type="text" name="ccvno" id="ccvno" placeholder="CCV Number" class="form-control" value="" maxlength="3">
                                        <p class="text-red" id="ERR_CCV"></p>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                    	<label><b>Name of Card</b></label><br />
                                        <input type="text" name="name_of_card" id="name_of_card" placeholder="Name of Card" class="form-control" value="">
                                        <p class="text-red" id="ERR_NAME_CARD"></p>
                                    </div>
                                </div>
                               

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="Save" value="Save" class="btn btn-full" onclick="return validateSettings()">
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

function validateEditDriver()
{	
	var err_flag = false;
	var re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	// Email Id Validation
	var email = document.getElementById("r_email").value.trim();
	if (email == "") {
	document.getElementById("R_ERR_EMAIL").innerHTML = "Please Enter Email Address.";
	err_flag = true;
	}
	else if (!re_email.test(email)) {
	document.getElementById("R_ERR_EMAIL").innerHTML = "Please Enter Valid Email Address.";
	}
	else {
	document.getElementById("R_ERR_EMAIL").innerHTML = "";
	}

	// Firstname Validation
	var fname = document.getElementById("r_fname").value.trim();
	if (fname == "") {
	document.getElementById("R_ERR_FNAME").innerHTML = "Please Enter First Name.";
	err_flag = true;
	}
	else {
	document.getElementById("R_ERR_FNAME").innerHTML = "";
	}

	// Lastname Validation
	var lname = document.getElementById("r_lname").value.trim();
	if (lname == "") {
	document.getElementById("R_ERR_LNAME").innerHTML = "Please Enter Last Name.";
	err_flag = true;
	}
	else {
	document.getElementById("R_ERR_LNAME").innerHTML = "";
	}

	// Mobile Number Validation
	var mobile = document.getElementById("r_mobile").value.trim();
	if (mobile == "" || isNaN(mobile)) 
	{
	document.getElementById("R_ERR_MOBILE").innerHTML = "Please Enter Mobile Number.";
	err_flag = true;
	}
	else {
	document.getElementById("R_ERR_MOBILE").innerHTML = "";
	}

	//alert(err_flag);
	if (err_flag == true)
	return false;
	else
	return true;
}


function validateChangePass()
{	
	var err_flag = false;

	// Password Validation confirmPassword
	var oldpassword = document.getElementById("oldpassword").value;
	var newpassword = document.getElementById("newpassword").value;
	var confpassword = document.getElementById("confpassword").value;
	
	if(oldpassword == '')
	{
		document.getElementById("ERR_OLD_PASSWORD").innerHTML = "Please Enter Old Password.";
		err_flag = true;
	}
	else
	{
		document.getElementById("ERR_OLD_PASSWORD").innerHTML = "";
	}
	
	if(newpassword == '')
	{
		document.getElementById("ERR_NEW_PASSWORD").innerHTML = "Please Enter New Password.";
		err_flag = true;
	}
	else
	{
		document.getElementById("ERR_NEW_PASSWORD").innerHTML = "";
	}
	
	if(confpassword == '')
	{
		document.getElementById("ERR_CONF_PASSWORD").innerHTML = "Please Enter Confirm Password.";
		err_flag = true;
	}
	else
	{
		document.getElementById("ERR_CONF_PASSWORD").innerHTML = "";
	}
	
	if (newpassword != confpassword) 
	{
		document.getElementById("ERR_CONF_PASSWORD").innerHTML = "Confirm Password is not match.";
		err_flag = true;
	}
	else 
	{
		document.getElementById("ERR_CONF_PASSWORD").innerHTML = "";
	}


	//alert(err_flag);
	if (err_flag == true)
	return false;
	else
	return true;
}

function validateSettings()
{	
	var err_flag = false;
	var re_ccv = /^[0-9]{3,4}$/;
	
	var cardType = document.getElementById("card_type").value.trim();
	if (cardType == "" || cardType == 0) {
		document.getElementById("ERR_CARD_TYPE").innerHTML = "Please Select Valid Card Type.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_CARD_TYPE").innerHTML = "";
	}

	// Card Number Validation
	var cardnumber = document.getElementById("cardno").value.trim();
	if (cardnumber == "") {
		document.getElementById("ERR_CARD_NO").innerHTML = "Please Enter your Card Number.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_CARD_NO").innerHTML = "";
	}
	
	var ccv = document.getElementById("ccvno").value.trim();
	if (ccv == "") {
		document.getElementById("ERR_CCV").innerHTML = "Please Enter CCV Number.";
		err_flag = true;
	}
	else if (!re_ccv.test(email)) {
		cument.getElementById("ERR_CCV").innerHTML = "Please Enter Valid CCV Number.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_CCV").innerHTML = "";
	}
	
	var expiremonth = document.getElementById("month").value.trim();
	var expireyear = document.getElementById("year").value.trim();
	if (expiremonth == "" || expiremonth == 0 || expireyear == "" || expireyear == 0) {
		document.getElementById("ERR_EXP").innerHTML = "Please Enter Expiry Date.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_EXP").innerHTML = "";
	}
	//alert(err_flag);
	if (err_flag == true)
	return false;
	else
	return true;
}


</script>

<?php 
	
	if ($this->session->flashdata('ERR_EDIT_DRIVER_EMAILDUPLICATION') != "") 
	{
	?>
    	<script type="application/javascript">
			$( document ).ready(function() {
			$('#myModalEdit').modal('show');
			});
		</script>
    <?php	
	}
	
	if ($this->session->flashdata('ERR_PASSWORD_MESSAGE') != "") 
	{
	?>
    	<script type="application/javascript">
			$( document ).ready(function() {
			$('#myModalChangePass').modal('show');
			});
		</script>
    <?php	
	}
	
?>


