<!-- /Profile Section BOC -->
<?php
$this->load->helpers('encryption_helper');
$this->load->view('web/riderprofile/profile-header'); 
?> 
<?php
//print_r($_SESSION);
//echo '<pre>';print_r($ride_details);exit;
?>
<!-- /Profile Listing Section BOC -->
<section class="profile-list">

    <div class="container"> 
    	<?php if($this->session->flashdata('PAY_BY_DEPOSIT') != "") : ?>
            <div class="row">
            <div class="col-md-12">
            <div class="panel panel-success">
  				<div class="panel-heading">
    				Ride booked successfully.
  				</div>
                <div class="panel-body">
                	<?=$this->session->flashdata('PAY_BY_DEPOSIT');?>
  				</div>
			</div>
            </div>
            </div>
            <?php endif; ?>
			
			
			<?php if ($this->session->flashdata('RIDE_SAVED') != ""): ?>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-success">
  							<div class="panel-heading">
								Ride booked successfully.
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			
        <?php /* ?>
        <div class="row">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Ride Selected</th>
                        <th>Source</th>
                        <th>Destination</th>
                        <th>Estimated Distance</th>
                        <th>Estimated Fare</th>
                        <th>Paid Amount</th>
                        <th>Trip Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($ride_details as $value) : ?>
                        <tr>
                            <td><?= $value->id ?> <?= $value->model_name ?></td>
                            <td><?= $value->start_city ?></td>
                            <td><?= $value->end_city ?></td>
                            <td><?= $value->estimated_distance ?> km</td>
                            <td>$ <?= $value->estimated_fare ?></td>
                            <td>$ <?= $paid_amount = ($value->paid_amount != "") ? $value->paid_amount : 0 ?></td>
                            <td>
                                <?php if ($value->ride_status == 0): // For Waiting ?>
                                    Waiting for Confirmation
                                <?php elseif ($value->ride_status == 1): // For Driver Request Sent ?>
                                    Ride Confirmed and Waiting For Driver
                                <?php elseif ($value->ride_status == 2): // For Driver Confirm ?>
                                    Driver Confirm
                                <?php elseif ($value->ride_status == 3): // For Start ?>
                                    Running
                                <?php elseif ($value->ride_status == 4): // For Finish ?>
                                    Finish
                                <?php elseif ($value->ride_status == 5): // Rejected Request ?>
                                    Rejected
                                <?php elseif ($value->ride_status == 6): // Cancled Ride ?>
                                    Canceled
                                <?php endif; ?>
                            </td>
<!--                            <td>
                                
                                <?php if (in_array($value->ride_status, array(0, 1, 2, 3, 4))) : // For Waiting ?>
                                    <?php if ($value->estimated_fare > $paid_amount): ?>
                                        <a href="#" class="btn btn-default">Make Payment</a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>-->
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <?php  */ ?>
        
        <!-- /Profile Items BOC -->
        <?php //echo '<pre>'; print_r($ride_details);  echo '</pre>';?>
        <?php foreach ($ride_details as $value) : ?>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6"> 
                    <?php if ($value->image_name != ""): ?>
                        <img alt="Modle Car" src="<?= base_url('assets/') ?>/uploads/Car/<?= $value->car_id ?>/<?= $value->image_name ?>" class="img-border"> 
                    <?php else: ?>
                        <img alt="Modle Car" src="<?= base_url('assets/web') ?>/images/no_image.gif" class="img-border"> 
                    <?php endif; ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4 class="text-left"><?= $value->model_name ?></h4>
                    <div class="content">
                        <h5>Trip Status</h5>
                        <h6>
                            <?php if ($value->ride_status == 0): // For Waiting ?>
                                Waiting for Confirmation
                            <?php elseif ($value->ride_status == 1): // For Driver Request Sent ?>
                                Ride Confirmed and Waiting For Driver
                            <?php elseif ($value->ride_status == 2): // For Driver Confirm ?>
                                Driver Confirm
                            <?php elseif ($value->ride_status == 3): // For Start ?>
                                Running
                            <?php elseif ($value->ride_status == 4): // For Finish ?>
                                Finish
                            <?php elseif ($value->ride_status == 5): // Rejected Request ?>
                                Rejected
                            <?php elseif ($value->ride_status == 6): // Cancled Ride ?>
                                Canceled
                            <?php endif; ?>
                        </h6>
                    </div>

                    <div class="content">
                        <h5>Trip Rout</h5>
                        <h6><?= $value->pickup_location ?>, <?= $value->pickup_city ?> to <?= $value->destination_location ?>, <?= $value->destination_city ?></h6>
                    </div>

                    <?php if (in_array($value->ride_status, array(0, 1, 2, 3))) : // For Waiting ?>
                        <div class="content">
                            <h5>Estimated Distance</h5>
                            <h6><?= $value->estimated_distance ?> km</h6>
                        </div>
                        <div class="content">
                            <h5>Pick-up Datetime</h5>
                            <h6><?= date('d/m/Y @ h:i A', strtotime($value->pickup_datetime)) ?></h6>
                        </div>

                        <div class="content">
                            <h5>Estimates Fare</h5>
                            <h6>$ <?= $value->estimated_fare ?> 
                                <?php /* ?>    $ <?= $value->base_fare ?> + $ (<?= $value->per_km_rate ?> * <?= $value->estimated_distance ?>) = $ <?=($value->base_fare+($value->per_km_rate*$value->estimated_distance))?> <?php */ ?>
                            </h6>
                        </div>

                    <?php endif; ?>
                    <div class="content">
                        <h5>Paid Amount</h5>
                        <h6>$ <?=$paid_amount=($value->paid_amount != "") ? $value->paid_amount : 0 ?></h6>
                    </div>
                    
                    <?php $due_amount = $value->estimated_fare - $paid_amount ?>
                    <?php if($due_amount > 0): ?>
                    <div class="content">
                        <h5>Due Amount</h5>
                        <h6>$ <?=$due_amount?></h6>
                    </div>
                    <div class="content">
                        <a href="<?= base_url('ride/book_details/'.$value->id)?>" onclick="" class="btn"> View Details</a>
                        <?php /* ?><a href="javascript:void(0);" onclick="return sureToPaymeny('<?=  base_url('ride/payment/'.$value->car_category_id.'/'.$value->id.'/'.$due_amount)?>');" class="btn"> Make Payment</a><?php */?>
                        <script>
                            function sureToPaymeny(URL)
                            {
                                if(confirm("Proceed to Full Payment?")) {
                                    //return true;
                                    window.location = URL;
                                }
                                else {
                                    return false;
                                }
                            }
                        </script>
                    </div>
                    <?php endif;?>
                </div>
            </div>
            <hr/>
        <?php endforeach; ?>
        <!-- /Profile Items EOC --> 
    </div>
</section>
<!-- /Profile Listing Section EOC -->


