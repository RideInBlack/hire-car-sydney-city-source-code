<ul class="car-list slider multiple-cars">
	<?php foreach ($cars as $car): ?>
	<?php
	if ($car->is_available == 1) :
		$href= base_url('ride/book_details?selected_ride='.  base64_encode($car->car_id));
		$onclick = "";
	else:
		$href = "Javascript:void(0);";
		$onclick = "return carNotAvailable();";
	endif;
	?>
	<li class="col-lg-4 col-sm-4 col-md-4"> 
		<a href="<?=@$href?>" title="<?= $car->category_name ?> <?= $car->model_name ?>" onclick="<?= @$onclick ?>">
			<img src="<?= base_url('assets') ?>/uploads/Car/<?= $car->car_id ?>/<?= $car->image_name ?>" alt="Audi" class="car-img" />
			<h2 class="text-center" style="font-size:110%">
				<?= $car->category_name ?>
				<?= $car->model_name ?>
			</h2>
		</a> 
	</li>
	</a>
	</li>
	<?php endforeach; ?>
</ul>
<script>
$(document).ready(function() {
	$('.multiple-cars').slick({
		lazyLoad: 'ondemand',
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay:true,
		autoplaySpeed:3000,
		responsive: [
   {
     breakpoint: 991,
     settings: {
       slidesToShow: 3,
       slidesToScroll: 3,
       infinite: true,
       //dots: true
     }
   },
   {
     breakpoint: 768,
     settings: {
       slidesToShow: 1,
       slidesToScroll: 1
     }
   }
   // You can unslick at a given breakpoint now by adding:
   // settings: "unslick"
   // instead of a settings object
 ]
	});
});	
</script>