<!-- ====== WRAPPER BOC ====== -->
<script>
    function validateAddBook()
    {
        //var a = [0-9]*[.][0-9]+$;
        var err_flag = false;

        // City Validation
        var start = document.getElementById("start").value.trim();
        if (start == 0) {
            document.getElementById("ERR_Start").innerHTML = "Please Select City.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_Start").innerHTML = "";
        }

        // Destination Validation
        var end = document.getElementById("end").value.trim();
        if (end == 0) {
            document.getElementById("ERR_end").innerHTML = "Please Select Destination.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_end").innerHTML = "";
        }

        // pick-up-date Name Validation
        var pick = document.getElementById("pick-up-date").value.trim();
        if (pick == "") {
            document.getElementById("ERR_pick-up-date").innerHTML = "Please Enter pick up date.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_pick-up-date").innerHTML = "";
        }

        // pick-up-time Validation
        var pickup = document.getElementById("pick-up-time").value.trim();
        if (pickup == "") {
            document.getElementById("ERR_pick-up-time").innerHTML = "Please Enter pick up time.";
            err_flag = true;
        }
        else {
            document.getElementById("ERR_pick-up-time").innerHTML = "";
        }

        //event Validation

        var events = document.getElementById("event").value.trim();

        if (events == 0) {
            document.getElementById("ERR_event").innerHTML = "Please select event.";
            err_flag = true;
        }

        else {
            document.getElementById("ERR_event").innerHTML = "";
        }



        if (err_flag == true)
            return false;
        else
            return true;
    }
</script>

<div class="page-wrapper">
    <!-- ====== CONTENT BOC ====== --> 
    <!-- /Booknow Section BOC -->
    <section class="inquiry">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3>book now</h3>
                    <hr class="style-two"/>
                </div>
            </div>

            <!-- /Form BOC -->
            <div class="row">
                <form id="" action="<?= base_url("Home/book_now") ?>" method="post" name="booknow-form">
                    <div class="common-form">
                        <!-- Personal Details -->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h6>Personal Details</h6>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <?php if(isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)) : ?>
                  <input type="text" name="name" id="firstname" value="<?= @$_SESSION['HIRE_CAR']['RIDER']->first_name.' '.$_SESSION['HIRE_CAR']['RIDER']->last_name  ?>" placeholder="Enter Name" class="form-control" disabled="disabled"  />
                <?php else: ?>
                                <input type="text" name="name" id="firstname" value="" placeholder="Enter Name" class="form-control" />
                <?php endif; ?>
                <?php /*?><input type="text" name="name" id="firstname" placeholder="Enter Name" class="form-control" /><?php */?>
                                <p id="ERR_FIRSTNAME" class="text-red"></p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <?php if(isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)) : ?>
                  <input type="tel" name="phone" id="phone" value="<?= @$_SESSION['HIRE_CAR']['RIDER']->mobile_number?>" placeholder="Enter Phone" class="form-control" disabled="disabled" />  
                <?php else: ?>
                                <input type="tel" name="phone" id="phone" value="" placeholder="Enter Phone" class="form-control" />  
                <?php endif; ?>
                <?php /*?><input type="tel" name="phone" id="phone" placeholder="Enter Phone" class="form-control" /> <?php */?>
                                <p id="ERR_PHONE" class="text-red"></p> 
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <?php if(isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)) : ?>
                  <input type="email" name="email" id="email" value="<?= @$_SESSION['HIRE_CAR']['RIDER']->email_id?>" placeholder="Enter Email Address" class="form-control" disabled="disabled"  />  
                <?php else: ?>
                                <input type="email" name="email" id="email" value="" placeholder="Enter Email Address" class="form-control" /> 
                <?php endif; ?>
                <?php /*?><input type="email" name="email" id="email" placeholder="Enter Email Address" class="form-control" /> <?php */?>
                                <p id="ERR_EMAIL" class="text-red"></p>
                            </div>
                        </div>

                        <!-- Pick up location details -->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h6>Pick Up Details</h6>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="pick-address" id="pick-address" placeholder="Pick up Address" class="form-control">
                                <p id="ERR_S" class="text-red"></p>  
                            </div>
                        </div>

                        <?php /*?><div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="pick-city" id="pick-city" placeholder="Pick up City" class="form-control" value="">
                                <p id="ERR_CITY" class="text-red"></p>  
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="pick-postal" id="pick-address" placeholder="Pick up Postal Code" class="form-control" value="">
                                <p id="ERR_POSTAL" class="text-red"></p>  
                            </div>
                        </div><?php */?>
                        <!-- Drop off location details -->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h6>Drop Off Details</h6>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="drop-address" id="drop-address" placeholder="Drop Off Address" class="form-control" value="">
                                <p id="ERR_D" class="text-red"></p>
                            </div>
                        </div>

                        <?php /*?><div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="drop-city" id="drop-city" placeholder="Drop Off City" class="form-control" value="">
                                <p id="ERR_CITY" class="text-red"></p>  
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <input type="text" name="drop-postal" id="drop-address" placeholder="Drop Off Postal Code" class="form-control" value="">
                                <p id="ERR_POSTAL" class="text-red"></p>  
                            </div>
                        </div><?php */?>
                        <!-- Other Trip Details location details -->
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="pick-up-date" id="pick-up-date" placeholder="Pick-Up Date" class="form-control homedatepicker" value="">
                                <i class="fa fa-calendar-o"></i> 
                                <span id="ERR_pick-up-date" class="text-red"></span>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group" id="timepick">
                                <input type="text" name="pick-up-time" id="pick-up-time" placeholder="Pick-Up Time" class="form-control hometimepicker" value="">
                                <i class="fa fa-clock-o"></i>
                                <span id="ERR_pick-up-time" class="text-red"></span>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <select id="event" name="event" class="form-control text-black">
                                    <option value="0">Type of Occasion</option>
                                    <?php foreach ($event_details AS $val): ?>
                                        <option id="<?= $val->event_id ?>" value="<?= $val->event_id ?>"><?= $val->event_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span id="ERR_event" class="text-red"></span>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="number_passengers" id="number_passengers" placeholder="No of Passengers" class="form-control" value="">
                                <p id="ERR_NO_PESSENGER" class="text-red"></p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="special_requirement" id="special_requirement" placeholder="Special Requirement" class="form-control" value="">
                            </div>
                        </div>

                        <?php /* ?>
                          <div class="col-lg-3 col-sm-3 col-md-3">
                          <div class="form-group">
                          <select id="start" name="start" class="form-control text-black">
                          <option value="0">Select Pick-Up Location</option>
                          <?php foreach ($city AS $val): ?>
                          <option id="<?= $val->city_id ?>" value="<?= $val->city_id ?>"><?= $val->name ?></option>
                          <?php endforeach; ?>

                          </select>
                          <span id="ERR_Start" class="text-red"></span>
                          </div>
                          </div>

                          <div class="col-lg-3 col-sm-3 col-md-3">
                          <div class="form-group">
                          <select id="end" name="end" class="form-control text-black">
                          <option value="0">Select Destination</option>
                          <?php foreach ($city AS $val): ?>
                          <option id="<?= $val->city_id ?>" value="<?= $val->city_id ?>"><?= $val->name ?></option>
                          <?php endforeach; ?>
                          </select>
                          <span id="ERR_end" class="text-red"></span>

                          </div>
                          </div>

                          <div class="col-lg-3 col-sm-3 col-md-3">
                          <div class="form-group">
                          <input type="text" name="pick-up-date" id="pick-up-date" placeholder="Pick-Up Date" class="form-control datepicker" value="">
                          <i class="fa fa-calendar-o"></i>
                          <span id="ERR_pick-up-date" class="text-red"></span>
                          </div>
                          </div>

                          <div class="col-lg-3 col-sm-3 col-md-3">
                          <div class="form-group">
                          <input type="text" name="pick-up-time" id="pick-up-time" placeholder="Pick-Up Time" class="form-control timepicker" value="">
                          <i class="fa fa-clock-o"></i>
                          <span id="ERR_pick-up-time" class="text-red"></span>
                          </div>
                          </div>

                          <div class="col-lg-3 col-sm-3 col-md-3 ">
                          <div class="form-group">
                          <select id="event" name="event" class="form-control text-black">
                          <option value="0">Select Event</option>
                          <?php foreach ($event_details AS $val): ?>
                          <option id="<?= $val->event_id ?>" value="<?= $val->event_id ?>"><?= $val->event_name ?></option>
                          <?php endforeach; ?>
                          </select>
                          <span id="ERR_event" class="text-red"></span>
                          </div>
                          </div>
                          <?php */ ?>
                        <div class="col-lg-4 col-sm-4 ol-md-4 pull-right">
                            <div class="form-group">
                                <input type="submit" name="book-now" value="Calculate Fare" class="btn btn-full" onclick="return bookingDetailValidation()">
                            </div>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    
    <script>
    function bookingDetailValidation() 
    {
      <?php if(!isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)) :?>
      var bookingName = document.getElementById('firstname').value;
      var bookingPhone = document.getElementById('phone').value;
      var bookingEmail = document.getElementById('email').value;
      
      if(bookingName == "" || bookingEmail == "" || bookingPhone == "")
      {
        if(bookingName == "") {
          alert("Please Enter Name.");
          document.getElementById("firstname").focus();
          return false;
        }
        
        else if(bookingPhone == "") {
          alert("Please Enter Phone.");
          document.getElementById("phone").focus();
          return false;
        }
        else if(bookingEmail == "") {
          alert("Please Enter Email.");
          document.getElementById("email").focus();
          return false;
        }
      }
      <?php endif; ?>
      
      var bookingPickAddr = document.getElementById('pick-address').value;
      if(bookingPickAddr == "")
      {
        alert("Please Enter Pick up Address.");
        document.getElementById("pick-address").focus();
        return false;
      }
      
      var bookingDropaddr = document.getElementById('drop-address').value;
      if(bookingDropaddr == "")
      {
        alert("Please Enter Drop off Address.");
        document.getElementById("drop-address").focus();
        return false;
      }
      
      var bookingDate = document.getElementById('pick-up-date').value;
      if(bookingDate == "")
      {
        alert("Please Enter Pick up Date.");
        document.getElementById("pick-up-date").focus();
        return false;
      }
      
      var bookingTime = document.getElementById('pick-up-time').value;
      if(bookingTime == "")
      {
        alert("Please Enter Pick up time.");
        document.getElementById("pick-up-time").focus();
        return false;
      }
      
      if(document.getElementById('event').selectedIndex == 0) {
        alert("Please Select Event Type.");
        document.getElementById("event").focus();
        return false;
      }
      return true;
    }
    </script>
        <!-- /Form EOC --> 

    </section>
    <!-- /Booknow Section EOC --> 

    <!-- /Service Section BOC -->
    <section class="our-service grey-color-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h3>our services</h3>
                    <hr class="style-two"/>
                </div>
            </div>

            <!--  /Service Listing BOC -->
            <div class="">
                <ul class="service-list slider multiple-items">
                    <li class="col-lg-4 col-sm-4 col-md-4"> <a href="<?= base_url('our-services/wedding'); ?>" title="Wedding"> 

                            <img src="<?= base_url('assets') ?>/web/images/img-marriege.jpg" alt="Wedding"/>
                            <div class="items">
                                <div class="items-content">
                                    <h2>wedding</h2>
                                    <p class="text-center">The wedding of your dreams isn't complete without seamless transport that ensures everything runs like clockwork. <b>Read more</b></p>
                                </div>
                            </div>
                        </a> 
                    </li>
                    <li class="col-lg-4 col-sm-4 col-md-4"> <a href="<?= base_url('our-services/corporate'); ?>" title="Corporate"> 

                            <img src="<?= base_url('assets') ?>/web/images/img-corporate.jpg" alt="Corporate"/>
                            <div class="items">
                                <div class="items-content">
                                    <h2>Corporate</h2>
                                    <p class="text-center">We offer a luxurious chauffeur driven service in luxury cars for corporate executives, VIPs and others in Sydney. <b>Read more</b></p>
                                </div>
                            </div>
                        </a> 
                    </li>
                    <li class="col-lg-4 col-sm-4 col-md-4"> <a href="<?= base_url('our-services/fun'); ?>" title="Party Fun"> 

                            <img src="<?= base_url('assets') ?>/web/images/img-party.jpg" alt="Party Fun"/>
                            <div class="items">
                                <div class="items-content">
                                    <h2>party</h2>
                                    <p class="text-center">Our Service includes airport transfers, private tours, formals, hens party, and any other special occasions. <b>Read more</b></p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="col-lg-4 col-sm-4 col-md-4"> <a href="<?= base_url('our-services/airport'); ?>" title="Airport Transfer"> 

                            <img src="<?= base_url('assets') ?>/web/images/img-airport-transfer.jpg" alt="Airport Transfer"/>
                            <div class="items">
                                <div class="items-content">
                                    <h2>Airport Transfer</h2>
                                    <p class="text-center">Looking for a ride from or to Sydney Airport? Are you looking for professional, luxurious airport transfers <b>Read more</b></p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="col-lg-4 col-sm-4 col-md-4"> <a href="<?= base_url('our-services/door'); ?>" title="Door to Door Pickup"> 

                            <img src="<?= base_url('assets') ?>/web/images/img-door-to-door-pickup.jpg" alt="Door to Door Pickup"/>
                            <div class="items">
                                <div class="items-content">
                                    <h2>Door to Door Pickup</h2>
                                    <p class="text-center">We offer a luxurious chauffeur driven service in luxury cars for corporate executives. <b>Read more</b></p>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
            <!--  /Service Listing EOC --> 
        </div>
    </section>
    <!-- /Service Section EOC --> 

    <!-- /Welcome Section BOC -->
    <section class="introduction">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3><?php
                        if (isset($welcomeContent->title)) {
                            echo $welcomeContent->title;
                        }
                        ?></h3>
                    <hr class="style-two">
                </div>
            </div>

            <!-- /Welcome Content BOC -->
            <div class="row">
                <div class="col-lg-8 col-sm-8 col-md-8">
                    <?php
                    if (isset($welcomeContent->title)) {
                        echo $welcomeContent->content;
                    }
                    ?>    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="hire-car"> <img src="<?= base_url('assets') ?>/web/images/hirecar.jpg" alt="Hirecar"/>
<!--                        <div class="items">
                            <div class="items-content">
                                <h3 class="font-x-small white-color-text">Hire car</h3>
                                <h2>sydney city </h2>
                            </div>
                        </div>-->
                    </div>
                    <div class="appstore-icon">
                        <div class="row">
                          <div class="col-md-6 col-sm-12 col-xs-6">
                            <div class="appstore-icon1"></div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-6">
                            <div class="appstore-icon2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Welcome Content EOC --> 

        </div>
    </section>
    <!-- /Welcome Section EOC --> 

    <!-- /Car Gallery Section BOC -->
    <section class="car-gallery grey-color-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h4>Select The Car You want</h4>
                    <hr class="style-two"/>
                </div>
            </div>

            <!-- /Photo Gallery Slider BOC -->
            <?php /* ?>
              <div class="row">
              <ul class="car-list slider multiple-items">
              <?php foreach ($CarCatData as $getCarCatData): ?>
              <li class="col-lg-3 col-sm-3 col-md-3"> <a href="Javascript:void(0);" title="<?= $getCarCatData->name ?>"> <img src="<?= base_url('assets') ?>/uploads/CarCategory/<?= $getCarCatData->category_id ?>/<?= $getCarCatData->car_image ?>" alt="Audi"/>
              <h2><?= $getCarCatData->name ?></h2>
              </a> </li>
              </a> </li>
              <?php endforeach; ?>
              </ul>
              </div>
              <?php */ ?>

            <div class="row" id="carsSlider">
        <div id="car-slider-loader"><img src="<?=base_url('/assets/web/images/loading3.gif')?>"/></div>
                <?php /*?><ul class="car-list slider multiple-items">
                    <?php foreach ($cars as $car): ?>
                        <?php
                        if ($car->is_available == 1) :
                            $href= base_url('ride/book_details?selected_ride='.  base64_encode($car->car_id));
                            $onclick = "";
                        else:
                            $href = "Javascript:void(0);";
                            $onclick = "return carNotAvailable();";
                        endif;
                        ?>
                        <li class="col-lg-3 col-sm-3 col-md-3"> 
                            <a href="<?=@$href?>" title="<?= $car->category_name ?> <?= $car->model_name ?>" onclick="<?= @$onclick ?>"> 
                                <img src="<?= base_url('assets') ?>/uploads/Car/<?= $car->car_id ?>/<?= $car->image_name ?>" alt="Audi" class="car-img" />
                                <h2 class="text-center" style="font-size:110%"><?= $car->category_name ?> <?= $car->model_name ?></h2>
                            </a> 
                        </li>
                    <?php endforeach; ?>
                </ul><?php */?>
        <script>
                    function carNotAvailable() {
                        //alert("Please select other car, or just book in we will come to pick you with any other available luxury car.");
                        $('#notAvailable').modal('show');
                    }
                </script>
            </div>
            <!-- /Photo Gallery Slider EOC --> 

        </div>
    </section>
    <!-- /Car Gallery Section EOC --> 

    <!-- ====== Car Not Available MODAL  ====== -->
    <div class="modal fade" id="notAvailable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="common-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <hr class="style-two"/>
                    </div>

                    <div class="modal-body text-center">
                        <b>Please select other car, or just book in we will come to pick you with any other available luxury car.</b>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ====== Car Not Available MODAL END  ====== -->

    <!-- /Giftcard Section BOC -->
    <section class="gift-gallery">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h4>choose gift card</h4>
                    <hr class="style-two"/>
                </div>
            </div>

            <!-- /Gift Package BOC -->
            <div class="row">
                <ul class="gift-package slider multiple-items">
                    <?php foreach ($giftCardData as $getgiftCardData) { ?>
                        <li class="col-lg-4 col-sm-4 col-md-4"> 
                            <a href="<?=  base_url('giftcard/details/'.$getgiftCardData->giftcard_id)?>" title="<?= $getgiftCardData->giftcard_name ?>"> <img src="<?= base_url('assets') ?>/admin/GiftCardImages/<?= $getgiftCardData->image_name ?>" alt="Drive Couple"/>
                                <div class="items">
                                    <div class="items-content">
                                        <!--<div class="price-tag text-center">$<?= $getgiftCardData->price ?></div>-->

                                        <h5><?= $getgiftCardData->giftcard_name ?></h5>
                                    </div>
                                </div>
                            </a> </li>
                    <?php } ?>
                </ul>
            </div>
            <!-- /Gift Package EOC --> 
        </div>
    </section>
    <!-- /Giftcard Section EOC --> 
    <!-- ====== CONTENT EOC ====== -->

    <!-- /Go To Top BOC -->
    <div class="back-top text-center"> 
        <a href="#" id="toTop"><i class="fa fa fa-angle-up img-circle"></i> </a> 
    </div>
</div>
<!-- ====== WRAPPER EOC ====== -->

<section class="brand">
    <div class="container-fluid">
        <div class="row"> 
            <!-- /Brand Logo Slider BOC -->
            <ul class="brand-gallery slider multiple-items-1">
                <?php foreach ($CarCatData as $getCarCatData) : ?>
                    <li class="col-lg-2 col-md-2 col-sm-2"> <a href="Javascript:void(0);" title="<?= $getCarCatData->name ?>"><img class="img-center" src="<?= base_url('assets') ?>/uploads/CarCategory/<?= $getCarCatData->category_id ?>/<?= $getCarCatData->car_logo ?>" alt="Audi"/></a> </li>
                        <?php endforeach; ?>
            </ul>
            <!-- /Brand Logo Slider EOC --> 
        </div>
    </div>
</section>

<script>
function loadCarsSlider(){
  $.ajax({ 
    url:'<?=base_url('home/carImageAJAX')?>',
    type:"post",
    success:function(data){
      $('#carsSlider').html(data);
    }
  });
}

$(window).load(function(){
  //$('#carsSlider').html('<div id="car-slider-loader"><img src="<?=base_url('/assets/web/images/loading3.gif')?>"/></div>');
  //setTimeout(loadCarsSlider,10000);
  loadCarsSlider();
});
</script>
<style>
@media (min-width: 979px) {
#car-slider-loader{
  margin-left:45%;
  padding: 60px 0px;
}
}
@media (min-width: 768px) and (max-width: 978px) {
#car-slider-loader{
  margin-left:40%;
  padding: 60px 0px
}
}

@media (min-width:481px) and (max-width: 767px) {
#car-slider-loader{
  margin-left:40%;
  padding: 50px 0px
}
}

@media (min-width:320px) and (max-width:480px) {
#car-slider-loader{
  margin-left:5%;
  padding: 50px 0px
}
}
</style>

<!-- MAP Related JS -->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback"></script>

<script>
$(document).ready(function() {
  var input = document.getElementById('pick-address');
  var searchBox = new google.maps.places.SearchBox(input);
  
  var input2 = document.getElementById('drop-address');
  var searchBox = new google.maps.places.SearchBox(input2);
});
</script>
