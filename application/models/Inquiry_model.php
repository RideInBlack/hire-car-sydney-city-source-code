<?php
/**
 * Description of region_model: Get Region Details from databse
 *
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Inquiry_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
		
    }
    
    public function count_total($search_where = "")
    {
        $this->db->select('inquiry_id');
        $query = $this->db->from('inquiry_details');
        
        // for search
        if($search_where != "")
        {
            $this->db->where($search_where);
        }
        
        $result = $query->get();
        return $result->num_rows();
    }
    
    // Get All User List
    public function list_all($per_page=PER_PAGE_RECORD, $offset = 0, $search_where = "")
    {
        $this->db->select('*');
        $this->db->limit($per_page, $offset);
        $this->db->order_by('inquiry_id', 'DESC');
        
        // for search
        if($search_where != "")
        {
            $this->db->where($search_where);
        }
        
        $query = $this->db->from('inquiry_details');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Insert Inquiry from to database
    public function insert_inquiry($data)
    {
        $this->db->insert('inquiry_details', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
    
    public function fetch_details($id)
    {
        $this->db->select('*');
        $this->db->where('inquiry_id', $id);
        $query = $this->db->from('inquiry_details');
        
        $result = $query->get();
        $result = $result->result();
        
        return $result[0];
    }
}

?>