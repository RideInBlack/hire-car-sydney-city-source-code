<?php

/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // Get Single driver Details
    public function fetch_ride_payment($id) {
        $this->db->select(' *');
        $this->db->where('ride_id', $id);
        $this->db->where('status', '1');
        $query = $this->db->from('payment_details');
        
        $result = $query->get();
        $result = $result->result();
        
        return $result;
    }
	
	// Get Payment Details
    public function fetch_details($id)
    {
        $this->db->select('*');
        $this->db->where('payment_id', $id);
        $query = $this->db->from('payment_details');
        
        $result = $query->get();
        
		if($result->num_rows() > 0) {
			$result = $result->result();
			return $result[0];
		}
		else {
			return false;
		}
    }

    // Add New driver
    public function add($data) {
        $this->db->insert('payment_details', $data);

        if ($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }

    // Edit driver Details
    public function edit($data, $id) {
        $this->db->where('payment_id', $id);
        $this->db->update('payment_details', $data);

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    // Delete driver
    public function delete($id) {
        $this->db->where('driver_id', $id);
        $this->db->delete('driver');

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    // Check Unique in database for email and username
    public function check_unique($column, $value, $where = "") {
        $this->db->select('driver_id');
        $this->db->where($column, $value);

        if ($where != "")
            $this->db->where($where);

        $query = $this->db->from('driver');
        $result = $query->get();

        if ($result->num_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    // For Login User
    public function driver_authentication($emailadd, $password) {
        $this->db->select('driver_id, first_name, last_name, email_id, username, mobile_number, companyname, profile_image, abn, address, city, invitecode, licensedetails,zipcode');
        $this->db->where('email_id', $emailadd);
        $this->db->where('password', $password);
        //$this->db->where('status','1');

        $check_login = $this->db->get('driver');
        //echo $this->db->last_query();exit;
        if ($check_login->num_rows() > 0) {
            $admin_data = $check_login->result();
            return $admin_data[0];
        } else {
            return 0;
        }
    }

    // For Forgot password chck Email id
    public function check_driver_email($email_id) {
        $this->db->select('*');
        $this->db->where('email_id', $email_id);

        $check_email = $this->db->get('driver');

        //echo $check_login->num_rows() ;exit;
        if ($check_email->num_rows() > 0) {
            $result = $check_email->result();
            return $result[0];
        } else {
            return FALSE;
        }
    }

    // Get Credit Card Details
    public function fetch_creditcard($id, $type) {
        $this->db->select(' *');
        $this->db->where('user_id', $id);
        $this->db->where('user_type', $type);
        $query = $this->db->from('creditcard_detail');

        $result = $query->get();
        $result = $result->result();
        if (!empty($result)) {
            return $result[0];
        } else {
            return '';
        }
    }

    public function editcard($data, $id) {
        $this->db->where('user_id', $id);
        $this->db->update('creditcard_detail', $data);

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    public function addcard($data) {
        $this->db->insert('creditcard_detail', $data);

        if ($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }

    public function addFacebookUser($data) {
        $this->db->select('*');
        $this->db->where('first_name', $data['first_name']);
        $this->db->where('last_name', $data['last_name']);
        $this->db->where('email_id', $data['email_id']);

        $checkData = $this->db->get('driver');

        if ($checkData->num_rows() > 0) {
            $result = $checkData->result();
            return $result[0];
        } else {
            $this->db->insert('driver', $data);
            $getID = $this->db->insert_id();

            $this->db->select('*');
            $this->db->where('driver_id', $getID);

            $checkData = $this->db->get('driver');
            $result = $checkData->result();
            return $result[0];
        }
    }

}
