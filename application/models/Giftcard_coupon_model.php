<?php

/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Giftcard_coupon_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function check_unique($column, $value, $where = "") {
        $this->db->where($column, $value);
        if ($where != "") {
            $this->db->where();
        }

        $query = $this->db->get('giftcard_coupon');

        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function add_card($data) {
		$this->db->insert('giftcard_coupon', $data);

		if ($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }

    public function details($id) {
        $this->db->select(' *');
        $this->db->where('coupon_id', $id);
        $query = $this->db->from('giftcard_coupon');

        $result = $query->get();
        $result = $result->result();

        return $result[0];
    }

    public function edit($data, $id) {
        $this->db->where('coupon_id', $id);
        $this->db->update('giftcard_coupon', $data);

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    public function get_rider_coupon($rider_id) {
        $this->db->select('*');
        $this->db->where("(rider_id = ".$rider_id." AND status = '1')");
        $this->db->or_where("(rider_id = ".$rider_id." AND payment_type = 'deposit')");
		$this->db->order_by('coupon_id', 'desc');
        $query = $this->db->from('giftcard_coupon');
		
        $result = $query->get();
		//echo $this->db->last_query();
        $result = $result->result();

        return $result;
    }

	public function count_total($search_where = "",$driverSR)
    {
       $this->db->select('gc.*, r.first_name, r.last_name, r.email_id, r.mobile_number');
        $this->db->limit($per_page, $offset);
		$this->db->join('rider AS r', 'r.rider_id = gc.rider_id');
		
		// for search
		if($driverSR !="")
        {
            $this->db->where('rider_id',$driverSR);
        }
        

		if($search_where !="")
		{
			$this->db->where($search_where);
		}
        
       $query = $this->db->from('giftcard_coupon AS gc');
        
        $result = $query->get();
        
        return $result->num_rows();
    }

        public function count_total_printed($search_where = "",$driverSR)
    {
        $this->db->select('*');
        $this->db->where('rider_id','0');
        $this->db->join('giftcard', 'giftcard.giftcard_id = giftcard_coupon.giftcard_id');
        $this->db->order_by('giftcard_coupon.coupon_id', 'DESC');
        $this->db->from('giftcard_coupon');
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
	
    public function get_coupon_details($code) {
        $this->db->select('*');
        $this->db->where('coupon_code', $code);
        $this->db->where('status', '1');
        $query = $this->db->from('giftcard_coupon');

        $query = $query->get();
        $result = $query->result();

        if ($query->num_rows() > 0) {
            return $result[0];
        } else {
            return FALSE;
        }
    }
	
	public function all_register_gift_card($per_page=10, $offset = 0, $where = "") {
		$this->db->select('gc.*, r.first_name, r.last_name, r.email_id, r.mobile_number');
        $this->db->limit($per_page, $offset);
		$this->db->join('rider AS r', 'r.rider_id = gc.rider_id');
		//$this->db->where('payment_type', 'deposit');
		
		/*if($where != "") {
			$this->db->where($where);
		}*/
        
		$this->db->order_by('generate_datetime', 'DESC');
		
        $query = $this->db->from('giftcard_coupon AS gc');
        
        $result = $query->get();
        
        return $result->result();
	}

        public function all_printed_coupons($per_page=10, $offset = 0, $where = "") {

        $this->db->select('giftcard_coupon.*,giftcard.giftcard_name,giftcard_coupon.status as statusPrinted');
        $this->db->limit($per_page, $offset);
        $this->db->where('rider_id','0');
        $this->db->join('giftcard', 'giftcard.giftcard_id = giftcard_coupon.giftcard_id');
        $this->db->order_by('giftcard_coupon.coupon_id', 'DESC');
        $query = $this->db->from('giftcard_coupon');
        
        $result = $query->get();
        //echo $this->db->last_query();
        return $result->result();
    }
	

public function get_use_gift_card($coupan_id) {
	
/*	$this->db->select('payment_details.ride_id,payment_details.amount,payment_details.coupon_id, payment_details.payment_datetime,giftcard_coupon.price,giftcard_coupon.remaining_amount,ride_details.pickup_location,ride_details.destination_location'); */

$this->db->select('payment_details.*,giftcard_coupon.price,giftcard_coupon.remaining_amount,ride_details.pickup_location,ride_details.destination_location');
    $this->db->from('payment_details');
    $this->db->join('giftcard_coupon', 'payment_details.coupon_id = giftcard_coupon.coupon_id');
	$this->db->join('ride_details', 'payment_details.ride_id=ride_details.id'); 
	$this->db->where('payment_details.coupon_id', $coupan_id);
	$this->db->where('payment_details.status =','1');
	
    $query = $this->db->get();
 //echo $this->db->last_query();
   return $query->result();
	
		
	}
}
