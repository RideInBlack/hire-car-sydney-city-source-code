 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_Letter extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }

// Add New Rider
    public function add($data)
    {
        $this->db->insert('news_subscriber', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
    
}
?>