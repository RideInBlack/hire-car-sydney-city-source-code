<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	public function getCoupons($where) {
		$this->db->select('giftcard_coupon.*, rider.email_id, rider.first_name, rider.last_name');
		$this->db->join('rider', 'giftcard_coupon.rider_id = rider.rider_id');
		$this->db->where($where);
		$this->db->from('giftcard_coupon');
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		
		return $query->result();
		
	}
	
	public function deleteCoupon($where) {
		$this->db->where($where);
		$this->db->delete('giftcard_coupon');
		
		//echo $this->db->last_query();echo $this->db->affected_rows();
	}
}
