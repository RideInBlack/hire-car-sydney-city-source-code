<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // Count Total driver 
    public function count_total($where = "")
    {
        $this->db->select('*');
        $query = $this->db->from('setting');
        
        $result = $query->get();
        
        return $result->num_rows();
    }
    
    // Get All driver List
    public function list_all($per_page=PER_PAGE_RECORD, $offset = 0)
    {
        $this->db->select('*');
        $this->db->limit($per_page, $offset);
        $query = $this->db->from('setting');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get Single setting Details
    public function fetch_details($id)
    {
        $this->db->select('*');
        $this->db->where('setting_id', $id);
        $query = $this->db->from('setting');
        
        $result = $query->get();
        $result = $result->result();
        
        return $result[0];
    }
	
	public function fetch_setting($url = null)
    {
        $this->db->select('*');
        $this->db->where('url', $url);
        $query = $this->db->from('setting');
        
        $result = $query->get();
        $result = $result->result();
        
		if(!empty($result[0]))
		{
        	return $result[0];
		}
		else
		{
			return '';
		}
    }
    
    // Edit driver Details
    public function index($data, $id)
    {
        $this->db->where('setting_id', $id);
        $this->db->update('setting', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
	
	public function updateFare($data)
    {
        $this->db->update('car', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
	
	
}
