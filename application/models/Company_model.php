<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // Count Total Company 
    public function count_total($where = "")
    {
        $this->db->select('company_id');
        $query = $this->db->from('company');
        
        $result = $query->get();
        
        return $result->num_rows();
    }
    
    // Get All Company List
    public function list_all($per_page=PER_PAGE_RECORD, $offset = 0)
    {
        $this->db->select('co.company_id, co.name, co.address, ci.name AS city, co.zipcode, status, co.add_datetime');
        $this->db->limit($per_page, $offset);
        $this->db->join('city ci', 'co.city_id=ci.city_id', 'left');
        $this->db->order_by('co.name');
        $query = $this->db->from('company AS co');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get Single Company Details
    public function fetch_details($id)
    {
        $this->db->select('company_id, name, address, city_id, zipcode, add_datetime');
        $this->db->where('company_id', $id);
        $query = $this->db->from('company');
        
        $result = $query->get();
        $result = $result->result();
        
        return $result[0];
    }
    
    // Add New Company
    public function add($data)
    {
        $this->db->insert('company', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
    
    // Edit Company Details
    public function edit($data, $id)
    {
        $this->db->where('company_id', $id);
        $this->db->update('company', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    // Delete Company
    public function delete($id)
    {
        $this->db->where('company_id', $id);
        $this->db->delete('company');
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
}
