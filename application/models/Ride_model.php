<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Ride_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // Get All Car List
    public function fetch_details($id)
    {
        $this->db->select('ride_details.*, assign_ride.*');
        //$this->db->limit($per_page, $offset);
        $this->db->join('ride_details', ' ride_details.id = assign_ride.ride_id');
        $this->db->order_by('assign_ride.id', 'DESC');
        $this->db->where('assign_ride.assign_driver_id', $id);
        $query = $this->db->from('assign_ride');
        
        $result = $query->get();
        return $result->result();
    }
    
    public function fetch_ride($id)
    {
        $this->db->select('ride.*, payment.amount AS paid_amount');
        $this->db->where('id', $id);
        $this->db->join('(SELECT ride_id,SUM(amount) AS amount FROM payment_details WHERE status="1" GROUP BY ride_id) AS payment', 'payment.ride_id = ride.id', 'LEFT');
        $query = $this->db->from('ride_details AS ride');
        
        $result = $query->get();
        $result = $result->result();
        return $result[0];
    }
    
    public function count_total($search_where = "", $isDriver = 0)
    {
        $this->db->select('ride_details.id');
        $this->db->join('rider', ' ride_details.rider_id = rider.rider_id');
        if($isDriver == 1) {
            $this->db->join('driver', ' ride_details.driver_id = driver.driver_id');
        }
        $query = $this->db->from('ride_details');
	// for search

        if($search_where !="") {
                $this->db->where($search_where);
        }
        
        $result = $query->get();
        return $result->num_rows();
    }
    
    public function rides_list($per_page=10, $offset = 0, $search_where = "")
    {
        $this->db->select('ride_details.*, rider.first_name, rider.last_name, rider.email_id');
        $this->db->join('rider', ' ride_details.rider_id = rider.rider_id', 'left');
        $this->db->limit($per_page, $offset);
		$this->db->order_by('id', 'DESC');
		//for search
		if($search_where !="")
		{
			$this->db->where($search_where);
		}
		
        $query = $this->db->from('ride_details');
        $result = $query->get();
        
        //echo $this->db->last_query();exit;
        return $result->result();
    }
    
    public function runningRidesList($per_page=10, $offset = 0, $search_where = "")
    {
        $this->db->select('ride_details.*, rider.first_name, rider.last_name, rider.email_id, driver.first_name AS d_first_name, driver.last_name AS d_last_name, driver.email_id AS d_email_id');
        $this->db->join('rider', ' ride_details.rider_id = rider.rider_id','left');
        $this->db->join('driver', ' ride_details.driver_id = driver.driver_id','left');
        $this->db->limit($per_page, $offset);
	$this->db->order_by('ride_details.id', 'DESC');
	//for search
	
        if($search_where !="")
	{
            $this->db->where($search_where);
	}
		
        $query = $this->db->from('ride_details');
        $result = $query->get();
        return $result->result();
    }
    
    public function rideslistfordriver($per_page=10, $offset = 0, $search_where = "")
    {
        $this->db->select('assign_ride.*, ride_details.*, car.car_id, car.category_id, car.city_id, car.model_name, car.number, car.base_fare, car.per_minute_rate, car.per_km_rate, car.driver_rent, car.registration_filename, car.status, car.is_allocated, car.event_id AS event_type,image.image_name');
        $this->db->join('ride_details', ' ride_details.id = assign_ride.ride_id','left');
        $this->db->join('car', 'ride_details.car_category_id = car.car_id', 'left');
        $this->db->join('car_images AS image', 'car.car_id = image.car_id', 'left');
        $this->db->limit($per_page, $offset);
	$this->db->order_by('ride_details.id', 'DESC');
	//for search
	
        if($search_where !="")
	{
            $this->db->where($search_where);
	}
		
        $query = $this->db->from('assign_ride');
        $result = $query->get();
        return $result->result();
    }
    
    public function search_ride($car_category_id = 0 ,$city_id = 0) 
    {
        $this->db->select('car.*, image.image_name');
        
        if($car_category_id != 0) {
            $this->db->where('category_id', $car_category_id);
        }
        
        if($city_id != 0) {
            $this->db->where('city_id', $city_id);
        }
        
        $this->db->where('status', '1');
        $this->db->join('car_images AS image', 'image.car_id = car.car_id');
        $query = $this->db->from('car');
        
        $result = $query->get();
        return $result->result();
    }
    
    // Assign New ride to driver
    public function assignride($data)
    {
        $this->db->insert('assign_ride', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
    
    public function updateRideStatus($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('ride_details', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    public function book_ride($data)
    {
        $this->db->insert('ride_details', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
    
    public function edit_ride_details($data, $ride_id)
    {
        $this->db->where('id', $ride_id);
        $this->db->update('ride_details', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    public function fetch_ride_details($id)
    {
        $this->db->select('ride_details.*, rider.first_name, rider.last_name, rider.email_id');
        $this->db->join('rider', ' ride_details.rider_id = rider.rider_id', 'left');
        //$this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->from('ride_details');
        $query = $query->get();
        
        $result = $query->result();
        return($result[0]);
    }
    
    // fetch rides details for rider and driver
    public function fetch_rides($for, $id)
    {
        $this->db->select('ride.*,car.*, image.image_name, payment.amount AS paid_amount');
        $this->db->join('car', 'ride.car_category_id = car.car_id', 'left');
        $this->db->join('car_images AS image', 'car.car_id = image.car_id', 'left');
        //$this->db->join('city AS start', 'ride.pickup_location = start.city_id');
        //$this->db->join('city AS end', 'ride.destination_location = end.city_id');
        $this->db->join('(SELECT ride_id,SUM(amount) AS amount FROM payment_details WHERE status="1" GROUP BY ride_id) AS payment', 'payment.ride_id = ride.id', 'LEFT');
        $this->db->where($for, $id);
        $this->db->group_by('ride.id');
        $this->db->order_by('ride.pickup_datetime', 'DESC');
        $query = $this->db->from('ride_details AS ride');
        $result = $query->get();
        //echo $this->db->last_query();exit;
        return $result->result();
    }
    
    // fetch rides details for rider and driver
    public function fetch_paymentdetails($id)
    {
        $this->db->select('ride_id,SUM(amount) AS amount');
        $this->db->where('status="1" AND ride_id="'.$id.'"');
        $this->db->group_by('ride_id');
        $query = $this->db->from('payment_details');
        $query = $query->get();
        //echo $this->db->last_query();exit;
        $result = $query->result();
        if($query->num_rows() > 0) {
            return $result[0];
        } else {
            return;
        }
    }
    
    // fetch payment detils of
    public function fetch_payment_rows($id)
    {
        $this->db->select('*');
        $this->db->where('status="1" AND ride_id="'.$id.'"');
        $query = $this->db->from('payment_details');
        $query = $query->get();
        //echo $this->db->last_query();exit;
        $result = $query->result();
        if($query->num_rows() > 0) {
            return $result;
        } else {
            return;
        }
    }
    
    public function get_ride_details($id) {
        $this->db->select('ride.*, start.name AS start, end.name AS end, car.model_name AS car_name, rider.first_name AS rider_name, rider.email_id AS rider_email');
        $this->db->join('car', 'ride.car_category_id = car.car_id');
        $this->db->join('rider', 'ride.rider_id = rider.rider_id');
        $this->db->join('city AS start', 'start.city_id = ride.pickup_location');
        $this->db->join('city AS end', 'end.city_id = ride.destination_location');
        $this->db->where('ride.id', $id);
        $query = $this->db->from('ride_details AS ride');
        $query = $query->get();
        
        $result = $query->result();
        
        return $result;
    }
    
    public function get_payment_status($id) {
        $this->db->select('payment_id');
        $this->db->where('id', $id);
        $query = $this->db->from('ride_details');
        $result = $query->get();
        //echo $this->db->last_query();exit;
        $result = $result->result();
        $ride_details = $result[0];
        
        return $ride_details->payment_id;
    }
}
