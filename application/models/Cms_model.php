<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // Count Total driver 
    public function count_total($where = "")
    {
        $this->db->select('*');
        $query = $this->db->from('cms');
        
        $result = $query->get();
        
        return $result->num_rows();
    }
    
    // Get All driver List
    public function list_all($per_page=PER_PAGE_RECORD, $offset = 0)
    {
        $this->db->select('*');
        $this->db->limit($per_page, $offset);
        $query = $this->db->from('cms');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get Single driver Details
    public function fetch_details($id)
    {
        $this->db->select('*');
        $this->db->where('cms_id', $id);
        $query = $this->db->from('cms');
        
        $result = $query->get();
        $result = $result->result();
        return $result[0];
    }
	
	public function fetch_cms($url = null)
    {
        $this->db->select('*');
        $this->db->where('url', $url);
        $query = $this->db->from('cms');
        
        $result = $query->get();
        $result = $result->result();
        
		if(!empty($result[0]))
		{
        	return $result[0];
		}
		else
		{
			return '';
		}
    }
    
    // Edit driver Details
    public function edit($data, $id)
    {
        $this->db->where('cms_id', $id);
        $this->db->update('cms', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
}
