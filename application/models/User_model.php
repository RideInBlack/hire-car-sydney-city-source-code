<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // get all users email 
    public function get_admin_email($id = "") {
        $this->db->select('email_id');
        
        if($id != "") {
            $this->db->where('admin_id', $id);
        }
        
        $query = $this->db->from('admin_user');
        $result = $query->get();
        $result = $result->result();
        
        return $result;
    }
    
    // Count Total Company 
    public function count_total($search_where = "")
    {
        $this->db->select('admin_id');
        $this->db->where('admin_type!=', '1');
        
        // for search
        if($search_where != "")
        {
            $this->db->where($search_where);
        }
        
        $query = $this->db->from('admin_user');
        $result = $query->get();
        
        return $result->num_rows();
    }
    
    // Get All User List
    public function list_all($per_page=PER_PAGE_RECORD, $offset = 0, $search_where = "")
    {
        $this->db->select('admin_id, first_name, last_name, nick_name, email_id, status, add_datetime');
        $this->db->limit($per_page, $offset);
        $this->db->order_by('add_datetime', 'DESC');
        $this->db->where('admin_type!=', '1');
        
        // for search
        if($search_where != "")
        {
            $this->db->where($search_where);
        }
        
        $query = $this->db->from('admin_user');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get Single Company Details
    public function fetch_details($id)
    {
        $this->db->select('admin_id, first_name, last_name, nick_name, email_id, status, add_datetime');
        $this->db->where('admin_id', $id);
        $query = $this->db->from('admin_user');
        
        $result = $query->get();
        $result = $result->result();
        
        return $result[0];
    }
    
    // Add New User
    public function add($data)
    {
        $this->db->insert('admin_user', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
    
    // Edit Company Details
    public function edit($data, $id)
    {
        $this->db->where('admin_id', $id);
        $this->db->update('admin_user', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    // Delete Company
    public function delete($id)
    {
        $this->db->where('admin_id', $id);
        $this->db->delete('admin_user');
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    // Check Unique in database for email and username
    public function check_unique($column, $value, $where="")
    {
        $this->db->select('admin_id');
        $this->db->where($column, $value);
        
        if($where != "")
            $this->db->where($where);
        
        $query = $this->db->from('admin_user');
        $result = $query->get();
        
        if($result->num_rows() > 0)
            return TRUE;
        else 
            return FALSE;
    }
}
