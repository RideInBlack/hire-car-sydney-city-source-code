<?php
/**
 * Description of region_model: Get Region Details from databse
 *
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
		
    }
    
    // Insert Inquiry from to database
    public function insert_contact($data)
    {
        $this->db->insert('contact_us_details', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
}

?>