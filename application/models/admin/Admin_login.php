<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    // For Login User
    public function admin_authentication($username, $password)
    {
        $this->db->select('admin_id, first_name, last_name, email_id, nick_name, admin_type');
        $this->db->where('nick_name', $username);
        $this->db->where('password_hash', md5($password));
        $this->db->where('status', '1');
        $check_login = $this->db->get('admin_user');
        
        if($check_login->num_rows() > 0) {
            $admin_data = $check_login->result();
            return $admin_data[0];
        }
        else {
            return 0;
        }
    }
    
    // For Forgot password chck Email id
    public function check_admin_email($email_id)
    {
        $this->db->select('*');
        $this->db->where('email_id', $email_id);
        
        $check_email = $this->db->get('admin_user');
        
        //echo $check_login->num_rows() ;exit;
        if($check_email->num_rows() > 0) {
            $result = $check_email->result();
            return $result[0];
        }
        else {
            return FALSE;
        }
    }
    
    // For Forgot password chck Email id
    public function check_admin_password($password, $id)
    {
        $this->db->select('*');
        $this->db->where('password', $password);
        $this->db->where('admin_id', $id);
        
        $check_password = $this->db->get('admin_user');
        
        if($check_password->num_rows() > 0) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
}