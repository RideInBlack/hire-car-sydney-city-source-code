<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Giftcard_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // Count Total Company 
    public function count_total($where = "")
    {
        $this->db->select('giftcard_id');
        $query = $this->db->from('giftcard');
        
        $result = $query->get();
        
        return $result->num_rows();
    }
    
    // Get All User List
    public function list_all($per_page=PER_PAGE_RECORD, $offset = 0)
    {
        $this->db->select('*');
        $this->db->limit($per_page, $offset);
        $this->db->order_by('add_datetime', 'DESC');
        $query = $this->db->from('giftcard');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get All User List
    public function get_all()
    {
        $this->db->select('*');
        $this->db->order_by('add_datetime', 'DESC');
        $this->db->where('status', '1');
        $query = $this->db->from('giftcard');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get Single Company Details
    public function fetch_details($id)
    {
		$this->db->select(' *');
        $this->db->where('giftcard_id', $id);
        $query = $this->db->from('giftcard');
        
        $result = $query->get();
        $result = $result->result();
        
        return $result[0];
	}
    
    // Add New User
    public function add($data)
    {
        $this->db->insert('giftcard', $data);
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
    
    // Edit Company Details
    public function edit($data, $id)
    {
        $this->db->where('giftcard_id', $id);
        $this->db->update('giftcard', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    // Delete Company
    public function delete($id)
    {
        $this->db->where('giftcard_id', $id);
        $this->db->delete('giftcard');
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
	
	public function check_unique($column, $value, $where = "") {
		$this->db->where($column, $value);
		if($where != "") {
			$this->db->where();
		}
		
		$query = $this->db->get('giftcard');
		
		if($query->num_rows() > 0) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public function giftdetails($id)
	{
		//echo $id; die;
		$this->db->select('giftcard_id,price, pricepercentage, coupon_code');
		$this->db->from('giftcard');
		$this->db->where('giftcard_id',$id); 
		$query=$this->db->get();
		return $query->result();
	}
}
