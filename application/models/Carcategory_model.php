<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Carcategory_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // Count Total Cars 
    public function count_total($search_where = "")
    {
        $this->db->select('category_id');
        $query = $this->db->from('car_category');
	
        $result = $query->get();
        
        return $result->num_rows();
    }
    
    // Get All Car Category List
    public function list_all($per_page=PER_PAGE_RECORD, $offset = 0, $search_where = "")
    {
        $this->db->select('*');
        $this->db->limit($per_page, $offset);
	$this->db->order_by('name');
        $query = $this->db->from('car_category');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get Single Car Details
    public function fetch_details($id)
    {
        $this->db->select('*');
        $this->db->where('category_id', $id);
        $query = $this->db->from('car_category');
        
        $result = $query->get();
        $result = $result->result();
        
        return $result[0];
    }
    
    // Add New Car
    public function add($data)
    {
        $this->db->insert('car_category', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
    
    // Edit Company Details
    public function edit($data, $id)
    {
        $this->db->where('category_id', $id);
        $this->db->update('car_category', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    // Delete Car
    public function delete($id)
    {
        // Delete Car Details
        $this->db->where('car_id', $id);
        $this->db->delete('car');
        
        // Delete All Car Images
        $this->db->where('car_id', $id);
        $this->db->delete('car_images');
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
}
