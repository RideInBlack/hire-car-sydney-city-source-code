<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // get all city from database
    public function get_all_event()
    {
        $this->db->select('*');
        $query = $this->db->from('events');
        $result = $query->get();
        $result = $result->result();
        
        return $result;
    }
    
    public function get_event_name($id)
    {
        $this->db->select('event_name');
        $this->db->where('event_id', $id);
        $query = $this->db->from('events');
        $query = $query->get();
        $result = $query->result();
        
        if($query->num_rows() > 0)
            return $result[0]->event_name;
        else
            return FALSE;
    }
}
