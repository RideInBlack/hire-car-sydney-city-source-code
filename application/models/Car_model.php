<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Car_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // Count Total Cars 
    public function count_total($search_where = "")
    {
        $this->db->select('car_id');
        $query = $this->db->from('car');
	
        $result = $query->get();
        
        return $result->num_rows();
    }
    
    // Get All Car List
    public function list_all($per_page=PER_PAGE_RECORD, $offset = 0, $search_where = "")
    {
        $this->db->select('car.*, cat.name AS category_name, city.name AS city');
        $this->db->limit($per_page, $offset);
	$this->db->join('car_category AS cat', ' car.category_id = cat.category_id');
        $this->db->join('city', ' car.city_id = city.city_id');
        $this->db->order_by('car_id', 'DESC');
        $query = $this->db->from('car');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get All Car List
    public function fetch_all()
    {
        $this->db->select('car.*, cat.name AS category_name');
        $this->db->where("car.is_available ='1'");
        $this->db->where("car.status ='1'");
        $this->db->join('car_category AS cat', ' car.category_id = cat.category_id');
        $this->db->order_by('car_id', 'DESC');
        $query = $this->db->from('car');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get All Car List
    public function fetch_all_cars()
    {
        $this->db->select('car.*, cat.name AS category_name, img.image_name');
        $this->db->where("car.status ='1'");
        $this->db->join('car_category AS cat', ' car.category_id = cat.category_id', 'left');
        $this->db->join('car_images AS img', ' img.car_id = car.car_id', 'left');
        $this->db->group_by('img.car_id');
        $this->db->order_by('car_id', 'DESC');
        $query = $this->db->from('car');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get Single Car Details
    public function fetch_details($id)
    {
        $this->db->select('*');
        $this->db->where('car_id', $id);
        $query = $this->db->from('car');
        
        $result = $query->get();
        $result = $result->result();
        
        return $result[0];
    }
    
    // Add New Car
    public function add($data)
    {
        $this->db->insert('car', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
    
    // Edit Company Details
    public function edit($data, $id)
    {
        $this->db->where('car_id', $id);
        $this->db->update('car', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    // Delete Car
    public function delete($id)
    {
        // Delete Car Details
        $this->db->where('car_id', $id);
        $this->db->delete('car');
        
        // Delete All Car Images
        $this->db->where('car_id', $id);
        $this->db->delete('car_images');
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    // Get All Car Category
    public function fetch_all_category()
    {
        $this->db->select('*');
        $query = $this->db->from('car_category');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Add New Car
    public function add_car_image($data)
    {
        $this->db->insert('car_images', $data);
        
        if($this->db->insert_id() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    // Fetch All Car Images
    public function fetch_car_images($id)
    {
        $query = $this->db->get_where('car_images', array('car_id'=> $id));
        
        if($query->num_rows() > 0)
        {
            $result = $query->result();
            return $result;
        }
        else
        {
            return array();
        }
        
    }
    
    // Delete Car Images
    public function delete_image($id)
    {
        $this->db->where('image_id', $id);
        $this->db->delete('car_images');
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
	
    public function list_category_all($per_page=PER_PAGE_RECORD, $offset = 0, $search_where = "")
    {
        $this->db->select('*');
        $query = $this->db->from('car_category');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    public function get_carcategory_name($id)
    {
        $this->db->select('*');
        $this->db->where('category_id', $id);
        $query = $this->db->from('car_category');
        $query = $query->get();
        $result = $query->result();
        
        if($query->num_rows() > 0)
            return $result[0];
        else
            return FALSE;
    }
}
