<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Riderapi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    // For Login User
    public function rider_authentication($email, $password)
    {
		$this->db->select('rider_id, first_name, last_name, email_id, mobile_number');
        $this->db->where('email_id', $email);
        $this->db->where('password', $password);
		$this->db->where('status','1');
		
        $check_login = $this->db->get('rider');
        
        if($check_login->num_rows() > 0) {
            $rider_data = $check_login->result();
            return $rider_data[0];
        }
        else {
            return false;
        }
    }
	
	// For Forgot password chck Email id
    public function check_rider_email($email_id)
    {
        $this->db->select('*');
        $this->db->where('email_id', $email_id);
        
        $check_email = $this->db->get('rider');
        
        //echo $check_login->num_rows() ;exit;
        if($check_email->num_rows() > 0) {
            $result = $check_email->result();
            return $result[0];
        }
        else {
            return false;
        }
    }
	
	// Check Email is available or not
	// true = Available OR false = Not Available
    public function check_unique($column, $value, $where="")
    {
        $this->db->select('rider_id');
		$this->db->where($column, $value);

		 if($where != "")
            $this->db->where($where);
        $query = $this->db->from('rider');
        
        $result = $query->get();
         if($result->num_rows() > 0)
            return TRUE;
        else 
            return FALSE;
    }
	
	// Get Single Rider Details
    public function fetch_details($id)
    {
        $this->db->select('rider_id, first_name, last_name, email_id, mobile_number, language,add_datetime');
        $this->db->where('rider_id', $id);
        $query = $this->db->from('rider');
        
        $result = $query->get();
        $result = $result->result();
        
        return $result[0];
    }
	
	// Add New Rider
    public function add($data)
    {
        $this->db->insert('rider', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
	
	// Edit Rider Details
    public function edit_profile($data, $id)
    {
        $this->db->where('rider_id', $id);
        $this->db->update('rider', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
}