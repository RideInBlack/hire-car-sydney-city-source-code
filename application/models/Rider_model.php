<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Rider_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // Count Total Rider 
    public function count_total($search_where = "")
    {
        $this->db->select('rider_id, first_name, last_name, email_id, mobile_number, add_datetime');
        $query = $this->db->from('rider');
		
		// for search
		
		if($search_where !="")
		{
			$this->db->where($search_where);
		}
        
        $result = $query->get();
        
        return $result->num_rows();
    }
    
    // Get All Rider List
    public function list_all($per_page=10, $offset = 0, $search_where = "")
    {
        $this->db->select('rider_id, first_name, last_name, email_id, mobile_number, status, add_datetime');
        $this->db->limit($per_page, $offset);
		
		$this->db->order_by('add_datetime', 'DESC');
		
		// for search
		
		if($search_where !="")
		{
			$this->db->where($search_where);
		}
		
        $query = $this->db->from('rider');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    // Get Single Rider Details
    public function fetch_details($id)
    {
        $this->db->select('*');
        $this->db->where('rider_id', $id);
        $query = $this->db->from('rider');
        
        $result = $query->get();
        $result = $result->result();
        return $result[0];
    }
	
	// Get Single Rider Details for password
    public function fetch_password($id)
    {
        $this->db->select('password');
        $this->db->where('rider_id', $id);
        $query = $this->db->from('rider');
        
        $result = $query->get();
        $result = $result->result();
        
        return $result[0];
    }
	
	public function emailduplication($emailId)
    {
        $this->db->select('rider_id');
        $this->db->where('email_id', $emailId);
        $query = $this->db->from('rider');
        
        $result = $query->get();
        $result = $result->result();
        return $result[0];
    }
    
    // Add New Rider
    public function add($data)
    {
        $this->db->insert('rider', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
    
    // Edit Rider Details
    public function edit($data, $id)
    {
        $this->db->where('rider_id', $id);
        $this->db->update('rider', $data);
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    // Delete Rider
    public function delete($id)
    {
        $this->db->where('rider_id', $id);
        $this->db->delete('rider');
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

        // Check Email is available or not
    public function check_unique($column, $value, $where="")
    {
        $this->db->select('rider_id');
		$this->db->where($column, $value);

		 if($where != "")
            $this->db->where($where);
        $query = $this->db->from('rider');
        
        $result = $query->get();
         if($result->num_rows() > 0)
            return TRUE;
        else 
            return FALSE;
    }
	
	// For Login User
    public function rider_authentication($emailadd, $password)
    {
		$this->db->select('rider_id, first_name, last_name, email_id, mobile_number,address, suburb, city,state, zipcode');
		$this->db->where('email_id', $emailadd);
		$this->db->where('password', $password);
		$this->db->where('status','1');
		
		$check_login = $this->db->get('rider');
		
		if($check_login->num_rows() > 0) {
			$admin_data = $check_login->result();
			return $admin_data[0];
		}
		else {
			return 0;
		}
	}
    
    // For Forgot password chck Email id
    public function check_rider_email($email_id)
    {
        $this->db->select('*');
        $this->db->where('email_id', $email_id);
        
        $check_email = $this->db->get('rider');
        
        //echo $check_login->num_rows() ;exit;
        if($check_email->num_rows() > 0) {
            $result = $check_email->result();
            return $result[0];
        }
        else {
            return FALSE;
        }
    }
	
	public function addFacebookUser($data)
    {
        $this->db->select('*');
		$this->db->where('email_id', $data['email_id']);
        
        $checkData = $this->db->get('rider');
        
        if($checkData->num_rows() > 0) 
		{
            $result = $checkData->result();
            return $result[0];
        }
        else 
		{
           	$this->db->insert('rider', $data);
			$getID = $this->db->insert_id();
			
			$this->db->select('*');
			$this->db->where('rider_id', $getID);
			
			$checkData = $this->db->get('rider');
			$result = $checkData->result();
            return $result[0];
		}
    }
	
	public function getRiderIdFromEmail($email) {
		$this->db->select('*');
        $this->db->where('email_id', $email_id);
        
        $check_email = $this->db->get('rider');
        
        //echo $check_login->num_rows() ;exit;
        if($check_email->num_rows() > 0) {
            $result = $check_email->result();
            return $result[0]->rider_id;
        }
        else {
            return FALSE;
        }
	}
	
	public function addNewsLetter($data)
    {
        $this->db->insert('news_subscriber', $data);
        
        if($this->db->insert_id() > 0)
            return $this->db->insert_id();
        else
            return FALSE;
    }
	
	public function check_unique_newsletter($column, $value)
    {
        $this->db->select('*');
		$this->db->where($column, $value);
        $query = $this->db->from('news_subscriber');
        $result = $query->get();
		
        if($result->num_rows() > 0)
            return TRUE;
        else 
            return FALSE;
    }
}
