<?php
/**
 * Description of region_model: Get Region Details from databse
 *
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Region_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    // get all city from database
    public function get_all_city()
    {
        $this->db->select('*');
        $query = $this->db->from('city');
        $query = $this->db->order_by("name", "asc");
        $result = $query->get();
        $result = $result->result();
        
        return $result;
    }
	
	// get all state from database
    public function get_all_state()
    {
        $this->db->select('*');
        $query = $this->db->from('state');
        $query = $this->db->order_by("name", "asc");
        $result = $query->get();
        $result = $result->result();
        
        return $result;
    }
    
    // -------------------------------------------------------------------------
    // Count Total City 
    public function count_city($search_where = "")
    {
        $this->db->select('city_id');
        
        // Search condition
        if($search_where != "")
            $this->db->where($search_where);
        
        $query = $this->db->from('city');
        $result = $query->get();
        
        return $result->num_rows();
    }
    
    public function city_list($per_page=PER_PAGE_RECORD, $offset = 0, $search_where = "") 
    {
        $this->db->select('city_id, name');
        $this->db->limit($per_page, $offset);
        $this->db->order_by('name');
        
        // Search condition
        if($search_where != "")
            $this->db->where($search_where);
        
        $query = $this->db->from('city');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    public function fetch_city_details($id) 
    {
        $this->db->select('city_id, name');
        $this->db->where('city_id', $id);
        $query = $this->db->from('city');
        
        $result = $query->get();
        
        return $result->result();
    }
    
    public function city_add($data) 
    {
        $this->db->insert('city', $data);
        
        return $this->db->insert_id();
    }
    
    public function city_edit($data, $id) 
    {
        $this->db->where('city_id', $id);
        $this->db->update('city', $data);
        
        return TRUE;
        //echo $this->db->last_query();exit;
    }
    
    public function city_delete($id) 
    {
        $this->db->where('city_id', $id);
        $this->db->delete('city');
        
        if($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
        //echo $this->db->last_query();exit;
    }
    
    // Check Unique in database for email and username
    public function check_unique_city($column, $value, $where="")
    {
        $this->db->select('city_id');
        $this->db->where($column, $value);
        
        if($where != "")
            $this->db->where($where);
        
        $query = $this->db->from('city');
        $result = $query->get();
        
        if($result->num_rows() > 0)
            return TRUE;
        else 
            return FALSE;
    }
    // -------------------------------------------------------------------------
    
    public function get_city_name($id)
    {
        $this->db->select('name');
        $this->db->where('city_id', $id);
        $query = $this->db->from('city');
        $query = $query->get();
        $result = $query->result();
        
        if($query->num_rows() > 0)
            return $result[0]->name;
        else
            return FALSE;
    }
    
    
}
