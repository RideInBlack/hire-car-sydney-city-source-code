<!-- /Brand Gallery Section BOC -->
<!-- /Brand Gallery Section EOC --> 
<!-- ====== CONTENT EOC ====== --> 
<!-- ====== FOOTER BOC ====== -->

<?php if($this->session->flashdata('SUCC_DRIVER_REG')): ?>
<div class="modal fade" id="driverRegistrationMSG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <hr class="style-two"/>
                </div>

                <div class="modal-body text-center">
                    <b><b><?=$this->session->flashdata('SUCC_DRIVER_REG')?></b></b>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(e) {
	$('#driverRegistrationMSG').modal('show');
})
</script>
<?php endif; ?>
<?php if($this->session->flashdata('SUCC_RIDER_REG')): ?>
<div class="modal fade" id="riderRegistrationMSG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <hr class="style-two"/>
                </div>

                <div class="modal-body text-center">
                    <b><?=$this->session->flashdata('SUCC_RIDER_REG')?></b>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(e) {
	$('#riderRegistrationMSG').modal('show');
})
</script>
<?php endif; ?>


<footer>
    <div class="container">
        <div class="row"> 
            <!-- /Quick Links BOC -->
            <div class="col-lg-3 col-sm-3 col-md-3">
                <h6>Quick Links</h6>
                <ul class="footer-nav">
                    <li><a href="<?= base_url(); ?>" title="Home">Home</a></li>
                    <li><a href="<?= base_url(); ?>cms/view/WhatWeAreAbout" title="About Us">About Us</a></li>
                    <li><a href="<?= base_url(); ?>pages/support" title="Support">Support</a></li>
                    <li><a href="<?= base_url(); ?>cms/view/Payment" title="Payment">Payment</a></li>
                    <li><a href="<?= base_url() ?>pages/our_products">Gift Coupons</a></li>
                    <li><a href="<?= base_url('contact-us') ?>" title="Contact Us">Contact Us</a></li>
                </ul>
            </div>
            <!-- /Quick Links EOC --> 

            <!-- /Information BOC -->
            <div class="col-lg-3 col-sm-3 col-md-3">
                <h6>Informations</h6>
                <ul class="footer-nav">
                    <li><a href="javascript:void(0)" title="Ride In Black" target="__blank">Ride In Black</a></li>
                    <li><a href="<?= base_url(); ?>cms/view/PrivacyPolicy" title="Privacy Policy">Privacy Policy</a></li>
                    <li><a href="<?= base_url(); ?>cms/view/TermsAndCondition" title="Terms &amp; Condition">Terms &amp; Condition</a></li>
                </ul>
            </div>
            <!-- /Information EOC --> 

            <!-- /Contact Us BOC -->
            <div class="col-lg-3 col-sm-3 col-md-3">
                <h6>Get in touch</h6>
                <ul class="get-in-touch">
                    <li><i class="fa fa-map-marker"></i>
                        <p>PO Box 450 Dulwich Hill NSW 2203, Sydney, Australia</p>
                    </li>
                    <li><i class="fa fa-envelope"></i><a href="mailto:admin@loremipsum.com" title="info@loremipsum.com">admin@hirecarsydneycity.com.au</a></li>
                    <li><i class="fa fa-phone"></i>
                        <p>+ 1300665711</p>
                    </li>
                    <li><i class="fa fa-phone"></i>
                        <p>+ 0431722784</p>
                    </li>
                </ul>
            </div>
            <!-- /Contact Us EOC --> 

            <!-- /Newsletter BOC -->
            <div class="col-lg-3 col-sm-3 col-md-3">
                <h6>Sign up for newsletter</h6>
                <div class="newsletter">
                    <form id="" action="<?= base_url(); ?>home/newsleter" method="post" name="newsletter-form">
                        <div class="common-form">
                            <div class="form-group">
                                <input type="text" name="news-email" id="news-email" placeholder="Enter your email address" class="form-control" value="">
                                <p class="col-sm-12 text-red" id="ERR__NEWA_EMAIL"></p>                                    
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" value="Register" class="btn btn-full" onclick="return newsletterValidation();">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /Newsletter EOC --> 
        </div>
    </div>

    <div class="container">
        <div class="row"> 

            <!-- <div id="fb-root"></div>
 
 <script>(function(d, s, id) {
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) return;
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=176289299137121";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));</script>
 
 <div class="fb-share-button" data-href="http://192.168.1.102/hirecar/" data-layout="button_count"></div>-->

            <!-- /Copyright BOF -->
            <div class="copyright">
                <p>&copy; 2015 <span class="yellow-color-text">Hire Car Sydney City</span>. All Rights Reserved.</p>
                <ul class="social-listing">
                    <li><a title="Facebook" href="https://www.facebook.com/HireCarSydneyCity/" data-layout="button_count"><i class="fa fa-facebook"></i></a></li>
                    <li><a title="Twitter" href="https://twitter.com/Sydney_HireCar"><i class="fa fa-twitter"></i></a></li>
                    <li><a title="Google +" href="https://plus.google.com/u/0/102118780071653059660" onclick="javascript:window.open(this.href,
                                    '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                            return false;"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
            <!-- /Copyright EOF --> 

        </div>
    </div>
</footer>
<!-- ====== FOOTER EOC ====== --> 

<!-- /Go To Top BOC -->
<div class="back-top text-center"> <a href="#" id="toTop"> <i class="fa fa fa-angle-up img-circle"></i> </a> </div>
</div>


<!-- ====== LOGIN MODAL  ====== -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Login</h4>
                    <hr class="style-two"/>
                </div>

                <div class="modal-body">
                    <form id="userlogin" action="<?= base_url('login') ?>?redirect=<?=uri_string()?>" method="post" name="login-form">
                        <div class="common-form">
                            <div class="row">

                                <div class="col-lg-12 text-center text-red"id="ERR_LOGIN"></div>

                                <?php if ($this->session->flashdata('ERR_LOGIN') != ""): ?>
                                    <div class="col-lg-12">
                                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_LOGIN') ?>
                                        </div>
                                    </div>

                                    <script>
                                        // for open login modal if password is invalid
                                        jQuery(document).ready(function($) {
                                            $('#login').modal('show')
                                        });
                                    </script>

                                <?php endif; ?> 

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="user-selection">
                                            <div class="radio-inline">
                                                <label><input type="radio" name="select" value="rider" checked="checked" />Rider</label>
                                            </div>
                                            <div class="radio-inline">
                                                <label><input type="radio" name="select" value="driver" />Driver</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="emailadd" id="emailadd" placeholder="Enter Email" class="form-control" value="<?php
                                        if ($this->input->cookie('emailadd') != '') {
                                            echo $this->input->cookie('emailadd');
                                        } else if ($this->input->post('emailadd') != '') {
                                            echo $this->input->post('emailadd');
                                        }
                                        ?>">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" placeholder="Enter Password" class="form-control" value="<?php
                                        if ($this->input->cookie('password') != '') {
                                            echo $this->input->cookie('password');
                                        } else if ($this->input->post('password') != '') {
                                            echo $this->input->post('password');
                                        }
                                        ?>">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="user-selection">
                                            <label class="radio" for="rememberme">
                                                <input type="checkbox" name="rememberme" value="rememberme" id="rememberme" <?php if ($this->input->cookie('emailadd') != '') { ?> checked <?php } ?>> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="submit" id="login" onclick="return loginValidation();" value="Login" class="btn btn-full">
                                    </div>
                                </div>
                                
								<div class="col-lg-12">
                                    <div class="form-group">
                                		<a href="Javascript:void(0);" onclick="return after_login_button()" class="btn btn-full facebook" style="padding-right:15px;">Login With Facebook</a>
                                    </div>
                                </div>
								
								<?php /*?><div class="col-lg-12">
                                    <div class="form-group">
                                		<a href="Javascript:void(0);" onclick="return after_login_button()"><img src="<?=base_url('assets/web/images/fb-btn.png')?>" style="width: 420px; height: 47px;"></a>
                                    </div>
                                </div><?php */?>
                                
                                <div class="col-lg-12">
                                    <div class="form-group"> <a class="pull-right font-medium" href="Javascript:void(0);" title="Forgot Password" data-toggle="modal" data-target="#forgotpassword">Forgot Password ?</a> </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ====== LOGIN MODAL END  ====== -->
<script>
    function loginValidation()
    {
        var err_flag = false;
        var emailadd = document.getElementById("emailadd").value.trim();
        var password = document.getElementById("password").value;

        if (emailadd == "" && password == "") {
            document.getElementById("ERR_LOGIN").innerHTML = "Please Enter Email  and Password.";
            err_flag = true;
        }
        else {
            if (emailadd == "") {
                document.getElementById("ERR_LOGIN").innerHTML = "Please Enter Email.";
                err_flag = true;
            }
            else if (password == "") {
                document.getElementById("ERR_LOGIN").innerHTML = "Please Enter Password.";
                err_flag = true;
            }
            else {
                document.getElementById("ERR_LOGIN").innerHTML = "";
            }
        }

        if (err_flag == true)
            return false;
        else
            return true;
    }

    $(".alert").fadeTo(2000, 500).slideUp(500, function() {
        $(".alert").alert('close');
    });
</script>
<!-- ====== FORGOT PASSWORD MODAL  ====== -->
<div class="modal fade" id="forgotpassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
                    <hr class="style-two"/>
                </div>

                <div class="modal-body">
                    <form id="forgotpassword" action="<?= base_url('forgotpassword') ?>?redirect=<?=uri_string()?>" method="post" name="login-form">
                        <div class="common-form">
                            <div class="row">

                                <?php if ($this->session->flashdata('ERR_FORGOT_PASSWORD') != ""): ?>
                                    <div class="col-lg-12">
                                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_FORGOT_PASSWORD') ?>
                                        </div>
                                    </div>
                                    <script>
                                        // for open login modal if password is invalid
                                        jQuery(document).ready(function($) {
                                            $('#forgotpassword').modal('show')
                                        });
                                    </script>
                                <?php endif; ?> 
                                <?php if ($this->session->flashdata('SUC_FORGOT_PASSWORD') != ""): ?>
                                    <div class="col-lg-12">
                                        <div class="alert alert-success alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <i class="icon fa fa-check"></i> <?= $this->session->flashdata('SUC_FORGOT_PASSWORD') ?>
                                        </div>
                                    </div>
                                    <script>
                                        // for open login modal if password is invalid
                                        jQuery(document).ready(function($) {
                                            $('#forgotpassword').modal('show')
                                        });
                                    </script>
                                <?php endif; ?> 

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="user-selection">
                                            <div class="radio-inline">
                                                <label><input type="radio" name="type" value="rider" checked="checked" />Rider</label>
                                            </div>
											<div class="radio-inline">
                                                <label><input type="radio" name="type" value="driver" />Driver</label>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" placeholder="Enter Email" class="form-control" value="">
                                    </div>
                                    <p class="col-sm-4 text-red" id="ERR_EMAIL"></p>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="submit" id="forgotpassword_submit" onclick="return forgotPasswordValidation();" value="Forgot Password" class="btn btn-full">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#forgotpassword').on('show.bs.modal', function() {
        // Load up a new modal...
        $('#login').modal('hide');
		$('#loginFrom').modal('hide');
		$('#loginGiftCard').modal('hide');
    })
</script>
<!-- ====== FORGOT PASSWORD MODAL END  ====== -->


<?php //Print Error Message    ?>
<?php if ($this->session->flashdata('ERR_MESSAGE') != ""): ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_MESSAGE') ?>
    </div>
<?php elseif ($this->session->flashdata('SUC_MESSAGE') != ""): ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="icon fa fa-check-circle"></i> <?= $this->session->flashdata('SUC_MESSAGE') ?>
    </div>
<?php endif; ?>


<!-- /Sign Up Popup Content BOC -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Sign Up As Rider</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_EMAILDUPLICATION') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_EMAILDUPLICATION') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?=base_url('home')?>?redirect=<?=uri_string()?>" id="register-form" method="post" name="register-form">
                        <div class="common-form">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="email" id="rider_email" placeholder="Email Address" class="form-control" value="">
                                        <p class="text-red" id="ERR_RIDER_EMAIL"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password11" placeholder="Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_PASSWORD"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="confirmPassword" id="password22" placeholder="Confirm Password" class="form-control" value="">
                                        <p class="text-red" id="ERR_PASSWORD2"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="fname" id="fname" placeholder="First Name" class="form-control" value="">
                                        <p class="text-red" id="ERR_FNAME"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="lname" id="lname" placeholder="Last Name" class="form-control" value="">
                                        <p class="text-red" id="ERR_LNAME"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="tel" name="mobile" id="mobile" placeholder="Mobile No" class="form-control" value="">
                                        <p class="text-red" id="ERR_MOBILE"></p>
                                    </div>
                                </div>
                              	
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="address" id="street_no" placeholder="Street Number and Name" class="form-control" value="">
                                        <p class="text-red" id="ERR_STREET_NO"></p>
                                    </div>
                                </div>
                                <?php /*?><div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="street_name" id="street_name" placeholder="Street Name" class="form-control" value="">
                                        <p class="text-red" id="ERR_STREET_NAME"></p>
                                    </div>
                                </div><?php */?>
								<div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="suburb" id="suburb" placeholder="Suburb" class="form-control" value="">
                                        <p class="text-red" id="ERR_SUBURB"></p>
                                    </div>
                                </div>
								<div class="col-lg-12">
                                    <div class="form-group">
                                        <select class="form-control " name="d_city" id="d_city" placeholder="Select City">
                                            <option value="">Select City</option>
                                            <?php foreach ($city as $cities) { ?>
                                                <option value="<?= $cities->city_id; ?>"><?= $cities->name ?></option>
                                            <?php } ?>
                                        </select>
                                        <p class="text-red" id="ERR_CITY"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                         <select class="form-control" name="state" id="state" value=""  placeholder="Select State">
                                        <option value="">Select State</option>
                                            <?php foreach ($state as $states) { ?>
                                                <option value="<?= $states->state_id; ?>"><?= $states->name ?></option>
                                            <?php } ?>
                                        </select>
                                        <p class="text-red" id="ERR_STATE"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="postcode" id="postcode" placeholder="Post Code" class="form-control" value="">
                                        <p class="text-red" id="ERR_POSTCODE"></p>
                                    </div>
                                </div>
                                
                                <?php /*?><div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="tel" name="language" id="language" placeholder="Language" class="form-control" value="">
                                        <p class="text-red" id="ERR_LANGUAGE"></p>
                                    </div>
                                </div><?php */?>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="register" value="Register" class="btn btn-full" onclick="return validateAddRider()">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="common-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Sign Up As Driver</h4>
                    <hr class="style-two"/>
                </div>
                <div class="modal-body">
                    <?php if ($this->session->flashdata('ERR_DRIVER_EMAILDUPLICATION') != ""): ?>
                        <div class="alert alert-danger alert-dismissable" id="alert" style="padding-top: 10px; padding-bottom: 10px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <i class="icon fa fa-warning"></i> <?= $this->session->flashdata('ERR_DRIVER_EMAILDUPLICATION') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url(); ?>home/signupdriver" id="register-form" method="post" name="register-form" enctype="multipart/form-data">
                        <div class="common-form">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="d_fname" id="d_fname" placeholder="First Name" class="form-control" value="">
                                        <p class="text-red" id="D_ERR_FNAME"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="d_lname" id="d_lname" placeholder="Last Name" class="form-control" value="">
                                        <p class="text-red" id="D_ERR_LNAME"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12">
                                    <div class="form-group">
                                        Driver's Photo : <input type="file" id="d_driverimage" name="d_driverimage">
										<p class="col-sm-8 text-red" id="D_ERR_DRIVERIMAGE"></p>
                                    </div>
                                </div>
								
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="tel" name="d_mobile" id="d_mobile" placeholder="Mobile No" class="form-control" value="">
                                        <p class="text-red" id="D_ERR_MOBILE"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="d_email" id="d_email" placeholder="Email Address" class="form-control" value="">
                                        <p class="text-red" id="D_ERR_EMAIL"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="d_password" id="d_password" placeholder="Password" class="form-control" value="">
                                        <p class="text-red" id="D_ERR_PASSWORD"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="d_confirmPassword" id="d_password2" placeholder="Confirm Password" class="form-control" value="">
                                        <p class="text-red" id="D_ERR_PASSWORD2"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="d_companyname" id="d_companyname" value="" placeholder="Company Name">
                                        <p class="col-sm-4 text-red" id="D_ERR_COMPANYNAME"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="d_abn" id="d_abn" value=""  placeholder="ABN">
                                        <p class="col-sm-4 text-red" id="D_ERR_ABN"></p>
                                    </div>
                                </div>

                                <div class="col-lg-12"><p><b>Address Details</b></p></div>
								<div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="d_address" id="d_address" value=""  placeholder="Street Number & Name">
                                        <p class="text-red" id="D_ERR_ADDRESS"></p>
                                    </div>
                                </div>
                                
								<div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="d_suburb" id="d_suburb" value=""  placeholder="Subrub">
                                        <p class="text-red" id="D_ERR_SUBURB"></p>
                                    </div>
                                </div>
								
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <select class="form-control" name="d_state" id="d_state" value=""  placeholder="Select State">
                                        <option value="">Select State</option>
                                            <?php foreach ($state as $states) { ?>
                                                <option value="<?= $states->state_id; ?>"><?= $states->name ?></option>
                                            <?php } ?>
                                        </select>
                                        <p class="text-red" id="D_ERR_STATE"></p>
                                    </div>
                                </div>
								
								<?php /*?><div class="col-lg-12">
                                    <div class="form-group">
                                        <select class="form-control " name="d_city" id="d_city" placeholder="Select City">
                                            <option value="">Select City</option>
                                            <?php foreach ($city as $cities) { ?>
                                                <option value="<?= $cities->city_id; ?>"><?= $cities->name ?></option>
                                            <?php } ?>
                                        </select>
                                        <p class="text-red" id="D_ERR_CITY"></p>
                                    </div>
                                </div><?php */?>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">

                                       <input type="text" class="form-control" name="d_postcode" id="d_postcode" value="" placeholder="Post Code">
                                        <p class="text-red" id="D_ERR_POSTCODE"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12"><p><b>Licence Details</b></p></div>
								<div class="col-lg-12">
                                    <div class="form-group">
                                        Copy of Driver's Licence : <input type="file" id="licensedetails" name="licensedetails[]" multiple class="multi" maxlength="4" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG">
										<?php /*?><?php if(!empty($_SESSION['HIRE_CAR']['DRIVER']->docDriverLicenseImage)) { 
										$driverImage = base_url()."assets/uploads/driver/license/".$_SESSION['HIRE_CAR']['DRIVER']->docDriverLicenseImage;?>
										<div>
											<img alt="Driver's Licence Image" class="img" src="<?php echo $driverImage; ?>" width="130">
										</div>
										<?php } ?><?php */?>
										<p class="col-sm-12 text-red" id="D_ERR_LICENSEUPLOAD"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12">
                                    <div class="form-group">
										Copy of Driver's HC Authority Card : <input type="file" id="hc_authority_card" name="hc_authority_card[]" multiple class="multi" maxlength="4" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG">
										<?php /*?><?php if(!empty($_SESSION['HIRE_CAR']['DRIVER']->docHCAuthorityImage)) { 
										$driverImage = base_url()."assets/uploads/driver/hcauthoritycertificate/".$_SESSION['HIRE_CAR']['DRIVER']->docHCAuthorityImage;?>
										<div>
											<img alt="Driver's HC Authority Image" class="img" src="<?php echo $driverImage; ?>" width="130">
										</div>
										<?php } ?><?php */?>
										<p class="col-sm-12 text-red" id="D_ERR_HCAUTHORITY"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12">
                                    <div class="form-group">
										<input type="text" class="form-control" name="d_licensedetails" id="d_licensedetails" value="" placeholder="License Details">
										<p class="text-red" id="D_ERR_LICENSEDETAILS"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12"><p><b>Car Details</b></p></div>
								<div class="col-lg-12">
                                    <div class="form-group">
                                        Copy of Vehicle Registration Document :  <input type="file" id="registrationnumber" name="registrationnumber[]" multiple class="multi" maxlength="4" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG">
										<?php /*?><?php if(!empty($_SESSION['HIRE_CAR']['DRIVER']->docRegistrationImage)) { 
										$driverImage = base_url()."assets/uploads/driver/registrationnumber/".$_SESSION['HIRE_CAR']['DRIVER']->docRegistrationImage;?>
										<div>
											<img alt="Driver's Licence Image" class="img" src="<?php echo $driverImage; ?>" width="130">
										</div>
										<?php } ?><?php */?>
										<p class="col-sm-12 text-red" id="D_ERR_REGNUMBER"></p>
                                    </div>
                                </div>
								<div class="col-lg-12">
                                    <div class="form-group">
										<input type="text" class="form-control" name="d_carmake" id="d_carmake" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->zipcode;?>" placeholder="Make">
										<p class="col-sm-12 text-red" id="D_ERR_CARMAKE"></p>
                                    </div>
                                </div>
								<div class="col-lg-12">
                                    <div class="form-group">
										<input type="text" class="form-control" name="d_carYearModel" id="d_carYearModel" value="<?php echo $_SESSION['HIRE_CAR']['DRIVER']->zipcode;?>" placeholder="Year & Model">
										<p class="col-sm-12 text-red" id="D_ERR_CARYEARMODEL"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12"><p><b>Insurance Details</b></p></div>
								<div class="col-lg-12">
                                    <div class="form-group">
										<input type="text" class="form-control" name="d_company" id="d_company" value="" placeholder="Company">
										<p class="col-sm-12 text-red" id="D_ERR_INSURANCECOMPANY"></p>
                                    </div>
                                </div>
								<div class="col-lg-12">
                                    <div class="form-group">
                                        Copy of Insurance Document:  <input type="file" id="insurancetype" name="insurancetype[]" multiple multiple class="multi" maxlength="4" accept="gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG">
										<?php /*?><?php if(!empty($_SESSION['HIRE_CAR']['DRIVER']->docInsuranceImage)) { 
										$driverImage = base_url()."assets/uploads/driver/insurance/".$_SESSION['HIRE_CAR']['DRIVER']->docInsuranceImage;?>
										<div>
											<img alt="Driver's Licence Image" class="img" src="<?php echo $driverImage; ?>" width="130">
										</div>
										<?php } ?><?php */?>
										<p class="col-sm-12 text-red" id="D_ERR_INSURANCETYPE"></p>
                                    </div>
                                </div>
								
								<div class="col-lg-12">
                                    <div class="form-group">
										<input type="text" class="form-control" name="d_invitecode" id="d_invitecode" value="" placeholder="Invite Code">
										<p class="text-red" id="D_ERR_INVITECODE"></p>
									</div>
								</div>
								
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" name="register" value="Register" class="btn btn-full" onclick="return validateAddDriver()">
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Sign Up Popup Content EOC -->

<script type="application/javascript">
function validateAddDriver()
{	
    var err_flag = false;
    var re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    // Firstname Validation
    var fname = document.getElementById("d_fname").value.trim();
    if (fname == "") {
		document.getElementById("D_ERR_FNAME").innerHTML = "Please Enter First Name.";
		err_flag = true;
    }
    else {
		document.getElementById("D_ERR_FNAME").innerHTML = "";
    }

    // Lastname Validation
    var lname = document.getElementById("d_lname").value.trim();
    if (lname == "") {
		document.getElementById("D_ERR_LNAME").innerHTML = "Please Enter Last Name.";
		err_flag = true;
    }
    else {
		document.getElementById("D_ERR_LNAME").innerHTML = "";
    }
	
    // Mobile Number Validation
    var mobile = document.getElementById("d_mobile").value.trim();
    if (mobile == "" || isNaN(mobile)) 
    {
		document.getElementById("D_ERR_MOBILE").innerHTML = "Please Enter Mobile Number.";
		err_flag = true;
    }
    else {
		document.getElementById("D_ERR_MOBILE").innerHTML = "";
    }

    // Email Id Validation
    var email = document.getElementById("d_email").value.trim();
    if (email == "") {
		document.getElementById("D_ERR_EMAIL").innerHTML = "Please Enter Email Address.";
		err_flag = true;
    }
    else if (!re_email.test(email)) {
		document.getElementById("D_ERR_EMAIL").innerHTML = "Please Enter Valid Email Address.";
    }
    else {
		document.getElementById("D_ERR_EMAIL").innerHTML = "";
    }
	
    // Password Validation confirmPassword
    var password1 = document.getElementById("d_password").value;
    var password2 = document.getElementById("d_password2").value;
    if (password1 == "") {
		document.getElementById("D_ERR_PASSWORD").innerHTML = "Please Enter Password.";
		err_flag = true;
    }
    else {
		if (password1 != password2) {
			document.getElementById("D_ERR_PASSWORD2").innerHTML = "Confirm Password is not match.";
			err_flag = true;
		}
		else {
			document.getElementById("D_ERR_PASSWORD2").innerHTML = "";
		}
		document.getElementById("D_ERR_PASSWORD").innerHTML = "";
	}
	
	var address = document.getElementById("d_address").value;
    if(address == '') {
		document.getElementById("D_ERR_ADDRESS").innerHTML = "Please Enter valid Street Number and Name.";
		err_flag = true;
	}
    else {
		document.getElementById("D_ERR_ADDRESS").innerHTML = "";	
    }
	
	var state = document.getElementById("d_state").value;
    if(state == '') {
		document.getElementById("D_ERR_STATE").innerHTML = "Please Select State.";
		err_flag = true;
	}
    else {
		document.getElementById("D_ERR_STATE").innerHTML = "";	
    }
	
	var suburb = document.getElementById("d_suburb").value;
    if(suburb == '') {
		document.getElementById("D_ERR_SUBURB").innerHTML = "Please Enter Suburb.";
		err_flag = true;
	}
    else {
		document.getElementById("D_ERR_SUBURB").innerHTML = "";	
	}
		
	var zipcode = document.getElementById("d_postcode").value;
    if(zipcode == '') {
		document.getElementById("D_ERR_POSTCODE").innerHTML = "Please Enter valid post code.";
		err_flag = true;
	}
    else {
		document.getElementById("D_ERR_POSTCODE").innerHTML = "";	
    }
	
	var fup = document.getElementById('d_driverimage');
    var fileName = fup.value;
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
    if(fileName != '') {
		if(ext =="GIF" || ext=="gif" || ext =="PNG" || ext=="png" || ext =="JPG" || ext=="jpg" || ext =="JPEG" || ext=="jpeg") {
			document.getElementById("D_ERR_DRIVERIMAGE").innerHTML = "";	
		}
		else {	
			document.getElementById("D_ERR_DRIVERIMAGE").innerHTML = "Please upload only png,jpg,jpeg and gif image file.";
			err_flag = true;
		}
    }
	
	// Validation For Licence Details
	var licence = document.getElementById("d_licensedetails").value.trim();
	if (licence == "") {
		document.getElementById("D_ERR_LICENSEDETAILS").innerHTML = "Please Enter Licence Detail.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_LICENSEDETAILS").innerHTML = "";
	}
	// Validation For Licence Details
	var licenceUpload = document.getElementById("licensedetails").value.trim();
	if (licenceUpload == "") {
		document.getElementById("D_ERR_LICENSEUPLOAD").innerHTML = "Please Upload Licence Image.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_LICENSEUPLOAD").innerHTML = "";
	}
	// HC Authority Details
	var hcAuthory = document.getElementById("hc_authority_card").value.trim();
	if (hcAuthory == "") {
		document.getElementById("D_ERR_HCAUTHORITY").innerHTML = "Please Upload HC Authoirity Image.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_HCAUTHORITY").innerHTML = "";
	}
	// HC Authority Details
	var regNumber = document.getElementById("registrationnumber").value.trim();
	if (regNumber == "") {
		document.getElementById("D_ERR_REGNUMBER").innerHTML = "Please Upload Registration Detail Image.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_REGNUMBER").innerHTML = "";
	}
	// Car Make
	var carMake = document.getElementById("d_carmake").value.trim();
	if (carMake == "") {
		document.getElementById("D_ERR_CARMAKE").innerHTML = "Please Enter Car Make and Year.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_CARMAKE").innerHTML = "";
	}
	// Car Year Model
	var carMake = document.getElementById("d_carYearModel").value.trim();
	if (carMake == "") {
		document.getElementById("D_ERR_CARYEARMODEL").innerHTML = "Please Enter Car Make and Year.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_CARYEARMODEL").innerHTML = "";
	}
	// Insurance Company
	var carMake = document.getElementById("d_company").value.trim();
	if (carMake == "") {
		document.getElementById("D_ERR_INSURANCECOMPANY").innerHTML = "Please Enter Insurance Company.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_INSURANCECOMPANY").innerHTML = "";
	}
	
	// Insurance Type
	var carMake = document.getElementById("insurancetype").value.trim();
	if (carMake == "") {
		document.getElementById("D_ERR_INSURANCETYPE").innerHTML = "Please Upload Insurence Type Image.";
		err_flag = true;
	}
	else {
		document.getElementById("D_ERR_INSURANCETYPE").innerHTML = "";
	}
	
	if (err_flag == true)
    	return false;
    else
    	return true;
}

function validateAddRider()
{
	var err_flag = false;
	var re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	
	// Email Id Validation
	var email = document.getElementById("rider_email").value.trim();
	if (email == "") {
		document.getElementById("ERR_RIDER_EMAIL").innerHTML = "Please Enter Email Address.";
		err_flag = true;
	}
	else if (!re_email.test(email)) {
		document.getElementById("ERR_RIDER_EMAIL").innerHTML = "Please Enter Valid Email Address.";
	}
	else {
		document.getElementById("ERR_RIDER_EMAIL").innerHTML = "";
	}
	
	// Password Validation confirmPassword
	var password1 = document.getElementById("password11").value;
	var password2 = document.getElementById("password22").value;
	if (password1 == "") {
		document.getElementById("ERR_PASSWORD").innerHTML = "Please Enter Password.";
		err_flag = true;
	}
	else {
		if (password1 != password2) {
			document.getElementById("ERR_PASSWORD2").innerHTML = "Confirm Password is not match.";
		}
		else {
			document.getElementById("ERR_PASSWORD2").innerHTML = "";
		}
		document.getElementById("ERR_PASSWORD").innerHTML = "";
	}
	
	// Firstname Validation
	var fname = document.getElementById("fname").value.trim();
	if (fname == "") {
		document.getElementById("ERR_FNAME").innerHTML = "Please Enter First Name.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_FNAME").innerHTML = "";
	}
	
	// Lastname Validation
	var lname = document.getElementById("lname").value.trim();
	if (lname == "") {
		document.getElementById("ERR_LNAME").innerHTML = "Please Enter Last Name.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_FNAME").innerHTML = "";
	}
	
	// Mobile Number Validation
	var mobile = document.getElementById("mobile").value.trim();
	if (mobile == "") {
		document.getElementById("ERR_MOBILE").innerHTML = "Please Enter Mobile Number.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_MOBILE").innerHTML = "";
	}
	
	// Mobile Number Validation
    var address = document.getElementById("street_no").value.trim();
    if (address == "") {
    document.getElementById("ERR_STREET_NO").innerHTML = "Please Enter Street Number and Name.";
    err_flag = true;
    }
    else {
    document.getElementById("ERR_STREET_NO").innerHTML = "";
    }
	
	// Mobile Number Validation
    var suburb = document.getElementById("suburb").value.trim();
    if (suburb == "") {
    document.getElementById("ERR_SUBURB").innerHTML = "Please Enter Suburb.";
    err_flag = true;
    }
    else {
    document.getElementById("ERR_SUBURB").innerHTML = "";
    }
	
	// Mobile Number Validation
    var city = document.getElementById("d_city").value.trim();
    if (city == "") {
    document.getElementById("ERR_CITY").innerHTML = "Please Select City.";
    err_flag = true;
    }
    else {
    document.getElementById("ERR_CITY").innerHTML = "";
    }
	
	// Mobile Number Validation
    var state = document.getElementById("d_state").value.trim();
    if (state == "") {
    document.getElementById("ERR_STATE").innerHTML = "Please Select State.";
    err_flag = true;
    }
    else {
    document.getElementById("ERR_STATE").innerHTML = "";
    }
	
	
	// Mobile Number Validation
    var postcode = document.getElementById("postcode").value.trim();
    if (postcode == "") {
    document.getElementById("ERR_POSTCODE").innerHTML = "Please Enter Post Code.";
    err_flag = true;
    }
    else {
    document.getElementById("ERR_POSTCODE").innerHTML = "";
    }

	/* // Mobile Number Validation
	var mobile = document.getElementById("language").value.trim();
	if (mobile == "") {
		document.getElementById("ERR_LANGUAGE").innerHTML = "Please Enter Language.";
		err_flag = true;
	}
	else {
		document.getElementById("ERR_LANGUAGE").innerHTML = "";
	}*/
	
	if (err_flag == true)
		return false;
	else
		return true;
}


    function newsletterValidation()
    {	
    var err_flag = false;
    var re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    // Email Id Validation
    var email = document.getElementById("news-email").value.trim();
    if (email == "") 
    {
    document.getElementById("ERR__NEWA_EMAIL").innerHTML = "Please Enter Email Address.";
    err_flag = true;
    }
    else if (!re_email.test(email)) 
    {
    document.getElementById("ERR__NEWA_EMAIL").innerHTML = "Please Enter Valid Email Address.";
    err_flag = true;
    }
    else 
    {
    document.getElementById("ERR__NEWA_EMAIL").innerHTML = "";
    }

    //alert(err_flag);
    if (err_flag == true)
    return false;
    else
    return true;
    }
</script>
<?php
if ($this->session->flashdata('ERR_EMAILDUPLICATION') != "") {
    ?>
    <script type="application/javascript">
        $( document ).ready(function() {
        $('#myModal1').modal('show');
        });
    </script>
    <?php
}

if ($this->session->flashdata('ERR_DRIVER_EMAILDUPLICATION') != "") {
    ?>
    <script type="application/javascript">
        $( document ).ready(function() {
        $('#myModal2').modal('show');
        });
    </script>
    <?php
}
date_default_timezone_set('Australia/Brisbane');
 $current_hours =  date('h:ia');

?>




<?php /*?><!-- MAP Related JS -->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap"></script>

<script>
$(document).ready(function() {
  var input = document.getElementById('pick-address');
  var searchBox = new google.maps.places.SearchBox(input);
  
  var input2 = document.getElementById('drop-address');
  var searchBox = new google.maps.places.SearchBox(input2);
});
</script><?php */?>

<script>
$('#pick-address').on('keyup keypress', function(e) {
  var code = e.keyCode || e.which;
  if (code == 13) { 
    e.preventDefault();
    return false;
  }
});

$('#drop-address').on('keyup keypress', function(e) {
  var code = e.keyCode || e.which;
  if (code == 13) { 
    e.preventDefault();
    return false;
  }
});
</script>

<!-- =================== WEB JS BOC ==================== -->
<script type="text/javascript" src="<?= base_url('assets/web') ?>/js/fancy-select.js"></script> 
<script type="text/javascript" src="<?= base_url('assets/web') ?>/js/custom.js"></script> 

<!-- =================== SLICK SLIDER ==================== -->
<?php /*?><script type="text/javascript" src="<?= base_url('assets/web') ?>/js/slick.js"></script> <?php */?>
<script type="text/javascript" src="<?= base_url('assets/web') ?>/js/slick.min.js"></script> 

<!-- =================== Components JS  BOC ==================== --> 
<!-- bootstrap Date picker -->
<script type="text/javascript" src="<?= base_url('assets/web') ?>/plugins/bootstrap-datepicker-1.4.0/js/bootstrap-datepicker.min.js"></script> 
<!-- JQuery Time picker -->
<script type="text/javascript" src="<?= base_url('assets/web') ?>/plugins/jquery-timepicker-1.3.2/jquery.timepicker.min.js"></script> 

<!-- Select2 JS -->
<script type="text/javascript" src="<?= base_url('assets/web') ?>/js/select2.full.min.js"></script>

<!-- ---------------------------------- -->
<script>
$(document).ready(function() {
	$('.multiple-items').slick({
		lazyLoad: 'ondemand',
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay:true,
		autoplaySpeed:3000,
		responsive: [
   {
     breakpoint: 991,
     settings: {
       slidesToShow: 3,
       slidesToScroll: 3,
       infinite: true,
       //dots: true
     }
   },
   {
     breakpoint: 768,
     settings: {
       slidesToShow: 1,
       slidesToScroll: 1
     }
   }
   // You can unslick at a given breakpoint now by adding:
   // settings: "unslick"
   // instead of a settings object
 ]
	});
	$('.multiple-items-1').slick({
		lazyLoad: 'ondemand',
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 1,
		autoplay:true,
		autoplaySpeed:3000,
		prevArrow:false,
		nextArrow:false,
		responsive: [
   {
     breakpoint: 991,
     settings: {
       slidesToShow: 3,
       slidesToScroll: 3,
       infinite: true,
       //dots: true
     }
   },
   {
     breakpoint: 768,
     settings: {
       slidesToShow: 1,
       slidesToScroll: 1
     }
   }
   
   // You can unslick at a given breakpoint now by adding:
   // settings: "unslick"
   // instead of a settings object
 ]
	});
});

$(".homedatepicker").datepicker({
	format: 'dd/mm/yyyy',
	startDate: "today",
	todayHighlight: true,
});

$(".homedatepicker").change(function(){
	action ="datetimechange";
	date = $(this).val();
	res = date.split("/");
	
	var  selecteddate =res[0];
	var selectedmonth =res[1];
	var  selectedyear = res[2];
	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	currentdate = dd+'/'+mm+'/'+yyyy;
	
	var minites =  today.getMinutes(); // =>  30
	var hours = today.getHours() > 12 ? today.getHours() - 12 : today.getHours();
	var am_pm = today.getHours() >= 12 ? "pm" : "am";
	
	var starttime ;var endtime;
	if(dd == selecteddate && mm == selectedmonth){
		//starttime = String(hours+':'+ minites+am_pm);
		starttime= '<?php  echo $current_hours; ?>';
		//starttime = starttime.toString();
		// endtime = "12:00pm";
	}else{
		starttime ="00:00am";
		//endtime = "12:00pm";
	}
	
	$.ajax({
		method: "POST",
		url: "<?php echo site_url('home/changedatetime'); ?>" ,
		data: {
			'action': action, 
			'date': date,
		}
	}).done(function(result) {
		
		$("#timepick").html(result);
		//alert(result);
		$('input.hometimepicker').timepicker({
			'minTime': starttime,
			'maxTime': '11:59pm',
			'showDuration': false,
			timeFormat: 'hh:mm p',
			interval: 15 // 15 minutes
		});
	});
})

$(".datepicker").datepicker({
	format: 'dd/mm/yyyy',
	startDate: "today",
	todayHighlight: true,
});

//Timepicker
(function($) {
	$(function() {
		$('input.timepicker').timepicker({
			'scrollDefault': 'now',
			timeFormat: 'hh:mm p',
			interval: 15 // 15 minutes
		});
	});
})(jQuery);

// Sign Up Pop Over
$(document).ready(function() { $('#signup-popover').popover(); });
//Initialize Select2 Elements
$(document).ready(function() { $(".select2").select2(); });
</script>
<script type="text/javascript" src="<?=base_url('assets/admin/plugins/multiple-file-upload')?>/jquery.MultiFile.js"></script> 
<script>
// Facebook Login
window.fbAsyncInit = function()
{
	FB.init({
		appId: '1474456009546793',
		/*status: true, // check login status
		cookie: true, // enable cookies to allow the server to access the session
		xfbml: true,
		version    : 'v2.2'*/
		xfbml      : true,
		version    : 'v2.5'
	});
};

(function(d) {
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	
	if (d.getElementById(id)) {
		return;
	}
	
	js = d.createElement('script');
	js.id = id;
	js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));


//LOGIN FUNCTION 
function after_login_button() {FB.login(function(response) 
{
	if (response.authResponse) 
	{
		FB.api('/me', {fields: 'email,first_name,last_name'},function(response) {
			var user_tye = $('input[name="select"]:checked').val();
		
			//console.log(response);return 0;
			if (response.id != 'undefined')
			{
				var res = encodeURIComponent(response.first_name + '/' + response.last_name + '/' + response.email + '/' + user_tye);
				var redirectURL =  '<?=uri_string()?>';
				
				window.location = '<?=base_url()?>home/facebooklogin?data='+res+'&redirect='+redirectURL;
				//window.location = 'home/facebooklogin?data=' + res;
			}
			else
			{
				window.location = 'home';
			}
		});
	}
	else 
	{
		window.location = '';
	}
}, {scope: 'email,user_birthday,user_about_me,read_insights'});
}

function after_logout_button()
{
	FB.getLoginStatus(function(response) {
		if (response.status == "connected") {
			FB.api('/me', function(response) {
				FB.logout(function(response)
				{
					window.location = 'home/logout';
				});
			});
		}
		else
		{
			//alert('sdfsdf');
			window.location = 'home/logout';
		}
	}, true);
}
</script>

</body>
</html>
