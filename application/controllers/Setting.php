<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('cms_model', 'model');
    }
    
    public function view()
    {
		$url = $this->uri->segment(3); 
		
		if($url != '')
		{
			$cmsData = $this->model->fetch_cms($url);
		}
		else
		{
			$cmsData = '';
		}
		//echo "<pre>";
		//print_r($cmsData);die;
		
		$data['cmsData'] = $cmsData;
        $this->load->view('web/template/header');
        $this->load->view('web/cms/index',$data);
        $this->load->view('web/template/footer');
    }
}
