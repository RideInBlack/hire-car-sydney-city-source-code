<?php

// http://papermashup.com/php-gd-generate-an-image-with-text-and-font-embedding/
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Giftcard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Check Admin login from function from (h: function)
        check_admin_login();
        // Load Rider Model
        $this->load->model('giftcard_model', 'model');
    }

    // View All Users List
    public function view($page = 1) {
        // Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;

        // Fetch details from model
        $user = $this->model->list_all($record_per_page, ($page - 1) * $record_per_page);

        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url() . 'admin/giftcard/view/', $this->model->count_total(), $record_per_page);

        // Store details of rider
        $data['giftcard_details'] = $user;
        $data['sr'] = (($page - 1) * PER_PAGE_RECORD) + 1;

        $page_details['title'] = "View Gift Cards";

        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/giftcard/giftcard-index', $data);
        $this->load->view('admin/template/footer');
    }
	
	// View All Users List Who have Register Gift Card and Choose payment method "Bank Deposit"
    public function registered($page = 1) {
		
		$search_where = "";
		if(isset($_GET['search_email']) && !empty($_GET['search_email'])) {
			$search_where = "email_id LIKE '%".$_GET['search_email']."%'";
		}
		
        // Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;

        // Fetch details from model
		$this->load->model('giftcard_coupon_model');
        $gift_cards = $this->giftcard_coupon_model->all_register_gift_card($record_per_page, ($page - 1) * $record_per_page,$search_where);
		
				
        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url() . 'admin/giftcard/registered/', $this->giftcard_coupon_model->count_total($search_where),$record_per_page);
		
		// Get Giftcard register details
		$data['gift_cards'] = $gift_cards;
        $data['sr'] = (($page - 1) * PER_PAGE_RECORD) + 1;

        $page_details['title'] = "View Gift Cards";

		// Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/giftcard/giftcard-registered', $data);
        $this->load->view('admin/template/footer');
    }

        // View All Users List Who have Register Gift Card and Choose payment method "Bank Deposit"
    public function printed($page = 1) {
        
        $search_where = "";
        if(isset($_GET['search_email']) && !empty($_GET['search_email'])) {
            $search_where = "email_id LIKE '%".$_GET['search_email']."%'";
        }
        
        // Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;

        // Fetch details from model
        $this->load->model('giftcard_coupon_model');
        $gift_cards = $this->giftcard_coupon_model->all_printed_coupons($record_per_page, ($page - 1) * $record_per_page,$search_where);
        //echo "<pre>";print_r($gift_cards);
                
        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url() . 'admin/giftcard/registered/', $this->giftcard_coupon_model->count_total_printed($search_where),$record_per_page);
        
        // Get Giftcard register details
        $data['gift_cards'] = $gift_cards;
        $data['sr'] = (($page - 1) * PER_PAGE_RECORD) + 1;

        $page_details['title'] = "View genarated coupons";

        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/giftcard/printed-coupon', $data);
        $this->load->view('admin/template/footer');
    }
    
	
	public function chageStatus($coupon_id=0, $status=0) {
		if($coupon_id != 0){
			$fields = array('status' => $status, 'generate_datetime' => date('Y-m-d H:i:s', time()));
			
			$this->load->model('giftcard_coupon_model');
			$this->giftcard_coupon_model->edit($fields, $coupon_id);
			
			if($status == 1) {
				// -----------------------------------------------------------------------
				// Email To Rider For Coupon Register
				// -----------------------------------------------------------------------
				$coupon_details = $this->giftcard_coupon_model->details($coupon_id);
				
				$this->load->model('rider_model');
				$rider_details = $this->rider_model->fetch_details($coupon_details->rider_id);
				
				// Load email library for send email for booking ride
				$this->load->library('email');
				$this->email->from('admin@hirecarssydneycity.com.au', "Hire Car Sydney City - Team");
				
				$this->email->to($rider_details->email_id);
				$this->email->subject("Coupon Activation Details");
	
				$message = "<div style='float:left;min-height:auto;width:100%'>
					<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
					<a href='" . base_url() . "'>";
	
				$message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
				$message .= "</a></div>
							<div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
							<div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
							<div style='padding-left:10px;'>
							<p>Dear ".$rider_details->first_name." ".$rider_details->last_name.",</p>
							<p>Coupon Details.</p>
							<p><b>Coupon Code:</b> ".$coupon_details->coupon_code."</p>
							<p><b>Price:</b> $".$coupon_details->price."</p>
							<hr/>
							<p><b>Your coupon is activated. Now you can use it.</b></p>
							<hr/>
							<p>--</p>
							<p>Regards,<br />
							<b>Hire Car Sydney City Team<b></p>
							</div>
							</div>
							</div>";
	
				$this->email->message($message);
				$this->email->send();
				
				// -----------------------------------------------------------------------
			}
		}
		
		redirect(base_url('admin/giftcard/registered'));
	}

	public function sendReminderEmail($coupon_id = 0) {
		if($coupon_id != 0){
			$this->load->model('giftcard_coupon_model');
			
			// Get bank details from setting
			$this->load->model('setting_model');
			$setting_details = $this->setting_model->fetch_details(1);
			// -----------------------------------------------------------------------
			// Email To Rider For Coupon Register
			// -----------------------------------------------------------------------
			$coupon_details = $this->giftcard_coupon_model->details($coupon_id);
				
			$this->load->model('rider_model');
			$rider_details = $this->rider_model->fetch_details($coupon_details->rider_id);
			
			// Load email library for send email for booking ride
			$this->load->library('email');
			$this->email->from('admin@hirecarssydneycity.com.au', "Hire Car Sydney City - Team");
				
			$this->email->to($rider_details->email_id);
			$this->email->subject("Coupon Payment Reminder");
	
			$message = "<div style='float:left;min-height:auto;width:100%'>
					<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
					<a href='" . base_url() . "'>";
	
			$message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
			$message .= "</a></div>
						<div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
						<div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
						<div style='padding-left:10px;'>
						<p>Dear ".$rider_details->first_name." ".$rider_details->last_name.",</p>
						<p>Coupon Details.</p>
						<p><b>Coupon Code:</b> ".$coupon_details->coupon_code."</p>
						<p><b>Price:</b> $".$coupon_details->price."</p>
						<hr/>
						<p><b>Bank:</b> ".$setting_details->setting_bank."</p>
                    	<p><b>Account Name:</b> ".$setting_details->setting_account_name."</p>
                    	<p><b>BSB:</b> ".$setting_details->setting_bsb."</p>
                    	<p><b>Account Number:</b> ".$setting_details->setting_account_number."</p>
						<p><b>Please deposit the coupon amount in bank otherwise coupon will expired soon.</b></p>
						<hr/>
						<p>--</p>
						<p>Regards,<br />
						<b>Hire Car Sydney City Team<b></p>
						</div>
						</div>
						</div>";
	
			$this->email->message($message);
			$this->email->send();
			// -----------------------------------------------------------------------
		}
		
		redirect(base_url('admin/giftcard/registered'));
	}

    //gift coupon using ajax call
    public function pricepercentage() {
        $gid = $_POST['giftcard'];
        $data = $this->model->giftdetails($gid);
        //print_r((array)$h);
        //return $data;
        //echo json_encode($data);
        //print_r($data);
        foreach ($data as $v) {
            echo $v->pricepercentage;
        }
        //print($u->price);
    }

    public function price() {
        $gid = $_POST['giftcard'];
        $data = $this->model->giftdetails($gid);
        foreach ($data as $v) {
            echo $v->price;
        }
    }

    public function coupon_code() {
        $gid = $_POST['giftcard'];
        $data = $this->model->giftdetails($gid);
        foreach ($data as $v) {
            echo $v->coupon_code;
        }
    }

    // Add new Gift Card
    public function add() {
        if ($this->input->post()) {
            $target = "./assets/admin/GiftCardImages/";
            $filename = pathinfo($_FILES['imageFile']['name'], PATHINFO_FILENAME);
            $extension = strtolower(pathinfo($_FILES['imageFile']['name'], PATHINFO_EXTENSION));
            $filename = str_replace(' ', '_', $filename);
            $newFile = $filename . uniqid() . '.' . $extension;
            $filenamewittargetfolder = $target . $newFile;
            if (in_array($extension, array('jpg', 'jpeg', 'gif', 'png', 'bmp'))) {
                if (move_uploaded_file($_FILES['imageFile']['tmp_name'], $filenamewittargetfolder)) {
                    $newUploadedFile = $newFile;
                } else {
                    $newUploadedFile = '';
                    $this->session->set_flashdata('ERR_MESSAGE', "Error while Image uploading. Please try again.");
                }
                $fields = array("giftcard_name" => trim($this->input->post('name')),
                    "image_name" => $newUploadedFile,
                    "price" => $this->input->post('price'),
                    "pricepercentage" => trim($this->input->post('pricepercentage')),
                    'coupon_code' => trim($this->input->post('coupon_code')),
                    "add_datetime" => date('Y-m-d H:i:s', time()),
                    "update_datetime" => date('Y-m-d H:i:s', time())
                );

                $giftcard_id = $this->model->add($fields);

                if ($giftcard_id) {
                    $this->session->set_flashdata('SUCC_MESSAGE', 'Successfully Created Gift Card.');
                    redirect('admin/giftcard/view');
                } else {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error to create new Gift Card.');
                }
            } else {

                $this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');
            }
        }

        $page_details['title'] = "Add Gift Card";
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/giftcard/giftcard-add');
        $this->load->view('admin/template/footer');
    }

    // View User details
    public function details($id) {
        $data['disable'] = "readonly disabled";

        $data['giftcard_details'] = $this->model->fetch_details($id);
        $data['action'] = 'view';

        $page_details['title'] = "View Gift Card Details";

        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/giftcard/giftcard-details', $data);
        $this->load->view('admin/template/footer');
    }

    // Edit User details
    public function edit($id) {
        $data['giftcard_details'] = $this->model->fetch_details($id);

        if ($this->input->post() && $this->input->post()) {
            print_r($_FILES['imageFile']['name']);

            if (!empty($_FILES['imageFile']['name'])) {

                $target = "./assets/admin/GiftCardImages/";
                $filename = pathinfo($_FILES['imageFile']['name'], PATHINFO_FILENAME);
                $extension = strtolower(pathinfo($_FILES['imageFile']['name'], PATHINFO_EXTENSION));
                $filename = str_replace(' ', '_', $filename);
                $newFile = $filename . uniqid() . '.' . $extension;
                $filenamewittargetfolder = $target . $newFile;

                if (in_array($extension, array('jpg', 'jpeg', 'gif', 'png', 'bmp'))) {
                    if (move_uploaded_file($_FILES['imageFile']['tmp_name'], $filenamewittargetfolder)) {
                        unlink($target . $data['giftcard_details']->image_name);
                        $newUploadedFile = $newFile;
                    } else {
                        $newUploadedFile = $data['giftcard_details']->image_name;
                        $this->session->set_flashdata('ERR_MESSAGE', "Error while Image uploading. Please try again.");
                    }
                } else {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');
                }
            } else {
                $newUploadedFile = $data['giftcard_details']->image_name;
            }
            $fields = array("giftcard_name" => trim($this->input->post('name')),
                "image_name" => $newUploadedFile,
                "pricepercentage" => trim($this->input->post('pricepercentage')),
                "price" => $this->input->post('price'),
                'coupon_code' => trim($this->input->post('coupon_code')),
                "update_datetime" => date('Y-m-d H:i:s', time())
            );
            $rider_id = $this->model->edit($fields, $id);

            if ($rider_id) {
                $this->session->set_flashdata('SUCC_MESSAGE', 'Gift Card Details Saved Successfully.');
                redirect('admin/giftcard/view');
            } else {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to edit Gift Card details.');
            }
        }

        $data['disable'] = "";
        $data['action'] = 'edit';

        $page_details['title'] = "Edit Rider Details";

        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/giftcard/giftcard-details', $data);
        $this->load->view('admin/template/footer');
    }

    // Delete User
    public function delete($id) {
        $data['giftcard_details'] = $this->model->fetch_details($id);
        $target = "assets/admin/GiftCardImages/";
        unlink($target . $data['giftcard_details']->image_name);
        $result = $this->model->delete($id);

        if ($result) {
            $this->session->set_flashdata('SUCC_MESSAGE', 'Gift Card Deleted Successfully.');
        } else {
            $this->session->set_flashdata('ERR_MESSAGE', 'Error to Delete Gift Card.');
        }
        redirect('admin/giftcard/view');
    }

    public function printcoupon() {

        $gift_cards = $this->model->get_all();
        $data['dift_cards'] = $gift_cards;

        $page_details['title'] = "Print Gift Card";
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/giftcard/giftcard-printcoupon', $data);
        $this->load->view('admin/template/footer');
    }

    public function changestatus($id) {
        //echo $_POST['status'];exit;
        $data = array('status' => $this->input->post('status'));

        $result = $this->model->edit($data, $id);

        if ($result) {
            echo 'success';
        } else {
            echo 'error';
        }
        exit();
    }

    public function generateCoupon() {
        $this->load->helper('string');

        do {
            $coupon_code = strtoupper(random_string('alnum', 6));
            $is_unique = $this->model->check_unique('coupon_code', $coupon_code);
        } while (!$is_unique);

        echo $coupon_code;
        exit;
    }

    public function generateAdminCoupon() {
        $this->load->helper('string');

        do {
            $coupon_code = strtoupper(random_string('alnum', 10));
			$this->load->model('Giftcard_coupon_model');
            $is_unique = $this->Giftcard_coupon_model->check_unique('coupon_code', $coupon_code);
        } while (!$is_unique);

        echo $coupon_code;
        exit;
    }

    public function makecoupanimage() {
		$coupan_code = $_POST['coupon_code'];
        $gid = $_POST['gid'];
		
		$details = $this->model->fetch_details($gid);
		
		$this->load->model('Giftcard_coupon_model');
		$is_unique = $this->Giftcard_coupon_model->check_unique('coupon_code', $coupan_code);
		
		if(!$is_unique) {
			echo 0; 
			exit;
		}
		
		$fields = array('coupon_code' => $coupan_code,
                        'giftcard_id' => $gid,
						'price' => $details->price,
						'remaining_amount' => $details->price,
						'pricepercentage' => $details->pricepercentage,
						'generate_by' => '1',
						'rider_id' => 0,
						'coupon_code' => $_POST['coupon_code'],
                        'rider_name' => $_POST['rider_name'],
                        'Address' => $_POST['Address'],
                        'rider_phone' => $_POST['rider_phone'],
                        'rider_email' => $_POST['rider_email'],
                        'admin_comment' => $_POST['admin_comment'],
						'generate_datetime' => date('Y-m-d H:i:s', time()),
						);
		$this->Giftcard_coupon_model->add_card($fields);
		
        //$image = imagecreatefrompng(base_url() . '/assets/admin/businesscard-4.png');
        $image = imagecreatefrompng(base_url() . '/assets/admin/GiftCardImages/'.$details->image_name);
		imagealphablending($image, true);

        $red = imagecolorallocate($image, 150, 0, 0);
        imagefttext($image, 25, 0, 320, 525, $red, 'assets/admin/fonts/verdana.ttf', $coupan_code);

        $newfilename = $gid . "_" . $coupan_code;
        $filename = 'assets/admin/coupan_cards/' . $newfilename . '.png';
        imagepng($image, $filename);
        imagedestroy($image);

         $image = base_url() . $filename;
		
		// Email to admin for generate  new coupon
            //------------------------------------------------------------------
             // Load email library for send email for Generate coupan code
            $this->load->library('email');
			
			$this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
		    $this->email->to("admin@hirecarsydneycity.com.au");
         // $this->email->to("ashvin.solanki@ifuturz.com");
            $this->email->subject("New Gift Card Genereted");
			
			$message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                        <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                        <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                        <div style='padding-left:10px;'>
                        <p>Dear Admin,</p>
						<p>New Gift Card is generated.</p>
                        <p><b>Coupon Details</b>.</p>
						<p><b>Coupon Code:</b> ".$coupan_code."</p>";
			$message .= '<img src="'.$image.'" alt="" style="display: block; width: 450px;" />';
                       $message .= "<p>--</p>
                        <p>Regards,<br />
                        <b>Hire Car Sydney City Team<b></p>
                        </div>
                        </div>
                        </div>";
			
			$this->email->message($message);
            $this->email->send();
		
		echo $image;
		
        die;
    }

	public function getComment($id = 0)
	{
		$data = array();
		if($id != 0) {
			$this->load->model('giftcard_coupon_model');
			if($card_details = $this->giftcard_coupon_model->details($id)) {
				$data['giftcard_id'] = $id;
				$data['comment'] = $card_details->comment;
				$this->load->view('admin/giftcard/giftcard-comment-form.php', $data);
			}
		}
	}
	
	public function saveComment()
	{
		$card_id = $this->input->post('cardId');
		$comment = $this->input->post('comment');
		
		if($card_id != 0)
		{
			$this->load->model('giftcard_coupon_model');
			$this->giftcard_coupon_model->edit(array('comment' => $comment), $card_id);
				
			echo 'Success';
			exit;
		}
		echo 'Fail';
		exit;
	}
	
	public function getCodeuses($coupan_id = 0)
	{
		
		$data = array();
		if($coupan_id != 0) {
			$this->load->model('giftcard_coupon_model');
				$uses_card_details = $this->giftcard_coupon_model->get_use_gift_card($coupan_id);
		
				$data['coupan_id'] = $coupan_id;
				$data['alldetail'] = $uses_card_details;

			$this->load->view('admin/giftcard/giftcard-code-uses.php', $data);	
		}
	}

      public function changestatusPrintedGift($id) 
    {

         $data = array('status' => $this->input->post('status'));
        $this->db->where('coupon_id',$id);
       $result = $this->db->update('giftcard_coupon',$data);
echo $this->db->last_query();


        if ($result) 

        {

            echo 'success';

        } 

        else 

        {

            echo 'error';

        }

        exit();
    }
}
