<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();

        // Check Admin login from function from (h: function)
        check_admin_login();

        // Load Company Model
        $this->load->model('company_model', 'model');
    }

    // View All Company
    public function view($page = 1) 
    {
        $company = $this->model->list_all(PER_PAGE_RECORD, ($page - 1) * PER_PAGE_RECORD);

        $this->load->helper('pagination_helper');
        set_pagination(base_url() . 'admin/company/view/', $this->model->count_total(), PER_PAGE_RECORD);
        
        $this->load->model('region_model');
        $data['city_list'] = $this->region_model->get_all_city();
        
        $data['company_details'] = $company;
        $data['sr'] = (($page - 1) * PER_PAGE_RECORD) + 1;

        $page_details['title'] = "View Company";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/company/company-index', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Add New Company
    public function add() 
    {
        if ($this->input->post()) 
        {
            $fields = array(
                "name" => trim($this->input->post('name')),
                "address" => trim($this->input->post('address')),
                "city_id" => trim($this->input->post('city_id')),
                "zipcode" => $this->input->post('zipcode'),
                "add_datetime" => date('Y-m-d H:i:s', time()),
                "update_datetime" => date('Y-m-d H:i:s', time())
            );

            $company_id = $this->model->add($fields);

            if ($company_id) 
            {
                $this->session->set_flashdata('SUCC_MESSAGE', 'New Company Added Successfully.');
                redirect('admin/company/view');
            } else 
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to create new Company.');
            }
        }

        $this->load->model('region_model');
        $data['city_list'] = $this->region_model->get_all_city();

        $page_details['title'] = "Add Company";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/company/company-add', $data);
        $this->load->view('admin/template/footer');
    }
    
    // View Comoany details
    public function details($id) 
    {
        $data['disable'] = "readonly disabled";
        $data['action'] = 'view';

        $data['company_details'] = $this->model->fetch_details($id);
        // Load City Details
        $this->load->model('region_model');
        $data['city_list'] = $this->region_model->get_all_city();

        $page_details['title'] = "View Company Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/company/company-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Edit Company details
    public function edit($id) 
    {
        if ($this->input->post() && $this->input->post()) 
        {
            $fields = array(
                "name" => trim($this->input->post('name')),
                "address" => trim($this->input->post('address')),
                "city_id" => trim($this->input->post('city_id')),
                "zipcode" => $this->input->post('zipcode'),
                "update_datetime" => date('Y-m-d H:i:s', time())
            );

            $company_id = $this->model->edit($fields, $id);

            if ($company_id > 0) 
            {
                $this->session->set_flashdata('SUCC_MESSAGE', 'Company Details Edited Successfully.');
                redirect('admin/company/view');
            }
            else 
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to Edit Company.');
            }
        }

        $data['disable'] = "";
        $data['action'] = 'edit';

        $data['company_details'] = $this->model->fetch_details($id);

        // Load City Details
        $this->load->model('region_model');
        $data['city_list'] = $this->region_model->get_all_city();

        $page_details['title'] = "Edit Company Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/company/company-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Delete Compnay by id
    public function delete($id) 
    {
        //$result = $this->model->delete($id);

        if ($result) 
        {
            $this->session->set_flashdata('SUCC_MESSAGE', 'Company Deleted Successfully.');
            redirect('admin/company/view');
        } 
        else 
        {
            $this->session->set_flashdata('ERR_MESSAGE', 'Error to Delete Company.');
        }

        redirect('admin/company/view');
    }
    
    public function changestatus($id) 
    {
        //echo $_POST['status'];exit;
        $data = array('status' => $this->input->post('status'));
        
        $result = $this->model->edit($data, $id);

        if ($result) 
        {
            echo 'success';
        } 
        else 
        {
            echo 'error';
        }
        exit();
    }
}
