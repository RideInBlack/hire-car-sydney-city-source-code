<?php 
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        // Check Admin login from function from (h: function)
        check_admin_login();
        
        // Load Rider Model
        $this->load->model('user_model', 'model');
    }
    
    // Edit User details
    public function index()
    {
        $id = $_SESSION['HIRE_CAR']['ADMIN']->admin_id;
        
        if($this->input->post() && isset($_POST['edit_profile']))
        {
            $unique_flag = 1;
            // Check unique email
            if($this->model->check_unique('email_id', trim($this->input->post('email')), 'admin_id != '.$id) == TRUE)
            {
                $unique_flag = 0;
                $_POST['ERR_EMAIL'] = 'Email Id already exist.';
            }
            if($this->model->check_unique('nick_name', trim($this->input->post('nick_name')), 'admin_id != '.$id) == TRUE)
            {
                $unique_flag = 0;
                $_POST['ERR_NICK_NAME'] = 'Nick Name already exist.';
            }
            
            if($unique_flag == 1)
            {
                $fields = array("first_name" => trim($this->input->post('fname')),
                    "last_name" => trim($this->input->post('lname')),
                    "nick_name" => trim($this->input->post('nick_name')),
                    "email_id" => trim($this->input->post('email')),
                    "update_datetime" => date('Y-m-d H:i:s', time())
                    );

                $user_id = $this->model->edit($fields, $id);

                if($user_id)
                {
                    $this->session->set_flashdata('SUCC_MESSAGE', 'Profile Details Saved Successfully.');
                    redirect('admin/account');
                }
                else
                {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error to Save Profile Details.');
                    redirect('admin/account');
                }
            }
        }
        
        if($this->input->post() && isset($_POST['change_password']))
        {
            $this->load->model('admin/admin_login', 'admin_login');
            $check_password = $this->admin_login->check_admin_password($this->input->post('oldpassword'), $id);
            
            if($check_password == TRUE)
            {
                $fields = array("password" => $this->input->post('newpassword'),
                    "password_hash" => md5($this->input->post('newpassword')),
                    "update_datetime" => date('Y-m-d H:i:s', time())
                    );

                $user_id = $this->model->edit($fields, $id);

                if($user_id)
                {
                    $this->session->set_flashdata('SUCC_MESSAGE', 'Password changed Successfully.');
                    redirect('admin/account');
                }
                else
                {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error to Change Password.');
                    redirect('admin/account');
                }
            }
            else
            {
                $_POST['ERR_OLDPASSWORD'] = "Please Enter Valid Old Password.";
            }
        }
        
        $data['disable'] = "";
        
        $data['user_details'] = $this->model->fetch_details($id);
        $data['action'] = 'edit';
        
        $page_details['title'] = "Edit Admin User Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/account/index', $data);
        $this->load->view('admin/template/footer');
    }
}