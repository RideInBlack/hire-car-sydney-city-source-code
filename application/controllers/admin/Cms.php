<?php 
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        // Check Admin login from function from (h: function)
        check_admin_login();
        
        // Load Rider Model
        $this->load->model('cms_model', 'model');
    }
    
    public function view($page=1) 
    {
        $cms = $this->model->list_all(PER_PAGE_RECORD, ($page - 1) * PER_PAGE_RECORD);
		//echo "<pre>";
		//print_r($rider);die;
        
        $this->load->helper('pagination_helper');
        set_pagination(base_url().'admin/cms/view/', $this->model->count_total(), PER_PAGE_RECORD);
        
        $data['cms_details'] = $cms;
        $data['sr'] = (($page - 1) * PER_PAGE_RECORD) + 1;
        
        $page_details['title'] = "CMS";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/cms/cms-index', $data);
        $this->load->view('admin/template/footer');
    }
    
    
    public function edit($id)
    {
        if($this->input->post())
        {
            $fields = array("content" => trim($this->input->post('editor1'))
                );
            
            $cms_id = $this->model->edit($fields, $id);
            
            
            $this->session->set_flashdata('SUCC_MESSAGE', 'CMS Details Edited Successfully.');
            redirect('admin/cms/edit/'.$id);
        }
        
        $data['disable'] = "";
        
        $data['cms_details'] = $this->model->fetch_details($id);
        $data['action'] = 'edit';
        
        $this->load->view('admin/template/header');
        $this->load->view('admin/cms/cms-edit', $data);
        $this->load->view('admin/template/footer');
    }
    
    public function delete($id)
    {
        //$result = $this->model->delete($id);
        if ($result) 
        {
            $this->session->set_flashdata('SUCC_MESSAGE', 'Driver Deleted Successfully.');
        } 
        else 
        {
            $this->session->set_flashdata('ERR_MESSAGE', 'Error to Delete Driver.');
        }

        redirect('admin/driver/view');
    }
    
    public function changestatus($id) 
    {
        //echo $_POST['status'];exit;
        $data = array('status' => $this->input->post('status'));
        
        $result = $this->model->edit($data, $id);

        if ($result) 
        {
            echo 'success';
        } 
        else 
        {
            echo 'error';
        }
        exit();
    }
}