<?php 
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Carcategory extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        // Check Admin login from function from (h: function)
        check_admin_login();
        
        // Load Rider Model
        $this->load->model('CarCategory_model', 'model');
    }
    
    // View All Car Category
    public function view($page = 1) 
    {
        $search_where = "";
	
        // Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;
        
        // Fetch details from model
        $car_category = $this->model->list_all($record_per_page, ($page-1)*$record_per_page, $search_where);
        
        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url().'admin/carcategory/view/', $this->model->count_total($search_where), $record_per_page);
        
        // Store details of rider
        $data['car_category_details'] = $car_category;
        $data['sr'] = (($page-1)*PER_PAGE_RECORD)+1;
        
         $page_details['title'] = "View Car Category";
        
        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/carcategory/carcategory-index', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Add new Car Category
    public function add() 
    {
        if($this->input->post())
        {
            //echo '<pre>';print_r($_POST); exit;
            $datetime = date('Y-m-d H:i:s', time());
            
            $fields = array(
                "name" => trim($this->input->post("category_name")),
                "add_datetime" => $datetime,
                "update_datetime" => $datetime,
                );
            
            $category_id = $this->model->add($fields);
            
            // Create Directory if not exist
            if (!is_dir('./assets/uploads/CarCategory/'.$category_id))
            {
                mkdir('./assets/uploads/CarCategory/'.$category_id, 0777, TRUE);
            }
            
            // Upload Default Car Image
            if($_FILES['default_image'] != "")
            {
                $image_name = time().'_image.png';
                
                $config['upload_path']  = './assets/uploads/CarCategory/'.$category_id.'/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite'] = false;
                $config['file_name'] = $image_name;
                
                $this->load->library('upload', $config);
                
                // Upload 
                if($this->upload->do_upload('default_image'))
                {
                    // Update car registration number file in database
                    $this->model->edit(array('car_image'=>$this->upload->data('file_name')), $category_id);
                }
            }
            // Upload Category Logo
            if($_FILES['category_logo'] != "")
            {
                $image_name = time().'_logo.png';
                
                $config['upload_path']  = './assets/uploads/CarCategory/'.$category_id.'/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite'] = false;
                $config['file_name'] = $image_name;
                
                $this->load->library('upload', $config);
                
                // Upload 
                if($this->upload->do_upload('category_logo'))
                {
                    // Update car registration number file in database
                    $this->model->edit(array('car_logo'=>$this->upload->data('file_name')), $category_id);
                }
            }
            
            if($category_id)
            {
                $this->session->set_flashdata('SUCC_MESSAGE', 'Car Category Added successfully;');
                redirect('admin/carcategory/view');
            }
            else
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to Add New Car Category.');
            }
        }
        
        $page_details['title'] = "Add Car Category";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/carcategory/carcategory-add');
        $this->load->view('admin/template/footer');
    }
    
    // view Car Category details
    public function details($id)
    {
        $data['car_category_details'] = $this->model->fetch_details($id);
        
        $data['disable'] = "readonly disabled";
        $data['action'] = 'details';
        
        $page_details['title'] = "View Car Cateogry Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/carcategory/carcategory-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Edit car Category details
    public function edit($id)
    {
        $car_category_details = $this->model->fetch_details($id);
        
        if($this->input->post())
        {
            $datetime = date('Y-m-d H:i:s', time());
            
            $category_id = $id;
            
            $fields = array(
                "name" => trim($this->input->post("category_name")),
                "update_datetime" => $datetime,
                );
            
            $affected_category_id = $this->model->edit($fields, $category_id);
            
            // Create Directory if not exist
            if (!is_dir('./assets/uploads/Car/'.$car_id))
            {
                mkdir('./assets/uploads/Car/'.$car_id, 0777, TRUE);
            }
            
            // Upload Default Car Image
            if($_FILES['default_image'] != "")
            {
                $image_name = time().'_image.png';
                
                $config['upload_path']  = './assets/uploads/CarCategory/'.$category_id.'/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite'] = false;
                $config['file_name'] = $image_name;
                
                $this->load->library('upload', $config);
                
                // Upload 
                if($this->upload->do_upload('default_image'))
                {
                    $file_path = './assets/uploads/CarCategory/'.$category_id.'/'.$car_category_details->car_image;
                    unlink($file_path);
                    
                    // Update car registration number file in database
                    $this->model->edit(array('car_image'=>$this->upload->data('file_name')), $category_id);
                }
            }
            // Upload Category Logo
            if($_FILES['category_logo'] != "")
            {
                $image_name = time().'_logo.png';
                
                $config['upload_path']  = './assets/uploads/CarCategory/'.$category_id.'/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite'] = false;
                $config['file_name'] = $image_name;
                
                $this->load->library('upload', $config);
                
                // Upload 
                if($this->upload->do_upload('category_logo'))
                {
                    $file_path = './assets/uploads/CarCategory/'.$category_id.'/'.$car_category_details->car_logo;
                    unlink($file_path);
                    
                    // Update car registration number file in database
                    $this->model->edit(array('car_logo'=>$this->upload->data('file_name')), $category_id);
                }
            }
            
            if($affected_category_id)
            {
                $this->session->set_flashdata('SUCC_MESSAGE', 'Car Category Details Saved Successfully.');
                redirect('admin/carcategory/view');
            }
            else
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to edit Car Category details.');
                redirect('admin/carcategory/edit/'.$id);
            }
        }
        
        $data['disable'] = "";
        $data['action'] = 'edit';
        $data['car_category_details'] = $car_category_details;
        
        $page_details['title'] = "Edit Car Category Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/carcategory/carcategory-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Delete Car Category
    public function delete($id)
    {
        //$result = $this->model->delete($id);
        
        //$this->load->helper("file"); // load the helper
        //$folder_path = "./assets/uploads/Car/".$id;
        //delete_files($folder_path, true); // delete all files/folders
        //rmdir($folder_path); // Delete empty Directory
        
        if ($result) 
        {
            $this->session->set_flashdata('SUCC_MESSAGE', 'Car Deleted Successfully.');
        } 
        else 
        {
            $this->session->set_flashdata('ERR_MESSAGE', 'Error to Delete Car.');
        }
        
        redirect('admin/car/view');
    }
    
    public function changestatus($id) 
    {
        //echo $_POST['status'];exit;
        $data = array('status' => $this->input->post('status'));
        
        $result = $this->model->edit($data, $id);

        if ($result) 
        {
            echo 'success';
        } 
        else 
        {
            echo 'error';
        }
        exit();
    }
    
    public function delete_image($car_id, $image_id, $image_name) 
    {
        $image_id = $this->model->delete_image($image_id);
        
        if($image_id > 0)
        {
            $file_path = './assets/uploads/Car/'.$car_id.'/'.$image_name;
            unlink($file_path);
            echo 'success';
        }
        else
        {
            echo 'error';
        }
    }
}