<?php

/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Ride extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();

        // Load Ride Model
        $this->load->model('ride_model', 'model');
    }
    
    public function search_ride($id = 0, $city_id = 0) 
    {
        if($id != 0 && $city_id != 0) {
            $data['rides'] = $this->model->search_ride($id, $city_id);
        }
        else {
            $data['rides'] = $this->model->search_ride();
        }
        
        //echo '<pre>'; print_r($data); echo '</pre>';
        if(count($data['rides']) > 0)
        {
            $this->load->view('web/ride/search', $data);
        }
        else
        {
            echo '';
        }
    }
    
    public function change_status($id, $status) 
    {
        $fields = array('ride_status' => $status);
        $this->model->edit_ride_details($fields, $id);
        
        redirect(base_url('admin/bookhistory/waiting'));
    }
    
    public function book_details()
    {
        if(!isset($_SESSION['RIDE']))
        {
            redirect(base_url());
        }
        //echo '<pre>'; print_r($_SESSION); echo '</pre>'; exit;
        
        $ride_id = $_SESSION['RIDE']['selected_ride'];
        
        // Get Car details
        if($_SESSION['RIDE']['selected_ride'] != 0) {
            $this->load->model('car_model');
            $data['car_details'] = $this->car_model->fetch_details($ride_id);
            $data['car_images']  = $this->car_model->fetch_car_images($ride_id);
        }
        
        // Get Setting Details
        $this->load->model('setting_model');
        $data['setting'] = $this->setting_model->fetch_details(1);
        
        $this->load->model('event_model');
        $data['event_details'] = $this->event_model->get_all_event();
        
        // Get Source and Destination
        $this->load->model('region_model');
        $data['source'] = $this->region_model->get_city_name($_SESSION['RIDE']['pickup_location']);
        $data['destination'] = $this->region_model->get_city_name($_SESSION['RIDE']['destination_location']);
        
        $this->load->view('web/template/header');
        $this->load->view('web/ride/book_detail', $data);
        $this->load->view('web/template/footer');
    }
    
    public function save_ride()
    {
        //echo '<pre>'; print_r($_SESSION); echo '</pre>';
        //echo '<pre>'; print_r($_POST); echo '</pre>'; exit;
        
        // Save Ride Details in Databases with details
        $trip_datetime = $this->input->post('pick-up-date').' '.$this->input->post('pick-up-time');
        $new_trip_datetime = DateTime::createFromFormat('m/d/Y h:i A', $trip_datetime)->format('Y-m-d H:i:s');
        
        if($this->input->post())
        {
            if(isset($_SESSION['HIRE_CAR']['RIDER']) && !empty($_SESSION['HIRE_CAR']['RIDER']->rider_id))
            {
                $fields = array(
                    'rider_id' => $_SESSION['HIRE_CAR']['RIDER']->rider_id,
                    'driver_id' => 0,
                    'car_category_id' => $_SESSION['RIDE']['selected_ride'],
                    'pickup_location' => $this->input->post('pick-address'),
                    'pickup_city' => $this->input->post('pick-city'),
                    'pickup_postal' => $this->input->post('pick-postal'),
                    'destination_location' => $this->input->post('drop-address'),
                    'destination_city' => $this->input->post('drop-city'),
                    'destination_postal' => $this->input->post('drop-postal'),
                    'pickup_datetime' => $new_trip_datetime,
                    'ride_request_datetime' => date('Y-m-d H:i:s', time()),
                    'ride_status' => '0',
                    'event_id' => $this->input->post('event'),
                    'estimated_distance' => (float)$this->input->post('estimated_distance'),
                    'estimated_fare' => (float)$this->input->post('estimated_fare'),
                    'actual_distance' => (float)$this->input->post('estimated_distance'),
                    'actual_fare' => (float)$this->input->post('estimated_fare'),
                    'special_requirement' => $this->input->post('special_requirement'),
                );
            }
            else
            {
                $fields = array(
                    'rider_id' => 0,
                    'driver_id' => 0,
                    'name' => trim($this->input->post('name')),
                    'email' => trim($this->input->post('email')),
                    'phone' => trim($this->input->post('phone')),
                    'car_category_id' => $_SESSION['RIDE']['selected_ride'],
                    'pickup_location' => trim($this->input->post('pick-address')),
                    'pickup_city' => trim($this->input->post('pick-city')),
                    'pickup_postal' => $this->input->post('pick-postal'),
                    'destination_location' => trim($this->input->post('drop-address')),
                    'destination_city' => trim($this->input->post('drop-city')),
                    'destination_postal' => trim($this->input->post('drop-postal')),
                    'pickup_datetime' => $new_trip_datetime,
                    'ride_request_datetime' => date('Y-m-d H:i:s', time()),
                    'ride_status' => '0',
                    'event_id' => trim($this->input->post('event')),
                    'estimated_distance' => (float)$this->input->post('estimated_distance'),
                    'estimated_fare' => (float)$this->input->post('estimated_fare'),
                    'actual_distance' => (float)$this->input->post('estimated_distance'),
                    'actual_fare' => (float)$this->input->post('estimated_fare'),
                    'special_requirement' => $this->input->post('special_requirement'),
                );
            }
            
            // Save Data
            $book_ride_id = $this->model->book_ride($fields);
            unset($_SESSION['RIDE']);
            
            // Email to admin for New Ride Booked.
            //------------------------------------------------------------------
            // Email to All Admin
            //------------------------------------------------------------------
            // Load email library for send email for booking ride
            $this->load->library('email');
            $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");

            $this->load->model('user_model');   // Load Admin Model
            $admin_email = $this->user_model->get_admin_email();

            foreach($admin_email AS $email) {
                $email_array[] = $email->email_id;
            }

            $this->email->to($email_array);
            $this->email->subject("Ride Book Details");

            $message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                        <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                        <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                        <div style='padding-left:10px;'>
                        <p>Dear Admin,</p>
                        <p>New Ride is Booked.</p>
                        <p><b>Name:</b> ".$this->input->post('name')."</p>
                        <p><b>Email:</b> ".$this->input->post('email')."</p>
                        <p><b>Phone:</b> ".$this->input->post('phone')."</p><hr>
                        <p><b>Ride Details</b></p>
                        <p><b>Pick Up Location:</b> ".trim($this->input->post('pick-address')).", ".trim($this->input->post('pick-city'))."</p>
                        <p><b>Pick Up Date time:</b> ".date('d/m/Y @ h:i A', strtotime($new_trip_datetime))."</p>
                        <p><b>Drop Off Location:</b> ".trim($this->input->post('drop-address')).", ".trim($this->input->post('drop-city'))."</p><hr>
                        </p>
                        <p>--</p>
                        <p>Regards,<br />
                        <b>Hire Car Sydney City Team<b></p>
                        </div>
                        </div>
                        </div>";

            $this->email->message($message);
            $this->email->send();
            //----------------------------------------------------------------------------------
            $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
            $this->email->to($this->input->post('email'));
            $this->email->subject("Ride Booked Successfully");
            
            $message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
                <a href='" . base_url() . "'>";
            
            $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                <div style='padding-left:10px;'>
                <p>Dear ".trim($this->input->post('name')).",</p>
                <p>Your ride is booked successfully.</p>
                <p><b>Pick Up Location:</b> ".trim($this->input->post('pick-address')).", ".trim($this->input->post('pick-city'))."</p>
                <p><b>Pick Up Date time:</b> ".date('d/m/Y @ h:i A', strtotime($new_trip_datetime))."</p>
                <p><b>Drop Off Location:</b> ".trim($this->input->post('drop-address')).", ".trim($this->input->post('drop-city'))."</p><hr>
                </p>
                <p>--</p>
                <p>Regards,<br />
                <b>Hire Car Sydney City Team<b></p>
                </div>
                </div>
                </div>";
            
            $this->email->message($message);
            $this->email->send();
            //------------------------------------------------------------------------------------------------------
        }
        //echo '<pre>'; print_r($fields); echo '</pre>';exit;
        
        if($this->input->post('book-n-pay'))
        {
            // Selected Car Id
            $car_id = $_SESSION['RIDE']['selected_ride'];
            $this->payment($car_id, $book_ride_id, $this->input->post('estimated_fare'));
        }
        else 
        {
            $this->session->set_flashdata('ride_book', '1');
            redirect('ride/ride_saved');
        }
    }
    
    public function ride_saved() 
    {
        if(!$this->session->flashdata('ride_book'))
        {
            redirect('pages/search');
        }
        
        $this->load->view('web/template/header');
        $this->load->view('web/ride/save_ride');
        $this->load->view('web/template/footer');
    }
    
    // Payment Page 
    public function payment($car_id ,$ride_id, $amount)
    {
        $data['ride_id'] = $ride_id;
        $data['amount'] = $amount;
        
        // Insetrt Before Payment 
        $this->load->model('payment_model');
        $fields = array('ride_id' => $ride_id, 'amount' => $amount, 'payment_datetime' => date('y-m-d H:i:s', time()));
        $payment_id = $this->payment_model->add($fields);
        
        $data['buisness_id'] = "jitendra.prajapati@ifuturz.com";
        $data['paypal_url'] = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        $data['payment_id'] = $payment_id;
        
        // Get Car details
        //$this->load->model('car_model');
        //$data['car_details'] = $this->car_model->fetch_details($car_id);
        
        //echo 'hi'; exit;
        $this->load->view('web/ride/payment', $data);
    }
    
    // Payment Result 
    public function payment_result($result)
    {
        $payment_id = $_GET['paymentId'];
        $ride_id = $_GET['rideId'];
        
        $this->load->model('payment_model');
        
        if($result == 1) {
            $this->session->set_flashdata('PAYMENT_SUCC', 'Payment is Successful.');
            
            // Get Ride Information
            $this->load->model('ride_model');
            $ride_details = $this->ride_model->get_ride_details($ride_id);
            //echo '<pre>';print_r($ride);exit;
            
            // Payment Model for change payment status
            $fields = array('status' => '1','payment_datetime' => date('y-m-d H:i:s', time()));
            $payment_id = $this->payment_model->edit($fields, $payment_id);
            
            // Ride Model for change payment status
            $this->load->model('ride_model');
            // Get Payment Status
            $payment_status  = $this->ride_model->get_payment_status($ride_id);
            
            if($payment_status == 0) {
                $payment_complete = $this->ride_model->edit_ride_details(array('payment_id' => '1'), $ride_id);
            }
            else if($payment_status == 1) {
                $payment_complete = $this->ride_model->edit_ride_details(array('payment_id' => '2'), $ride_id);
            }

            // Car Model for change payment status
            $this->load->model('car_model');
            $payment_complete = $this->car_model->edit(array('is_allocated' => '1'), $ride_id);
            
            
            if($payment_status == 0) {
                // Email to admin for New Ride Booked.
                //------------------------------------------------------------------
                // Email to All Admin
                //------------------------------------------------------------------
                // Load email library for send email for booking ride
                $this->load->library('email');
                $this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");

                $this->load->model('user_model');   // Load Admin Model
                $admin_email = $this->user_model->get_admin_email();

                foreach($admin_email AS $email) {
                    $email_array[] = $email->email_id;
                }

                $this->email->to($email_array);
                $this->email->subject("Ride Book Details");

                $message = "<div style='float:left;min-height:auto;width:100%'>
                    <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
                    <a href='" . base_url() . "'>";

                $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
                $message .= "</a></div>
                            <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                            <div style='padding-left:10px;'>
                            <p>Dear Admin,</p>
                            <p>New ride is booked.</p><hr>
                            <p><b>Booking Details:</b></p>
                            <p>Rider Name: ".$ride_details->rider_name." [".$ride_details->rider_email."]</p>
                            <p>Ride Date-Time: ".date("d/m/Y h:i:s A", strtotime($ride_details->pickup_datetime))."</p>
                            <p>Pick-up City: ".$ride_details->start."</p>
                            <p>Destination City: ".$ride_details->end."</p>

                            </p>
                            <p>--</p>
                            <p>Regards,<br />
                            <b>Hire Car Sydney City Team<b></p>
                            </div>
                            </div>
                            </div>";

                $this->email->message($message);
                $this->email->send();
                //------------------------------------------------------------------
                // Email to Rider
                //------------------------------------------------------------------
                $this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
                $this->email->to($ride_details->rider_email);
                $this->email->subject("Ride Booked Successfully");

                $message = "<div style='float:left;min-height:auto;width:100%'>
                    <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
                    <a href='" . base_url() . "'>";

                $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
                $message .= "</a></div>
                            <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                            <div style='padding-left:10px;'>
                            <p>Dear ".$ride_details->rider_name.",</p>
                            <p>New ride is booked.</p><hr>
                            <p><b>Booking Details:</b></p>
                            <p>Ride Date-Time: ".date("d/m/Y h:i:s A", strtotime($ride_details->pickup_datetime))."</p>
                            <p>Pick-up City: ".$ride_details->start."</p>
                            <p>Destination City: ".$ride_details->end."</p>

                            </p>
                            <p>--</p>
                            <p>Regards,<br />
                            <b>Hire Car Sydney City Team<b></p>
                            </div>
                            </div>
                            </div>";

                $this->email->message($message);
                $this->email->send();
                //echo $this->email->print_debugger();
                //------------------------------------------------------------------
            }
        }
        else {
            //echo 'Failed<br/>';
            $this->session->set_flashdata('PAYMENT_ERR', 'Payment is Failed.');
            $fields = array('status' => '0','payment_datetime' => date('y-m-d H:i:s', time()));
            
            $payment_id = $this->payment_model->edit($fields, $payment_id);
        }
        
        unset($_SESSION['RIDE']);
        //exit;
        redirect(base_url('riderprofile'));
    }
    
}