<?php 
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Car extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        // Check Admin login from function from (h: function)
        check_admin_login();
        
        // Load Rider Model
        $this->load->model('car_model', 'model');
    }
    
    // View All City List
    public function view($page = 1) 
    {
        $search_where = "";
	
        // Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;
        
        // Fetch details from model
        $rider = $this->model->list_all($record_per_page, ($page-1)*$record_per_page, $search_where);
        
        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url().'admin/car/view/', $this->model->count_total($search_where), $record_per_page);
        
        // Store details of rider
        $data['car_details'] = $rider;
        $data['sr'] = (($page-1)*PER_PAGE_RECORD)+1;
        
        $page_details['title'] = "View City";
        $data['category'] = $this->model->fetch_all_category();
	
        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/car/car-index', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Add new Car
    public function add() 
    {
        if($this->input->post())
        {
            //print_r($_POST);exit;
            
            $datetime = date("Y-m-d, H:i:s", time());
            
            $fields = array("category_id" => $this->input->post('category'),
                "city_id" => $this->input->post('city'),
                "model_name" => trim($this->input->post("model_name")),
                "is_available" => $this->input->post('is_available'),
                "number" => trim($this->input->post("car_number")),
                "base_fare" => trim($this->input->post("base_fare")),
                "min_fare" => trim($this->input->post("min_fare")),
                "per_km_rate" => trim($this->input->post("per_km_fare")),
                "per_minute_rate" => trim($this->input->post("per_min_fare")),
                "driver_rent" => trim($this->input->post("driver_rent")),
                "add_datetime" => $datetime,
                "update_datetime" => $datetime,
                );
            
            $car_id = $this->model->add($fields);
            
            // Create Directory if not exist
            if (!is_dir('./assets/uploads/Car/'.$car_id))
            {
                mkdir('./assets/uploads/Car/'.$car_id, 0777, TRUE);
            }
            
            // for Vehicle image upload
            $count = count($_FILES['image']['name']);
            for($i=0; $i<$count; $i++) 
            {
                $image_name = "";
                $image_data = array();
                
                if($_FILES['image']['name'][$i] != "")
                {
                    $_FILES['uploadimage']['name'] = $_FILES['image']['name'][$i];
                    $_FILES['uploadimage']['type'] = $_FILES['image']['type'][$i];
                    $_FILES['uploadimage']['tmp_name'] = $_FILES['image']['tmp_name'][$i];
                    $_FILES['uploadimage']['error'] = $_FILES['image']['error'][$i];
                    $_FILES['uploadimage']['size'] = $_FILES['image']['size'][$i];
                    
                    $image_name = time().'.png';

                    $config['upload_path']          = './assets/uploads/Car/'.$car_id.'/';
                    $config['allowed_types']        = 'gif|jpg|png';
                    $config['overwrite']            = false;
                    $config['file_name']            = $image_name;
                    
                    $this->load->library('upload', $config);

                    // Upload 
                    if($this->upload->do_upload('uploadimage'))
                    {
                        $image_data = array('car_id'=>$car_id, 'image_name' => $this->upload->data('file_name'), 'add_datetime' => $datetime);
                        // Update car registration number file in database
                        $this->model->add_car_image($image_data);
                    }
                }
            }
            
            // for vehicle registration file upload
            if($_FILES['image']['tmp_name'] != "")
            {
                $registration_file_name = time().'.png';
                
                $config['upload_path']          = './assets/uploads/Car/'.$car_id.'/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['file_name']            = $registration_file_name;
                
                $this->load->library('upload', $config);
                
                // Upload 
                if($this->upload->do_upload('vehicle_registration'))
                {
                    // Update car registration number file in database
                    $this->model->edit(array('registration_filename'=>$registration_file_name), $car_id);
                }
            }
            
            if($car_id)
            {
                $this->session->set_flashdata('SUCC_MESSAGE', 'Car Added successfully;');
                redirect('admin/car/view');
            }
            else
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to Add New Car.');
            }
        }
        
        $page_details['title'] = "Add Car";
        $data['category'] = $this->model->fetch_all_category();
        
        // For fetch All City
        $this->load->model('region_model');
        $data['city'] = $this->region_model->get_all_city();
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/car/car-add', $data);
        $this->load->view('admin/template/footer');
    }
    
    // view Car details
    public function details($id)
    {
        $data['car_details'] = $this->model->fetch_details($id);
        $data['car_images']  = $this->model->fetch_car_images($id);
        
        // for fetch all category
        $data['category'] = $this->model->fetch_all_category();
        
        // For fetch All City
        $this->load->model('region_model');
        $data['city'] = $this->region_model->get_all_city();
        
        $data['disable'] = "readonly disabled";
        $data['action'] = 'details';
        
        $page_details['title'] = "Edit Car Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/car/car-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Edit car details
    public function edit($id)
    {
        if($this->input->post())
        {
            $car_id = $id;
            $datetime = date('Y-m-d H:i:s', time());
            
            //echo '<pre>';print_r($_POST);exit;
            $fields = array("category_id" => $this->input->post('category'),
                "city_id" => $this->input->post('city'),
                "model_name" => trim($this->input->post("model_name")),
                "is_available" => $this->input->post('is_available'),
                "number" => trim($this->input->post("car_number")),
                "base_fare" => trim($this->input->post("base_fare")),
                "min_fare" => trim($this->input->post("min_fare")),
                "per_km_rate" => trim($this->input->post("per_km_fare")),
                "per_minute_rate" => trim($this->input->post("per_min_fare")),
                "driver_rent" => trim($this->input->post("driver_rent")),
				"update_datetime" => $datetime,
                );

            $affected_car_id = $this->model->edit($fields, $id);
            
            // Create Directory if not exist
            if (!is_dir('./assets/uploads/Car/'.$car_id))
            {
                mkdir('./assets/uploads/Car/'.$car_id, 0777, TRUE);
            }
            
            // for Vehicle image upload
            $count = count($_FILES['image']['name']);
            for($i=0; $i<$count; $i++) 
            {
                $image_name = "";
                $image_data = array();
                
                if($_FILES['image']['name'][$i] != "")
                {
                    $_FILES['uploadimage']['name'] = $_FILES['image']['name'][$i];
                    $_FILES['uploadimage']['type'] = $_FILES['image']['type'][$i];
                    $_FILES['uploadimage']['tmp_name'] = $_FILES['image']['tmp_name'][$i];
                    $_FILES['uploadimage']['error'] = $_FILES['image']['error'][$i];
                    $_FILES['uploadimage']['size'] = $_FILES['image']['size'][$i];
                    
                    $image_name = time().'.png';

                    $config['upload_path']          = './assets/uploads/Car/'.$car_id.'/';
                    $config['allowed_types']        = 'gif|jpg|png';
                    $config['overwrite']            = false;
                    $config['file_name']            = $image_name;
                    
                    $this->load->library('upload', $config);

                    // Upload 
                    if($this->upload->do_upload('uploadimage'))
                    {
                        $image_data = array('car_id'=>$car_id, 'image_name' => $this->upload->data('file_name'), 'add_datetime' => $datetime);
                        // Update car registration number file in database
                        $this->model->add_car_image($image_data);
                    }
                }
            }
            
            if($affected_car_id)
            {
                $this->session->set_flashdata('SUCC_MESSAGE', 'Car Details Saved Successfully.');
                redirect('admin/car/view');
            }
            else
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to edit Car details.');
                redirect('admin/car/edit/'.$id);
            }
        }
        
        $data['car_details'] = $this->model->fetch_details($id);
        $data['car_images']  = $this->model->fetch_car_images($id);
        
        // For fetch All Category
        $data['category'] = $this->model->fetch_all_category();
        
        // For fetch All City
        $this->load->model('region_model');
        $data['city'] = $this->region_model->get_all_city();
        
        $data['disable'] = "";
        $data['action'] = 'edit';
        
        $page_details['title'] = "Edit Car Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/car/car-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Delete CAr
    public function delete($id)
    {
        $result = $this->model->delete($id);
        
        $this->load->helper("file"); // load the helper
        $folder_path = "./assets/uploads/Car/".$id;
        delete_files($folder_path, true); // delete all files/folders
        rmdir($folder_path); // Delete empty Directory
        
        if ($result) 
        {
            $this->session->set_flashdata('SUCC_MESSAGE', 'Car Deleted Successfully.');
        } 
        else 
        {
            $this->session->set_flashdata('ERR_MESSAGE', 'Error to Delete Car.');
        }
        
        redirect('admin/car/view');
    }
    
    public function changestatus($id) 
    {
        //echo $_POST['status'];exit;
        $data = array('status' => $this->input->post('status'));
        
        $result = $this->model->edit($data, $id);

        if ($result) 
        {
            echo 'success';
        } 
        else 
        {
            echo 'error';
        }
        exit();
    }
    
    public function delete_image($car_id, $image_id, $image_name) 
    {
        $image_id = $this->model->delete_image($image_id);
        
        if($image_id > 0)
        {
            $file_path = './assets/uploads/Car/'.$car_id.'/'.$image_name;
            unlink($file_path);
            echo 'success';
        }
        else
        {
            echo 'error';
        }
    }
}