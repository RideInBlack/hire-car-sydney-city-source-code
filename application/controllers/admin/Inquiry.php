<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Inquiry extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Inquiry_model', 'model');
    }
    
    public function view($page = 1)
    {
        $record_per_page = PER_PAGE_RECORD;
        // Fetch details from model
        $inquiryDetails = $this->model->list_all($record_per_page, ($page-1)*$record_per_page);
        
        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url().'admin/bookhistory/waiting/', $this->model->count_total(), $record_per_page);
        
        // Store details of rider
        $data['inquirydetails'] = $inquiryDetails;
        $data['sr'] = (($page-1)*PER_PAGE_RECORD)+1;
        
        $page_details['title'] = "View Inquiries";
        
        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/inquiry/inquiry-index', $data);
        $this->load->view('admin/template/footer');
    }
   
    public function details($id)
    {
       if(!empty($id)) {
            $inquiryDetails = $this->model->fetch_details($id);
            $data['inquiry'] = $inquiryDetails;
            
            $this->load->view('admin/inquiry/inquiry-detail', $data);
       }
    }
}
