<?php

/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookhistory extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // Check Admin login from function from (h: function)
        check_admin_login();

        // Load Rider Model
        $this->load->model('ride_model', 'model');
    }

    // View All waiting Rides List
    public function waiting($page = 1) {
        $search_where = "";
        // Get nSearch Element and prepare query
        if (isset($_GET['ridername']) && !empty($_GET['ridername'])) {
            $search_where .= " (rider.first_name LIKE '%" . $_GET['ridername'] . "%' OR rider.last_name LIKE '%" . $_GET['ridername'] . "%')";
        }
        if (isset($_GET['rideremail']) && !empty($_GET['rideremail'])) {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "rider.email_id LIKE '%" . $_GET['rideremail'] . "%'";
        }
        if (isset($_GET['ridestatus']) && $_GET['ridestatus'] != '') {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "ride_details.ride_status = '" . $_GET['ridestatus'] . "'";
        } else {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "(ride_status = '0' OR ride_status = '1' OR ride_status = '2')";
        }
        $this->load->model('driver_model');
        //Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;
        // Fetch details from model
        $rides = $this->model->rides_list($record_per_page, ($page - 1) * $record_per_page, $search_where);

        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url() . 'admin/bookhistory/waiting/', $this->model->count_total($search_where), $record_per_page);

        // Store details of rider
        $data['waitingridedetails'] = $rides;
        $data['driverdetails'] = $this->driver_model->fetch_alldrivers();
        $data['sr'] = (($page - 1) * PER_PAGE_RECORD) + 1;

        $page_details['title'] = "View Waiting Rides";

        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/bookhistory/bookhistory-waiting', $data);
        $this->load->view('admin/template/footer');
    }

    public function running($page = 1) {
        $search_where = "";
        // Get nSearch Element and prepare query
        if (isset($_GET['ridername']) && !empty($_GET['ridername'])) {
            $search_where .= " (rider.first_name LIKE '%" . $_GET['ridername'] . "%' OR rider.last_name LIKE '%" . $_GET['ridername'] . "%')";
        }
        if (isset($_GET['rideremail']) && !empty($_GET['rideremail'])) {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "rider.email_id LIKE '%" . $_GET['rideremail'] . "%'";
        }
        if (isset($_GET['drivername']) && !empty($_GET['drivername'])) {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= " (driver.first_name LIKE '%" . $_GET['drivername'] . "%' OR driver.last_name LIKE '%" . $_GET['drivername'] . "%')";
        }
        if (isset($_GET['driveremail']) && !empty($_GET['driveremail'])) {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "driver.email_id LIKE '%" . $_GET['driveremail'] . "%'";
        }
        if (isset($_GET['ridestatus']) && $_GET['ridestatus'] != '') {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "ride_details.ride_status = '" . $_GET['ridestatus'] . "'";
        } else {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "(ride_status = '3')";
        }
        $this->load->model('driver_model');
        //Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;
        // Fetch details from model
        $rides = $this->model->runningRidesList($record_per_page, ($page - 1) * $record_per_page, $search_where);

        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url() . 'admin/bookhistory/waiting/', $this->model->count_total($search_where, 1), $record_per_page);

        // Store details of rider
        $data['runningridedetails'] = $rides;
        $data['sr'] = (($page - 1) * PER_PAGE_RECORD) + 1;

        $page_details['title'] = "View Running Rides";

        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/bookhistory/bookhistory-running', $data);
        $this->load->view('admin/template/footer');
    }

    public function completed($page = 1) {
        $search_where = "";
        // Get nSearch Element and prepare query
        if (isset($_GET['ridername']) && !empty($_GET['ridername'])) {
            $search_where .= " (rider.first_name LIKE '%" . $_GET['ridername'] . "%' OR rider.last_name LIKE '%" . $_GET['ridername'] . "%')";
        }
        if (isset($_GET['rideremail']) && !empty($_GET['rideremail'])) {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "rider.email_id LIKE '%" . $_GET['rideremail'] . "%'";
        }
        if (isset($_GET['drivername']) && !empty($_GET['drivername'])) {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= " (driver.first_name LIKE '%" . $_GET['drivername'] . "%' OR driver.last_name LIKE '%" . $_GET['drivername'] . "%')";
        }
        if (isset($_GET['driveremail']) && !empty($_GET['driveremail'])) {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "driver.email_id LIKE '%" . $_GET['driveremail'] . "%'";
        }
        if (isset($_GET['ridestatus']) && $_GET['ridestatus'] != '') {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "ride_details.ride_status = '" . $_GET['ridestatus'] . "'";
        } else {
            if ($search_where != '') {
                $search_where .= " AND ";
            }
            $search_where .= "(ride_status = '4' OR ride_status = '5')";
        }

        $this->load->model('driver_model');
        //Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;
        $statuscond = "(ride_status = '4' OR ride_status = '5')";
        // Fetch details from model
        $rides = $this->model->runningRidesList($record_per_page, ($page - 1) * $record_per_page, $search_where, $statuscond);

        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url() . 'admin/bookhistory/waiting/', $this->model->count_total($search_where, $statuscond, 1), $record_per_page);

        // Store details of rider
        $data['completedridedetails'] = $rides;
        $data['sr'] = (($page - 1) * PER_PAGE_RECORD) + 1;

        $page_details['title'] = "View Completed Rides";

        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/bookhistory/bookhistory-completed', $data);
        $this->load->view('admin/template/footer');
    }

    public function assign() {
        if ($this->input->post()) {

            $rideId = $this->input->post('rideid');
            if (!empty($_POST['driver']) && $rideId != '') {

                $this->load->model('driver_model');
                $this->load->model('ride_model');
                $rideDetails = $this->ride_model->fetch_ride($rideId);

                for ($i = 0; $i < count($_POST['driver']); $i++) {

                    $fields = array("ride_id" => trim($rideId),
                        "assign_driver_id" => trim($_POST['driver'][$i]),
                        "assign_datetime" => date('Y-m-d H:i:s', time())
                    );

                    $rider_id = $this->ride_model->assignride($fields);
                    $driverdetails = $this->driver_model->fetch_details($_POST['driver'][$i]);
                    //Email configuration
                    $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://smtp.googlemail.com',
                        'smtp_port' => 465,
                        'smtp_user' => 'hirecar2015@gmail.com',
                        'smtp_pass' => 'hirecar@anju',
                        'mailtype' => 'html',
                        'wordwrap' => TRUE,
                        'charset' => 'iso-8859-1',
                        'newline' => "\r\n"
                    );

                    $message = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
<a href='" . base_url() . "'>";
                    $message .= '<img src="http://192.168.1.102/hirecar/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
                    $message .= "</a></div>
                    <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                    <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                    <div style='padding-left:10px;'>
                    <p>Dear " . $driverdetails->first_name . " " . $driverdetails->last_name . ",</p>
                    <p>Your are assign for a ride. Ride details are as below.</p>
                    <p><b>Pickup:</b> " . $rideDetails->pickup_location . " <br>
                    <b>Destination:</b> " . $rideDetails->destination_location . " <br>
                    <b>Pick Date and Time :</b> " . $rideDetails->pickup_datetime . " <br><br>

                    if you are available then you can accept this request from your Hire Car Sydney City account. Please login into your account and accept the request from your dashboard. 
                    </p>
                    <p>Regards,<br />
                    Hire Car Sydney City Team</p>
                    </div>
                    </div>
                    </div>";
                    $this->load->library('email', $config);
                    $this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
                    $this->email->to($driverdetails->email_id);
                    //$this->email->cc("testcc@domainname.com");
                    $this->email->subject("Request for New Ride");
                    $this->email->message($message);
                    $this->email->send();
                }

                $fields = array("ride_status" => '1',
                    "driver_assign_datetime" => date('Y-m-d H:i:s', time())
                );
                $rideDetails = $this->ride_model->updateRideStatus($fields, $rideId);
                $this->session->set_flashdata('SUCC_MESSAGE', "Drivers are assign for Ride Successfully");
            } else {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to Assign Drivers for Ride.');
            }
        }
        redirect('admin/bookhistory/waiting');
    }

    // View rider details
    public function details($id) {
        $data['disable'] = "readonly disabled";
        $this->load->model('ride_model');
        $ride_details = $this->ride_model->fetch_ride_details($id);
        $data['ride_details'] = $ride_details;

        $this->load->view('admin/bookhistory/booking-details', $data);
    }

    // Edit rider details
    public function edit($id) {
        if ($this->input->post() && $this->input->post()) {
            $fields = array("first_name" => trim($this->input->post('fname')),
                "last_name" => trim($this->input->post('lname')),
                "email_id" => trim($this->input->post('email')),
                "mobile_number" => trim($this->input->post('mobile')),
                "update_datetime" => date('Y-m-d H:i:s', time())
            );

            $rider_id = $this->model->edit($fields, $id);

            if ($rider_id) {
                $this->session->set_flashdata('SUCC_MESSAGE', 'Rider Details Saved Successfully.');
                redirect('admin/rider/view');
            } else {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to edit Rider details.');
            }
        }

        $data['disable'] = "";

        $data['rider_details'] = $this->model->fetch_details($id);
        $data['action'] = 'edit';

        $page_details['title'] = "Edit Rider Details";

        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/rider/rider-details', $data);
        $this->load->view('admin/template/footer');
    }

    // Delete Ridder
    public function delete($id) {
        //$result = $this->model->delete($id);

        if ($result) {
            $this->session->set_flashdata('SUCC_MESSAGE', 'Rider Deleted Successfully.');
        } else {
            $this->session->set_flashdata('ERR_MESSAGE', 'Error to Delete Rider.');
        }

        redirect('admin/rider/view');
    }

    public function change_status($id, $status) {
        $fields = array('ride_status' => $status);
        $this->model->edit_ride_details($fields, $id);

        redirect(base_url('admin/bookhistory/waiting'));
    }

    public function changestatus($id) {
        //echo $_POST['status'];exit;
        $data = array('status' => $this->input->post('status'));

        $result = $this->model->edit($data, $id);

        if ($result) {
            echo 'success';
        } else {
            echo 'error';
        }
        exit();
    }

    public function paymentdetails($id) {
        $data['disable'] = "readonly disabled";

        $this->load->model('ride_model');
        $ride_details = $this->ride_model->fetch_ride_details($id);
        $data['ride_details'] = $ride_details;

        $payment_details = $this->ride_model->fetch_payment_rows($id);
        //echo '<pre>'; print_r($payment_details);echo '</pre>';

        $paid_amount = $this->ride_model->fetch_paymentdetails($id);
        if (count($paid_amount) > 0) {
            $data['paid_amount'] = $paid_amount->amount;
        } else {
            $data['paid_amount'] = 0;
        }


        $data['payment_details'] = $payment_details;
        $this->load->view('admin/bookhistory/booking-view-details', $data);
    }

    // for manual payment received
    public function manual_payment($rideid, $amount) {
        // Insetrt Before Payment 
        if ($amount > 0 && $rideid > 0) {

            $this->load->model('payment_model');
            $fields = array('ride_id' => $rideid, 'amount' => $amount, 'status' => '1', 'payment_type' => 'Manual', 'payment_datetime' => date('y-m-d H:i:s', time()));
            $payment_id = $this->payment_model->add($fields);

            $this->load->model('ride_model');
            $this->ride_model->edit_ride_details(array('payment_id' => 1), $rideid);

            // Send invoice in email
            $this->invoice_mail($rideid);
        }
        redirect(base_url('admin/bookhistory/waiting'));
    }
	
	// for manual payment received
    public function bankDeposit($rideid, $amount) {
        // Insetrt Before Payment 
        if ($amount > 0 && $rideid > 0) {

            $this->load->model('payment_model');
            $fields = array('ride_id' => $rideid, 'amount' => $amount, 'status' => '1', 'payment_type' => 'Deposit', 'payment_datetime' => date('y-m-d H:i:s', time()));
            $payment_id = $this->payment_model->add($fields);

            $this->load->model('ride_model');
            $this->ride_model->edit_ride_details(array('payment_id' => 1), $rideid);

            // Send invoice in email
            $this->invoice_mail($rideid);
        }
        redirect(base_url('admin/bookhistory/waiting'));
    }

    public function invoice_mail($rideid) {
        // Get Ride Details
        $this->load->model('ride_model');
        $ride_details = $this->ride_model->fetch_ride_details($rideid);
        $data['ride_details'] = $ride_details;
        
        // Ride Payment Details
        $this->load->model('ride_model');
        $data['payment_details'] = $this->ride_model->fetch_payment_rows($rideid);
        
        // Get Rider Details
        if ($ride_details->rider_id > 0 && $ride_details->rider_id != "") {
            // Get Rider Details
            $this->load->model('rider_model');
            $rider_details = $this->rider_model->fetch_details($ride_details->rider_id);

            $rider['name'] = $rider_details->first_name . ' ' . $rider_details->last_name;
            $rider['email'] = $rider_details->email_id;
            $rider['phone'] = $rider_details->mobile_number;
            //print_r($rider_details);exit;
        } else {
            $rider['name'] = $ride_details->name;
            $rider['email'] = $ride_details->email;
            $rider['phone'] = $ride_details->phone;
        }
		
		

        // Get All Admin email for email sending
        /*$this->load->model('user_model');   // Load Admin Model
        $admin_email = $this->user_model->get_admin_email();

        foreach ($admin_email AS $email) {
            $admin_emails[] = $email->email_id;
        }*/

        //exit;
        $data['rideid'] = $rideid;
		$data['rider'] = $rider;

		// load setting details (Bank and ABN)
		$this->load->model('setting_model');
		$data['setting_details'] = $this->setting_model->fetch_details(1);

        //$email = "virag.shah@ifuturz.com";
        $mailtemplate = $this->load->view('web/ride/invoiceMailtempalte', $data, TRUE);

        //------------------------------------------------------
        // Send Email to all admin 
        //------------------------------------------------------
        $this->load->library('email');
        /*$this->email->from('admin@hirecarsydneycity.com', "Hire Car Sydney City - Team");
        $this->email->to($admin_emails);
        $this->email->subject("[Hire Car Sydney City] Invoice ");

        $this->email->message($mailtemplate);
        $this->email->send();*/
        //------------------------------------------------------
        //------------------------------------------------------
        // Send Email to all Rider 
        //------------------------------------------------------
        //$this->load->library('email');
        $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
        $this->email->to($rider['email']);
        $this->email->subject("[Hire Car Sydney City] Invoice ");

        $this->email->message($mailtemplate);
        $this->email->send();
        //------------------------------------------------------
    }

}
