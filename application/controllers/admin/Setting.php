<?php

/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // Check Admin login from function from (h: function)
        check_admin_login();

        // Load Rider Model
        $this->load->model('setting_model', 'model');
    }

    public function index() {
        if ($this->input->post()) {
            $fields = array(
                "setting_paypal_email_id" => trim($this->input->post('setting_paypal_email_id')),
                //"setting_paypal_api_username" => trim($this->input->post('setting_paypal_api_username')),
                //"setting_paypal_password" => trim($this->input->post('setting_paypal_password')),
                //"setting_paypal_signature" => trim($this->input->post('setting_paypal_signature')),
                "setting_paypal_mode" => trim($this->input->post('setting_paypal_mode')),
                "setting_paypal_email_id_live" => trim($this->input->post('setting_paypal_email_id_live')),
                //"setting_paypal_api_username_live" => trim($this->input->post('setting_paypal_api_username_live')),
                //"setting_paypal_password_live" => trim($this->input->post('setting_paypal_password_live')),
                //"setting_paypal_signature_live" => trim($this->input->post('setting_paypal_signature_live')),
                
				"setting_eway_mode" => trim($this->input->post('setting_paypal_mode')),
				"setting_eway_api_key" => trim($this->input->post('setting_eway_api_key')),
				"setting_eway_password" => trim($this->input->post('setting_eway_password')),
				"setting_eway_api_key_live" => trim($this->input->post('setting_eway_api_key_live')),
				"setting_eway_password_live" => trim($this->input->post('setting_eway_password_live')),
				
				"setting_bank" => trim($this->input->post('bank')),
				"setting_account_name" => trim($this->input->post('account_name')),
				"setting_bsb" => trim($this->input->post('bsb')),
				"setting_account_number" => trim($this->input->post('account_number')),
				
				"setting_abn" => trim($this->input->post('abn')),
				//"min_fare" => trim($this->input->post('min_fare')),
                //"base_fare" => trim($this->input->post('base_fare')),
                //"per_minute_rate" => trim($this->input->post('per_min_fare')),
                //"per_km_rate" => trim($this->input->post('per_km_fare')),
                "update_datetime" => date('Y-m-d H:i:s', time()),
                //"driver_rent" => trim($this->input->post('driver_rent')),
            );
			
            $setting_id = $this->model->index($fields, 1);

            if ($setting_id) {
				/*$fields2 = array(
					"min_fare" => trim($this->input->post('min_fare')),
					"base_fare" => trim($this->input->post('base_fare')),
					"per_minute_rate" => trim($this->input->post('per_min_fare')),
					"per_km_rate" => trim($this->input->post('per_km_fare')),
            	);*/
				
                //$this->model->updateFare($fields2);
				
                $this->session->set_flashdata('SUCC_MESSAGE', 'Details Edited Successfully.');
                redirect('admin/setting');
            } else {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error Setting Details.');
            }
        }

        $data['disable'] = "";

        $data['setting_details'] = $this->model->fetch_details(1);
		
		//echo '<pre>';print_r($data['setting_details']); exit;

        $data['action'] = 'index';

        $this->load->view('admin/template/header');
        $this->load->view('admin/setting/setting-index', $data);
        $this->load->view('admin/template/footer');
    }

    /*public function edit($id) {
        if ($this->input->post()) {
            $fields = array("content" => trim($this->input->post('editor1'))
            );

            $setting_id = $this->model->index($fields, $id);

            if ($setting_id) {
                $this->session->set_flashdata('SUCC_MESSAGE', 'Details Edited Successfully.');
                redirect('admin/setting');
            } else {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error Setting Details.');
            }
        }

        $data['disable'] = "";

        $data['setting_details'] = $this->model->fetch_details($id);
        $data['action'] = 'edit';

        $this->load->view('admin/template/header');
        $this->load->view('admin/setting', $data);
        $this->load->view('admin/template/footer');
    }*/

}
