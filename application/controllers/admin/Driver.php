<?php 
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        // Check Admin login from function from (h: function)
        check_admin_login();
        
        // Load Rider Model
        $this->load->model('driver_model', 'model');
    }
    
    public function view($page=1) 
    {
        $search_where="";
		
		if(isset($_GET['search_firstname']) && !empty($_GET['search_firstname']))
        {
            $search_where = "first_name LIKE '%".$_GET['search_firstname']."%'";
        }
		
		if(isset($_GET['search_email']) && !empty($_GET['search_email']))
        {
            $search_where = "email_id LIKE '%".$_GET['search_email']."%'";
        }
		
		
        $rider = $this->model->list_all(PER_PAGE_RECORD, ($page - 1) * PER_PAGE_RECORD, $search_where);
        
        $this->load->helper('pagination_helper');
        set_pagination(base_url().'admin/driver/view/', $this->model->count_total($search_where), PER_PAGE_RECORD);
        
        $data['driver_details'] = $rider;
        $data['sr'] = (($page - 1) * PER_PAGE_RECORD) + 1;
        
        $page_details['title'] = "Driver Company";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/driver/driver-index', $data);
        $this->load->view('admin/template/footer');
    }
    
    public function add() 
    {
        if($this->input->post())
        {
            $unique_flag = 1;
            // Check unique email
            if($this->model->check_unique('email_id', trim($this->input->post('email'))) == TRUE)
            {
                $unique_flag = 0;
                $_POST['ERR_EMAIL'] = 'Email Id already exist.';
            }
            if($this->model->check_unique('email_id', trim($this->input->post('email'))) == TRUE)
            {
                $unique_flag = 0;
                $_POST['ERR_USERNAME'] = 'Username already exist.';
            }
            
            if($unique_flag == 1)
            {
                $fields = array("first_name" => trim($this->input->post('fname')),
                    "last_name" => trim($this->input->post('lname')),
                    "email_id" => trim($this->input->post('email')),
                    "password" => $this->input->post('password'),
                    "username" => $this->input->post('username'),
                    "mobile_number" => trim($this->input->post('mobile')),
                    "add_datetime" => date('Y-m-d H:i:s', time()),
                    "update_datetime" => date('Y-m-d H:i:s', time())
                    );

                $rider_id = $this->model->add($fields);

                if($rider_id)
                {
					$message = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
<a href='" . base_url() . "'>";

                    $message .= '<img src="http://192.168.1.102/hirecar/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';

                    $message .= "</a></div>
					<div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'>Register Successfully.</div>
					<div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
					<div style='padding-left:10px;'>
					<p>Dear " . $fields['first_name'] . " " . $fields['last_name'] . ",</p>
					<p>Your have successfully register for Hire Car Sydney City. Your login details are as below.</p>
					<p><b>Email:</b> " . $fields['email_id'] . " <br>
					<b>Password:</b> " . $fields['password'] . " <br>
					</p>
					<p>Regards,<br />
					Hire Car Sydney City Team</p>
					</div>
					</div>
					</div>";
                    $this->load->library('email');
                    $this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
                    $this->email->to($fields['email_id']);
                    //$this->email->cc("testcc@domainname.com");
                    $this->email->subject("Sign Up By Driver For Hire Car Sydney City");
                    $this->email->message($message);

                    $this->email->send();
					
                    $this->session->set_flashdata('SUCC_MESSAGE', 'Successfully Created New Driver');
                    redirect('admin/driver/view');
                }
                else
                {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error to create New Driver.');
                }
            }
        }
        
        $page_details['title'] = "Add Driver";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/driver/driver-add');
        $this->load->view('admin/template/footer');
    }
    
    public function details($id)
    {
        $data['disable'] = "readonly disabled";
        
        $data['driver_details'] = $this->model->fetch_details($id);
		$data['licence_uploads'] = $this->model->get_uploads($id, '0');
		$data['hc_authority_uploads'] = $this->model->get_uploads($id, '1');
		$data['registration_uploads'] = $this->model->get_uploads($id, '2');
		$data['insurance_uploads'] = $this->model->get_uploads($id, '3');

        $data['action'] = 'view';
        
        $this->load->view('admin/template/header');
        $this->load->view('admin/driver/driver-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    public function edit($id)
    {
		$this->load->model('region_model');
        $data['driver_details'] = $this->model->fetch_details($id);
		
		$data['licence_uploads'] = $this->model->get_uploads($id, '0');
		$data['hc_authority_uploads'] = $this->model->get_uploads($id, '1');
		$data['registration_uploads'] = $this->model->get_uploads($id, '2');
		$data['insurance_uploads'] = $this->model->get_uploads($id, '3');
		
        if($this->input->post())
        {
			//echo '<pre>';print_r($_FILES); die;
			
			// Create Directory for driver if not available
			/*if (!file_exists("./assets/admin/DriverImages/".$id."/")) {
				mkdir("./assets/admin/DriverImages/".$id."/", 0777);
			}
			else {
				chmod("./assets/admin/DriverImages/".$id."/", 0777);
			}*/
			
			// Upload Driver Photo
			if(!empty($_FILES['driverimage']['name'])) {
				$target = "./assets/uploads/driver/";
				$filename = pathinfo($_FILES['driverimage']['name'],PATHINFO_FILENAME);
				$extension = strtolower(pathinfo($_FILES['driverimage']['name'], PATHINFO_EXTENSION));
				$filename = str_replace(' ','_',$filename);
				$newFile = $id.'.'.$extension;
				$filenamewittargetfolder = $target.$newFile;
				
				if(in_array($extension , array('jpg','jpeg', 'gif', 'png', 'bmp'))) {
					if(move_uploaded_file($_FILES['driverimage']['tmp_name'], $filenamewittargetfolder)) {
						//unlink($target.$data['driver_details']->profile_image);	
						$newUploadedFile = $newFile;
						//$newServerPath = 'rib';
					} else {
						$newUploadedFile = $data['driver_details']->profile_image;
						$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's Photo uploading. Please try again.");
					}
				} else {
					$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
				}
			} else {
				$newUploadedFile = $data['driver_details']->profile_image;
				$newServerPath = $data['driver_details']->profile_image_server_path;
			}
			
			// Upload Multiple Licence Details (licensedetails)
			$total_licence = count($_FILES['licensedetails']['name']);
			
			for($i=0; $i<$total_licence; $i++)
			{
				if(!empty($_FILES['licensedetails']['name'][$i])) {
					$target    = "./assets/uploads/driver/license/";
					$filename  = pathinfo($_FILES['licensedetails']['name'][$i], PATHINFO_FILENAME);
					$extension = strtolower(pathinfo($_FILES['licensedetails']['name'][$i], PATHINFO_EXTENSION));
					$filename  = str_replace(' ', '_', $filename);
					$newFile   = $id.'_'.time().$i.'.'.$extension;
					$filenamewittargetfolder = $target.$newFile;
					
					if(in_array($extension, array('jpg','jpeg', 'gif', 'png', 'bmp'))) {
						if(move_uploaded_file($_FILES['licensedetails']['tmp_name'][$i], $filenamewittargetfolder)) {
							//unlink($target.$data['driver_details']->docDriverLicenseImage);
							$newLicenceUploadedFile[] = $newFile;
						} else {
							//$newLicenceUploadedFile = $data['driver_details']->docDriverLicenseImage;
							//$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's Licence uploading. Please try again.");
						}
					} else {
						$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
					}
				} else {
					//$newLicenceUploadedFile = $data['driver_details']->docDriverLicenseImage;
				}
				
			}
			//echo '<pre>';print_r($newLicenceUploadedFile);
			
			// Upload Multiple HC Authory Card (hc_authority_card)
			$total_hc_card = count($_FILES['hc_authority_card']['name']);
			
			for($i=0; $i<$total_hc_card; $i++)
			{
				if(!empty($_FILES['hc_authority_card']['name'][$i])) {
					$target    = "./assets/uploads/driver/hcauthoritycertificate/";
					$filename  = pathinfo($_FILES['hc_authority_card']['name'][$i], PATHINFO_FILENAME);
					$extension = strtolower(pathinfo($_FILES['hc_authority_card']['name'][$i], PATHINFO_EXTENSION));
					$filename  = str_replace(' ', '_', $filename);
					$newFile   = $id.'_'.time().$i.'.'.$extension;
					$filenamewittargetfolder = $target.$newFile;
					
					if(in_array($extension, array('jpg','jpeg', 'gif', 'png', 'bmp'))) {
						if(move_uploaded_file($_FILES['hc_authority_card']['tmp_name'][$i], $filenamewittargetfolder)) {
							//unlink($target.$data['driver_details']->docHCAuthorityImage);	
							$newHCAuthorityUploadedFile[] = $newFile;
						} else {
							//$newHCAuthorityUploadedFile = $data['driver_details']->docHCAuthorityImage;
							$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's HC Authority Card uploading. Please try again.");
						}
					} else {
						$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
					}
				} else {
					//$newHCAuthorityUploadedFile = $data['driver_details']->docHCAuthorityImage;
				}
			}
			//print_r($newHCAuthorityUploadedFile);
			
			
			// Upload Multiple registration number(registrationnumber)
			$total_registrationnumber = count($_FILES['registrationnumber']['name']);
			
			for($i=0; $i<$total_registrationnumber; $i++)
			{
				if(!empty($_FILES['registrationnumber']['name'][$i])) {
					$target    = "./assets/uploads/driver/registrationnumber/";
					$filename  = pathinfo($_FILES['registrationnumber']['name'][$i], PATHINFO_FILENAME);
					$extension = strtolower(pathinfo($_FILES['registrationnumber']['name'][$i], PATHINFO_EXTENSION));
					$filename  = str_replace(' ', '_', $filename);
					$newFile   = $id.'_'.time().$i.'.'.$extension;
					$filenamewittargetfolder = $target.$newFile;
					
					if(in_array($extension, array('jpg','jpeg', 'gif', 'png', 'bmp'))) {
						if(move_uploaded_file($_FILES['registrationnumber']['tmp_name'][$i], $filenamewittargetfolder)) {
							//unlink($target.$data['driver_details']->docRegistrationImage);	
							$newRegistrationUploadedFile[] = $newFile;
						} else {
							//$newRegistrationUploadedFile = $data['driver_details']->docRegistrationImage;
							$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's HC Authority Card uploading. Please try again.");
						}
					} else {
						$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
					}
				} else {
					//$newRegistrationUploadedFile = $data['driver_details']->docRegistrationImage;
				}
			}
			//print_r($newRegistrationUploadedFile);
			
			// Upload Multiple insurance type (insurancetype)
			$total_insurancetype = count($_FILES['insurancetype']['name']);
			
			for($i=0; $i<$total_insurancetype; $i++)
			{
				if(!empty($_FILES['insurancetype']['name'][$i])) {
					$target    = "./assets/uploads/driver/insurance/";
					$filename  = pathinfo($_FILES['insurancetype']['name'][$i], PATHINFO_FILENAME);
					$extension = strtolower(pathinfo($_FILES['insurancetype']['name'][$i], PATHINFO_EXTENSION));
					$filename  = str_replace(' ', '_', $filename);
					$newFile   = $id.'_'.time().$i.'.'.$extension;
					$filenamewittargetfolder = $target.$newFile;
					
					if(in_array($extension, array('jpg','jpeg', 'gif', 'png', 'bmp'))) {
						if(move_uploaded_file($_FILES['insurancetype']['tmp_name'][$i], $filenamewittargetfolder)) {
							//unlink($target.$data['driver_details']->docInsuranceImage);	
							$newInsuranceUploadedFile[] = $newFile;
						} else {
							//$newInsuranceUploadedFile = $data['driver_details']->docInsuranceImage;
							$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's HC Authority Card uploading. Please try again.");
						}
					} else {
						$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
					}
				} else {
					//$newInsuranceUploadedFile = $data['driver_details']->docInsuranceImage;
				}
			}
			//print_r($newInsuranceUploadedFile);die;
			
			$datetime = date('Y-m-d H:i:s', time());
			$upload_details = array();
			
			// All Upload Details (Licence)
			foreach($newLicenceUploadedFile as $val) {
				$upload_details[] = array('driver_id' => $id, 'file_name' => $val, 'type' => '0', 'add_datetime' => $datetime);
			}
			
			// All Upload Details (HC Authority)
			foreach($newHCAuthorityUploadedFile as $val) {
				$upload_details[] = array('driver_id' => $id, 'file_name' => $val, 'type' => '1', 'add_datetime' => $datetime);
			}
			
			// All Upload Details (Registration Image)
			foreach($newRegistrationUploadedFile as $val) {
				$upload_details[] = array('driver_id' => $id, 'file_name' => $val, 'type' => '2', 'add_datetime' => $datetime);
			}
			
			// All Upload Details (Insurance Image)
			foreach($newInsuranceUploadedFile as $val) {
				$upload_details[] = array('driver_id' => $id, 'file_name' => $val, 'type' => '3', 'add_datetime' => $datetime);
			}
			//echo '<pre>';print_r($upload_details); die;
			
			
			$fields = array("first_name" => trim($this->input->post('fname')),
				"last_name" => trim($this->input->post('lname')),
				"email_id" => trim($this->input->post('email')),
				"username" => trim($this->input->post('username')),
				"password" => $this->input->post('password'),
				"mobile_number" => trim($this->input->post('mobile')),
				"companyname" => trim($this->input->post('companyname')),
				"abn" => trim($this->input->post('abn')),
				"address" => trim($this->input->post('address')),
				"state" => trim($this->input->post('state')),
				"city" => trim($this->input->post('city')),
				"zipcode" => $this->input->post('postcode'),
				"invitecode" => trim($this->input->post('invitecode')),
				"profile_image" => $newUploadedFile,
				"profile_image_server_path" => $newServerPath,
				"licensedetails" => $this->input->post('licensedetails'),
				"update_datetime" => $datetime,
				"car_make" => $this->input->post('carmake'),
				"car_yearmodel" => $this->input->post('modelnyear'),
				"insurance_company" => $this->input->post('insurancecompany'),
				//"docDriverLicenseImage" => $newLicenceUploadedFile,
				//"docHCAuthorityImage" => $newHCAuthorityUploadedFile,
				//"docRegistrationImage" => $newRegistrationUploadedFile,
				//"docInsuranceImage" => $newInsuranceUploadedFile,
				);
			
			$driver_id = $this->model->edit($fields, $id);
			$this->model->add_uploads($upload_details);
			
			if($driver_id)
			{
				$this->session->set_flashdata('SUCC_MESSAGE', 'Driver Details Edited Successfully.');
				redirect('admin/driver/edit/'.$id);
			}
			else
			{
				$this->session->set_flashdata('ERR_MESSAGE', 'Error Edit Driver Details.');
			}
		}
        
        $data['disable'] = "";
        
		$data['city_list']=$this->region_model->get_all_city();
        $data['action'] = 'edit';
        $data['state_list']=$this->region_model->get_all_state();
		
        $this->load->view('admin/template/header');
        $this->load->view('admin/driver/driver-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    public function delete($id)
    {
        $result = $this->model->delete($id);
        if ($result) 
        {
            $this->session->set_flashdata('SUCC_MESSAGE', 'Driver Deleted Successfully.');
        } 
        else 
        {
            $this->session->set_flashdata('ERR_MESSAGE', 'Error to Delete Driver.');
        }

        redirect('admin/driver/view');
    }
    
    public function changestatus($id) 
    {
        //echo $_POST['status'];exit;
		
		//echo $this->input->post('status');
		
		//echo $id;
		
		
     /*   $data = array('status' => $this->input->post('status'));
		$this->db->where('driver_id',$id);
        $result = $this->db->update('driver',$data);	
        
		$driverUniqueId = 'D'.str_pad($id, 4, "0", STR_PAD_LEFT);
        
		print_r($data);
		
		//$result = $this->model->edit($data, $id);
		
		
		
		$driver_details = $this->model->fetch_details($id);
		
		echo $result;
		
		//print_r($driver_details);
		*/
		
		
		$driverUniqueId = 'D'.str_pad($id, 4, "0", STR_PAD_LEFT);
         $data = array('status' => $this->input->post('status'));

         $this->db->where('driver_id',$id);
         $this->db->update('driver',$data);

         $this->db->where('driver_id',$id);
         $this->db->select('email_id,first_name,last_name');
         $result = $this->db->get('driver')->result();
			//print_r($result);
        if ($result) 
        {
			
			
            echo 'success';
			
		if($this->input->post('status') == '1'){



                    $message = "
                    <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>

                    <div style='padding-left:10px;'>

                    <p>Dear " . $result[0]->first_name . " " . $result[0]->last_name . ",</p>
                    
					<p>Your Hire Car Sydney City profile has been activated. You may request Hire Car Sydney City team to deactivated your profile. If you have any Questions or Concerns regarding your account, Please contact us at admin@hirecarsydneycity.com.au.</p>

                    </p>

                    <p>Regards,<br />

                    Hire Car Sydney City Team<br/>
    ".'<img src="'.base_url().'/assets/web/images/logo.png" alt="" style="width:60px;display:block;" />'."

                    </div>
                    </div>
                    </div>";

                    $this->load->library('email');

                   // $this->email->from('business@rideinblack.com.au', "Ride In Black");
				   
				    $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City: Admin Team");

                    $this->email->to($result[0]->email_id);

                    //$this->email->cc("testcc@domainname.com");

                    $this->email->subject('Important Notice regarding your Hire Car Sydney City account - '.$driverUniqueId,"Hire Car Sydney City");

                    $this->email->message($message);

                    $this->email->send();

            }else{
                    $message = "

                    <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>

                    <div style='padding-left:10px;'>

                    <p>Dear " . $result[0]->first_name . " " . $result[0]->last_name . ",</p>
										
                    <p>Your Hire Car Sydney City profile has been deactivated. You may request Hire Car Sydney City team to reactivate your profile. If you have any Questions or Concerns regarding your account, Please contact us at admin@hirecarsydneycity.com.au.</p>

                    </p>

                    <p>Regards,<br />

                    Hire Car Sydney City Team<br/>
    ".'<img src="'.base_url().'/assets/web/images/logo.png" alt="" style="width:60px;display:block;" />'."

                    </div>
                    </div>

                    </div>";

                    $this->load->library('email');

                   // $this->email->from('business@rideinblack.com.au', "Hire Car Sydney City");

				 $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City: Admin Team");
                
				 $this->email->to($result[0]->email_id);

                    //$this->email->cc("testcc@domainname.com");

                     $this->email->subject('Important Notice regarding your Hire Car Sydney City account - '.$driverUniqueId,"Hire Car Sydney City");

                    $this->email->message($message);

                    $this->email->send();
            } 
			
		
        } 
        else 
        {
            echo 'error';
        }
        exit();
    }
	
	public function delete_uploads($upload_id) 
	{
		$upload_details = $this->model->get_upload_details($upload_id);
		
		if($upload_details != false) {
			$folder_path = '';
			if($upload_details->type == 0) {
				$folder_path = 'license';
			}
			else if($upload_details->type == 1) {
				$folder_path = 'hcauthoritycertificate';
			}
			else if($upload_details->type == 2) {
				$folder_path = 'registrationnumber';
			}
			else if($upload_details->type == 3) {
				$folder_path = 'insurance';
			}
			
			$file_name = $upload_details->file_name;
			
			$delete_upload = $this->model->delete_upload($upload_id);
        
			if($delete_upload > 0)
			{
				if($upload_details->server_reference == 'rib') {
					$this->load->helper('ftp_helper');
					ftp_file_delete(array('/assets/uploads/driver/'.$folder_path.'/'.$file_name));
				}
				else {
					$file_path = './assets/uploads/driver/'.$folder_path.'/'.$file_name;
					unlink($file_path);
				}
				
				echo 'success';
			}
			else
			{
				echo 'error';
			}
		}
		exit;
	}
}