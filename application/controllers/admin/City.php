<?php 
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        // Check Admin login from function from (h: function)
        check_admin_login();
        
        // Load Rider Model
        $this->load->model('region_model', 'model');
    }
    
    // View All City List
    public function view($page = 1) 
    {
        $search_where = "";
        // Get nSearch Element and prepare query
        if(isset($_GET['search_city']) && !empty($_GET['search_city']))
        {
            $search_where = "name LIKE '%".$_GET['search_city']."%'";
        }
        
        // Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;
        
        // Fetch details from model
        $rider = $this->model->city_list($record_per_page, ($page-1)*$record_per_page, $search_where);
        
        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url().'admin/city/view/', $this->model->count_city($search_where), $record_per_page);
        
        // Store details of rider
        $data['city_details'] = $rider;
        $data['sr'] = (($page-1)*PER_PAGE_RECORD)+1;
        
        $page_details['title'] = "View City";
        
        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/city/city-index', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Add new City
    public function add() 
    {
        if($this->input->post())
        {
            $check_unique = $this->model->check_unique_city('name', trim($this->input->post('city')));
            
            if(!$check_unique)
            {
                $fields = array("name" => trim($this->input->post('city')));

                $city_id = $this->model->city_add($fields);

                if($city_id)
                {
                    $this->session->set_flashdata('SUCC_MESSAGE', 'City Added successfully;');
                    redirect('admin/city/view');
                }
                else
                {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error to Add New City.');
                }
            }
            else
            {
               $_POST['ERR_CITY'] = 'City already exist.'; 
            }
        }
        
        $page_details['title'] = "Add City";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/city/city-add');
        $this->load->view('admin/template/footer');
    }
    
    // Edit City details
    public function edit($id)
    {
        if($this->input->post() && $this->input->post())
        {
            $check_unique = $this->model->check_unique_city('name', trim($this->input->post('city')), ' city_id!='.$id);
            
            if(!$check_unique)
            {
                $fields = array("name" => trim($this->input->post('city')));

                $city_id = $this->model->city_edit($fields, $id);

                if($city_id)
                {
                    $this->session->set_flashdata('SUCC_MESSAGE', 'City Details Saved Successfully.');
                    redirect('admin/city/view');
                }
                else
                {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error to edit City details.');
                    redirect('admin/city/edit/'.$id);
                }
            }
            else
            {
               $_POST['ERR_CITY'] = 'City already exist.'; 
            }
        }
        
        $city_details = $this->model->fetch_city_details($id);
        $data['city_details']  = $city_details[0];
        
        $data['disable'] = "";
        $data['action'] = 'edit';
        
        $page_details['title'] = "Edit City Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/city/city-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Delete City
    public function delete($id)
    {
        $result = $this->model->city_delete($id);
        
        if ($result) 
        {
            $this->session->set_flashdata('SUCC_MESSAGE', 'City Deleted Successfully.');
        } 
        else 
        {
            $this->session->set_flashdata('ERR_MESSAGE', 'Error to Delete City.');
        }
        
        redirect('admin/city/view');
    }
}