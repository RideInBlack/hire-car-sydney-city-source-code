<?php 
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        // Check Admin login from function from (h: function)
        check_admin_login();
        
        // Load Rider Model
        $this->load->model('user_model', 'model');
    }
    
    // View All Users List
    public function view($page = 1) 
    {
        $search_where = "";
        
        // Get nSearch Element and prepare query
        if(isset($_GET['search_nickname']) && !empty($_GET['search_nickname']))
        {
            $search_where = "nick_name LIKE '%".$_GET['search_nickname']."%'";
        }
        if(isset($_GET['search_email']) && !empty($_GET['search_email']))
        {
            $search_where = ($search_where == "") ? "email_id LIKE '%".$_GET['search_email']."%'" : $search_where." AND email_id LIKE '%".$_GET['search_email']."%'";
        }
        
        // Get Per Page Record
        $record_per_page = PER_PAGE_RECORD;
        
        // Fetch details from model
        $user = $this->model->list_all($record_per_page, ($page-1)*$record_per_page, $search_where);
        
        //Apply Pagination 
        $this->load->helper('pagination_helper');
        set_pagination(base_url().'admin/user/view/', $this->model->count_total($search_where), $record_per_page);
        
        // Store details of rider
        $data['user_details'] = $user;
        $data['sr'] = (($page-1)*PER_PAGE_RECORD)+1;
        
        $page_details['title'] = "View Admin Users";
        
        // Load View
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/user/user-index', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Add new Admin User
    public function add() 
    {
        if(!is_super_admin())
        {
            redirect('admin/user/view');
        }
        
        if($this->input->post())
        {
            $unique_flag = 1;
            // Check unique email
            if($this->model->check_unique('email_id', trim($this->input->post('email'))) == TRUE)
            {
                $unique_flag = 0;
                $_POST['ERR_EMAIL'] = 'Email Id already exist.';
            }
            if($this->model->check_unique('nick_name', trim($this->input->post('nick_name'))) == TRUE)
            {
                $unique_flag = 0;
                $_POST['ERR_NICK_NAME'] = 'Nick Name already exist.';
            }
            
            if($unique_flag == 1)
            {
                $datetime = date('Y-m-d H:i:s', time());
                
                $fields = array("first_name" => trim($this->input->post('fname')),
                    "last_name" => trim($this->input->post('lname')),
                    "nick_name" => trim($this->input->post('nick_name')),
                    "email_id" => trim($this->input->post('email')),
                    "password" => $this->input->post('password'),
                    "password_hash" => md5($this->input->post('password')),
                    "add_datetime" => $datetime,
                    "update_datetime" => $datetime
                    );
            
                $user_id = $this->model->add($fields);

                if($user_id)
                {
					$message = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
<a href='" . base_url() . "'>";

                    $message .= '<img src="http://192.168.1.102/hirecar/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';

                    $message .= "</a></div>
					<div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'>Register Successfully.</div>
					<div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
					<div style='padding-left:10px;'>
					<p>Dear " . $fields['first_name'] . " " . $fields['last_name'] . ",</p>
					<p>Your have successfully register for Hire Car Sydney City. Your login details are as below.</p>
					<p><b>URL:</b> " . base_url('admin'). " <br>
					<p><b>Email:</b> " . $fields['email_id'] . " <br>
					<b>Password:</b> " . $fields['password'] . " <br>
					</p>
					<p>Regards,<br />
					Hire Car Sydney City Team</p>
					</div>
					</div>
					</div>";
                    $this->load->library('email');
                    $this->email->from('admin@hirecarssydneycity.com.au', "Admin Team");
                    $this->email->to($fields['email_id']);
                    //$this->email->cc("testcc@domainname.com");
                    $this->email->subject("Sign Up By Sub Admin For Hire Car Sydney City");
                    $this->email->message($message);

                    $this->email->send();
					
                    $this->session->set_flashdata('SUCC_MESSAGE', 'Successfully Created New Admun User');
                    redirect('admin/user/view');
                }
                else
                {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error to create new Admin User.');
                }
            }
        }
        
        $page_details['title'] = "Add Admin User";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/user/user-add');
        $this->load->view('admin/template/footer');
    }
    
    // View User details
    public function details($id)
    {
        if(!is_super_admin())
        {
            redirect('admin/user/view');
        }
        
        $data['disable'] = "readonly disabled";
        
        $data['user_details'] = $this->model->fetch_details($id);
        $data['action'] = 'view';
        
        $page_details['title'] = "View Admin User Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/user/user-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Edit User details
    public function edit($id)
    {
        if(!is_super_admin())
        {
            redirect('admin/user/view');
        }
        
        if($this->input->post())
        {
            $unique_flag = 1;
            // Check unique email
            if($this->model->check_unique('email_id', trim($this->input->post('email')), 'admin_id != '.$id) == TRUE)
            {
                $unique_flag = 0;
                $_POST['ERR_EMAIL'] = 'Email Id already exist.';
            }
            if($this->model->check_unique('nick_name', trim($this->input->post('nick_name')), 'admin_id != '.$id) == TRUE)
            {
                $unique_flag = 0;
                $_POST['ERR_NICK_NAME'] = 'Nick Name already exist.';
            }
            
            if($unique_flag == 1)
            {
                $fields = array("first_name" => trim($this->input->post('fname')),
                    "last_name" => trim($this->input->post('lname')),
                    "nick_name" => trim($this->input->post('nick_name')),
                    "email_id" => trim($this->input->post('email')),
                    "update_datetime" => date('Y-m-d H:i:s', time())
                    );

                $user_id = $this->model->edit($fields, $id);

                if($user_id)
                {
                    $this->session->set_flashdata('SUCC_MESSAGE', 'Admin User Details Saved Successfully.');
                    redirect('admin/user/view');
                }
                else
                {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error to edit Admin User details.');
                }
            }
        }
        
        $data['disable'] = "";
        
        $data['user_details'] = $this->model->fetch_details($id);
        $data['action'] = 'edit';
        
        $page_details['title'] = "Edit Admin User Details";
        
        $this->load->view('admin/template/header', $page_details);
        $this->load->view('admin/user/user-details', $data);
        $this->load->view('admin/template/footer');
    }
    
    // Delete User
    public function delete($id)
    {
        if(!is_super_admin())
        {
            redirect('admin/user/view');
        }
        
        $result = $this->model->delete($id);
        
        if ($result) 
        {
            $this->session->set_flashdata('SUCC_MESSAGE', 'Rider Deleted Successfully.');
        } 
        else 
        {
            $this->session->set_flashdata('ERR_MESSAGE', 'Error to Delete Rider.');
        }
        
        redirect('admin/user/view');
    }
    
    public function changestatus($id) 
    {
        if(!is_super_admin())
        {
            redirect('admin/user/view');
        }
        
        //echo $_POST['status'];exit;
        $data = array('status' => $this->input->post('status'));
        
        $result = $this->model->edit($data, $id);

        if ($result) 
        {
            echo 'success';
        } 
        else 
        {
            echo 'error';
        }
        exit();
    }
}