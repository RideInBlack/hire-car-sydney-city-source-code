<?php 
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        // Check Admin login from function from (h: function)
        check_admin_login();
    }
    
    public function index() 
    {
        //echo $this->router->fetch_class().'|';
        //echo $this->router->fetch_method();
		$this->load->model('driver_model');		
		$this->load->model('rider_model');
		$this->load->model('region_model');
		
		$totalDriver = $this->driver_model->count_total('');
		$totalRider = $this->rider_model->count_total('');
		$totalCity = $this->region_model->count_city('');
		
		//echo "<pre>";
		//print_r($totalCity);die;
		
		$data['totalDriver'] = $totalDriver;
		$data['totalRider'] = $totalRider;
		$data['totalCity'] = $totalCity;
		
        $this->load->view('admin/template/header');
        $this->load->view('admin/dashboard/index',$data);
        $this->load->view('admin/template/footer');
    }

}