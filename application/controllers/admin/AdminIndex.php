<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminIndex extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('session');
        
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
        
        $this->load->model('admin/admin_login', 'admin');
    }
    
    // Admin Login
    public function index() 
    {
        // If Admin Session is Set than
        if(isset($_SESSION['HIRE_CAR']['ADMIN']))
        {
            redirect('admin/dashboard');
        }
        
        if($this->input->post())
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            
            $admin_details = $this->admin->admin_authentication($username, $password);
            //echo '<pre>';print_r($admin_details);exit;
            
            // Set Admin Session if admin login is correct else flash message will display for incorrect login
            if($admin_details !== 0)
            {
                $_SESSION['HIRE_CAR']['ADMIN'] = $admin_details;
                redirect('admin/dashboard');
            }
            else
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Invalid Login Details');
            }
        }
        
        $this->load->view('admin/index/login');
    }
    
    // Admin Logout
    public function logout() 
    {
        unset($_SESSION['HIRE_CAR']['ADMIN']);
        redirect('admin/');
    }
    
    // Admin Forgot Password
    public function forgotpassword() 
    {
        if($this->input->post())
        {
            $email = $this->input->post('email');
            $admin_details = $this->admin->check_admin_email($email);
            
            if($admin_details != FALSE)
            {
                $message = "<div style='float:left;min-height:auto;width:100%'>
                    <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                    <a href='" . base_url() . "'>";

                $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
                $message .= "</a></div>
                            <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                            <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                            <div style='padding-left:10px;'>
                            <p>Hello, Admin<p><br/>
                            <p>Your Login credentials.<p>
                            <p><b>Username:</b> ".$admin_details->nick_name."<br/></p>
                            <p><b>Password:</b> ".$admin_details->password."</p>
                            <p></p>
                            <p>--</p>
                            <p>Regards,<br />
                            <b>Hire Car Sydney City Team<b></p>
                            </div>
                            </div>
                            </div>";
                
                $this->load->library('email');
                
                $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City");
                $this->email->to($email);
                $this->email->subject("[Hire Car Sydney City] Forgot Password");
                $this->email->message($message);
                
                if($this->email->send()){     
                    $this->session->set_flashdata('SUC_MESSAGE', 'Password is Sent to Your Email.');
                    redirect('admin/');
                } 
                else {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Please enter valid Email.');
                }
            }
            else
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Please enter valid Email.');
            }
        }
        
        $this->load->view('admin/index/forgotpassword');
    }
}
