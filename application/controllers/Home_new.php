<?php

/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
		
        //echo '<pre>'; print_r($_SESSION); echo '</pre>';
        if ($this->input->post()) {
            $this->load->model('rider_model');
			
            $riderDuplication = $this->rider_model->emailduplication($this->input->post('email'));

            if (empty($riderDuplication)) {
				
				$fields = array("first_name" => trim($this->input->post('fname')),
                    "last_name" => trim($this->input->post('lname')),
                    "email_id" => trim($this->input->post('email')),
                    "password" => $this->input->post('password'),
                    "mobile_number" => trim($this->input->post('mobile')),
                    "language" => trim($this->input->post('language')),
                    "add_datetime" => date('Y-m-d H:i:s', time()),
                    "update_datetime" => date('Y-m-d H:i:s', time())
                );
                $rider_id = $this->rider_model->add($fields); 

                if ($rider_id) {
                    // Email configuration
                
					$message = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
<a href='" . base_url() . "'>";

                    $message .= '<img src="http://192.168.1.102/hirecar/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';

                    $message .= "</a></div>
					<div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'>Register Successfully.</div>
					<div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
					<div style='padding-left:10px;'>
					<p>Dear " . $fields['first_name'] . " " . $fields['last_name'] . ",</p>
					<p>Your have successfully register for Hire Car Sydney City. Your login details are as below.</p>
					<p><b>Email:</b> " . $fields['email_id'] . " <br>
					<b>Password:</b> " . $fields['password'] . " <br>
					</p>
					<p>Regards,<br />
					Hire Car Sydney City Team</p>
					</div>
					</div>
					</div>";
                    $this->load->library('email');
                    $this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
                    $this->email->to($fields['email_id']);
                    //$this->email->cc("testcc@domainname.com");
                    $this->email->subject("Sign Up By Rider For Hire Car Sydney City");
                    $this->email->message($message);

                    $this->email->send();
                     
                    $this->session->set_flashdata('SUCC_RIDER_REG', 'You have successfully registered as Rider.');
                    //redirect('admin/rider/view');
					//redirect();
					redirect($this->input->get('redirect'));
                } else {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error occurred while registered as Rider.');
                }
            } else {
                $this->session->set_flashdata('ERR_EMAILDUPLICATION', 'Email id is already exist.');
				//redirect(base_url());
				redirect($this->input->get('redirect'));
            }
        }
        $this->load->model('region_model');
        $data['city'] = $this->region_model->get_all_city();
		
        // For fetch All Car Category
        $this->load->model('car_model');
        $data['car_category'] = $this->car_model->fetch_all_category();
        
        //$data['cars'] = $this->car_model->fetch_all_cars();
        
        
        $this->load->model('giftcard_model');
        $this->load->model('cms_model');
        $data['giftCardData'] = $this->giftcard_model->list_all();
	$data['CarCatData'] = $this->car_model->list_category_all();
        $data['welcomeContent'] = $this->cms_model->fetch_details(8);
        $this->load->model('event_model');
        $data['event_details'] = $this->event_model->get_all_event();
        
		$header_details['meta_keyword'] = 'Sydney Hire Cars, Hire Cars Sydney, Sydney Airport Transfer, Hire car with driver, Sydney Luxury Hire Cars, luxury  Hire Cars Sydney, luxury Hire car with driver, luxury hire car airport, Luxury Chauffeured Cars, Luxury Car Hire Sydney, Chauffeured Cars, Luxury Chauffeured for airport, Luxury Chauffeured for wedding,  Wedding cars with driver, sydney international airport transfers, sydney domestic airport transfers, professional driver sydney transfers, Rent luxury cars sydney, sydney rent cars, rent car for wedding, wedding car rent with drivers, Luxury Chauffeur Drive, Luxury wedding cars, Sydney Airport Transfers, door to door airport transfers, Business Class Airport Transfers';
		
		$header_details['meta_description'] = 'Ride in Style in Chauffeur Driven Luxury Cars on affordable Price anywhere in Sydney. Corporate Travel, Airport Transfer, wedding, Special Occasion, Party & more.';
		
		$header_details['title'] = "";
		
		//$this->output->cache(5);
		
        $this->load->view('web/template/header', $header_details);
        $this->load->view('web/home/index', $data);
        $this->load->view('web/template/footer');
    }
	
	
	public function carImageAJAX()
	{
		// For fetch All Car Category
        $this->load->model('car_model');
		$data['cars'] = $this->car_model->fetch_all_cars();
		
		$this->load->view('web/home/cars-slider', $data);
	}

    public function signupdriver() {
        if ($this->input->post()) {
            $this->load->model('driver_model');
            $riderDuplication = $this->driver_model->check_unique('email_id', $this->input->post('d_email'), "");

            if (empty($riderDuplication)) {
                //print_r($_FILES);die;
                if (!empty($_FILES['d_driverimage']['name'])) {
                    //echo "dfsd";die;
                    $target = "./assets/admin/DriverImages/";
                    $filename = pathinfo($_FILES['d_driverimage']['name'], PATHINFO_FILENAME);
                    $extension = strtolower(pathinfo($_FILES['d_driverimage']['name'], PATHINFO_EXTENSION));
                    $filename = str_replace(' ', '_', $filename);
                    $newFile = $filename . uniqid() . '.' . $extension;
                    $filenamewittargetfolder = $target . $newFile;
                    if (in_array($extension, array('jpg', 'jpeg', 'gif', 'png', 'bmp'))) {
                        if (move_uploaded_file($_FILES['d_driverimage']['tmp_name'], $filenamewittargetfolder)) {
                            $newUploadedFile = $newFile;
                        } else {

                            $this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's Photo uploading. Please try again.");
                        }
                    } else {
                        $this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');
                    }
                } else {
                    $newUploadedFile = '';
                }

                $fields = array(
                    "first_name" => trim($this->input->post('d_fname')),
                    "last_name" => trim($this->input->post('d_lname')),
                    "email_id" => trim($this->input->post('d_email')),
                    "password" => $this->input->post('d_password'),
                    "mobile_number" => trim($this->input->post('d_mobile')),
                    "companyname" => trim($this->input->post('d_companyname')),
                    "abn" => trim($this->input->post('d_abn')),
                    "address" => trim($this->input->post('d_address')),
                    "city" => trim($this->input->post('d_city')),
                    "zipcode" => trim($this->input->post('d_zipcode')),
                    "invitecode" => trim($this->input->post('d_invitecode')),
                    "profile_image" => $newUploadedFile,
                    "licensedetails" => trim($this->input->post('d_licensedetails')),
                    "add_datetime" => date('Y-m-d H:i:s', time()),
                    "update_datetime" => date('Y-m-d H:i:s', time())
                );
                $driver_id = $this->driver_model->add($fields);
                //echo $driver_id;die;
                if ($driver_id) {
                    // Email configuration
					
                    $message = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
<a href='" . base_url() . "'>";

                    $message .= '<img src="http://192.168.1.102/hirecar/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';

                    $message .= "</a></div><div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'>Register Successfully</div>
					<div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
					<div style='padding-left:10px;'>
					<p>Dear " . $fields['first_name'] . " " . $fields['last_name'] . ",</p>
					<p>Your have successfully register for Hire Car Sydney City. Your login details are as below.</p>
					<p><b>Email:</b> " . $fields['email_id'] . " <br>
					<b>Password:</b> " . $fields['password'] . " <br>
					</p>
					<p>Regards,<br />
					Hire Car Sydney City Team</p>
					</div>
					</div>
					</div>";

                    //echo $message;die;

                    $this->load->library('email');
                    $this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
                    $this->email->to($fields['email_id']);
                    //$this->email->cc("testcc@domainname.com");
                    $this->email->subject("Sign Up By Driver For Hire Car Sydney City");
                    $this->email->message($message);

                    $this->email->send();

                    $this->session->set_flashdata('SUCC_DRIVER_REG', 'You have successfully registered as Driver.');
                } else {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error occurred while registered as Rider.');
                }
            } else {
                $this->session->set_flashdata('ERR_DRIVER_EMAILDUPLICATION', 'Email id is already exist.');
            }
            redirect();
        }
    }
    
    public function book_now()
    {
        //echo '<pre>';print_r($_POST);
        if($this->input->post('book-now'))
        {
            $pickup_address = $this->input->post('pick-address');
            $pickup_city = $this->input->post('pick-city');
            $pickup_postal = $this->input->post('pick-postal');
            
            $destination_location = $this->input->post('drop-address');
            $destination_city = $this->input->post('drop-city');
            $destination_postal = $this->input->post('drop-postal');
            
            $puckup_date = $this->input->post('pick-up-date');
            $puckup_time = $this->input->post('pick-up-time');
            $event = $this->input->post('event');
            $number_passengers = $this->input->post('number_passengers');
            
            $_SESSION['RIDE'] = array(
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'pickup_location' => $pickup_address,
                'pickup_city' => $pickup_city,
                'pickup_postal' => $pickup_postal,
                'destination_location' => $destination_location,
                'destination_city' => $destination_city,
                'destination_postal' => $destination_postal,
                'pickup_date'  => $puckup_date,
                'pickup_time'  => $puckup_time,
                'event_type'   => $event,
                'number_passengers'   => $number_passengers,
                'special_requirement' => $this->input->post('special_requirement'),
                'car_category_id' => $this->input->post('selected-car')
                );
            
            redirect(base_url('ride/book_details'));
            //redirect(base_url('Pages/search'));
        }
        
        if($this->input->post('select-ride'))
        {
            $selectd_ride = ($this->input->post('selected-ride-id') != "") ? $this->input->post('selected-ride-id') : 0 ;
            $pickup_address = $this->input->post('pick-address');
            $pickup_city = $this->input->post('pick-city');
            $pickup_postal = $this->input->post('pick-postal');
            
            $destination_location = $this->input->post('drop-address');
            $destination_city = $this->input->post('drop-city');
            $destination_postal = $this->input->post('drop-postal');
            
            $puckup_date = $this->input->post('pick-up-date');
            $puckup_time = $this->input->post('pick-up-time');
            $event = $this->input->post('event');
            $number_passengers = $this->input->post('number_passengers');
            
            $estimated_distance = $this->input->post('estimated-distance');
            $pickup_date = ($_SESSION['RIDE']['pickup_date'] != "") ? $_SESSION['RIDE']['pickup_date'] : "";
            $pickup_time = ($_SESSION['RIDE']['pickup_time'] != "") ? $_SESSION['RIDE']['pickup_time'] : "";
            $event = ($_SESSION['RIDE']['event_type'] != "") ? $_SESSION['RIDE']['event_type'] : 0;
            
            $_SESSION['RIDE'] = array(
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'pickup_location' => $pickup_address,
                'pickup_city' => $pickup_city,
                'pickup_postal' => $pickup_postal,
                'destination_location' => $destination_location,
                'destination_city' => $destination_city,
                'destination_postal' => $destination_postal,
                'pickup_date'  => $puckup_date,
                'pickup_time'  => $puckup_time,
                'event_type'   => $event,
                'number_passengers'   => $number_passengers,
                'estimated_distance' => $estimated_distance,
                "selected_ride" => $selectd_ride,
                'special_requirement' => $this->input->post('special_requirement'),
                'car_category_id' => $this->input->post('selected-car')
                );
            
            redirect(base_url('ride/book_details'));
        }
        else
        {
            redirect(base_url());
        }
    }
    
    public function login() 
	{
		$this->load->helper('cookie');
        
        if ($this->input->post('select') == 'driver') {
            $emailadd = $this->input->post('emailadd');
            $password = $this->input->post('password');

            $this->load->model('driver_model');
            $driver = $this->driver_model->driver_authentication($emailadd, $password);
			
			if ($this->input->post('rememberme') == 'rememberme') 
			{
				 $cookie = array(
								'name' => 'emailadd',
								'value' => $this->input->post('emailadd'),
								'expire' => time()+(60*60*1*24*30),
								'path'   => '/',
								);
				 
				$this->input->set_cookie($cookie);
				
				$cookie = array(
								'name' => 'password',
								'value' => $this->input->post('password'),
								'expire' => time()+(60*60*1*24*30),
								'path'   => '/',
								);
								
				$this->input->set_cookie($cookie);
				
			 }
			 else
			 {
				 delete_cookie('username');
				 delete_cookie('password');	 
			 }

            if ($driver !== 0) {
                $_SESSION['HIRE_CAR']['DRIVER'] = $driver;
                //print_r($_SESSION);exit;

                redirect(base_url('driverprofile'));
            } else {
                $this->session->set_flashdata('ERR_LOGIN', 'Invalid Login Details');
                redirect(base_url());
            }
        } else if ($this->input->post('select') == 'rider') {
			$emailadd = $this->input->post('emailadd');
            $password = $this->input->post('password');

            $this->load->model('rider_model');
            $rider = $this->rider_model->rider_authentication($emailadd, $password);
			
			if ($this->input->post('rememberme') == 'rememberme') 
			{
				 $cookie = array(
								'name' => 'emailadd',
								'value' => $this->input->post('emailadd'),
								'expire' => time()+(60*60*1*24*30),
								'path'   => '/',
								);
				 
				$this->input->set_cookie($cookie);
				
				$cookie = array(
								'name' => 'password',
								'value' => $this->input->post('password'),
								'expire' => time()+(60*60*1*24*30),
								'path'   => '/',
								);
								
				$this->input->set_cookie($cookie);
			 }
			 else
			 {
				 delete_cookie('username');
				 delete_cookie('password');	 
			 }

            if ($rider !== 0) {
                $_SESSION['HIRE_CAR']['RIDER'] = $rider;
                
                if(isset($_GET['redirect'])) {
                    redirect(base_url($_GET['redirect']));
                }
                else {
                    redirect(base_url('riderprofile'));
                }
            } else {
                $this->session->set_flashdata('ERR_LOGIN', 'Invalid Login Details');
                //redirect(base_url());
				redirect(base_url($_GET['redirect']));
            }
        } else {
            $this->session->set_flashdata('ERR_LOGIN', 'Invalid Login Details');
            redirect(base_url());
        }
    }

	// End Login Action
    public function forgotpassword() {
        if ($this->input->post('type') == 'driver') {
            $this->load->model('driver_model');

            $email = $this->input->post('email');
            $driver_details = $this->driver_model->check_driver_email($email);

            if ($driver_details != FALSE) {
                $message = "Hello, " . $driver_details->username . "<br/><br/>";
                $message .= "Your Account Password.<br/>";
                $message .= "<b>Password:</b> " . $driver_details->password;
                $message .= "<br/><br/>--<br/>";
                $message .= "Thank You";

                $this->load->library('email');

                $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City");
                $this->email->to($email);
                $this->email->subject("[Hire Car Sydney City] Driver: Forgot Password");
                $this->email->message($message);

                if ($this->email->send()) {
                    $this->session->set_flashdata('SUC_FORGOT_PASSWORD', 'Password is Sent to Your Email.');
                    redirect(base_url());
                } else {
                    $this->session->set_flashdata('ERR_FORGOT_PASSWORD', 'Error to send email, Please try after some time.');
                    redirect(base_url());
                }
            } else {
                $this->session->set_flashdata('ERR_FORGOT_PASSWORD', 'Invalid Email Id.');
                redirect(base_url());
            }
        } else if ($this->input->post('type') == 'rider') {
            $this->load->model('rider_model');

            $email = $this->input->post('email');
            $rider_details = $this->rider_model->check_rider_email($email);

            if ($rider_details != FALSE) {
                $message = "Hello, " . $rider_details->first_name . "<br/><br/>";
                $message .= "Your Account Password.<br/>";
                $message .= "<b>Password:</b> " . $rider_details->password;
                $message .= "<br/><br/>--<br/>";
                $message .= "Thank You";

                $this->load->library('email');

                $this->email->from('admin@hirecarsydneycity.com', "Hire Car Sydney City");
                $this->email->to($email);
                $this->email->subject("[Hire Car Sydney City] Rider: Forgot Password");
                $this->email->message($message);

                if ($this->email->send()) {
                    $this->session->set_flashdata('SUC_FORGOT_PASSWORD', 'Password is Sent to Your Email.');
                    redirect($this->input->get('redirect'));
                } else {
                    echo $this->email->print_debugger();
                    exit;
                    $this->session->set_flashdata('ERR_FORGOT_PASSWORD', 'Error to send email, Please try after some time.');
                    redirect($this->input->get('redirect'));
                }
            } else {
                $this->session->set_flashdata('ERR_FORGOT_PASSWORD', 'Invalid Email Id.');
                redirect($this->input->get('redirect'));
            }
        } else {
            $this->session->set_flashdata('ERR_FORGOT_PASSWORD', 'Invalid Email Id.');
            redirect($this->input->get('redirect'));
        }
    }
    // End Login Action

    public function logout() {
        unset($_SESSION['HIRE_CAR']['DRIVER'], $_SESSION['HIRE_CAR']['RIDER']);
        redirect(base_url());
    }
	
	public function facebooklogin()
	{
		//echo "<pre>";
		//print_r($this->input->get());die;
		$this->load->model('rider_model');
		$this->load->model('driver_model');
		
		$data = urldecode($this->input->get('data'));
		$getfbdata = explode("/",$data);
		//echo "<pre>";
		//print_r($getfbdata);die;
		
		$userinfo = array();
		$userinfo['first_name'] = $getfbdata[0];
		$userinfo['last_name'] = $getfbdata[1];
		$userinfo['email_id'] = $getfbdata[2];
		$userinfo['status'] = '1';
		$userinfo['add_datetime'] = date('Y-m-d H:i:s');
		$userinfo['update_datetime'] = date('Y-m-d H:i:s');
		
		if($getfbdata[3] == 'rider')
		{
			$rider_id = $this->rider_model->addFacebookUser($userinfo);
			
			$login_type = (object) array('LOGIN_TYPE' => 'FB');
			$rider_id = (object) array_merge((array)$rider_id, (array)$login_type); 
			
			$_SESSION['HIRE_CAR']['RIDER'] = $rider_id;
			
			redirect(base_url($this->input->get('redirect')));
		}
		else
		{
			$driver_id = $this->driver_model->addFacebookUser($userinfo);
			
			$login_type = (object) array('LOGIN_TYPE' => 'FB');
			$driver_id = (object) array_merge((array)$driver_id, (array)$login_type); 
			
			$_SESSION['HIRE_CAR']['DRIVER'] = $driver_id;

			redirect(base_url($this->input->get('redirect')));
			//redirect(base_url('driverprofile'));
		}
	}
	
	public function newsleter() 
	{
		//echo "<pre>";
		//print_r($this->input->post());die;
		$this->load->model('rider_model');
		$userinfo = array();
		$userinfo['newsletter_email'] = $this->input->post('news-email');
		$userinfo['add_datetime'] = date('Y-m-d H:i:s');
		
		$rider_id = $this->rider_model->addNewsLetter($userinfo);
		
		$this->session->set_flashdata('SUCC_NEWSLETTER', 'You have successfully register for newsletter.');
		
		redirect(base_url('home'));
		
	}
	
	public function changedatetime(){
		
		if(isset($_POST['action']) && $_POST['action'] == "datetimechange"){
			$date = $_POST['date'];
	  echo '<input type="text" name="pick-up-time" id="pick-up-time" placeholder="Pick-Up Time" class="form-control hometimepicker" value=""><i class="fa fa-clock-o"></i>
                                <span id="ERR_pick-up-time" class="text-red"></span>';
	
		}
	
	}
	
}
