<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjobs extends CI_Controller {

    public function __construct() {
        parent::__construct();
		$this->load->model('cron_model', 'model');
    }

    public function removeUnpaidCoupon() {
        $date_before_2days = date('Y-m-d', strtotime('-2 day'));
		
		$where = 'DATE(giftcard_coupon.generate_datetime) < "'.$date_before_2days.'" AND giftcard_coupon.payment_type = "deposit" AND giftcard_coupon.status = "0" AND generate_by = "1"';
		$coupons = $this->model->getCoupons($where);
		
		// Get bank details from setting
		$this->load->model('setting_model');
		$setting_details = $this->setting_model->fetch_details(1);
		
		$this->load->library('email');
		
		foreach($coupons as $coupon_row) {
			//echo '<pre>';print_r($coupon_row); //exit;
			
			$this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
            
            $this->email->to($coupon_row->email_id);
            $this->email->subject("Coupon Payment Reminder");

            $message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                        <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                        <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                        <div style='padding-left:10px;'>
                        <p>Dear ".$coupon_row->first_name." ".$coupon_row->last_name.",</p>
                        <p>Coupon Details.</p>
                        <p><b>Coupon Code:</b> ".$coupon_row->coupon_code."</p>
                        <p><b>Price:</b> $".$coupon_row->price."</p>
                        
						<hr/>
						<p><b>Bank:</b> ".$setting_details->setting_bank."</p>
                    	<p><b>Account Name:</b> ".$setting_details->setting_account_name."</p>
                    	<p><b>BSB:</b> ".$setting_details->setting_bsb."</p>
                    	<p><b>Account Number:</b> ".$setting_details->setting_account_number."</p>
						<p><b>Your Coupon is expired please purchase new coupon.</b></p>
						
                        <p>--</p>
                        <p>Regards,<br />
                        <b>Hire Car Sydney City Team<b></p>
                        </div>
                        </div>
                        </div>";

            $this->email->message($message);
            $this->email->send();
			// -----------------------------------------------------------------------
		}
		
		//exit;
		
		$where = 'DATE(generate_datetime) < "'.$date_before_2days.'" AND status = "0"';
		$this->model->deleteCoupon($where);
    }
	
}
