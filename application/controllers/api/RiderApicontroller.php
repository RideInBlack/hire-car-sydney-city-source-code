<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class RiderApicontroller extends CI_Controller 
{
	public $inputparam = array();
	
    public function __construct() 
    {
        parent::__construct();
		
		$this->load->helper('api_helper');
        $this->load->model('api/riderapi_model', 'model');
    }
	
	public function __destruct()
	{
		$this->inputparam = array();
	}
	
	public function index() 
	{
		$this->inputparam = readJSON();
		
		if(isset($this->inputparam['mode']) && !empty($this->inputparam['mode']) && method_exists ($this, $this->inputparam['mode']))
		{
			$function = $this->inputparam['mode'];
			$this->$function();
		}
		else
		{
			writeJSON(array('status' => 0, 'message' => 'Invalid Request'));
		}
	}
	//================================================================================================================
	// Rider API
	//================================================================================================================	
	// API
	// Details: Rider Login
	// Developer: Virag Shah
	public function login()
	{
		$email = checkParam($this->inputparam['email'], 'Email is required.');
		$password = checkParam($this->inputparam['password'], 'Password is required.');
		
		$device_type = $this->inputparam['deviceType'] ? $this->inputparam['deviceType'] :"0";
		$device_token = ($this->inputparam['deviceToken']) ? $this->inputparam['deviceToken'] :"";
		
		$rider_details = $this->model->rider_authentication($email, $password);
		
		if($rider_details != false)
		{
			// Update Device token and Device type
			$fields = array('device_type' => (string)$device_type, 'device_token' => $device_token);
			$this->model->edit_profile($fields, $rider_details->rider_id);
			
			$rider_data = array('riderId' => (int)$rider_details->rider_id,
								'fName' => (string)$rider_details->first_name,
								'lName' => (string)$rider_details->last_name,
								'email' => (string)$rider_details->email_id,
								'mobile' => (string)$rider_details->mobile_number,
								);
			
			writeJSON(array('status' => 1, 'riderDetails' => $rider_data));
		}
		else 
		{
			writeJSON(array('status' => 0));
		}
	}
	//----------------------------------------------------------------------------------------------------------------
	// API
	// Details: Rider Logout
	// Developer: Virag Shah
	public function logout()
	{
		$rider_id = checkParam($this->inputparam['riderId'], 'Rider Id is required.');
		
		// Remove Device token and Device type
		$fields = array('device_type' => '0', 'device_token' => '');
		$rider_id = $this->model->edit_profile($fields, $rider_id);
		
		if($rider_id > 0)
		{
			writeJSON(array('status' => 1, 'message' => "User logged out successfully."));
		}
		else 
		{
			writeJSON(array('status' => 0));
		}
		
	}
	//----------------------------------------------------------------------------------------------------------------
	// API
	// Details: Rider Forgot Password
	// Developer: Virag Shah
	public function forgotpassword()
	{
		$email = checkParam($this->inputparam['email'], 'Email is required.');
		
		$rider_details = $this->model->check_rider_email($email);
		
		if($rider_details != false)
		{
			$message  = "Hello, ".$rider_details->first_name."<br/><br/>";
			$message .= "Your Account Password.<br/>";
			$message .= "<b>Password:</b> " . $rider_details->password;
			$message .= "<br/><br/>--<br/>";
			$message .= "Thank You";
			
			$this->load->library('email');
			$this->email->from('admin@adminhirecarsydneycity.com', "Hire Car Sydney City");
			$this->email->to($rider_details->email_id);
			$this->email->subject("[Hire Car Sydney City] Rider: Forgot Password");
			$this->email->message($message);
			$this->email->send();
			
			writeJSON(array('status' => 1));
		}
		else
		{
			writeJSON(array('status' => 0));
		}
	}
	//----------------------------------------------------------------------------------------------------------------
	// API
	// Details: Rider Registration
	// Developer: Virag Shah
	public function signup()
	{
		$first_name = checkParam($this->inputparam['fName'], 'First Name is required.');
		$last_name  = checkParam($this->inputparam['lName'], 'Last Name is required.');
		$email      = checkParam($this->inputparam['email'], 'Email Id is required.');
		$password   = checkParam($this->inputparam['password'], 'Password is required.');
		$mobile     = checkParam($this->inputparam['mobile'], 'Mobile Numer is required.');
		
		$device_type  = ($this->inputparam['deviceType']) ? $this->inputparam['deviceType'] :"0";
		$device_token = ($this->inputparam['deviceToken']) ? $this->inputparam['deviceToken'] :"";
		
		$is_available = $this->model->check_unique('email_id', $email); // true = Available OR false = Not Available
		
		if($is_available == true)
		{
			writeJSON(array('status' => 0, "message" => "This email id is already registered."));
		}
		else
		{
			$fields = array('first_name' => (string)$first_name,
							'last_name' => (string)$last_name,
							'email_id' => (string)$email,
							'mobile_number' => (string)$mobile,
							'password' => (string)$password,
							'device_type' => (string)$device_type, 
							'device_token' => (string)$device_token
							);
			
			$rider_id = $this->model->add($fields);
			
			if($rider_id > 0)
			{
				$rider_details = $this->model->fetch_details($rider_id);
				
				$rider_data = array('riderId' => (int)$rider_details->rider_id,
									'fName' => (string)$rider_details->first_name,
									'lName' => (string)$rider_details->last_name,
									'email' => (string)$rider_details->email_id,
									'mobile' => (string)$rider_details->mobile_number,
									);
				
				writeJSON(array('status' => 1, 'riderDetails' => $rider_data));
			}
			else
			{
				writeJSON(array('status' => 0, 'message' => "Sorry we are unable to register, please try after some time."));
			}
			
			
		}
	}
	//----------------------------------------------------------------------------------------------------------------
	// API
	// Details: Edit Rider Details
	// Developer: Virag Shah
	public function editProfile()
	{
		$rider_id   = checkParam($this->inputparam['riderId'], 'Rider Id is required.');
		$first_name = checkParam($this->inputparam['fName'], 'First Name is required.');
		$last_name  = checkParam($this->inputparam['lName'], 'Last Name is required.');
		$email      = checkParam($this->inputparam['email'], 'Email Id is required.');
		$mobile     = checkParam($this->inputparam['mobile'], 'Mobile Numer is required.');
		
		$is_available = $this->model->check_unique('email_id', $email, ' rider_id != '.$rider_id); // true = Available OR false = Not Available
		
		if($is_available == true)
		{
			writeJSON(array('status' => 0, "message" => "This email id is already registered."));
		}
		else
		{
			$fields = array('first_name' => (string)$first_name,
							'last_name' => (string)$last_name,
							'email_id' => (string)$email,
							'mobile_number' => (string)$mobile,
							);
			
			$rider_id = $this->model->edit_profile($fields, $rider_id);
			
			if($rider_id)
			{
				$rider_details = $this->model->fetch_details($rider_id);
				
				$rider_data = array('riderId' => (int)$rider_details->rider_id,
									'fName' => (string)$rider_details->first_name,
									'lName' => (string)$rider_details->last_name,
									'email' => (string)$rider_details->email_id,
									'mobile' => (string)$rider_details->mobile_number,
									);
				
				writeJSON(array('status' => 1, 'riderDetails' => $rider_data));
			}
			else
			{
				writeJSON(array('status' => 0, 'message' => "Sorry we are unable to register, please try after some time."));
			}
		}
	}
	//----------------------------------------------------------------------------------------------------------------
}
