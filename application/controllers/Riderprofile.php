<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class RiderProfile extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        check_rider_login();
    }
	
    public function index()
    {
        //echo '<pre>';print_r($_SESSION);echo '</pre>';
        
        $riderId = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
        $this->load->model('rider_model');
        $data['rider_details'] = $this->rider_model->fetch_details($riderId);
        $this->load->model('driver_model');
        $type = 2;
        $data['card_details'] = $this->driver_model->fetch_creditcard($riderId,$type);
        
        // Load Ride Details
        $this->load->model('ride_model');
        $data['ride_details'] = $this->ride_model->fetch_rides('ride.rider_id', $riderId);
        
		$this->load->model('region_model');
		$data['city_list']  = $this->region_model->get_all_city();
		$data['state_list'] = $this->region_model->get_all_state();
		
        $page_details['title'] = "Profile";
        
        $this->load->view('web/template/header', $page_details);
        $this->load->view('web/riderprofile/index', $data);
        $this->load->view('web/template/footer');
    }
    
    public function editprofile()
    {
        $this->load->model('rider_model');
        $id = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
        $data['rider_details'] = $this->rider_model->fetch_details($id);
        $riderDuplication = $this->rider_model->check_unique('email_id', $this->input->post('r_email'), "rider_id != '".$id."'");
        if (empty($riderDuplication)) 
        {
            if($this->input->post())
            {
                $fields = array("first_name" => trim($this->input->post('r_fname')),
                    "last_name" => trim($this->input->post('r_lname')),
                    "email_id" => trim($this->input->post('r_email')),
                    "mobile_number" => trim($this->input->post('r_mobile')),
					"language" => trim($this->input->post('r_language')),
					"address" => trim($this->input->post('address')),
					//"city" => trim($this->input->post('city')),
					"suburb" => trim($this->input->post('suburb')),
					"state" => trim($this->input->post('state')),
					"zipcode" => trim($this->input->post('postal')),
                    "update_datetime" => date('Y-m-d H:i:s', time())
                    );

                $rider_id = $this->rider_model->edit($fields, $id);

                if($rider_id)
                {
                    $this->session->set_flashdata('SUCC_MESSAGE', 'Profile Details Saved Successfully.');
                                    $_SESSION['HIRE_CAR']['RIDER'] = $data['rider_details'];

                }
                else
                {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error to edit Profile details.');
                }
            }
	} else {
            $data['rider_details'] = $this->rider_model->fetch_details($id);			
            $this->session->set_flashdata('ERR_EDIT_DRIVER_EMAILDUPLICATION', 'Email id is already exist.');
	}
        redirect('riderprofile');
    }

    public function changepassword()
    {
        $this->load->model('rider_model');
        $id = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
        $rider_details = $this->rider_model->fetch_password($id);

        if($this->input->post()) {
            if($rider_details->password == $this->input->post('oldpassword')) {
                $fields = array(
                        "password" => $this->input->post('newpassword'),
                        "update_datetime" => date('Y-m-d H:i:s', time())
                        );

                $rider_id = $this->rider_model->edit($fields, $id);

                if($rider_id) {
                        $this->session->set_flashdata('SUCC_MESSAGE', 'Your Password has been changed Successfully.');
                } else {
                        $this->session->set_flashdata('ERR_MESSAGE', 'Error to update password.');
                }
            } else {
                    $this->session->set_flashdata('ERR_PASSWORD_MESSAGE', 'Please enter valid Old password.');
            }
        }
	redirect('riderprofile');
    }
	
    public function updatecreditcard()
    {
        $this->load->model('driver_model');
		$this->load->helpers('encryption_helper');
        $id = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
        $type = 2;
        $credit_details = $this->driver_model->fetch_creditcard($id,$type);
        $fields = array(
                        "user_id" => $id,
                        "card_no" => encrypt($this->input->post('cardno')),
                        "exp_month" => $this->input->post('month'), 
                        "exp_year" => $this->input->post('year'),
                        "ccv_no" => $this->input->post('ccvno'),
                        "name_of_card" => $this->input->post('name_of_card'),
                        "card_type" => $this->input->post('card_type'),
                        "user_type" => 2 ,//1 => Driver 2 => Rider
                        "add_datetime" => date('Y-m-d H:i:s', time()),
                        "update_datetime" => date('Y-m-d H:i:s', time())
                );
		
        if($credit_details == '') {   
            $rider_id = $this->driver_model->addcard($fields);
        } else {
            $rider_id = $this->driver_model->editcard($fields, $id);
	}
		
        $this->session->set_flashdata('SUCC_CARD_MESSAGE', 'Your Credit Card details has updated Successfully.');
        redirect('riderprofile');
    }
    
    public function couponhistory()
    {
        $riderId = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
        $this->load->model('rider_model');
        $data['rider_details'] = $this->rider_model->fetch_details($riderId);
        //$data['state_list'] = $this->region_model->get_all_state();
		$this->load->model('region_model');
		$data['city_list']  = $this->region_model->get_all_city();
		$data['state_list'] = $this->region_model->get_all_state();
		
        $this->load->model('giftcard_coupon_model');
        
        $data['coupon_details'] = $this->giftcard_coupon_model->get_rider_coupon($_SESSION['HIRE_CAR']['RIDER']->rider_id);
        
		$page_details['title'] = "Your Gift Cards";
		
        $this->load->view('web/template/header');
        $this->load->view('web/riderprofile/history', $data);
        $this->load->view('web/template/footer');
    }
    
	public function deleteAccount() {
		$this->load->model('rider_model');
		$id = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
		
		$fields = array("status" => '2');
		
		$rider_id = $this->rider_model->edit($fields, $id);
		
		redirect(base_url('logout'));
	}
	
   /* 
    public function test()
    {
        $this->load->view('web/test');
    }*/
}
