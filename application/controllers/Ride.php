<?php

/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Ride extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // Load Ride Model
        $this->load->model('ride_model', 'model');
    }

    public function search_ride($id = 0, $city_id = 0) {
        if ($id != 0 && $city_id != 0) {
            $data['rides'] = $this->model->search_ride($id, $city_id);
        } else {
            $data['rides'] = $this->model->search_ride();
        }

        //echo '<pre>'; print_r($data); echo '</pre>';
        if (count($data['rides']) > 0) {
            $this->load->view('web/ride/search', $data);
        } else {
            echo '';
        }
    }
	
	public function storeRideData() {
		
		$_SESSION['RIDE'] = array(
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'pickup_location' => $this->input->post('pick-address'),
                /*'pickup_city' => $this->input->post('pick-city'),
                'pickup_postal' => $this->input->post('pick-postal'),*/
                'destination_location' => $this->input->post('drop-address'),
                /*'destination_city' => $this->input->post('drop-city'),
                'destination_postal' => $this->input->post('drop-postal'),*/
                'pickup_date'  => $this->input->post('pick-up-date'),
                'pickup_time'  => $this->input->post('pick-up-time'),
                'event_type'   => $this->input->post('event'),
                'number_passengers'   => $this->input->post('number_passengers'),
                'special_requirement' => $this->input->post('special_requirement'),
                'car_category_id' => $this->input->post('selected-car')
                );
		exit;
	}

    public function book_details($ride_id = 0) {
		if(isset($_GET['ride']) && base64_decode($_GET['ride']) == 'new') {
			unset($_SESSION['RIDE']);
		}
		
        //echo uri_string(); exit;
//        if(!isset($_SESSION['RIDE']))
//        {
//            redirect(base_url());
//        }
        //echo '<pre>'; print_r($_SESSION); echo '</pre>'; exit;
        //$ride_id = $_SESSION['RIDE']['selected_ride'];
        // Get Car details
//        if($_SESSION['RIDE']['selected_ride'] != 0) {
//            $this->load->model('car_model');
//            $data['car_details'] = $this->car_model->fetch_details($ride_id);
//            $data['car_images']  = $this->car_model->fetch_car_images($ride_id);
//        }
        // For View Ride details
        if ($ride_id != 0) {
            $this->load->model('ride_model');
            $ride_details = $this->ride_model->fetch_ride($ride_id);
			
			if(!$ride_details) {
				redirect(base_url('ride/book_details'));
			}
			
            //echo '<pre>'; print_r($ride_details);exit;
            $data['ride_id'] = $ride_id;
            $data['ride_details'] = $ride_details;

            $this->load->model('payment_model');
            $data['payment_details'] = $this->payment_model->fetch_ride_payment($ride_id);
            //echo '<pre>'; print_r($payment_details);exit;
        } else {
            $data['ride_id'] = 0;
        }

        // Get Setting Details
        $this->load->model('setting_model');
        $data['setting'] = $this->setting_model->fetch_details(1);

        $this->load->model('event_model');
        $data['event_details'] = $this->event_model->get_all_event();

        // For fetch All Car
        $this->load->model('car_model');
        $data['cars'] = $this->car_model->fetch_all();

        // Get Source and Destination
        $this->load->model('region_model');
        //$data['source'] = $this->region_model->get_city_name($_SESSION['RIDE']['pickup_location']);
        //$data['destination'] = $this->region_model->get_city_name($_SESSION['RIDE']['destination_location']);

		if($ride_id == 0) {
			$header_details['title'] = "Book your ride";
		}
		else {
			$header_details['title'] = "Booking details";
		}
		
		// Get bank details from setting
		$this->load->model('setting_model');
		$data['setting_details'] = $this->setting_model->fetch_details(1);

        $this->load->view('web/template/header', $header_details);
        $this->load->view('web/ride/book_detail', $data);
        $this->load->view('web/template/footer');
    }

    public function save_ride() {
        //echo '<pre>'; print_r($_SESSION); echo '</pre>';
        //echo '<pre>'; print_r($_POST); echo '</pre>'; exit;
        // Save Ride Details in Databases with details

        $trip_datetime = $this->input->post('pick-up-date') . ' ' . $this->input->post('pick-up-time');
        //DateTime::createFromFormat('d/m/Y h:i: A', $dateInput)->format('Y-m-d');
        $new_trip_datetime = DateTime::createFromFormat('d/m/Y h:i A', $trip_datetime)->format('Y-m-d H:i:s');

        if ($this->input->post()) {
            if (isset($_SESSION['HIRE_CAR']['RIDER']) && !empty($_SESSION['HIRE_CAR']['RIDER']->rider_id)) {
                $fields = array(
                    'rider_id' => $_SESSION['HIRE_CAR']['RIDER']->rider_id,
                    'driver_id' => 0,
                    'car_category_id' => $this->input->post('selected-car'),
                    'pickup_location' => $this->input->post('pick-address'),
                    /*'pickup_city' => $this->input->post('pick-city'),
                    'pickup_postal' => $this->input->post('pick-postal'),*/
                    'destination_location' => $this->input->post('drop-address'),
                    /*'destination_city' => $this->input->post('drop-city'),
                    'destination_postal' => $this->input->post('drop-postal'),*/
                    'pickup_datetime' => $new_trip_datetime,
                    'ride_request_datetime' => date('Y-m-d H:i:s', time()),
                    'ride_status' => '0',
                    'event_id' => $this->input->post('event'),
                    'estimated_distance' => (float) $this->input->post('estimated_distance'),
                    'estimated_fare' => (float) $this->input->post('estimated_fare'),
                    'actual_distance' => (float) $this->input->post('estimated_distance'),
                    'actual_fare' => (float) $this->input->post('estimated_fare'),
                    'special_requirement' => $this->input->post('special_requirement'),
                    'no_of_passenger' => $this->input->post('number_passengers'),
                );
            } else {
				$email =  trim($this->input->post('email'));
				
				$this->load->model('rider_model');
				$rider_id = $this->rider_model->getRiderIdFromEmail($email);
				
				if($ride_id != false) {
					$rider_as_id = $rider_id;
				}
				else {
					$rider_as_id = 0;
				}
				
                $fields = array(
                    'rider_id' => $rider_as_id,
                    'driver_id' => 0,
                    'name' => trim($this->input->post('name')),
                    'email' => trim($this->input->post('email')),
                    'phone' => trim($this->input->post('phone')),
                    'car_category_id' => $this->input->post('selected-car'),
                    'pickup_location' => trim($this->input->post('pick-address')),
                    /*'pickup_city' => trim($this->input->post('pick-city')),
                    'pickup_postal' => $this->input->post('pick-postal'),*/
                    'destination_location' => trim($this->input->post('drop-address')),
                    /*'destination_city' => trim($this->input->post('drop-city')),
                    'destination_postal' => trim($this->input->post('drop-postal')),*/
                    'pickup_datetime' => $new_trip_datetime,
                    'ride_request_datetime' => date('Y-m-d H:i:s', time()),
                    'ride_status' => '0',
                    'event_id' => trim($this->input->post('event')),
                    'estimated_distance' => (float) $this->input->post('estimated_distance'),
                    'estimated_fare' => (float) $this->input->post('estimated_fare'),
                    'actual_distance' => (float) $this->input->post('estimated_distance'),
                    'actual_fare' => (float) $this->input->post('estimated_fare'),
                    'special_requirement' => $this->input->post('special_requirement'),
                    'no_of_passenger' => $this->input->post('number_passengers'),
                );
				
            }

            // Save Data
            $ride_id = $this->model->book_ride($fields);
            unset($_SESSION['RIDE']);

            // Email to admin for New Ride Booked.
            //------------------------------------------------------------------
            // Email to All Admin
            //------------------------------------------------------------------
            // Load email library for send email for booking ride
			
			$name = $email = $phone = "";
			     
                $pickupdate = $this->input->post('pick-up-date');
                $pickuptime = $this->input->post('pick-up-time');
                $event = $this->input->post('event');
                $number_passengers = $this->input->post('number_passengers');
                $special_requirement = $this->input->post('special_requirement');
                $selectedcar = $this->input->post('selected-car');
                $estimated_distance = $this->input->post('estimated_distance');
                $estimated_fare = $this->input->post('estimated_fare');

			if(isset($_SESSION['HIRE_CAR']['RIDER']->rider_id)) {
				$name = $_SESSION['HIRE_CAR']['RIDER']->first_name;
				$email = $_SESSION['HIRE_CAR']['RIDER']->email_id;
				$phone = $_SESSION['HIRE_CAR']['RIDER']->mobile_number;

			}
			else {
				$name = $this->input->post('name');
				$email = $this->input->post('email');
				$phone = $this->input->post('phone');
			}
			
            $this->load->library('email');
            $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
			
			//$this->email->from("bipin.patel@ifuturz.com", "Hire Car Sydney City - Team");

            $this->load->model('user_model');   // Load Admin Model
            $admin_email = $this->user_model->get_admin_email();

            foreach ($admin_email AS $emails) {
                $email_array[] = $emails->email_id;
            }
            

            $this->db->select('car_id,model_name');
            $this->db->where('car_id',$selectedcar);
            $carSelected = $this->db->get('car')->result();
		    $carName = $carSelected[0]->model_name ;

            $this->db->select('event_id,event_name');
            $this->db->where('event_id',$event);
            $eventsSelected = $this->db->get('events')->result();
            $eventName = $eventsSelected[0]->event_name ;

            //$this->email->to($email_array);
			
			$New_adminEmail = "admin@hirecarsydneycity.com.au";

			//echo $New_adminEmail;die;
			//$this->email->to($email_array);
			$this->email->to($New_adminEmail);
			
            $this->email->subject("Ride Book Details");

            $message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="http://hirecarsydneycity.com.au/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                       <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                        <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                        <div style='padding-left:10px;'>
                        <p>Dear Admin,</p>
                        <p>New Ride is Booked.</p>
                        <p><b>Name:</b> " . $name . "</p>
                        <p><b>Email:</b> " . $email . "</p>
                        <p><b>Phone:</b> " . $phone . "</p><hr>
                        <p><b>Ride Details</b></p>
                        <p><b>Pickup Date:</b> " . $pickupdate . "</p>
                        <p><b>Pickup Time:</b> " . $pickuptime . "</p>
                        <p><b>Number Passengers:</b> " . $number_passengers . "</p>
                        <p><b>Special Requirement:</b> " . $special_requirement . "</p>
                        <p><b>Car:</b> " . $carName . "</p>
                        <p><b>Estimated Distance:</b> " . $estimated_distance . "</p>
                        <p><b>Estimated Fare:</b> " . $estimated_fare . "</p>
                        <p><b>Event Type:</b> " . $eventName . "</p>
                        <p><b>Pick Up Location:</b> " . trim($this->input->post('pick-address')) ."</p>
                        <p><b>Pick Up Date time:</b> " . date('d/m/Y @ h:i A', strtotime($new_trip_datetime)) . "</p>
                        <p><b>Drop Off Location:</b> " . trim($this->input->post('drop-address')) ."</p><hr>
                        </p>
                        <p>--</p>
                        <p>Regards,<br />
                        <b>Hire Car Sydney City Team<b></p>
                        </div>
                        </div>
                        </div>";

			//$message1 = " this is testing data";
				
            $this->email->message($message);
            $this->email->send();
            //----------------------------------------------------------------------------------
            /*$this->email->from('admin@hirecarsydneycity.com', "Hire Car Sydney City - Team");
            $this->email->to($this->input->post('email'));
            $this->email->subject("Ride Booked Successfully");

            $message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="' . base_url() . 'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                <div style='padding-left:10px;'>
                <p>Dear " . trim($this->input->post('name')) . ",</p>
                <p>Your ride is booked successfully.</p>
                <p><b>Pick Up Location:</b> " . trim($this->input->post('pick-address')) . ", " . trim($this->input->post('pick-city')) . "</p>
                <p><b>Pick Up Date time:</b> " . date('d/m/Y @ h:i A', strtotime($new_trip_datetime)) . "</p>
                <p><b>Drop Off Location:</b> " . trim($this->input->post('drop-address')) . ", " . trim($this->input->post('drop-city')) . "</p><hr>
                </p>
                <p>--</p>
                <p>Regards,<br />
                <b>Hire Car Sydney City Team<b></p>
                </div>
                </div>
                </div>";
				
            $this->email->message($message);
            $this->email->send();*/
            
			//------------------------------------------------------------------------------------------------------
        }

        //echo '<pre>'; print_r($_POST); echo '</pre>';exit;

        /* if($this->input->post('book-n-pay'))
          {
          // Selected Car Id
          $car_id = $_SESSION['RIDE']['selected_ride'];
          $this->payment($car_id, $ride_id, $this->input->post('estimated_fare'));
          } */

        $amount = floatval($this->input->post('estimated_fare'));
        $coupon_id = 0;

        if (isset($_POST['paymentSubmit'])) {

            // for coupon code details check
            if ($this->input->post("coupon_code") != "") {
                $amount = 0;
               
			    $this->load->model('giftcard_coupon_model');
				
				
				

                $coupon_details = $this->giftcard_coupon_model->get_coupon_details($this->input->post("coupon_code"));
//print_r($coupon_details);exit;
                // check coupon code is valid
                if (isset($coupon_details->coupon_id) && $coupon_details->coupon_id > 0 && $coupon_details->remaining_amount > 0) {
                    //  estimated fare is grater than coupon value 
                    $coupon_value = "";

                    if ((int) $this->input->post('estimated_fare') > $coupon_details->remaining_amount) {
                        $coupon_value = $coupon_details->remaining_amount;
                        $coupon_details_array = array("remaining_amount" => 0);
                        $amount = floatval($this->input->post('estimated_fare')) - $coupon_details->remaining_amount;
                    } else {
                        $coupon_value = floatval($this->input->post('estimated_fare'));
                        $coupon_details_array = array("remaining_amount" => $coupon_details->remaining_amount - floatval($this->input->post('estimated_fare')));

                        $amount = 0;
                    }

                    $this->load->model('payment_model');
					
					if($amount <= 0) {
						$coupon_id = $this->payment_model->add(array('ride_id' => $ride_id, "amount" => $coupon_value, 'coupon_id' => $coupon_details->coupon_id, "payment_type" => "Gift Card", "status" => '1', "payment_datetime" => date('Y-m-d H:i:s', time())));
						
						// Cut amount from coupon remaining amount
						$coupon_details_array = array('remaining_amount' => $coupon_details->remaining_amount - $coupon_value);

                		$this->giftcard_coupon_model->edit($coupon_details_array, $coupon_details->coupon_id);
						
						$this->session->set_flashdata('ride_saved', '1');
						$this->invoice_mail($ride_id);
						redirect(base_url('riderprofile'));
					}
					else {
                    	$coupon_id = $this->payment_model->add(array('ride_id' => $ride_id, "amount" => $coupon_value, 'coupon_id' => $coupon_details->coupon_id, "payment_type" => "Gift Card", "status" => '0', "payment_datetime" => date('Y-m-d H:i:s', time())));
					}
                    //$this->giftcard_coupon_model->edit($coupon_details_array, $coupon_details->coupon_id);
                }
                //exit;
            }

            // Via Pay pal
            if ($this->input->post('payOption') == 'paypal') {
                if ($amount > 0) {
                    //$car_id = $this->input->post('selected-car');
                    //$this->payment($car_id, $ride_id, $amount);
                    $this->paymentPayPalNew($ride_id, $amount, $coupon_id);
                } else {
                    $this->session->set_flashdata('ride_saved', '1');
                    redirect(base_url('ride/riderprofile'));
                }
			}
			else if($this->input->post('payOption') == 'cc') { // Via cradit card 
                if ($amount > 0) {
                    $this->paymentCraditCardNew($ride_id, $amount, $coupon_id);
					redirect(base_url('riderprofile'));
                } else {
                    $this->session->set_flashdata('ride_saved', '1');
                    redirect(base_url('ride/riderprofile'));
                }
            }
			else { // pay via bank deposit
				// $amount, $coupon_id
				if($coupon_id != 0)
				{
					$coupon_payment_details = $this->payment_model->fetch_details($coupon_id);
					
					// Check if pay by coupon also than also check that coupon payment is received
					if ($coupon_payment_details != false) 
					{
						//print_r($payment_details);exit;
						// Get the gift card details
						$this->load->model('giftcard_coupon_model');
						$coupon_details = $this->giftcard_coupon_model->details($coupon_payment_details->coupon_id);
						
						if($coupon_details->remaining_amount > 0)
						{
							$coupon_details_array = array('remaining_amount' => $coupon_details->remaining_amount - $coupon_payment_details->amount);
							$this->giftcard_coupon_model->edit($coupon_details_array, $coupon_payment_details->coupon_id);
							
							$fields = array('status' => '1', 'payment_datetime' => date('Y-m-d H:i:s', time()));
							$payment_id = $this->payment_model->edit($fields, $coupon_id);
						}
					}
				}
				
				$fields = array('pay_by_deposit' => '1');
				$ride_edit = $this->model->edit_ride_details($fields, $ride_id);
				
				$this->invoice_mail($ride_id);
			
				// Get bank details from setting
				$this->load->model('setting_model');
				$setting_details = $this->setting_model->fetch_details(1);
			
				$msg = "<p>Bank Details are.</p>
                 	   <p><b>Bank:</b> ".$setting_details->setting_bank."</p>
                 	   <p><b>Account Name:</b> ".$setting_details->setting_account_name."</p>
					   <p><b>BSB:</b> ".$setting_details->setting_bsb."</p>
					   <p><b>Account Number:</b> ".$setting_details->setting_account_number."</p>
					   <p>Please deposit giftcard amount in bank for complete booking of you ride.</p>";
			
            	$this->session->set_flashdata('PAY_BY_DEPOSIT', $msg);
				
				redirect(base_url('riderprofile'));
			}
        } else {
			$this->invoice_mail($ride_id);
			$this->session->set_flashdata('RIDE_SAVED', '1');
            
			if (isset($_SESSION['HIRE_CAR']['RIDER']) && !empty($_SESSION['HIRE_CAR']['RIDER']->rider_id)) {
				redirect('riderprofile');
			}
			else {
				redirect('ride/ride_saved');
			}
        }
    }

    // remaining
    public function payForRide($ride_id) {
        if (isset($_POST['paymentSubmit'])) {

            // for coupon code details check
            if ($this->input->post("coupon_code") != "") {
                $amount = 0;
                $this->load->model('giftcard_coupon_model');

                $coupon_details = $this->giftcard_coupon_model->get_coupon_details($this->input->post("coupon_code"));

                // check coupon code is valid
                if (isset($coupon_details->coupon_id) && $coupon_details->coupon_id > 0 && $coupon_details->remaining_amount > 0) {
                    //  estimated fare is grater than coupon value 
                    $coupon_value = "";

                    if (floatval($this->input->post('due_amount')) > $coupon_details->remaining_amount) {
                        $coupon_value = $coupon_details->remaining_amount;
                        $coupon_details_array = array("remaining_amount" => 0);
                        $amount = floatval($this->input->post('due_amount')) - $coupon_details->remaining_amount;
                    } else {
                        $coupon_value = floatval($this->input->post('estimated_fare'));
                        $coupon_details_array = array("remaining_amount" => $coupon_details->remaining_amount - floatval($this->input->post('due_amount')));

                        $amount = 0;
                    }

                    $this->load->model('payment_model');

                    $coupon_id = $this->payment_model->add(array('ride_id' => $ride_id, "amount" => $coupon_value, "payment_type" => "Gift Card", "status" => '1', "payment_datetime" => date('Y-m-d H:i:s', time())));

                    //$this->giftcard_coupon_model->edit($coupon_details_array, $coupon_details->coupon_id);
                    //echo 'hi'; exit;
                }
                //exit;
            }

            if ($this->input->post('payOption') == 'paypal') {
                //echo $amount." > 0"; exit;
                if ($amount > 0) {
                    $car_id = $this->input->post('selected-car');
                    $this->paymentPayPalNew($ride_id, $amount, $coupon_id);
                } else {
                    $this->session->set_flashdata('ride_saved', '1');
                    redirect(base_url('ride/riderprofile'));
                }
            } 
			else if($this->input->post('payOption') == 'deposit') {
				
			}
			else { // Via cradit card 
                if ($amount > 0) {
					$this->paymentCraditCardNew($ride_id, $amount, $coupon_id);
                }
                else {
                    $this->session->set_flashdata('ride_saved', '1');
                    redirect(base_url('ride/riderprofile'));
                }
            }
        }
    }

    public function invoice() {
        $this->load->view('web/ride/invoice');
    }

    public function ride_saved() {
        if (!$this->session->flashdata('RIDE_SAVED')) {
            redirect(base_url());
        }

        $this->load->view('web/template/header');
        $this->load->view('web/ride/save_ride');
        $this->load->view('web/template/footer');
    }

    // Payment Page 
    public function payment($car_id, $ride_id, $amount) {
        $data['ride_id'] = $ride_id;
        $data['amount'] = $amount;

        // Insetrt Before Payment 
        $this->load->model('payment_model');
        $fields = array('ride_id' => $ride_id, 'amount' => $amount, 'payment_datetime' => date('y-m-d H:i:s', time()));
        $payment_id = $this->payment_model->add($fields);

        $data['buisness_id'] = "jitendra.prajapati@ifuturz.com";
        $data['paypal_url'] = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        $data['payment_id'] = $payment_id;

        // Get Car details
        //$this->load->model('car_model');
        //$data['car_details'] = $this->car_model->fetch_details($car_id);
        //echo 'hi'; exit;
        $this->load->view('web/ride/payment', $data);
    }

    // Payment Result
    public function payment_result($result) {
        $payment_id = $_GET['paymentId'];
        $ride_id = $_GET['rideId'];

        $this->load->model('payment_model');

        if ($result == 1) {
            $this->session->set_flashdata('PAYMENT_SUCC', 'Payment is Successful.');

            // Get Ride Information
            $this->load->model('ride_model');
            $ride_details = $this->ride_model->get_ride_details($ride_id);
            //echo '<pre>';print_r($ride);exit;
            // Payment Model for change payment status
            $fields = array('status' => '1', 'payment_datetime' => date('y-m-d H:i:s', time()));
            $payment_id = $this->payment_model->edit($fields, $payment_id);

            // Ride Model for change payment status
            $this->load->model('ride_model');
            // Get Payment Status
            $payment_status = $this->ride_model->get_payment_status($ride_id);

            if ($payment_status == 0) {
                $payment_complete = $this->ride_model->edit_ride_details(array('payment_id' => '1'), $ride_id);
            } else if ($payment_status == 1) {
                $payment_complete = $this->ride_model->edit_ride_details(array('payment_id' => '2'), $ride_id);
            }

            // Car Model for change payment status
            $this->load->model('car_model');
            $payment_complete = $this->car_model->edit(array('is_allocated' => '1'), $ride_id);


            if ($payment_status == 0) {
                // Email to admin for New Ride Booked.
                //------------------------------------------------------------------
                // Email to All Admin
                //------------------------------------------------------------------
                // Load email library for send email for booking ride
                $this->load->library('email');
                $this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");

                $this->load->model('user_model');   // Load Admin Model
                $admin_email = $this->user_model->get_admin_email();

                foreach ($admin_email AS $email) {
                    $email_array[] = $email->email_id;
                }

               // $this->email->to($email_array);
			   
			   $New_adminEmail = "admin@hirecarsydneycity.com.au";
			
			//$this->email->to($email_array);
			$this->email->to($New_adminEmail);
			
                $this->email->subject("Ride Book Details");

                $message = "<div style='float:left;min-height:auto;width:100%'>
                    <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                    <a href='" . base_url() . "'>";

               // $message .= '<img src="' . base_url() . 'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
			   
			   $message .= '<img src="http://hirecarsydneycity.com.au/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
                $message .= "</a></div>
                            <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                            <div style='padding-left:10px;'>
                            <p>Dear Admin,</p>
                            <p>New ride is booked.</p><hr>
                            <p><b>Booking Details:</b></p>
                            <p>Rider Name: " . $ride_details->rider_name . " [" . $ride_details->rider_email . "]</p>
                            <p>Ride Date-Time: " . date("d/m/Y h:i:s A", strtotime($ride_details->pickup_datetime)) . "</p>
                            <p>Pick-up City: " . $ride_details->start . "</p>
                            <p>Destination City: " . $ride_details->end . "</p>

                            </p>
                            <p>--</p>
                            <p>Regards,<br />
                            <b>Hire Car Sydney City Team<b></p>
                            </div>
                            </div>
                            </div>";

                $this->email->message($message);
                $this->email->send();
                //------------------------------------------------------------------
                // Email to Rider
                //------------------------------------------------------------------
                /*$this->email->from('admin@hirecarsydneycity.com', "Admin Team");
                $this->email->to($ride_details->rider_email);
                $this->email->subject("Ride Booked Successfully");

                $message = "<div style='float:left;min-height:auto;width:100%'>
                    <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
                    <a href='" . base_url() . "'>";

                $message .= '<img src="' . base_url() . 'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
                $message .= "</a></div>
                            <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                            <div style='padding-left:10px;'>
                            <p>Dear " . $ride_details->rider_name . ",</p>
                            <p>New ride is booked.</p><hr>
                            <p><b>Booking Details:</b></p>
                            <p>Ride Date-Time: " . date("d/m/Y h:i:s A", strtotime($ride_details->pickup_datetime)) . "</p>
                            <p>Pick-up City: " . $ride_details->start . "</p>
                            <p>Destination City: " . $ride_details->end . "</p>

                            </p>
                            <p>--</p>
                            <p>Regards,<br />
                            <b>Hire Car Sydney City Team<b></p>
                            </div>
                            </div>
                            </div>";

                $this->email->message($message);
                $this->email->send();*/
				
				
                //echo $this->email->print_debugger();
                //------------------------------------------------------------------
            }
        } else {
            //echo 'Failed<br/>';
            $this->session->set_flashdata('PAYMENT_ERR', 'Payment is Failed.');
            $fields = array('status' => '0', 'payment_datetime' => date('y-m-d H:i:s', time()));

            $payment_id = $this->payment_model->edit($fields, $payment_id);
        }

        unset($_SESSION['RIDE']);
        //exit;
        redirect(base_url('riderprofile'));
    }

    // Payment New Function from coupon code and other options
    public function payForRideNew($ride_id) {
		$amount = floatval($this->input->post('due_amount'));
        $paid_coupon_id = 0;

        if (isset($_POST['paymentSubmit'])) {
            // for coupon code details check
            if ($this->input->post("coupon_code") != "") {
                $amount = 0;
                $this->load->model('giftcard_coupon_model');

                $coupon_details = $this->giftcard_coupon_model->get_coupon_details($this->input->post("coupon_code"));

                // check coupon code is valid
                if (isset($coupon_details->coupon_id) && $coupon_details->coupon_id > 0 && $coupon_details->remaining_amount > 0) {
                    //  estimated fare is grater than coupon value 
                    $coupon_value = "";

                    if (floatval($this->input->post('due_amount')) > $coupon_details->remaining_amount) {
                        $coupon_value = $coupon_details->remaining_amount;
                        $coupon_details_array = array("remaining_amount" => 0);
                        $amount = floatval($this->input->post('due_amount')) - $coupon_details->remaining_amount;
                    } else {
                        $coupon_value = floatval($this->input->post('estimated_fare'));
                        $coupon_details_array = array("remaining_amount" => $coupon_details->remaining_amount - floatval($this->input->post('due_amount')));

                        $amount = 0;
                    }

                    $this->load->model('payment_model');

					if($amount == 0)
					{
						$paid_coupon_id = $this->payment_model->add(array('ride_id' => $ride_id, "amount" => $coupon_value, "payment_type" => "Gift Card", "coupon_id"=>$coupon_details->coupon_id,  "status" => '1', "payment_datetime" => date('Y-m-d H:i:s', time())));
						
						// Cut amount from coupon remaining amount
						$coupon_details_array = array('remaining_amount' => $coupon_details->remaining_amount - $coupon_value);

                		$this->giftcard_coupon_model->edit($coupon_details_array, $coupon_details->coupon_id); 
						
						redirect(base_url('riderprofile'));
					}
					else {
						$paid_coupon_id = $this->payment_model->add(array('ride_id' => $ride_id, "amount" => $coupon_value, "payment_type" => "Gift Card", "status" => '0', "payment_datetime" => date('Y-m-d H:i:s', time())));
					}
					
                    ////$this->giftcard_coupon_model->edit($coupon_details_array, $coupon_details->coupon_id);
                    //echo 'hi'; exit;
                }
                //exit;
            }

            if ($this->input->post('payOption') == 'paypal') {
                if ($amount > 0) { //echo $amount." > 0"; exit;
                    $this->paymentPayPalNew($ride_id, $amount, $paid_coupon_id);
                }
            } 
			else if($this->input->post('payOption') == 'paypal'){
                if ($amount > 0) { // Via cradit card
					$this->paymentCraditCardNew($ride_id, $amount, $coupon_id);
                }
            }
			else { // pay via bank deposit
				$fields = array('pay_by_deposit' => '1');
				$ride_edit = $this->model->edit_ride_details($fields, $ride_id);
				
				$this->invoice_mail($ride_id);
				
				$this->session->set_flashdata('ride_saved', '1');
                redirect(base_url('riderprofile'));
			}
        }
    }

    // Payment Page New from Paypal
    public function paymentPayPalNew($ride_id, $amount, $coupon_payment_id = 0) {
        $data['ride_id'] = $ride_id;
        $data['amount'] = $amount;
        $data['coupon_payment_id'] = $coupon_payment_id;

        // Insetrt Before Payment
        $this->load->model('payment_model');
        $fields = array('ride_id' => $ride_id, 'amount' => $amount, 'payment_type' => 'PayPal', 'payment_datetime' => date('y-m-d H:i:s', time()));
        $payment_id = $this->payment_model->add($fields);
		
		//admin-facilitator@hirecarsydneycity.com.au
        //$data['buisness_id'] = "jitendra.prajapati@ifuturz.com";
        //$data['paypal_url'] = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        $data['payment_id'] = $payment_id;
		
		// Load Setting Model
		$this->load->model('setting_model');
		// Get Setting details
		$setting_details = $this->setting_model->fetch_details('1');
		
		if($setting_details->setting_paypal_mode == "live") {
			$data['paypal_url'] = "https://www.paypal.com/cgi-bin/webscr";
			$data['buisness_id'] = $setting_details->setting_paypal_email_id_live;
		}
		else {
			$data['paypal_url'] = "https://www.sandbox.paypal.com/cgi-bin/webscr";
			$data['buisness_id'] = $setting_details->setting_paypal_email_id;
		}
		
        // Load View
        $this->load->view('web/ride/paymentNew', $data);
    }
	
	public function paypal_ipn()
	{
		$payment_id = $_GET['paymentId'];
        $ride_id = $_GET['rideId'];
        $coupon_payment_id = $_GET['coupon_payment_id'];
		
		// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
		// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
		// Set this to 0 once you go live or don't require logging.
		define("DEBUG", 1);
		// Set to 0 once you're ready to go live
		
		// Load Setting Model
		$this->load->model('setting_model');
		// Get Setting details
		$setting_details = $this->setting_model->fetch_details('1');
		
		if($setting_details->setting_paypal_mode == "live") {
			define("USE_SANDBOX", 0);
		}
		else {
			define("USE_SANDBOX", 1);
		}
		
		
		//define("USE_SANDBOX", 1);
		define("LOG_FILE", "./ipn.log");
		// Read POST data
		// reading posted data directly from $_POST causes serialization
		// issues with array data in POST. Reading raw POST data from input stream instead.
		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
			$keyval = explode ('=', $keyval);
			
			if (count($keyval) == 2)
				$myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		
		foreach ($myPost as $key => $value) {
			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}
		// Post IPN data back to PayPal to validate the IPN data is genuine
		// Without this step anyone can fake IPN data
		if(USE_SANDBOX == true) {
			$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		} else {
			$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
		}
		
		$ch = curl_init($paypal_url);
		if ($ch == FALSE) {
			return FALSE;
		}
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		if(DEBUG == true) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		}
		// CONFIG: Optional proxy configuration
		//curl_setopt($ch, CURLOPT_PROXY, $proxy);
		//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
		// Set TCP timeout to 30 seconds
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
		// of the certificate as shown below. Ensure the file is readable by the webserver.
		// This is mandatory for some environments.
		//$cert = __DIR__ . "./cacert.pem";
		//curl_setopt($ch, CURLOPT_CAINFO, $cert);
		$res = curl_exec($ch);
		
		if (curl_errno($ch) != 0) // cURL error
		{
			if(DEBUG == true) {	
				error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
		}
		curl_close($ch);
		exit;
		} else {
			// Log the entire HTTP response if debug is switched on.
			if(DEBUG == true) {
				error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
				error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
			}
			curl_close($ch);
		}
		
		// Inspect IPN validation result and act accordingly
		// Split response headers and payload, a better way for strcmp
		$tokens = explode("\r\n\r\n", trim($res));
		$res = trim(end($tokens));
		
		$this->load->model('payment_model');
		
		if (strcmp ($res, "VERIFIED") == 0 && $_POST['payment_status'] == "Completed") {
			
			// Get Ride Information
            $this->load->model('ride_model');
            $ride_details = $this->ride_model->get_ride_details($ride_id);
            //echo '<pre>';print_r($ride);exit;
            // Payment Model for change payment status
            $fields = array('status' => '1', 'payment_complete_datetime' => '"'.$_POST['payment_date'].'"', 'txn_id' => (string)$_POST['txn_id']);
			$payment_id = $this->payment_model->edit($fields, $payment_id);
			
			// Get the gift coupon payment details
			$coupon_payment_details = $this->payment_model->fetch_details($coupon_payment_id);
				
            // Check if pay by coupon also than also check that coupon payment is received
            if ($coupon_payment_details != false) 
			{
                //print_r($payment_details);exit;
                // Get the gift card details
                $this->load->model('giftcard_coupon_model');
                $coupon_details = $this->giftcard_coupon_model->details($coupon_payment_details->coupon_id);
				
				if($coupon_details->remaining_amount > 0)
				{
					$coupon_details_array = array('remaining_amount' => $coupon_details->remaining_amount - $coupon_payment_details->amount);
                	$this->giftcard_coupon_model->edit($coupon_details_array, $coupon_payment_details->coupon_id);
					
					$fields = array('status' => '1', 'payment_datetime' => date('Y-m-d H:i:s', time()));
                	$payment_id = $this->payment_model->edit($fields, $coupon_payment_id);
				}
            }

            // Ride Model for change payment status
            $this->load->model('ride_model');
            // Get Payment Status
            $payment_status = $this->ride_model->get_payment_status($ride_id);

            if ($payment_status == 0) {
                $payment_complete = $this->ride_model->edit_ride_details(array('payment_id' => '1'), $ride_id);
            } else if ($payment_status == 1) {
                $payment_complete = $this->ride_model->edit_ride_details(array('payment_id' => '2'), $ride_id);
            }
			
			$this->invoice_mail($ride_id);	
            // Car Model for change payment status
            //$this->load->model('car_model');
            //$payment_complete = $this->car_model->edit(array('is_allocated' => '1'), $ride_id);
		}
		else if (strcmp ($res, "INVALID") == 0) {
			$fields = array('status' => '2', 'payment_complete_datetime' => $_POST['payment_date'], 'txn_id' => $_POST['txn_id']);
			// log for manual investigation
			// Add business logic here which deals with invalid IPN messages
			if(DEBUG == true) {
				error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
			}
		}
		/*$raw_post_data = file_get_contents('php://input');
		$this->load->library('email');
		$this->email->to("virag.shah@ifuturz.com");
		$this->email->from('admin@hirecarsydneycity.com.au');
		$this->email->subject('Paypal IPN'."Paypal IPN");
		$this->email->message($coupon_payment_id.'='.$raw_post_data);
		$this->email->send();*/
	}
	
	public function paypalReturn($result) {
		if ($result == 1) {
            $this->session->set_flashdata('PAYMENT_SUCC', 'Payment is Successful.');
		}
		else {
			$this->session->set_flashdata('PAYMENT_ERR', 'Payment is Failed.');
		}
		
		unset($_SESSION['RIDE']);
        redirect(base_url('riderprofile'));
	}

    public function paymentCraditCardNew($ride_id, $amount, $coupon_payment_id = 0) {
        check_rider_login();
        require(APPPATH . 'libraries/eWAY/RapidAPI.php');

        $this->load->helpers('encryption_helper');

        $rider_id = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
        $this->load->model('driver_model');
        $credit_details = $this->driver_model->fetch_creditcard($rider_id, 2);

        if (!empty($credit_details)) {
            $this->load->helpers('encryption_helper');

            $fields = array(
                'ride_id' => $ride_id,
                'amount' => $amount,
                'payment_type' => 'Credit Card',
                'status' => '0',
                'payment_datetime' => date('Y-m-d H:i:s', time())
            );

            $this->load->model('payment_model');
            $payment_id = $this->payment_model->add($fields);
            // we skip all validation but you should do it in real world
            // Create DirectPayment Request Object
            $request = new eWAY\CreateDirectPaymentRequest();

            // Populate values for Customer Object
            // Note: TokenCustomerID is required when update an exsiting TokenCustomer
            $request->Customer->TokenCustomerID = '';

            $request->Customer->Reference = '';
            $request->Customer->Title = '';
            $request->Customer->FirstName = $_SESSION['HIRE_CAR']['RIDER']->first_name;
            $request->Customer->LastName = $_SESSION['HIRE_CAR']['RIDER']->last_name;
            $request->Customer->CompanyName = '';
            $request->Customer->JobDescription = '';
            $request->Customer->Street1 = '';
            $request->Customer->City = '';
            $request->Customer->State = '';
            $request->Customer->PostalCode = '';
            $request->Customer->Country = '';
            $request->Customer->Email = $_SESSION['HIRE_CAR']['RIDER']->email_id;
            ;
            $request->Customer->Phone = '';
            $request->Customer->Mobile = $_SESSION['HIRE_CAR']['RIDER']->mobile_number;
            $request->Customer->Comments = '';
            $request->Customer->Fax = '';
            $request->Customer->Url = '';

            $request->Customer->CardDetails->Name = $credit_details->name_of_card;
            $request->Customer->CardDetails->Number = decrypt($credit_details->card_no);
            //$request->Customer->CardDetails->Number = '4444333322221111';
            //$request->Customer->CardDetails->ExpiryMonth = '09';
            $request->Customer->CardDetails->ExpiryMonth = $credit_details->exp_month;
            $request->Customer->CardDetails->ExpiryYear = $credit_details->exp_year;
            $request->Customer->CardDetails->StartMonth = '';
            $request->Customer->CardDetails->StartYear = '';
            $request->Customer->CardDetails->IssueNumber = '';
            $request->Customer->CardDetails->CVN = $credit_details->ccv_no;

            // Populate values for ShippingAddress Object.
            // This values can be taken from a Form POST as well. Now is just some dummy data.
            $request->ShippingAddress->FirstName = "";
            $request->ShippingAddress->LastName = "";
            $request->ShippingAddress->Street1 = "";
            $request->ShippingAddress->Street2 = "";
            $request->ShippingAddress->City = "";
            $request->ShippingAddress->State = "";
            $request->ShippingAddress->Country = "";
            $request->ShippingAddress->PostalCode = "";
            $request->ShippingAddress->Email = "";
            $request->ShippingAddress->Phone = "";
            // ShippingMethod, e.g. "LowCost", "International", "Military". Check the spec for available values.
            $request->ShippingAddress->ShippingMethod = "";

            //if ($_POST['ddlMethod'] == 'ProcessPayment' || $_POST['ddlMethod'] == 'Authorise' || $_POST['ddlMethod'] == 'TokenPayment') {
            // Populate values for LineItems
            $item1 = new eWAY\LineItem();
            $item1->SKU = "SKU1";
            $item1->Description = "Description1";
            $item2 = new eWAY\LineItem();
            $item2->SKU = "SKU2";
            $item2->Description = "Description2";
            $request->Items->LineItem[0] = $item1;
            $request->Items->LineItem[1] = $item2;

            // Populate values for Payment Object
            $request->Payment->TotalAmount = $amount;
            $request->Payment->InvoiceNumber = '';
            $request->Payment->InvoiceDescription = '';
            $request->Payment->InvoiceReference = '';
            $request->Payment->CurrencyCode = 'AUD';

            $request->Method = 'Authorise';
            $request->TransactionType = 'Purchase';

            // Call RapidAPI
            $eway_params = array();
            //if ($_POST['ddlSandbox']) {
            //$eway_params['sandbox'] = true;
            //}
			
			//------------------------------------------------------------
			// Load Setting Model
			$this->load->model('setting_model');
			// Get Setting details
			$setting_details = $this->setting_model->fetch_details('1');
			
			if($setting_details->setting_eway_mode == "live") {
				
				$api_key = $setting_details->setting_eway_api_key_live;
				$api_password = $setting_details->setting_eway_password_live;
				
				$eway_params['sandbox'] = false;
			}
			else {
				
				$api_key = $setting_details->setting_eway_api_key;
				$api_password = $setting_details->setting_eway_password;
				
				$eway_params['sandbox'] = true;
			}
			//------------------------------------------------------------
			
            $service = new eWAY\RapidAPI($api_key, $api_password, $eway_params);
            $result = $service->DirectPayment($request);

            // Check if any error returns
            if (isset($result->Errors)) {
                // Get Error Messages from Error Code.
                /* $ErrorArray = explode(",", $result->Errors);
                  $lblError = "";
                  foreach ( $ErrorArray as $error ) {
                  $error = $service->getMessage($error);
                  $lblError .= $error . "<br />\n";
                  }
                  echo $lblError;die; */
                redirect(base_url('ride/payment_result_new/0?paymentId='.$payment_id.'&coupon_payment_id=' . $coupon_payment_id . '&rideId=' . $ride_id));
            } else {
                /* $in_page = 'view_result';
                  echo "<pre>";print_r($result);die; */
                
                redirect(base_url('ride/payment_result_new/1?paymentId='.$payment_id.'&coupon_payment_id=' . $coupon_payment_id . '&rideId=' . $ride_id));
            }
        } else {
            //redirect(base_url('ride/payment_result_new/0?coupon_payment_id=' . $coupon_payment_id . '&rideId=' . $ride_id.'&riderId=' . $_SESSION['HIRE_CAR']['RIDER']->rider_id));
            //redirect(base_url('ride/payment_result_new/1?coupon_payment_id=' . $coupon_payment_id . '&rideId=' . $ride_id));
        }
        
        //$this->load->view('web/ride/paymentCCNew', $data);
		redirect(base_url('riderprofile'));
    }

    // Payment Result 
    public function payment_result_new($result) {
        $payment_id = $_GET['paymentId'];
        $ride_id = $_GET['rideId'];
        $coupon_payment_id = $_GET['coupon_payment_id'];
        
		//echo $result; exit;
		
        $this->load->model('payment_model');

        if ($result == 1) {
            $this->session->set_flashdata('PAYMENT_SUCC', 'Payment is Successful.');

            // Get Ride Information
            $this->load->model('ride_model');
            $ride_details = $this->ride_model->get_ride_details($ride_id);
            //echo '<pre>';print_r($ride);exit;
            // Payment Model for change payment status
            $fields = array('status' => '1', 'payment_datetime' => date('y-m-d H:i:s', time()));
            $payment_id = $this->payment_model->edit($fields, $payment_id);

            // Check if pay by coupon also than also check that coupon payment is received
            if ($coupon_payment_id != 0) {
                // Get the gift coupon payment details
                $this->load->model('payment_model');
                $payment_details = $this->payment_model->fetch_details($coupon_payment_id);

                //print_r($payment_details);exit;
                // Get the gift card details
                $this->load->model('giftcard_coupon_model');
                $coupon_details = $this->giftcard_coupon_model->details($payment_details->coupon_id);

                $coupon_details_array = array('remaining_amount' => $coupon_details->remaining_amount - $payment_details->amount);

                $this->giftcard_coupon_model->edit($coupon_details_array, $payment_details->coupon_id);

                $fields = array('status' => '1', 'payment_datetime' => date('y-m-d H:i:s', time()));
                $payment_id = $this->payment_model->edit($fields, $coupon_payment_id);
            }

            // Ride Model for change payment status
            $this->load->model('ride_model');
            // Get Payment Status
            $payment_status = $this->ride_model->get_payment_status($ride_id);

            if ($payment_status == 0) {
                $payment_complete = $this->ride_model->edit_ride_details(array('payment_id' => '1'), $ride_id);
            } else if ($payment_status == 1) {
                $payment_complete = $this->ride_model->edit_ride_details(array('payment_id' => '2'), $ride_id);
            }
			
			//echo 'hi'; exit;
			$this->invoice_mail($ride_id);	
            // Car Model for change payment status
            //$this->load->model('car_model');
            //$payment_complete = $this->car_model->edit(array('is_allocated' => '1'), $ride_id);
        } else {
            //echo 'Failed<br/>';
            $this->session->set_flashdata('PAYMENT_ERR', 'Payment is Failed.');
            $fields = array('status' => '0', 'payment_datetime' => date('y-m-d H:i:s', time()));

            $payment_id = $this->payment_model->edit($fields, $payment_id);
        }

        unset($_SESSION['RIDE']);
        //exit;
        redirect(base_url('riderprofile'));
    }


	public function invoice_mail($rideid) {
        // Get Ride Details
        $this->load->model('ride_model');
        $ride_details = $this->ride_model->fetch_ride_details($rideid);
        $data['ride_details'] = $ride_details;
        
        // Ride Payment Details
        $data['payment_details'] = $this->ride_model->fetch_payment_rows($rideid);
        
        // Get Rider Details
        if ($ride_details->rider_id > 0 && $ride_details->rider_id != "") {
            // Get Rider Details
            $this->load->model('rider_model');
            $rider_details = $this->rider_model->fetch_details($ride_details->rider_id);

            $rider['name'] = $rider_details->first_name . ' ' . $rider_details->last_name;
            $rider['email'] = $rider_details->email_id;
            $rider['phone'] = $rider_details->mobile_number;
            //print_r($rider_details);exit;
        } else {
            $rider['name'] = $ride_details->name;
            $rider['email'] = $ride_details->email;
            $rider['phone'] = $ride_details->phone;
        }

        // Get All Admin email for email sending
        /*$this->load->model('user_model');   // Load Admin Model
        $admin_email = $this->user_model->get_admin_email();

        foreach ($admin_email AS $email) {
            $admin_emails[] = $email->email_id;
        }*/

        //exit;
        $data['rideid'] = $rideid;
		$data['rider'] = $rider;
		
		// load setting details (Bank and ABN)
		$this->load->model('setting_model');
		$data['setting_details'] = $this->setting_model->fetch_details(1);
		
        //$email = "virag.shah@ifuturz.com";
        $mailtemplate = $this->load->view('web/ride/invoiceMailtempalte', $data, TRUE);

        //------------------------------------------------------
        // Send Email to all admin 
        //------------------------------------------------------
        $this->load->library('email');
        /*$this->email->from('admin@hirecarsydneycity.com', "Hire Car Sydney City - Team");
        $this->email->to($admin_emails);
        $this->email->subject("[Hire Car Sydney City] Invoice ");

        $this->email->message($mailtemplate);
        $this->email->send();*/
        //------------------------------------------------------
        //------------------------------------------------------
        // Send Email to all Rider 
        //------------------------------------------------------
        //$this->load->library('email');
        $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
        $this->email->to($rider['email']);
        $this->email->subject("[Hire Car Sydney City] Invoice ");

        $this->email->message($mailtemplate);
        $this->email->send();
		//anjou143@gmail.com
        //------------------------------------------------------
    }
	
	public function get_fare_details() {
		$this->load->view('web/ride/booking_fare_details');
	}
	
}
