<?php
/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Driverprofile extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
		check_driver_login();
    }
    
    public function index($page = 1)
    {
        //echo "<pre>";print_r($_SESSION['HIRE_CAR']['DRIVER']);echo "</pre>";
        $driverId = $_SESSION['HIRE_CAR']['DRIVER']->driver_id;
        $this->load->model('ride_model');
        $this->load->model('driver_model');
        $this->load->model('region_model');
        $record_per_page = PER_PAGE_RECORD;
        $search_where = "assign_ride.assign_driver_id = '".$driverId."'";
		
		$data['city_list'] = $this->region_model->get_all_city();
		$data['state_list'] = $this->region_model->get_all_state();
		
        $data['driver_details'] = $this->driver_model->fetch_details($driverId);
		
		$data['licence_uploads'] = $this->driver_model->get_uploads($driverId, '0');
		$data['hc_authority_uploads'] = $this->driver_model->get_uploads($driverId, '1');
		$data['registration_uploads'] = $this->driver_model->get_uploads($driverId, '2');
		$data['insurance_uploads'] = $this->driver_model->get_uploads($driverId, '3');
		
        $data['card_details'] = $this->driver_model->fetch_creditcard($driverId,'1');
        $data['ride_details'] = $this->ride_model->rideslistfordriver($record_per_page, ($page-1)*$record_per_page, $search_where);
        //echo "<pre>";print_r($data['ride_details']);echo "</pre>";
        $page_details['title'] = "Profile";
        $this->load->view('web/template/header', $page_details);
        $this->load->view('web/driverprofile/index', $data);
        $this->load->view('web/template/footer');
    }
	
	public function editprofile()
	{
		$this->load->model('driver_model');
		$id = $this->uri->segment(3);
		//echo $id;die;
		if($this->input->post())
        {
			$this->load->model('driver_model');
			$riderDuplication = $this->driver_model->check_unique('email_id', $this->input->post('d_email'), "driver_id != '".$id."'");
			//print_r($riderDuplication);die;
			
			if (empty($riderDuplication)) 
			{
				
				//=========================================================================================
				if(!empty($_FILES['d_driverimage']['name'])) 
				{
					//echo "dfsd";die;
					$target = "./assets/uploads/driver/";
					$filename = pathinfo($_FILES['d_driverimage']['name'],PATHINFO_FILENAME);
					$extension = strtolower(pathinfo($_FILES['d_driverimage']['name'], PATHINFO_EXTENSION));
					$filename = str_replace(' ','_',$filename);
					$newFile = $id.'.'.$extension;
					$filenamewittargetfolder = $target.$newFile;
					if(in_array($extension , array('jpg','jpeg', 'gif', 'png', 'bmp'))) 
					{
						if(move_uploaded_file($_FILES['d_driverimage']['tmp_name'],$filenamewittargetfolder)) {
							$newUploadedFile = $newFile;
						} 
						else {
							$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's Photo uploading. Please try again.");
						}
					} 
					else {
						$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
					}
				} 
				else {
					$newUploadedFile = '';
				}
				//=========================================================================================
				// Upload Multiple Licence Details (licensedetails)
				$total_licence = count($_FILES['licensedetails']['name']);
				
				for($i=0; $i<$total_licence; $i++)
				{
					if(!empty($_FILES['licensedetails']['name'][$i])) {
						$target    = "./assets/uploads/driver/license/";
						$filename  = pathinfo($_FILES['licensedetails']['name'][$i], PATHINFO_FILENAME);
						$extension = strtolower(pathinfo($_FILES['licensedetails']['name'][$i], PATHINFO_EXTENSION));
						$filename  = str_replace(' ', '_', $filename);
						$newFile   = $id.'_'.time().$i.'.'.$extension;
						$filenamewittargetfolder = $target.$newFile;
						
						if(in_array($extension, array('jpg','jpeg', 'gif', 'png', 'bmp'))) {
							if(move_uploaded_file($_FILES['licensedetails']['tmp_name'][$i], $filenamewittargetfolder)) {
								//unlink($target.$data['driver_details']->docDriverLicenseImage);
								$newLicenceUploadedFile[] = $newFile;
							} else {
								//$newLicenceUploadedFile = $data['driver_details']->docDriverLicenseImage;
								//$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's Licence uploading. Please try again.");
							}
						} else {
							$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
						}
					} else {
						//$newLicenceUploadedFile = $data['driver_details']->docDriverLicenseImage;
					}
					
				}
				//echo '<pre>';print_r($newLicenceUploadedFile);
				
				// Upload Multiple HC Authory Card (hc_authority_card)
				$total_hc_card = count($_FILES['hc_authority_card']['name']);
				
				for($i=0; $i<$total_hc_card; $i++)
				{
					if(!empty($_FILES['hc_authority_card']['name'][$i])) {
						$target    = "./assets/uploads/driver/hcauthoritycertificate/";
						$filename  = pathinfo($_FILES['hc_authority_card']['name'][$i], PATHINFO_FILENAME);
						$extension = strtolower(pathinfo($_FILES['hc_authority_card']['name'][$i], PATHINFO_EXTENSION));
						$filename  = str_replace(' ', '_', $filename);
						$newFile   = $id.'_'.time().$i.'.'.$extension;
						$filenamewittargetfolder = $target.$newFile;
						
						if(in_array($extension, array('jpg','jpeg', 'gif', 'png', 'bmp'))) {
							if(move_uploaded_file($_FILES['hc_authority_card']['tmp_name'][$i], $filenamewittargetfolder)) {
								//unlink($target.$data['driver_details']->docHCAuthorityImage);	
								$newHCAuthorityUploadedFile[] = $newFile;
							} else {
								//$newHCAuthorityUploadedFile = $data['driver_details']->docHCAuthorityImage;
								$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's HC Authority Card uploading. Please try again.");
							}
						} else {
							$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
						}
					} else {
						//$newHCAuthorityUploadedFile = $data['driver_details']->docHCAuthorityImage;
					}
				}
				//print_r($newHCAuthorityUploadedFile);
				
				
				// Upload Multiple registration number(registrationnumber)
				$total_registrationnumber = count($_FILES['registrationnumber']['name']);
				
				for($i=0; $i<$total_registrationnumber; $i++)
				{
					if(!empty($_FILES['registrationnumber']['name'][$i])) {
						$target    = "./assets/uploads/driver/registrationnumber/";
						$filename  = pathinfo($_FILES['registrationnumber']['name'][$i], PATHINFO_FILENAME);
						$extension = strtolower(pathinfo($_FILES['registrationnumber']['name'][$i], PATHINFO_EXTENSION));
						$filename  = str_replace(' ', '_', $filename);
						$newFile   = $id.'_'.time().$i.'.'.$extension;
						$filenamewittargetfolder = $target.$newFile;
						
						if(in_array($extension, array('jpg','jpeg', 'gif', 'png', 'bmp'))) {
							if(move_uploaded_file($_FILES['registrationnumber']['tmp_name'][$i], $filenamewittargetfolder)) {
								//unlink($target.$data['driver_details']->docRegistrationImage);	
								$newRegistrationUploadedFile[] = $newFile;
							} else {
								//$newRegistrationUploadedFile = $data['driver_details']->docRegistrationImage;
								$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's HC Authority Card uploading. Please try again.");
							}
						} else {
							$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
						}
					} else {
						//$newRegistrationUploadedFile = $data['driver_details']->docRegistrationImage;
					}
				}
				//print_r($newRegistrationUploadedFile);
				
				// Upload Multiple insurance type (insurancetype)
				$total_insurancetype = count($_FILES['insurancetype']['name']);
				
				for($i=0; $i<$total_insurancetype; $i++)
				{
					if(!empty($_FILES['insurancetype']['name'][$i])) {
						$target    = "./assets/uploads/driver/insurance/";
						$filename  = pathinfo($_FILES['insurancetype']['name'][$i], PATHINFO_FILENAME);
						$extension = strtolower(pathinfo($_FILES['insurancetype']['name'][$i], PATHINFO_EXTENSION));
						$filename  = str_replace(' ', '_', $filename);
						$newFile   = $id.'_'.time().$i.'.'.$extension;
						$filenamewittargetfolder = $target.$newFile;
						
						if(in_array($extension, array('jpg','jpeg', 'gif', 'png', 'bmp'))) {
							if(move_uploaded_file($_FILES['insurancetype']['tmp_name'][$i], $filenamewittargetfolder)) {
								//unlink($target.$data['driver_details']->docInsuranceImage);	
								$newInsuranceUploadedFile[] = $newFile;
							} else {
								//$newInsuranceUploadedFile = $data['driver_details']->docInsuranceImage;
								$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's HC Authority Card uploading. Please try again.");
							}
						} else {
							$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
						}
					} else {
						//$newInsuranceUploadedFile = $data['driver_details']->docInsuranceImage;
					}
				}
				//print_r($newInsuranceUploadedFile);die;
				
				$datetime = date('Y-m-d H:i:s', time());
				$upload_details = array();
				
				// All Upload Details (Licence)
				foreach($newLicenceUploadedFile as $val) {
					$upload_details[] = array('driver_id' => $id, 'file_name' => $val, 'type' => '0', 'add_datetime' => $datetime);
				}
				
				// All Upload Details (HC Authority)
				foreach($newHCAuthorityUploadedFile as $val) {
					$upload_details[] = array('driver_id' => $id, 'file_name' => $val, 'type' => '1', 'add_datetime' => $datetime);
				}
				
				// All Upload Details (Registration Image)
				foreach($newRegistrationUploadedFile as $val) {
					$upload_details[] = array('driver_id' => $id, 'file_name' => $val, 'type' => '2', 'add_datetime' => $datetime);
				}
				
				// All Upload Details (Insurance Image)
				foreach($newInsuranceUploadedFile as $val) {
					$upload_details[] = array('driver_id' => $id, 'file_name' => $val, 'type' => '3', 'add_datetime' => $datetime);
				}
				
				
                $fields = array(
                    "first_name" => trim($this->input->post('d_fname')),
                    "last_name" => trim($this->input->post('d_lname')),
                    "email_id" => trim($this->input->post('d_email')),
                    "mobile_number" => trim($this->input->post('d_mobile')),
                    "companyname" => trim($this->input->post('d_companyname')),
                    "abn" => trim($this->input->post('d_abn')),
                    "address" => trim($this->input->post('d_address')),
                    "state" => trim($this->input->post('d_state')),
                    "zipcode" => trim($this->input->post('d_zipcode')),
                    "licensedetails" => trim($this->input->post('d_licensedetails')),
					"update_datetime" => date('Y-m-d H:i:s', time()),
					"car_make" => trim($this->input->post('d_carmake')),
					"car_yearmodel" => trim($this->input->post('d_carYearModel')),
					"insurance_company" => trim($this->input->post('d_insurancecompany')),
                );
				
				// For Driver Image
				if(!empty($_FILES['d_driverimage']['name'])) 
				{
					 $fields["profile_image"] = $newUploadedFile;
				}
				/*// For Driver Licence Image
				if(!empty($_FILES['licensedetails']['name'])) 
				{
					 $fields = array("docDriverLicenseImage" => $newLicenceUploadedFile);
				}
				// For HC Authority Image
				if(!empty($_FILES['hc_authority_card']['name'])) 
				{
					 $fields = array("docHCAuthorityImage" => $newHCAuthorityUploadedFile);
				}
				// For Registration Image
				if(!empty($_FILES['registrationnumber']['name'])) 
				{
					 $fields = array("docRegistrationImage" => $newRegistrationUploadedFile);
				}
				// For Insurance Image
				if(!empty($_FILES['insurancetype']['name'])) 
				{
					 $fields = array("docInsuranceImage" => $newInsuranceUploadedFile);
				}*/
				
				$driver_id = $this->driver_model->edit($fields, $id);
				
				if(sizeof($upload_details) > 0) { 
					$this->driver_model->add_uploads($upload_details);
				}
				
                //echo $driver_id;die;
                if ($driver_id) 
                {
					$driver = $this->driver_model->fetch_details($id);
            
					if ($driver !== 0) 
					{
						$data['driver_details'] = $driver;
						$_SESSION['HIRE_CAR']['DRIVER'] = $driver;
					}
                    $this->session->set_flashdata('SUCC_EDIT_DRIVER_MESSAGE', 'Your profile has been updated successfully.');
                } 
                else 
                {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error occurred while update profile.');
                }
            } 
            else 
            {
				$data['driver_details'] = $this->driver_model->fetch_details($driverId);
				
                $this->session->set_flashdata('ERR_EDIT_DRIVER_EMAILDUPLICATION', 'Email id is already exist.');
            }
			redirect('driverprofile');
        }
       
    }
	
    public function editprofile_OLD()
    {
		$this->load->model('driver_model');
		$id = $this->uri->segment(3);
		//echo $id;die;
        if($this->input->post())
        {
            $this->load->model('driver_model');
            $riderDuplication = $this->driver_model->check_unique('email_id', $this->input->post('d_email'), "driver_id != '".$id."'");
            //print_r($riderDuplication);die;
            
            if (empty($riderDuplication)) 
            {
				//print_r($_FILES);die;
				if(!empty($_FILES['d_driverimage']['name'])) 
				{
					//echo "dfsd";die;
					$target = "./assets/admin/DriverImages/";
					$filename = pathinfo($_FILES['d_driverimage']['name'],PATHINFO_FILENAME);
					$extension = strtolower(pathinfo($_FILES['d_driverimage']['name'], PATHINFO_EXTENSION));
					$filename = str_replace(' ','_',$filename);
					$newFile = $filename.uniqid().'.'.$extension;
					$filenamewittargetfolder = $target.$newFile;
					if(in_array($extension , array('jpg','jpeg', 'gif', 'png', 'bmp'))) 
					{
						if(move_uploaded_file($_FILES['d_driverimage']['tmp_name'],$filenamewittargetfolder)) {
							$newUploadedFile = $newFile;
						} else {
							
							$this->session->set_flashdata('ERR_MESSAGE', "Error while Driver's Photo uploading. Please try again.");
						}
					} else {
						$this->session->set_flashdata('ERR_MESSAGE', 'Error! Please upload valid Image.');	
					}
				} else {
					$newUploadedFile = '';
				}
				
                $fields = array(
                    "first_name" => trim($this->input->post('d_fname')),
                    "last_name" => trim($this->input->post('d_lname')),
                    "email_id" => trim($this->input->post('d_email')),
                    "mobile_number" => trim($this->input->post('d_mobile')),
                    "companyname" => trim($this->input->post('d_companyname')),
                    "abn" => trim($this->input->post('d_abn')),
                    "address" => trim($this->input->post('d_address')),
                    "city" => trim($this->input->post('d_city')),
                    "zipcode" => trim($this->input->post('d_zipcode')),
                    "licensedetails" => trim($this->input->post('d_licensedetails')),
                    "update_datetime" => date('Y-m-d H:i:s', time())
                );
				
				if(!empty($_FILES['d_driverimage']['name'])) 
				{
					 $fields = array("profile_image" => $newUploadedFile);
				}
				
                $driver_id = $this->driver_model->edit($fields, $id);
                //echo $driver_id;die;
                if ($driver_id) 
                {
					$driver =$this->driver_model->fetch_details($id);
            
					if ($driver !== 0) 
					{
						$data['driver_details'] = $driver;
						$_SESSION['HIRE_CAR']['DRIVER'] = $driver;
					}
                    $this->session->set_flashdata('SUCC_EDIT_DRIVER_MESSAGE', 'Your profile has been updated successfully.');
                } 
                else 
                {
                    $this->session->set_flashdata('ERR_MESSAGE', 'Error occurred while update profile.');
                }
            } 
            else 
            {
				$data['driver_details'] = $this->driver_model->fetch_details($driverId);
				
                $this->session->set_flashdata('ERR_EDIT_DRIVER_EMAILDUPLICATION', 'Email id is already exist.');
            }
			redirect('driverprofile');
        }
       
    }
	
	public function changepassword()
    {
		$this->load->model('driver_model');
		$id = $this->uri->segment(3);
		$driver_details = $this->driver_model->fetch_details($id);
				
        if($this->input->post())
        {
			if($driver_details->password == $this->input->post('oldpassword'))
			{
				$fields = array(
					"password" => $this->input->post('newpassword'),
					"update_datetime" => date('Y-m-d H:i:s', time())
					);
				
				$rider_id = $this->driver_model->edit($fields, $id);
				
				if($rider_id)
				{
					$this->session->set_flashdata('SUCC_MESSAGE', 'Your Password has been changed Successfully.');
				}
				else
				{
					$this->session->set_flashdata('ERR_MESSAGE', 'Error to update password.');
				}
			}
			else
			{
				$this->session->set_flashdata('ERR_PASSWORD_MESSAGE', 'Please enter valid Old password.');
			}
        }
		redirect('driverprofile');
        
    }
	
    public function updatecreditcard()
    {
        $this->load->model('driver_model');
		$this->load->helpers('encryption_helper');
        $id = $this->uri->segment(3);
        $credit_details = $this->driver_model->fetch_creditcard($id,'1');

        //echo "<pre>";
        //print_r($credit_details);die;

        $fields = array(
                        "user_id" => $id,
                        "card_no" => encrypt($this->input->post('cardno')),
                        "exp_month" => $this->input->post('month'),
                        "exp_year" => $this->input->post('year'),
        				"ccv_no" => $this->input->post('ccvno'),
                        "name_of_card" => $this->input->post('name_of_card'),
                        "card_type" => $this->input->post('card_type'),
                        "user_type" => 1, //1 => Driver 2 => Rider
						"add_datetime" => date('Y-m-d H:s:i'),
						"update_datetime" => date('Y-m-d H:s:i'),
        );
		
		//echo "<pre>";
		//print_r($fields);die;

        if($credit_details == '')
        {   
            $rider_id = $this->driver_model->addcard($fields);
        }
        else
        {
                $rider_id = $this->driver_model->editcard($fields, $id);
        }

        $this->session->set_flashdata('SUCC_CARD_MESSAGE', 'Your  credit card details has been updated Successfully.');
        redirect('driverprofile');
    }
   
    public function status()
    {
        $status =  $this->uri->segment(3);
        $rideId =  $this->uri->segment(4);
        $driverId = $_SESSION['HIRE_CAR']['DRIVER']->driver_id;
        $this->load->model('ride_model');
        $search = 'ride_details.id = "'.$rideId.'"';
        $rideDetailsQuery = $this->ride_model->rides_list('','',$search);
        $rideDetails = $rideDetailsQuery[0];
        if($status == 'accept') {
            if($rideDetails->driver_id == '0') {
                $fields = array(
                        "driver_id" => $driverId,
                        "ride_confirm_datetime" => date('Y-m-d H:s:i'),
                        "ride_status" => '2'
                );
                $ride_id = $this->ride_model->edit_ride_details($fields, $rideId);
                $message = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
<a href='" . base_url() . "'>";
                $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
                $message .= "</a></div>
                <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('".base_url()."assets/web/images/logo.png') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                <div style='padding-left:10px;'>
                <p>Dear " . $rideDetails->first_name . " " . $rideDetails->last_name . ",</p>
                <p>Your Ride has been successfully confirmed with Hire Car Sydney City. Your Driver details are as below.</p>
                <p><b>Driver Name:</b> ".$_SESSION['HIRE_CAR']['DRIVER']->first_name." ".$_SESSION['HIRE_CAR']['DRIVER']->last_name." <br>
                <b>Contact Number:</b> ".$_SESSION['HIRE_CAR']['DRIVER']->mobile_number." <br>
                </p>
                <p>Regards,<br />
                Hire Car Sydney City Team</p>
                </div>
                </div>
                </div>";
                $this->load->library('email');
                $this->email->from('admin@hirecarssydneycity.com.au', "Admin Team");
                $this->email->to($rideDetails->email_id);
                //$this->email->cc("testcc@domainname.com");
                $this->email->subject("Ride Confirm with Hire Car Sydney City");
                $this->email->message($message);
                $this->email->send();
                
                /*$message1 = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
<a href='" . base_url() . "'>";
                $message1 .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
                $message1 .= "</a></div>
                <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('".base_url()."assets/web/images/logo.png') no-repeat scroll 0 0 transparent; padding-top:30px;'>Register Successfully.</div>
                <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                <div style='padding-left:10px;'>
                <p>Dear Admin,</p>
                <p>Ride has been successfully confirmed with Hire Car Sydney City. Your Driver details are as below.</p>
                <p><b>Driver Name:</b> ".$_SESSION['HIRE_CAR']['DRIVER']->first_name." ".$_SESSION['HIRE_CAR']['DRIVER']->last_name." <br>
                <b>Contact Number:</b> ".$_SESSION['HIRE_CAR']['DRIVER']->mobile_number." <br>
                </p>
                <p>Regards,<br />
                Hire Car Sydney City Team</p>
                </div>
                </div>
                </div>";
                $this->load->library('email');
                $this->email->from('admin@hirecarsydneycity.com', "Admin Team");
                $this->email->to($rideDetails->email_id);
                //$this->email->cc("testcc@domainname.com");
                $this->email->subject("Ride Confirm with Hire Car Sydney City");
                $this->email->message($message1);
                $this->email->send();*/
                
                $this->session->set_flashdata('MESSAGE', array('status'=>'Success','message' => 'You are Successfully assign for Ride.'));
                $this->load->view('web/driverprofile/driverprofile-status');
            } else {
                $this->session->set_flashdata('MESSAGE', array('status'=>'Error','message' => 'Error to assign Ride.'));
                $this->load->view('web/driverprofile/driverprofile-status');
            }
        } elseif ($status == 'start') {
            if($driverId == $rideDetails->driver_id) {
                $fields = array(
                        "ride_start_datetime" => date('Y-m-d H:s:i'),
                        "ride_status" => '3'
                );
                $ride_id = $this->ride_model->edit_ride_details($fields, $rideId);
                $this->session->set_flashdata('MESSAGE', array('status'=>'Ride Started','message' => 'Your Ride has been started.'));
                $this->load->view('web/driverprofile/driverprofile-status');
            } else {
                $this->session->set_flashdata('MESSAGE', array('status'=>'Error','message' => 'Error to start Ride.'));
                $this->load->view('web/driverprofile/driverprofile-status');
            }
        } elseif ($status == 'finish') {
            if($driverId == $rideDetails->driver_id) {
               $fields = array(
                        "ride_finish_datetime" => date('Y-m-d H:s:i'),
                        "ride_status" => '4'
                );
                $ride_id = $this->ride_model->edit_ride_details($fields, $rideId);
                $this->session->set_flashdata('MESSAGE', array('status'=>'Success','message' => 'Your Ride has been finished Successfully.'));
                $this->load->view('web/driverprofile/driverprofile-status');
            } else {
                $this->session->set_flashdata('MESSAGE', array('status'=>'Error','message' => 'Error to finish Ride.'));
                $this->load->view('web/driverprofile/driverprofile-status');
            }
        } elseif ($status == 'payment') {
            if($driverId == $rideDetails->driver_id) {
               $this->load->model('payment_model');
               $paymentDetails = $this->ride_model->fetch_paymentdetails($rideId);
               if(!empty($paymentDetails)) {
                    $remainingFare = ($rideDetails->estimated_fare - $paymentDetails->amount);
               } else {
                    $remainingFare = $rideDetails->estimated_fare;
               }
               $fields = array(
                        "ride_id" => $rideId,
                        "amount" => $remainingFare,
                        "payment_type" => 'Manual',
                        "status" => '1',
                        "payment_datetime" => date('Y-m-d H:s:i')
                );
                $paymentId = $this->payment_model->add($fields);
                if($paymentId != '') {
                    $this->session->set_flashdata('MESSAGE', array('status'=>'Success','message' => 'Payment details for Ride has been updated Successfully.'));
                    $this->load->view('web/driverprofile/driverprofile-status');
                } else {
                    $this->session->set_flashdata('MESSAGE', array('status'=>'Error','message' => 'Error occurred while update payment.'));
                    $this->load->view('web/driverprofile/driverprofile-status');
                }
            } else {
                $this->session->set_flashdata('MESSAGE', array('status'=>'Error','message' => 'Error occurred while update payment.'));
                $this->load->view('web/driverprofile/driverprofile-status');
            }
        }
    }
	
	public function deleteAccount() {
		$this->load->model('driver_model');
		$id = $_SESSION['HIRE_CAR']['DRIVER']->driver_id;
		
		$fields = array("status" => '2');
		
		$rider_id = $this->rider_model->edit($fields, $id);
		
		redirect(base_url('logout'));
	}
	
	public function delete_uploads($upload_id) 
	{
		$this->load->model('driver_model', 'model');
		
		$upload_details = $this->model->get_upload_details($upload_id);
		
		if($_SESSION['HIRE_CAR']['DRIVER']->driver_id != $upload_details->driver_id) {
			echo 'error';
			exit;
		}
		
		if($upload_details != false) {
			$folder_path = '';
			if($upload_details->type == 0) {
				$folder_path = 'license';
			}
			else if($upload_details->type == 1) {
				$folder_path = 'hcauthoritycertificate';
			}
			else if($upload_details->type == 2) {
				$folder_path = 'registrationnumber';
			}
			else if($upload_details->type == 3) {
				$folder_path = 'insurance';
			}
			
			$file_name = $upload_details->file_name;
			
			$delete_upload = $this->model->delete_upload($upload_id);
        
			if($delete_upload > 0)
			{
				if($upload_details->server_reference == 'rib') {
					$this->load->helper('ftp_helper');
					ftp_file_delete(array('/assets/uploads/driver/'.$folder_path.'/'.$file_name));
				}
				else {
					$file_path = './assets/uploads/driver/'.$folder_path.'/'.$file_name;
					unlink($file_path);
				}
				
				//$file_path = './assets/uploads/driver/'.$folder_path.'/'.$file_name;
				//unlink($file_path);
				echo 'success';
			}
			else
			{
				echo 'error';
			}
		}
		exit;
	}
	
}
