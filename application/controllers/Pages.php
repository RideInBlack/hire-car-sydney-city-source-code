<?php

/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();

        // Load Inquiry Model
        //$this->load->model('Inquiry_model');
    }

	public function map3() {
		$this->load->view('web/pages/search_ride3');
	}
	
    public function contact_us() 
    {
        $this->load->model('Contact_model');
		
		//echo phpinfo();
		

        if ($this->input->post()) {
            $fields = array("user_name" => trim($this->input->post('firstname')),
                "user_phone" => trim($this->input->post('phone')),
                "user_email_id" => $this->input->post('email'),
                "trip_details" => trim($this->input->post('trip')),
                "contact_datetime" => date('Y-m-d H:i:s', time())
            );

            $contact_us_id = $this->Contact_model->insert_contact($fields);
			
            if ($contact_us_id) 
			{
				//mail("virag.shah@ifuturz.com"," Test", " Hello test mesage from Hire Car"); exit;
				
				$this->load->library('email');
				
				// Email to admin for New Ride Booked.
            	//------------------------------------------------------------------
            	// Email to All Admin
            	//------------------------------------------------------------------
            	// Load email library for send email for booking ride
				$this->load->model('user_model');   // Load Admin Model
	            $admin_email = $this->user_model->get_admin_email();
				
            	foreach ($admin_email AS $emails) {
                	$email_array[] = $emails->email_id;
            	}
				
				/*if ($_SERVER['REMOTE_ADDR'] == '180.211.97.81'){
			
			$this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
			
			print_r($email_array);
			
			die;
			
			//$this->email->to("ashvin.solanki@ifuturz.com");
			$this->email->to($email_array);
			
			
				}else{
			
				$this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
				$this->email->to($email_array);	
					
				}*/
				
				$this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
				//$this->email->to($email_array);
				
				$New_adminEmail = "admin@hirecarsydneycity.com.au";
				
				$this->email->to($New_adminEmail);
				
				$this->email->subject("Contact US Details");
				
				$message = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
<a href='" . base_url() . "'>";

               /* $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';*/
			   
			    $message .= '<img src="http://hirecarsydneycity.com.au/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';

                $message .= "</a></div>
					<div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://hirecarsydneycity.com.au/assets/web/images/logo.png') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
					<div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
					<div style='padding-left:10px;'>
					<p>Dear Admin,</p>
					<br/>
					<p><b>Name:</b> ".$this->input->post('firstname')."</p>
					<p><b>Phone:</b> ".$this->input->post('phone')."</p>
					<p><b>Email Id:</b> ".$this->input->post('email')."</p>
					<p><b>Message:</b> ".$this->input->post('trip')."</p>
					</p>
					<p>Regards,<br />
					Hire Car Sydney City Team</p>
					</div>
					</div>
					</div>";
				
                $this->email->message($message);
				$this->email->send();
				//echo $this->email->print_debugger(); exit;
				//------------------------------------------------------------------
				// Email configuration
                $message = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
<a href='" . base_url() . "'>";

                /*$message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';*/
				$message .= '<img src="http://hirecarsydneycity.com.au/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
				
				

                $message .= "</a></div>
					<div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://hirecarsydneycity.com.au/assets/web/images/logo.png') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
					<div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
					<div style='padding-left:10px;'>
					<p>Dear ".$this->input->post('firstname').",</p>
					
					<p>Thank you for contacting Hire Car Sydney City. We will be in touch with you shortly.</p>
					
					</p>
					<p>Regards,<br />
					Hire Car Sydney City Team</p>
					</div>
					</div>
					</div>";
                
                $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City: Admin Team");
                $this->email->to($this->input->post('email'));
                //$this->email->cc("testcc@domainname.com");
                $this->email->subject("Contact US");
                $this->email->message($message);

                $this->email->send();


                $this->session->set_flashdata('SUCC_MESSAGE', 'Thank you for contact us. We will contact you soon.');
                redirect('Pages/contact_us');
            } else {
                //$this->session->set_flashdata('ERR_MESSAGE', 'Error to create new Rider.');
            }
        }
		
		$header_details['title'] = 'Contact Us';
		
        $this->load->view('web/template/header', $header_details);
        $this->load->view('web/pages/contact_us');
        $this->load->view('web/template/footer');
    }

    public function inquiry() 
    {
        $this->load->model('Inquiry_model');
        if ($this->input->post()) {
            $fields = array("user_name" => trim($this->input->post('firstname')),
                "user_phone" => trim($this->input->post('phone')),
                "user_email_id" => $this->input->post('email'),
                "pick_up_address" => trim($this->input->post('pick-address')),
                "destination_address" => trim($this->input->post('drop-address')),
                "pick_up_datetime" => trim($this->input->post('pick-up-date')),
                "number_of_passengers" => trim($this->input->post('number_passengers')),
                "occasion_type" => trim($this->input->post('occassion')),
                "how_to_know_us" => trim($this->input->post('find-us')),
                "trip_details" => trim($this->input->post('trip')),
                "inquiry_datetime" => date('Y-m-d H:i:s', time())
            );

            $inquiry_id = $this->Inquiry_model->insert_inquiry($fields);

            if ($inquiry_id) {
				$this->load->library('email');
				
                $message = "<div style='float:left;min-height:auto;width:100%'>
<div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#5BA1FE;'>
<a href='" . base_url() . "'>";

                $message .= '<img src="http://hirecarsydneycity.com.au/assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';

                $message .= "</a></div>
					<div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'>Inquiry get successfully.</div>
					<div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
					<div style='padding-left:10px;'>
					<p>Dear Admin,</p>
					
					<p><b>Name:</b> " . $fields['user_name'] . " <br>
					<b>Phone:</b> " . $fields['user_phone'] . " <br>
					<b>Email:</b> " . $fields['user_email_id'] . " <br>
					<b>Pick-Up Date:</b> " . $fields['pick_up_datetime'] . " <br>
					<b>Pick-Up Address:</b> " . $fields['destination_address'] . " <br>
					<b>Drop Of Address:</b> " . $fields['destination_address'] . " <br>
					<b>No of Passengers:</b> " . $fields['number_of_passengers'] . " <br>
					<b>Type of Occasion:</b> " . $fields['occasion_type'] . " <br>
					<b>Information:</b> " . $fields['trip_details'] . " <br>
					</p>
					<p>Regards,<br />
					Hire Car Sydney City Team</p>
					</div>
					</div>
					</div>";
                
                $this->email->from('admin@adminhirecarsydneycity.com', "Admin Team");
                $this->email->to('admin@adminhirecarsydneycity.com');
				
                $this->email->subject("Inquiry");
                $this->email->message($message);

                if ($this->email->send()) {
                    $data['message'] = "Mail sent...";
                }

                $this->session->set_flashdata('SUCC_MESSAGE', 'Inquiry added Successfully.');
                redirect('Pages/inquiry');
            } else {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to create new Inquiry.');
            }
        }

        $this->load->view('web/template/header');
        $this->load->view('web/pages/inquiry');
        $this->load->view('web/template/footer');
    }

    public function our_services($type = '') 
    {
        $data['type'] = $type;
		$header_details['title'] = "Our Services";
		
        $this->load->view('web/template/header', $header_details);
        $this->load->view('web/pages/our_services', $data);
        $this->load->view('web/template/footer');
    }

    public function our_products() 
    {
        $this->load->model('giftcard_model');
        $data['giftCardData'] = $this->giftcard_model->list_all();
		
		$header_details['meta_keyword'] = 'Luxury ride Gift card, Gift card for Luxury Ride, Hire Car Gift card, Gift card ideas, Gift card sydney, Gift ideas for staff, Corporate Gift cards, Travel Gift Card, Gift Card for loved one, Limo ride gift card, gift cards online, sydney airport ride gift card, wedding gift card, patry gift card, gift card for wedding, gift card for party, Sydney Ride  Gift Card, Gift Card for Sydney Travel';
		
		$header_details['meta_description'] = "Looking for the Gift and you can't think of anything?? Why don't you get one of our Gift Card for your loved one and get them to Ride in Style... We are here to pick your loved one.";
		
		$header_details['title'] = "Choose You Gift Card";
		
        $this->load->view('web/template/header', $header_details);
        $this->load->view('web/pages/our_products', $data);
        $this->load->view('web/template/footer');
    }
	
	public function support() 
    {
		$header_details['title'] = "Support";
		
        $this->load->view('web/template/header', $header_details);
        $this->load->view('web/pages/support', $data);
        $this->load->view('web/template/footer');
    }
	
    public function search_ride() 
    {
    
        // For fetch All City
        $this->load->model('region_model');
        $data['city'] = $this->region_model->get_all_city();

        $this->load->view('web/pages/search_ride', $data);
    }

    public function search($eventType = '') 
    {
        // For fetch All City
        $this->load->model('region_model');
        $data['city'] = $this->region_model->get_all_city();

        // For fetch All Car
        $this->load->model('car_model');
        $data['cars'] = $this->car_model->fetch_all();

        // For fetch All Events
        $this->load->model('event_model');
        $data['event_details'] = $this->event_model->get_all_event();
        $data['event_type'] = $eventType;
        $this->load->view('web/template/header');
        $this->load->view('web/pages/search', $data);
        $this->load->view('web/template/footer');
    }

    public function sitemap() 
    {

        $this->load->view('web/template/header');
        $this->load->view('web/pages/sitemap');
        $this->load->view('web/template/footer');
    }
    
}
