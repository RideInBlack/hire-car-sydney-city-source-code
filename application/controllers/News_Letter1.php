<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class News_Letter1 extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
       
        
        // Load News Letter Model
        $this->load->model('News_Letter');
    }


// Add new News Letter
    public function add() 
    {
        if($this->input->post())
        {
            $fields = array(
                "email_id" => trim($this->input->post('email')),
                "add_datetime" => date('Y-m-d H:i:s', time())
                
                );
            
            $id = $this->News_Letter->add($fields);
            
            if($id)
            {
				
                                      
                    
                    				
                $this->session->set_flashdata('SUCC_MESSAGE', 'Successfully Created New News Letter');
                redirect('admin/template/footer');
            }
            else
            {
                $this->session->set_flashdata('ERR_MESSAGE', 'Error to create new News Letter.');
            }
        }
        
       
        
       // $this->load->view('admin/template/header', $page_details);
        //$this->load->view('admin/rider/rider-add');
        $this->load->view('admin/template/footer');
    }

}
?>