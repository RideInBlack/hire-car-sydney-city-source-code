<?php

/**
 * @Developer Virag Shah
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Giftcard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('giftcard_model', 'model');
    }

    public function details($id = 0) {
        
        if($id == 0) {
            redirect(base_url('pages/our_products'));
        }
        
        $data['gift_card_details'] = $this->model->fetch_details($id);
        
		$header_details['meta_keyword'] = 'Luxury ride Gift card, Gift card for Luxury Ride, Hire Car Gift card, Gift card ideas, Gift card sydney, Gift ideas for staff, Corporate Gift cards, Travel Gift Card, Gift Card for loved one, Limo ride gift card, gift cards online, sydney airport ride gift card, wedding gift card, patry gift card, gift card for wedding, gift card for party, Sydney Ride  Gift Card, Gift Card for Sydney Travel';
		
		$header_details['meta_description'] = "Looking for the Gift and you can't think of anything?? Why don't you get one of our Gift Card for your loved one and get them to Ride in Style... We are here to pick your loved one.";
		
		$header_details['title'] = $data['gift_card_details']->giftcard_name;
		
		$this->load->model('region_model');
		//$data['city_list']  = $this->region_model->get_all_city();
		$data['state'] = $this->region_model->get_all_state();

        $this->load->view('web/template/header', $header_details);
        $this->load->view('web/giftcard/details', $data);
        $this->load->view('web/template/footer');
    }
    
    public function payment() {     
        $id = $_GET['gift'];
        $gift_card_details = $this->model->fetch_details(base64_decode($id));
        
        $fields = array(
            'coupon_code' => $this->generateCoupon(),
            'price' => $gift_card_details->price,
            'remaining_amount' => $gift_card_details->price,
            'pricepercentage' => $gift_card_details->pricepercentage,
            'generate_by' => '1',
            'rider_id' => $_SESSION['HIRE_CAR']['RIDER']->rider_id,
            'status' => '0',
			'payment_type' => 'paypal',
            'generate_datetime' => date('Y-m-d H:i:s', time())
            );
        
        $this->load->model('giftcard_coupon_model');
        $data['coupon_id'] = $this->giftcard_coupon_model->add_card($fields);
        
        $data['gift_card_details'] = $gift_card_details;
        //$data['buisness_id'] = "jitendra.prajapati@ifuturz.com";
        //$data['paypal_url'] = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		
		// Load Setting Model
		$this->load->model('setting_model');
		// Get Setting details
		$setting_details = $this->setting_model->fetch_details('1');
		
		if($setting_details->setting_paypal_mode == "live") {
			$data['paypal_url'] = "https://www.paypal.com/cgi-bin/webscr";
			$data['buisness_id'] = $setting_details->setting_paypal_email_id_live;
		}
		else {
			$data['paypal_url'] = "https://www.sandbox.paypal.com/cgi-bin/webscr";
			$data['buisness_id'] = $setting_details->setting_paypal_email_id;
		}
        
        $this->load->view('web/giftcard/payment', $data);
    }
	
	public function paypal_ipn()
	{
		$coupon_id = $_GET['couponId'];
        $giftcard_id = $_GET['giftcardId'];
        $rider_id = $_GET['riderId'];
		
		// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
		// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
		// Set this to 0 once you go live or don't require logging.
		define("DEBUG", 1);
		// Set to 0 once you're ready to go live
		$this->load->model('setting_model');
		// Get Setting details
		$setting_details = $this->setting_model->fetch_details('1');
		
		if($setting_details->setting_paypal_mode == "live") {
			define("USE_SANDBOX", 0);
		}
		else {
			define("USE_SANDBOX", 1);
		}
		//define("USE_SANDBOX", 1);
		define("LOG_FILE", "./ipn.log");
		// Read POST data
		// reading posted data directly from $_POST causes serialization
		// issues with array data in POST. Reading raw POST data from input stream instead.
		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
			$keyval = explode ('=', $keyval);
			
			if (count($keyval) == 2)
				$myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		
		foreach ($myPost as $key => $value) {
			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}
		// Post IPN data back to PayPal to validate the IPN data is genuine
		// Without this step anyone can fake IPN data
		if(USE_SANDBOX == true) {
			$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		} else {
			$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
		}
		
		$ch = curl_init($paypal_url);
		if ($ch == FALSE) {
			return FALSE;
		}
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		if(DEBUG == true) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		}
		// CONFIG: Optional proxy configuration
		//curl_setopt($ch, CURLOPT_PROXY, $proxy);
		//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
		// Set TCP timeout to 30 seconds
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
		// of the certificate as shown below. Ensure the file is readable by the webserver.
		// This is mandatory for some environments.
		//$cert = __DIR__ . "./cacert.pem";
		//curl_setopt($ch, CURLOPT_CAINFO, $cert);
		$res = curl_exec($ch);
		
		// Inspect IPN validation result and act accordingly
		// Split response headers and payload, a better way for strcmp
		$tokens = explode("\r\n\r\n", trim($res));
		$res = trim(end($tokens));
		
		$this->load->model('payment_model');
		
		if (strcmp ($res, "VERIFIED") == 0) {
			
			$this->load->model('giftcard_coupon_model');
            
            $coupon_id = $_GET['couponId'];
            $fields = array('status' => '1');
            $this->giftcard_coupon_model->edit($fields, $coupon_id);
            
            $coupon_details = $this->giftcard_coupon_model->details($_GET['couponId']);
            
            $this->load->model('rider_model');
            $rider_details = $this->rider_model->fetch_details($coupon_details->rider_id);
            
            // Email to admin for Purchase new coupon
            //------------------------------------------------------------------
            // Email to Rider
            //------------------------------------------------------------------
            // Load email library for send email for booking ride
            $this->load->library('email');
			
			$this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
		    $this->email->to("admin@hirecarsydneycity.com.au");

            $this->email->subject("New Gift Card Purchase Details");
			
			$message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                        <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                        <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                        <div style='padding-left:10px;'>
                        <p>Dear Admin,</p>
						<p><b>Rider Details</b></p>
						<p><b>Name:</b> ".$rider_details->first_name." ".$rider_details->last_name.",</p>
						<p><b>Email:</b> ".$rider_details->email_id."</p>
						<hr>
                        <p><b>Coupon Details</b>.</p>
						<p><b>Coupon Code:</b> ".$coupon_details->coupon_code."</p>
                        <p><b>Price:</b> $".$coupon_details->price."</p>
                        <p><b>Payment Method:</b> PayPal</p>
                        <p>--</p>
                        <p>Regards,<br />
                        <b>Hire Car Sydney City Team<b></p>
                        </div>
                        </div>
                        </div>";
			
			$this->email->message($message);
            $this->email->send();
			
			
			
           // $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
            
           // $this->email->to($rider_details->email_id);
		  // $this->email->to('ashvin.solanki@ifuturz.com');
          //  $this->email->subject("Coupon Purchase Details ");

          /*  $message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                        <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                        <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                        <div style='padding-left:10px;'>
                        <p>Dear ".$rider_details->first_name." ".$rider_details->last_name.",</p>
                        <p>Coupon Details.</p>
                        <p><b>Coupon Code:</b> ".$coupon_details->coupon_code."</p>
                        <p><b>Price:</b> $".$coupon_details->price."</p>
                         <p><b>Payment method:</b> Paypal</p>
                        <p>--</p>
                        <p>Regards,<br />
                        <b>Hire Car Sydney City Team<b></p>
                        </div>
                        </div>
                        </div>";
						*/
						
						

            //$this->email->message($message);
           // $this->email->send();
		   
		   /* email template*/
		   	$data['firstname'] = $rider_details->first_name;
			$data['lastname'] = $rider_details->last_name;
			$data['coupon_code'] = $coupon_details->coupon_code;
			$data['price'] = $coupon_details->price;
			//$data['setting_bank'] = $setting_details->setting_bank;
			//$data['setting_account_name'] = $setting_details->setting_account_name;
			//$data['setting_bsb'] = $setting_details->setting_bsb;
			//$data['setting_account_number'] = $setting_details->setting_account_number;
			
			$this->load->library('email');
			
		$mailtemplate = $this->load->view('web/ride/invoiceMailtempalte_paypal', $data, TRUE);
			
		$this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
        $this->email->to($rider_details->email_id);
        $this->email->subject("Travel Card Purchased");

        $this->email->message($mailtemplate);
        $this->email->send();
			
		}
		else if (strcmp ($res, "INVALID") == 0) {
			
			// log for manual investigation
			// Add business logic here which deals with invalid IPN messages
			if(DEBUG == true) {
				error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
			}
		}
		/*$raw_post_data = file_get_contents('php://input');
		$this->load->library('email');
		$this->email->to("virag.shah@ifuturz.com");
		$this->email->from('admin@hirecarsydneycity.com');
		$this->email->subject('Paypal IPN'."Paypal IPN");
		$this->email->message($payment_id.'='.$res);
		$this->email->send();*/
	}
	
	public function paypal_return($result) {
		if($result == 1) {
            $this->session->set_flashdata("SUCC_GIFTCARD", "Gift Card is Succesfully sent to Your Registered email. ( Coupon Code: ".$coupon_details->coupon_code.")");
		}
		else {
			$this->session->set_flashdata("ERR_GIFTCARD", "Payment unsuccessfull, Please try again.");
		}
		redirect('our-products');
	}
	
	public function paybycreditcard()
	{
		check_rider_login();
		require(APPPATH.'libraries/eWAY/RapidAPI.php');
		$id = $_GET['gift'];
		$this->load->model('giftcard_model');
		
		$this->load->helpers('encryption_helper');
		$gift_card_details = $this->giftcard_model->fetch_details(base64_decode($id));
		$rider_id = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
		$type = 2;
		$this->load->model('driver_model');
        $credit_details = $this->driver_model->fetch_creditcard($rider_id,$type);
		if(!empty($credit_details)) {
			$this->load->helpers('encryption_helper');
			
			$fields = array(
				'coupon_code' => $this->generateCoupon(),
				'price' => $gift_card_details->price,
				'remaining_amount' => $gift_card_details->price,
				'pricepercentage' => $gift_card_details->pricepercentage,
				'generate_by' => '1',
				'rider_id' => $_SESSION['HIRE_CAR']['RIDER']->rider_id,
				'status' => '0',
				'payment_type' => 'eway',
				'generate_datetime' => date('Y-m-d H:i:s', time())
            );
			
			$this->load->model('giftcard_coupon_model');
			$data['coupon_id'] = $this->giftcard_coupon_model->add_card($fields);
			// we skip all validation but you should do it in real world
			// Create DirectPayment Request Object
			$request = new eWAY\CreateDirectPaymentRequest();
			
			// Populate values for Customer Object
			// Note: TokenCustomerID is required when update an exsiting TokenCustomer
			$request->Customer->TokenCustomerID = '';
			
			$request->Customer->Reference = '';
			$request->Customer->Title = '';
			$request->Customer->FirstName = $_SESSION['HIRE_CAR']['RIDER']->first_name;
			$request->Customer->LastName = $_SESSION['HIRE_CAR']['RIDER']->last_name;
			$request->Customer->CompanyName = '';
			$request->Customer->JobDescription = '';
			$request->Customer->Street1 = '';
			$request->Customer->City = '';
			$request->Customer->State = '';
			$request->Customer->PostalCode = '';
			$request->Customer->Country = '';
			$request->Customer->Email = $_SESSION['HIRE_CAR']['RIDER']->email_id;;
			$request->Customer->Phone = '';
			$request->Customer->Mobile = $_SESSION['HIRE_CAR']['RIDER']->mobile_number;
			$request->Customer->Comments = '';
			$request->Customer->Fax = '';
			$request->Customer->Url = '';
			
			$request->Customer->CardDetails->Name = $credit_details->name_of_card;
			$request->Customer->CardDetails->Number = decrypt($credit_details->card_no);
			//$request->Customer->CardDetails->Number = '4444333322221111';
			//$request->Customer->CardDetails->ExpiryMonth = '09';
			$request->Customer->CardDetails->ExpiryMonth = $credit_details->exp_month;
			$request->Customer->CardDetails->ExpiryYear = $credit_details->exp_year;
			$request->Customer->CardDetails->StartMonth = '';
			$request->Customer->CardDetails->StartYear = '';
			$request->Customer->CardDetails->IssueNumber ='';
			$request->Customer->CardDetails->CVN = $credit_details->ccv_no;
			
			// Populate values for ShippingAddress Object.
			// This values can be taken from a Form POST as well. Now is just some dummy data.
			$request->ShippingAddress->FirstName = "";
			$request->ShippingAddress->LastName = "";
			$request->ShippingAddress->Street1 = "";
			$request->ShippingAddress->Street2 = "";
			$request->ShippingAddress->City = "";
			$request->ShippingAddress->State = "";
			$request->ShippingAddress->Country = "";
			$request->ShippingAddress->PostalCode = "";
			$request->ShippingAddress->Email = "";
			$request->ShippingAddress->Phone = "";
			// ShippingMethod, e.g. "LowCost", "International", "Military". Check the spec for available values.
			$request->ShippingAddress->ShippingMethod = "";
			
			//if ($_POST['ddlMethod'] == 'ProcessPayment' || $_POST['ddlMethod'] == 'Authorise' || $_POST['ddlMethod'] == 'TokenPayment') {
			// Populate values for LineItems
			$item1 = new eWAY\LineItem();
			$item1->SKU = "SKU1";
			$item1->Description = "Description1";
			$item2 = new eWAY\LineItem();
			$item2->SKU = "SKU2";
			$item2->Description = "Description2";
			$request->Items->LineItem[0] = $item1;
			$request->Items->LineItem[1] = $item2;
			
			// Populate values for Payment Object
			$request->Payment->TotalAmount = $gift_card_details->price;
			$request->Payment->InvoiceNumber = '';
			$request->Payment->InvoiceDescription = '';
			$request->Payment->InvoiceReference = '';
			$request->Payment->CurrencyCode = 'AUD';
					
			$request->Method = 'Authorise';
			$request->TransactionType = 'Purchase';
			
			// Call RapidAPI
			$eway_params = array();
			//if ($_POST['ddlSandbox']) {
			//$eway_params['sandbox'] = true;
			//}
			
			//------------------------------------------------------------
			// Load Setting Model
			$this->load->model('setting_model');
			// Get Setting details
			$setting_details = $this->setting_model->fetch_details('1');
			
			if($setting_details->setting_eway_mode == "live") {
				
				$api_key = $setting_details->setting_eway_api_key_live;
				$api_password = $setting_details->setting_eway_password_live;
				
				$eway_params['sandbox'] = false;
			}
			else {
				
				$api_key = $setting_details->setting_eway_api_key;
				$api_password = $setting_details->setting_eway_password;
				
				$eway_params['sandbox'] = true;
			}
			//------------------------------------------------------------
			
			$service = new eWAY\RapidAPI($api_key, $api_password, $eway_params);
			$result = $service->DirectPayment($request);
			
			// Check if any error returns
			if (isset($result->Errors)) {
				// Get Error Messages from Error Code.
				/*$ErrorArray = explode(",", $result->Errors);
				$lblError = "";
				foreach ( $ErrorArray as $error ) {
					$error = $service->getMessage($error);
					$lblError .= $error . "<br />\n";
				}
				echo $lblError;die;*/
				redirect(base_url('giftcard/paymenyresult/0?couponId='.$data['coupon_id']));
			} else {
				/*$in_page = 'view_result';
				echo "<pre>";print_r($result);die;*/
				redirect(base_url('giftcard/paymenyresult/1?couponId='.$data['coupon_id']));
			}
		}
		else
		{
			redirect(base_url('giftcard/paymenyResult/0?giftcardId='.$gift_card_details->giftcard_id.'&riderId='.$_SESSION['HIRE_CAR']['RIDER']->rider_id));
			redirect(base_url('giftcard/paymenyresult/1?couponId='.$data['coupon_id']));
		}
	}
    
	public function depositToBank($gift_card_id = 0) 
	{
		if(!$gift_card_id || !is_numeric($gift_card_id)) {
			echo json_encode(array("status" => 0, "message" => "Please select valid gift card."));
			die;
		}
		if(!isset($_SESSION['HIRE_CAR']['RIDER']->rider_id) && !empty($_SESSION['HIRE_CAR']['RIDER']->rider_id)) {
			echo json_encode(array("status" => -1, "message" => "Invalid request."));
			die;
		}
		
		$rider_id = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
		
		// Get Gift Card Details
		$this->load->model('giftcard_model');
		$gift_card_details = $this->giftcard_model->fetch_details($gift_card_id);
		
		$fields = array('giftcard_id' => $gift_card_id,
						'coupon_code' => $this->generateCoupon(),	
						'price' => $gift_card_details->price,
						'remaining_amount' => $gift_card_details->price,
						'pricepercentage' => $gift_card_details->pricepercentage,
						'generate_by' => '0',
						'rider_id' => $rider_id,
						'status' => '0',
						'payment_type' => 'deposit',
						'generate_datetime' => date('Y-m-d H:i:s', time()),
						'use_datetime' => date('Y-m-d H:i:s', time()),
						);
		
		$this->load->model('giftcard_coupon_model');
		$coupon_id = $this->giftcard_coupon_model->add_card($fields);
		
		
		if($coupon_id > 0) {
			$this->load->model('setting_model');
			$setting_details = $this->setting_model->fetch_details(1);
			
			$msg = "<p>Bank Details are.</p>
                    <p><b>Bank:</b> ".$setting_details->setting_bank."</p>
                    <p><b>Account Name:</b> ".$setting_details->setting_account_name."</p>
                    <p><b>BSB:</b> ".$setting_details->setting_bsb."</p>
                    <p><b>Account Number:</b> ".$setting_details->setting_account_number."</p>
					<p>Please deposit giftcard amount in bank to enable this gift card.</p>";
			
			$this->session->set_flashdata('GIFT_CARD_DEPOSIT', $msg);
			
			
			$this->load->model('rider_model');
            $rider_details = $this->rider_model->fetch_details($rider_id);
            
			$coupon_details = $this->giftcard_coupon_model->details($coupon_id);
			// Email to admin for New Ride Booked.
            //------------------------------------------------------------------
            // Email to All Admin
            //------------------------------------------------------------------
            // Load email library for send email for booking ride
            $this->load->library('email');
           

			$this->load->model('user_model');   // Load Admin Model
            $admin_email = $this->user_model->get_admin_email();

            foreach ($admin_email AS $email) {
                $email_array[] = $email->email_id;
            }
			
			//echo "<pre>"; print_r($email_array); echo "</pre>";
			
           // $this->email->to($email_array);
		   $this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
		   $this->email->to("admin@hirecarsydneycity.com.au");

            $this->email->subject("New Gift Card Purchase Details");
			
			$message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                        <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                        <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                        <div style='padding-left:10px;'>
                        <p>Dear Admin,</p>
						<p><b>Rider Details</b></p>
						<p><b>Name:</b> ".$rider_details->first_name." ".$rider_details->last_name.",</p>
						<p><b>Email:</b> ".$rider_details->email_id."</p>
						<hr>
                        <p><b>Coupon Details</b>.</p>
						<p><b>Coupon Code:</b> ".$coupon_details->coupon_code."</p>
                        <p><b>Price:</b> $".$coupon_details->price."</p>
                         <p><b>Payment Method:</b> Bank Transer</p>
                        <p>--</p>
                        <p>Regards,<br />
                        <b>Hire Car Sydney City Team<b></p>
                        </div>
                        </div>
                        </div>";
			
			$this->email->message($message);
            $this->email->send();
			
			// -----------------------------------------------------------------------
			// Email To Rider For Coupon Register
			// -----------------------------------------------------------------------
			// Load email library for send email for booking ride
            //$this->load->library('email');
           // $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
            
          //  $this->email->to($rider_details->email_id);
           // $this->email->subject("Coupon Register Details");
		  // $this->email->subject("Travel Card Purchased");
		   
			$data['firstname'] = $rider_details->first_name;
			$data['lastname'] = $rider_details->last_name;
			$data['coupon_code'] = $coupon_details->coupon_code;
			$data['price'] = $coupon_details->price;
			$data['setting_bank'] = $setting_details->setting_bank;
			$data['setting_account_name'] = $setting_details->setting_account_name;
			$data['setting_bsb'] = $setting_details->setting_bsb;
			$data['setting_account_number'] = $setting_details->setting_account_number;
			
			
			
		$mailtemplate = $this->load->view('web/ride/invoiceMailtempalte_bank_transfer', $data, TRUE);
			
		$this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
		$this->email->to($rider_details->email_id);
        $this->email->subject("Travel Card Purchased");

        $this->email->message($mailtemplate);
        $this->email->send();
			
			

          /*  $message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                        <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                        <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                        <div style='padding-left:10px;'>
                        <p>Dear ".$rider_details->first_name." ".$rider_details->last_name.",</p>
                        <p>Coupon Details.</p>
                        <p><b>Coupon Code:</b> ".$coupon_details->coupon_code."</p>
                        <p><b>Price:</b> $".$coupon_details->price."</p>
                        
						<hr/>
						<p><b>Bank:</b> ".$setting_details->setting_bank."</p>
                    	<p><b>Account Name:</b> ".$setting_details->setting_account_name."</p>
                    	<p><b>BSB:</b> ".$setting_details->setting_bsb."</p>
                    	<p><b>Account Number:</b> ".$setting_details->setting_account_number."</p>
						<p>Please deposit giftcard amount in bank to enable this gift card.</p>
						
                        <p>--</p>
                        <p>Regards,<br />
                        <b>Hire Car Sydney City Team<b></p>
                        </div>
                        </div>
                        </div>"; */

          /*  $this->email->message($message);
            $this->email->send(); */
			
			// -----------------------------------------------------------------------
			
			echo json_encode(array("status" => 1, "message" => "Success"));
			die;
		}
		else {
			echo json_encode(array("status" => 0, "message" => "Something Wrong. Please try after sometime."));
			die;
		}
		
	}
	
    public function  paymenyResult($param) {
        if($param == 1) {
            $this->load->model('giftcard_coupon_model');
            
            $coupon_id = $_GET['couponId'];
            $fields = array('status' => '1');
            $this->giftcard_coupon_model->edit($fields, $coupon_id);
            
            $coupon_details = $this->giftcard_coupon_model->details($_GET['couponId']);
            
            $this->load->model('rider_model');
            $rider_details = $this->rider_model->fetch_details($coupon_details->rider_id);
            
            // Email to admin for Purchase new coupon
            //------------------------------------------------------------------
            // Email to Rider
            //------------------------------------------------------------------
            // Load email library for send email for booking ride
			
			$this->load->library('email');
			
			
		    $this->email->from('admin@hirecarsydneycity.com.au', "Admin Team");
		    $this->email->to("admin@hirecarsydneycity.com.au");
			//$this->email->to("ashvin.solanki@ifuturz.com");

            $this->email->subject("New Gift Card Purchase Details");
			
			$message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                        <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                        <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                        <div style='padding-left:10px;'>
                        <p>Dear Admin,</p>
						<p><b>Rider Details</b></p>
						<p><b>Name:</b> ".$rider_details->first_name." ".$rider_details->last_name.",</p>
						<p><b>Email:</b> ".$rider_details->email_id."</p>
						<hr>
                        <p><b>Coupon Details</b>.</p>
						<p><b>Coupon Code:</b> ".$coupon_details->coupon_code."</p>
                        <p><b>Price:</b> $".$coupon_details->price."</p>
                        <p><b>Payment Method:</b> eWay</p>
                        <p>--</p>
                        <p>Regards,<br />
                        <b>Hire Car Sydney City Team<b></p>
                        </div>
                        </div>
                        </div>";
			
			$this->email->message($message);
            $this->email->send();
			
			
          
			  /*  $this->load->library('email');
            $this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
            
            $this->email->to($rider_details->email_id);
            $this->email->subject("Coupon Purchase Details");

            $message = "<div style='float:left;min-height:auto;width:100%'>
                <div style='border:0px solid #000; height:82px; text-align:center; width:100%; padding-top:10px;background:#000000;'>
                <a href='" . base_url() . "'>";

            $message .= '<img src="'.base_url().'assets/web/images/logo.png" alt="" style="height:70px; margin-left: 42%; width:170px;display:block;" />';
            $message .= "</a></div>
                        <div style='border:0px solid #000; height:35px; text-align:center; width:70%; margin-left:15%; margin-right:15%; font-weight:bold;background:url('http://192.168.1.132/master/images/logo.jpg') no-repeat scroll 0 0 transparent; padding-top:30px;'></div>
                        <div style='background:none repeat scroll 0 0 #f9f9f9;border:1px solid #e9e9e9; margin-top:5px;float:left;min-height:auto;width:100%'>
                        <div style='padding-left:10px;'>
                        <p>Dear ".$rider_details->first_name." ".$rider_details->last_name.",</p>
                        <p>Coupon Details.</p>
                        <p><b>Coupon Code:</b> ".$coupon_details->coupon_code."</p>
                        <p><b>Price:</b> $".$coupon_details->price."</p>
                        
                        
                        <p>--</p>
                        <p>Regards,<br />
                        <b>Hire Car Sydney City Team<b></p>
                        </div>
                        </div>
                        </div>";

            $this->email->message($message);
            $this->email->send(); */
            
			
			$data['firstname'] = $rider_details->first_name;
			$data['lastname'] = $rider_details->last_name;
			$data['coupon_code'] = $coupon_details->coupon_code;
			$data['price'] = $coupon_details->price;
			//$data['setting_bank'] = $setting_details->setting_bank;
			//$data['setting_account_name'] = $setting_details->setting_account_name;
			//$data['setting_bsb'] = $setting_details->setting_bsb;
			//$data['setting_account_number'] = $setting_details->setting_account_number;
			
			
			
		$mailtemplate = $this->load->view('web/ride/invoiceMailtempalte_cc', $data, TRUE);
			
		$this->email->from('admin@hirecarsydneycity.com.au', "Hire Car Sydney City - Team");
        $this->email->to($rider_details->email_id);
        $this->email->subject("Travel Card Purchased");

        $this->email->message($mailtemplate);
        $this->email->send();
		
			
			
            $this->session->set_flashdata("SUCC_GIFTCARD", "Gift Card is Succesfully sent to Your Registered email. ( Coupon Code: ".$coupon_details->coupon_code.")");
        }
        else {
            $this->session->set_flashdata("ERR_GIFTCARD", "Payment unsuccessfull, Please try again.");
        }
        redirect('our-products#giftcard');
    }
    
//    public function saveGiftCard() {
//        if($_REQUEST['payment_status'] == 'Completed') {
//            $coupon_id = $_GET['couponId'];
//            
//            $this->load->model('giftcard_coupon_model');
//            $fields = array('status' => '1');
//            $this->giftcard_coupon_model->edit($fields, $coupon_id);
//            
//            $gift_card_details = $this->model->details($coupon_id);
//            $rider_id = $_GET['riderId'];
//        }
//    }
    
    public function validateCoupon($code = "") {
        $rider_id = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
        
        $this->load->model('giftcard_coupon_model');
        $coupon_details = $this->giftcard_coupon_model->get_coupon_details($code);
        $response = array();
        // Check coupon is available or Not
        if($coupon_details) {
            
            if(($coupon_details->generate_by == 1 && $coupon_details->rider_id == 0 && $coupon_details->remaining_amount == $coupon_details->price) || $rider_id == $coupon_details->rider_id) {
				
				if( $coupon_details->rider_id  == 0)
					$this->giftcard_coupon_model->edit(array('rider_id' => $rider_id), $coupon_details->coupon_id);
				
                if($coupon_details->remaining_amount > 0) {
                    $response = array('status' => 'success', "payableAmount" => $coupon_details->remaining_amount, "couponId" => $coupon_details->coupon_id);
                }
                else {
                    $response = array("status" => "invalid1");
                }
            }
            else {
                $response = array("status" => "invalid2");
            }
            
        }
        else {
            $response = array("status" => "invalid3");
        }
        
        echo json_encode($response);
        exit;
    }
    
    public function generateCoupon() {
        $this->load->helper('string');

        do {
            $this->load->model('giftcard_coupon_model');
            $coupon_code = strtoupper(random_string('alnum', 6));
            $is_unique = $this->giftcard_coupon_model->check_unique('coupon_code', $coupon_code);
        } while (!$is_unique);

        return $coupon_code;
    }
	
	public function checkforcreditcard()
	{
		$rider_id = $_SESSION['HIRE_CAR']['RIDER']->rider_id;
		$type = 2;
		$this->load->model('driver_model');
        $credit_details = $this->driver_model->fetch_creditcard($rider_id,$type);
		if(!empty($credit_details)) {
			echo 'yes';	exit;
		} else {
			echo 'no';	exit;
		}
	}
}
