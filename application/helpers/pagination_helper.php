<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function set_pagination($base_url, $total_record, $per_page_record)
{
    $CI =& get_instance();
    
    $CI->load->library('pagination');
    $config['base_url'] = $base_url;
    $config['total_rows'] = $total_record;
    $config['per_page'] = $per_page_record;
    $config['use_page_numbers'] = TRUE;
    $config['reuse_query_string'] = TRUE;
    $config['num_links'] = 2;
    
    //$config['suffix'] = (!empty($search_string)) ? '?'.$search_string : "";
    // Set Style for Pagination Link
    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';
    
    $config['first_link'] = 'First';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    
    $config['last_link'] = 'Last';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    
    $config['prev_link'] = 'Previous';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    
    $CI->pagination->initialize($config);
}