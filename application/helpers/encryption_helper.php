<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function encrypt($string)
{
    $CI =& get_instance();
    
    $CI->load->library('encrypt');
	
	$encrypted_string = $CI->encrypt->encode($string);
	
	return $encrypted_string;
}

function decrypt($string)
{
    $CI =& get_instance();
    
    $CI->load->library('encrypt');
	
	$encrypted_string = $CI->encrypt->decode($string);
	
	return $encrypted_string;
}