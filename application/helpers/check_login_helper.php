<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function check_admin_login()
{
    if(!isset($_SESSION['HIRE_CAR']['ADMIN']) || !isset($_SESSION['HIRE_CAR']['ADMIN']->admin_id))
    {
        redirect('admin/');
    }
}

function is_super_admin()
{
    if(isset($_SESSION['HIRE_CAR']['ADMIN']) && $_SESSION['HIRE_CAR']['ADMIN']->admin_type == 1 ) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}

function check_rider_login()
{
    if(isset($_SESSION['HIRE_CAR']['RIDER'])) {
        return TRUE;
    }
    else {
        redirect(base_url());
    }
}

function check_driver_login()
{
    if(isset($_SESSION['HIRE_CAR']['DRIVER'])) {
        return TRUE;
    }
    else {
        redirect(base_url());
    }
}