<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Read JSON from URL
function readJSON() 
{
	// Get JSON from URL
	$json = file_get_contents('php://input');
	// Decode JSON Input
	$jsonArr = json_decode($json,true);
	
	return $jsonArr;
}

// Return Json output 
function writeJSON($data, $is_exit = true)
{
	header('Content-Type: application/json');
	echo json_encode($data);
	
	if($is_exit) {
		exit(0);
	}
}

// Validate Input param
function checkParam($var, $msg) 
{
	if($var == "")
	{
		writeJSON(array('status' => 0, 'message' => $msg));
	}
	return $var;
}